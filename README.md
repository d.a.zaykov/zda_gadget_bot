# Telegram-bot "Гаечка"

## 1. Введение

**Название:** Гаечка

**Username:** @zda_gadget_bot

**URL:** https://t.me/zda_gadget_bot

**Функции:**

- Покупки:
  - отправить гонца за покупками для команды
  - ежедневные семейные покупки: кто возвращается с работы через магазины, тот и затаривается
  - личный список покупок
- ВКС — для создания личного или командного списка конференций со ссылками
- Спорт — учёт тренировок. Для тех, кто не ведёт дневник, но считать километры хочется.
- Мероприятия — регистрация на мероприятия
- Отправка серверных сообщений владельцу бота

## 2. Разработчик

[Зайков Дмитрий](https://gitlab.com/d.a.zaykov)

## 3. Web API

- GET /hc — Healthcheck. Возвращает текст "ок" в случае нормальной работы.
- POST /zda_gadget_bot — Команды для бота 


## 4. Регистрация бота в Telegram

### 4.1. Получение токена

```
@BotFather
/newbot
```

Проверка API: https://api.telegram.org/bot0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/getMe

### 4.2. Иконка бота

Минимум 200х200, квадратная
```
@BotFather
/mybots
/setuserpic
```

### 4.3. Меню бота
```
@BotFather
/mybots
/setcommands
	menu - Меню
```

### 4.4. Привилегии

Чтобы бот получал все сообщения группы, надо отключить прайвеси и переподключить бота к группе. Это нужно, чтобы принимать ответы от пользователей.
```
@BotFather
/mybots
/setprivacy
	disable
```

Права админа в группе давать не нужно.


### 4.5. Установка веб-хука

Чтобы бот начал получать апдейты от сервера Телеграм, надо сделать следующее:

Проверить API: https://api.telegram.org/bot0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/getMe

Удалить старый веб-хук: https://api.telegram.org/bot0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/deleteWebhook

Установить новый веб-хук: https://api.telegram.org/bot0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/setWebhook?url=https://yourserver.ru/zda_gadget_bot


## 5. Установка на сервере

### 5.1. Зависимости

Deprecated! Версия на python

- mysql/mariadb
- python 3.9
- tornado 6.1
- pycurl 7.43
- simplejson 3.17
- pymysql 0.9

Ubuntu 20.4 / Debian 11 / Devuan 4:
```
# apt install python3-tornado python3-dev python3-pycurl python3-simplejson python3-pymysql
```

Версия на java

- mysql/mariadb
- openjdk-11-jre
- jetty 9

Ubuntu 20.4 / Debian 11 / Devuan 4:
```
# apt install openjdk-11-jre
```

### 5.2. Создание БД

Используется СУБД mysql/mariadb
```
$ mysql -u admin -p
> create database zda_gadget_bot character set utf8 collate utf8_general_ci;
# > drop database zda_gadget_bot;
> show databases;

> create user 'zda_gadget_bot_user'@'%' identified by 'pass';
> select user, host, password from mysql.user;
> grant all privileges on zda_gadget_bot.* to 'zda_gadget_bot_user'@'%';
> flush privileges;
> exit

$ mysql -u zda_gadget_bot_user -p 
> use zda_gadget_bot;
> show tables;
```

При запуске бот самостоятельно создает структуру таблиц в БД. Но создать БД должен администратор.

### 5.3. Запуск бота
```
$ python3 zda_gadget_bot.py /path/zda_gadget_bot.conf
$ java -Xmx100m -Xms10m -jar zda_gadget_bot.jar /path/zda_gadget_bot.conf
```

zda_gadget_bot.conf - конфигурационнный файл
```
host=127.0.0.1						# Хост для прослушивания веб-запросов
port=1001							# Порт для прослушивания веб-запросов
log=/path/zda_gadget_bot.log		# Файл журнала. Бот не сам его ротирует. Нужно настраивать logrotate 
temp=/path/temp	# Путь до папки с временными файлами

tm_uri=https://api.telegram.org/bot							# URI Telegram API
tm_file_uri=https://api.telegram.org/file/bot				# URI Telegram API для скачивания файлов
bot_token=0000000000:qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq	# Токен Telegram API
#bot_uri=https://www.example.ru/zda_gadget_bot`				# Deprecated. URI, на котором бот прослушивает веб-запросы

db_host=127.0.0.1					# Хост mysql
db_name=zda_gadget_bot				# Имя базы
db_user=zda_gadget_bot_user			# Логин
db_pass=pass						# Пароль

tz_user=Europe/Moscow				# Временная зона пользователя (учетная временная зона). 
									# Т.к. нет возможности правильно получать временную зону пользователя, то считаем, что все пользователи находятся в этой временной зоне
									# Однако все хранимые метки времени в БД хранятся в UTC.
gmt=3								# Deprecated. Учетный часовой пояс. GMT+3 - Москва

bot_vcontact=yes					# Активировать функцию "ВКС"
bot_buy=yes							# Активировать функцию "Покупки"
bot_sport=yes						# Активировать функцию "Спорт"
bot_feedback=yes					# Активировать функцию "Обратная связь" владельцу бота
owner_chat_id=000000000				# chat_id владельца бота. К нему присылаются сообщения с обратной связью

bot_mess=yes						# Активировать функцию "Отправка серверных сообщений"
bot_mess_path=400000000:/path/mess1	# Регистрация папки для наблюдения	
									# 400000000 - chat_id получателя сообщений
									# /path/mess1 - путь до папки, которую будет отслеживать бот.
									# При появлении новых файлов в папке, бот отправляет их получателю.
									# Если файл имеет расширение *.txt, то сообщение отправляется как текст, а не как файл.
									# Если в текстовом файле больше 1000 символов, то первые 1000 символов отправляются как текст, плюс досылается весь файл.
									# Бот должен иметь доступ к папке и файлам в нем на чтение и запись.
bot_mess_path=400000000:/path/mess2	# Можно следить за разными папками
bot_mess_path=400000001:/path/mess3
bot_mess_path=400000003:/path/mess4

bot_event=yes						# Активировать функцию "Мероприятия"
bot_event_doc=https://path/file.pdf	# Инструкция для Организатора мероприятий
bot_event_org=eventname1:300000000	# Регистрация организатора мероприятия
									# eventname1 - код мероприятия
									# 300000000 - chat_id организатора
bot_event_org=eventname2:300000000	# Можно регистрировать несколько мероприятий
bot_event_org=eventname3:300000000
bot_event_org=eventname4:300000002
bot_event_org=eventname5:400000002

```
