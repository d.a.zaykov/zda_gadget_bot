package ru.zdame.zda_gadget_bot;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

// Лимиты
public class Limit extends Thread {

	private static volatile boolean killed = false; // Признак завершения программы
	private static final int iPeriod = 1; // Период вытекания из басейна (сек)
	private static final Object lock = new Object(); // Объект для синхронизации потоков
	private static final int capacityLimit = 1000; // Начальная длина mapMonitor
	private static volatile HashMap<String, LimitRow> mapLimit = new HashMap<String, LimitRow>(capacityLimit);

	// Класс для одной записи
	// burst - объем бассейна. Точность 1/10
	// limit - количество вытекания из бассейна в сек. Точность 1/10
	// value - текущий объем в бассейне. Точность 1/10
	private static class LimitRow {
		public double burst;
		public double limit;
		public double value;
	}


	@Override
	public void run()
	{
		Logger.add("Limit: started");
		try {Thread.sleep(iPeriod*1000);} catch (Exception ex) {}

		while (!killed) {
			String sError = "";
			try {
				synchronized (lock) {
					// Пройти по всем счетчикамм и вычесть из burst значение limit
					if (mapLimit.size() > 0) {
						Set<Map.Entry<String, LimitRow>> set = mapLimit.entrySet();
						for (Map.Entry<String, LimitRow> me : set) {
							LimitRow row = me.getValue();
							if (row.value >= row.limit) {
								// Вычесть из значения limit
								row.value = row.value - row.limit;
								// Округлить до 0.1
								int k = (int)Math.round(row.value * 10);
								row.value = (double)((double)k / (double)10.0);
							}
							else {
								row.value = 0.0;
							}
							mapLimit.put(me.getKey(), row);
						}
					}
				}
			}
			catch (Exception ex)
			{
				Logger.add("Limit.run: Error: " + ex.getMessage());
			}

			if (!killed)
				try {Thread.sleep(iPeriod*1000);} catch (Exception ex) {}
		}
		Logger.add("Limit: stopped");
	}

	public static void stopLimit() {
		killed = true;
	}

	// Регистрация счетчика
	public static void reg(String sName, double dBurst, double dLimit) {
		try {
			synchronized (lock) {
				if (!mapLimit.containsKey(sName)) {
					LimitRow row = new LimitRow();
					row.burst = dBurst;
					row.limit = dLimit;
					row.value = 0.0;
					mapLimit.put(sName, row);
				}
			}
		}
		catch (Exception ex) {}
	}

	// Добавить значение dValue в бассейн
	// Если такой счетчик не найден, то ничего не делать
	public static void add(String sName, double dValue) {
		try {
			synchronized (lock) {
				if (mapLimit.containsKey(sName)) {
					LimitRow row = mapLimit.get(sName);
					row.value += dValue;
					// Округлить до 0.1
					int k = (int)Math.round(row.value * 10);
					row.value = (double)((double)k / (double)10.0);
					//
					mapLimit.put(sName, row);
				}
			}
		}
		catch (Exception ex) {}
	}

	// Проверить, можно ли добавлять значение dValue в бассейн
	// Если бассейн уже полон, то вернуть False
	// Если такой счетчик не найден, то вернуть False
	public static boolean check(String sName, double dValue) {
		try {
			synchronized (lock) {
				if (!mapLimit.containsKey(sName))
					return false;
				LimitRow row = mapLimit.get(sName);
				if ((row.value + dValue) > row.burst)
					return false;
				else
					return true;
			}
		}
		catch (Exception ex) {
			return false;
		}
	}

}