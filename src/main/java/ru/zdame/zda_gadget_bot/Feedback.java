package ru.zdame.zda_gadget_bot;

import java.util.ArrayList;


// Фоновая обработка сообщений с обратной связью
public class Feedback extends Thread {

	private static volatile boolean killed = false; // Признак завершения программы
	private static final int iPeriod = 13; // Период проверки новых сообщений (сек)

	@Override
	public void run()
	{
		Logger.add("Feedback: started");
		try {Thread.sleep(iPeriod*1000);} catch (Exception ex) {}

		while (!killed) {
			try {
				// Обработать новые сообщения
				handler();
			}
			catch (Exception ex)
			{
				Logger.add("Feedback.run: Error: " + ex.getMessage());
				Monitor.add("", "", "", "", "", "FEEDBACK_THREAD_FAIL", ex.getMessage());
			}

			if (!killed)
				try {Thread.sleep(iPeriod*1000);} catch (Exception ex) {}
		}
		Logger.add("Feedback: stopped");
	}

	public static void stopFeedback() {
		killed = true;
	}

	// Обработать новые сообщения
	private static void handler() {
		try {
			//Logger.add("Feedback.handler");
			Monitor.add("", "", "", "", "", "FEEDBACK_HANDLER_TOTAL", "");

			// Выбрать необработанные сообщения для хозяина
			ArrayList<DB.feedback_row> rows = DB.feedback_selectByStatus("IN", "SENT"); // Входящие сообщения со статусом Отправлено
			if (rows != null)
			{
				for (DB.feedback_row row : rows) {
					try {
						// Отправить сообщение владельцу бота
						TMSend.TMMessage tmMessage = new TMSend.TMMessage();
						tmMessage.to_chat_id = Main.BOT_OWNER_CHAT_ID;
						tmMessage.text = "📩 (" + row.id + ") от @" + row.from_username + " " + row.from_name + ":\n" + row.message;
						tmMessage.prior = 2;
						TMSend.send(tmMessage);
						// Сменить статус сообщения на обработанное
						DB.feedback_updateStatus(row.chat_id, row.chat_name, row.from_id, row.from_username, row.from_name, row.id, "RECEIVED");
					}
					catch (Exception ex2) {
						Logger.add("Feedback.handler: Error 1: " + ex2.getMessage());
						Monitor.add("", "", "", "", "", "FEEDBACK_HANDLER_FAIL", "1: " + ex2.getMessage());
					}
				}
			}

			// Выбрать необработанные ответы
			rows = DB.feedback_selectByStatus("OUT", "SENT"); // Ответ со статусом Отправлено
			if (rows != null)
			{
				for (DB.feedback_row row : rows) {
					try {
						// Определить отправителя
						String chat_id2 = "";
						String from_username2 = "";
						ArrayList<DB.feedback_row> rows2 = DB.feedback_chatById(row.chat_id, row.chat_name, row.from_id, row.from_username, row.from_name, row.reply_to_id);
						if (rows2 != null)
							if (rows2.size() >= 1)
							{
								chat_id2 = rows2.get(0).chat_id;
								from_username2 = rows2.get(0).from_username;
							}
						if (!chat_id2.isEmpty() && !from_username2.isEmpty()) {
							// Отправить ответ
							TMSend.TMMessage tmMessage = new TMSend.TMMessage();
							tmMessage.to_chat_id = chat_id2;
							tmMessage.prior = 2;
							tmMessage.text = "📩 @" + from_username2 + ", пришел ответ от хозяина:\n" + row.message;
							TMSend.send(tmMessage);
							// Сменить статус сообщения на обработанное
							DB.feedback_updateStatus(row.chat_id, row.chat_name, row.from_id, row.from_username, row.from_name, row.id, "RECEIVED");
						}
					}
					catch (Exception ex2) {
						Logger.add("Feedback.handler: Error 1: " + ex2.getMessage());
						Monitor.add("", "", "", "", "", "FEEDBACK_HANDLER_FAIL", "1: " + ex2.getMessage());
					}
				}
			}

			Monitor.add("", "", "", "", "", "FEEDBACK_HANDLER_SUCCESS", "");
		}
		catch (Exception ex)
		{
			Logger.add("Feedback.handler: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "FEEDBACK_HANDLER_FAIL", ex.getMessage());
		}
	}

}