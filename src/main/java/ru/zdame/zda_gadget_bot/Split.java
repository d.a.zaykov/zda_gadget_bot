package ru.zdame.zda_gadget_bot;

// Разбить строку на две по символу
public class Split {
	public String s1;
	public String s2;

	public Split(String s, char c, String sDef) {
		this.s1 = ru.zdame.zda_gadget_bot.Utils.isNull(s, "");
		this.s2 = ru.zdame.zda_gadget_bot.Utils.isNull(sDef, "");
		s = ru.zdame.zda_gadget_bot.Utils.isNull(s, "");

		try {
			int k = s.indexOf(c);
			if (k == 0) {
				this.s1 = "";
				this.s2 = s.substring(1);
			}
			else if (k > 0) {
				this.s1 = s.substring(0, k);
				this.s2 = s.substring(k+1);
			}
		}
		catch (Exception ex) {}
	}
}
