package ru.zdame.zda_gadget_bot;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

// Чтение параметров конфига
public class Config {
	private String fileName;
	private List<String> lstConf;

	public Config(String fileName) {
		this.fileName = Utils.isNull(fileName, "");

		// Чтение конфига
		this.lstConf = Utils.readFileToList(this.fileName);
	}

	public String getParam(String sParam, String sDef) {
		String res = sDef;
		sParam = Utils.isNull(sParam, "");

		if (this.lstConf == null)
			return sDef;
		if (this.lstConf.size() == 0)
			return sDef;

		try {
			for (String s: this.lstConf) {
				s = Utils.isNull(s, "").trim();

				// Определить ключ и значение параметра
				Split sp = new Split(s, '=', "");
				String s1 = sp.s1.trim(); // Это искомый ключ
				String s2 = sp.s2.trim();

				if (s1.equals(sParam)) {
					// Отсечь из значения параметра комментарии #
					sp = new Split(s2, '#', "");
					res = sp.s1.trim(); // Это искомое значение
					break;
				}
			}
		}
		catch (Exception ex) {
			System.out.println("Config.getParam(" + sParam + "): Error: " + ex.getMessage());
			return sDef;
		}

		return res;
	}

	// Проверить существование значения в списочном параметре
	// bot_event_org=probike_bzp:378246090
	public boolean checkValueInList(String sParam, String sValue) {
		sParam = Utils.isNull(sParam, "");
		sValue = Utils.isNull(sValue, "");

		if (this.lstConf == null)
			return false;
		if (this.lstConf.size() == 0)
			return false;

		try {
			for (String s: this.lstConf) {
				s = Utils.isNull(s, "").trim();

				// Определить ключ и значение параметра
				Split sp = new Split(s, '=', "");
				String s1 = sp.s1.trim(); // Это искомый ключ
				String s2 = sp.s2.trim();

				if (s1.equals(sParam)) {
					// Отсечь из значения параметра комментарии #
					sp = new Split(s2, '#', "");
					String s3 = sp.s1.trim(); // Это искомое значение

					if (s3.equals(sValue))
						return true;
				}
			}
		}
		catch (Exception ex) {
			System.out.println("Config.checkValueInList(" + sParam + ", " + sValue + "): Error: " + ex.getMessage());
			return false;
		}

		return false;
	}

	// Проверить совпадение значения в списочном параметре по регулярке
	// bot_event_org=probike_bzp:378246090
	public boolean matchValueInList(String sParam, String pattern) {
		sParam = Utils.isNull(sParam, "");
		pattern = Utils.isNull(pattern, "");

		if (this.lstConf == null)
			return false;
		if (this.lstConf.size() == 0)
			return false;

		try {
			for (String s: this.lstConf) {
				s = Utils.isNull(s, "").trim();

				// Определить ключ и значение параметра
				Split sp = new Split(s, '=', "");
				String s1 = sp.s1.trim(); // Это искомый ключ
				String s2 = sp.s2.trim();

				if (s1.equals(sParam)) {
					// Отсечь из значения параметра комментарии #
					sp = new Split(s2, '#', "");
					String s3 = sp.s1.trim(); // Это искомое значение

					if (Pattern.matches(pattern, s3))
						return true;
				}
			}
		}
		catch (Exception ex) {
			System.out.println("Config.matchValueInList(" + sParam + ", " + pattern + "): Error: " + ex.getMessage());
			return false;
		}

		return false;
	}

	// Выбрать все event по оргпнизатору chat_id
	// bot_event_org=probike_bzp:378246090
	public ArrayList<String> getEventsInList(String sParam, String chat_id) {
		sParam = Utils.isNull(sParam, "");
		chat_id = Utils.isNull(chat_id, "");

		if (this.lstConf == null)
			return null;
		if (this.lstConf.size() == 0)
			return null;

		try {
			ArrayList<String> list = new ArrayList<String>();
			for (String s: this.lstConf) {
				s = Utils.isNull(s, "").trim();

				// Определить ключ и значение параметра
				Split sp = new Split(s, '=', "");
				String s1 = sp.s1.trim(); // Это искомый ключ
				String s2 = sp.s2.trim();

				if (s1.equals(sParam)) {
					// Отсечь из значения параметра комментарии #
					sp = new Split(s2, '#', "");
					String s3 = sp.s1.trim(); // Это искомое значение в конфиге

					// Разделить event:chat_id
					sp = new Split(s3, ':', "");
					String s5 = sp.s1.trim(); // event
					String s6 = sp.s2.trim(); // chat_id
					if (s6.equals(chat_id))
						list.add(s5);
				}
			}
			return list;
		}
		catch (Exception ex) {
			System.out.println("Config.getEventsInList(" + sParam + ", " + chat_id + "): Error: " + ex.getMessage());
			return null;
		}
	}

	// Выбрать все значения списочного параметра
	// bot_mess_path=400000000:/path/mess1
	public ArrayList<String> getList(String sParam) {
		sParam = Utils.isNull(sParam, "");

		if (this.lstConf == null)
			return null;
		if (this.lstConf.size() == 0)
			return null;

		try {
			ArrayList<String> list = new ArrayList<String>();
			for (String s: this.lstConf) {
				s = Utils.isNull(s, "").trim();

				// Определить ключ и значение параметра
				Split sp = new Split(s, '=', "");
				String s1 = sp.s1.trim(); // Это искомый ключ
				String s2 = sp.s2.trim();

				if (s1.equals(sParam)) {
					// Отсечь из значения параметра комментарии #
					sp = new Split(s2, '#', "");
					String s3 = sp.s1.trim(); // Это искомое значение в конфиге
					list.add(s3);
				}
			}
			return list;
		}
		catch (Exception ex) {
			System.out.println("Config.getList(" + sParam + "): Error: " + ex.getMessage());
			return null;
		}
	}

}