package ru.zdame.zda_gadget_bot;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TimeZone;
import java.time.LocalDateTime;


public class Utils {

	// Подготовка строки. Замена null на sDef
	public static String isNull(String s, String sDef) {
		if (s == null)
			return sDef;
		else
			return s;
	}

	// Чтение строки из JsonNode
	public static String jsonReadStr(JsonNode node, String param, String sDef) {
		try {
			if (node == null)
				return sDef;
			return  isNull(node.get(param).asText(), sDef);
		}
		catch (Exception ex) {
			return sDef;
		}
	}

	// Подготовка строки. Обрезание до max символов
	public static String strtrunc(String s, int max) {
		String s2 = isNull(s, "");
		int n = s2.length();
		if (n > max)
			return s2.substring(0, max);
		else
			return s2;
	}

	public static boolean pathExists(String s) {
		Path path = Paths.get(isNull(s, ""));
		if (Files.exists(path))
			if (!Files.isRegularFile(path))
				return true;
			else
				return false;
		else
			return false;
	}

	// Целое значение GMT перевести в отображаемую строку
	// 3 -> GMT+3
	// -3 -> GMT-3
	public static String gmt2str(int gmt) {
		if (gmt < 0)
			return "GMT" + gmt;
		else
			return "GMT+" + gmt;
	}

	// Временную зону TZ перевести в отображаемую строку
	// UTC+05:00
	// UTC-05:00
	public static String tz2str(TimeZone tz) {
		int tzServerRawOffsetMS = tz.getRawOffset(); // Смещение относительно UTC (мс)
		int tzServerRawOffsetS = (int)(tzServerRawOffsetMS / (int)1000); // Смещение относительно UTC (сек)
		int tzServerRawOffsetMin = (int)(tzServerRawOffsetS / (int)60); // Смещение относительно UTC (мин)
		int tzServerRawOffsetH = (int)(tzServerRawOffsetMin / (int)60); // Смещение относительно UTC (час)
		int tzServerRawOffsetH2 = (int)(tzServerRawOffsetMin % (int)60); // Смещение относительно UTC. Остаток (мин)

		if (tzServerRawOffsetMS < 0)
			return "UTC" + String.format("%02d", tzServerRawOffsetH) + ":" + String.format("%02d", tzServerRawOffsetH2);
		else
			return "UTC+" + String.format("%02d", tzServerRawOffsetH) + ":" + String.format("%02d", tzServerRawOffsetH2);
	}

	// Временную зону TZ перевести в GMT в  часах
	// UTC+05:00 = 5
	// UTC-05:30 = -5
	public static int tz2gmt(TimeZone tz) {
		int tzServerRawOffsetMS = tz.getRawOffset(); // Смещение относительно UTC (мс)
		int tzServerRawOffsetS = (int)(tzServerRawOffsetMS / (int)1000); // Смещение относительно UTC (сек)
		int tzServerRawOffsetMin = (int)(tzServerRawOffsetS / (int)60); // Смещение относительно UTC (мин)
		int tzServerRawOffsetH = (int)(tzServerRawOffsetMin / (int)60); // Смещение относительно UTC (час)
		return tzServerRawOffsetH;
	}

	public static String date2striso(LocalDateTime dt) {
		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		return dt.format(df);
	}

	public static String date2str(LocalDateTime dt) {
		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return dt.format(df);
	}

	// Локальное время в UTC для записи в БД
	// 28.11.2021 19:50 UTC+05:00 -> 28.11.2021 14:50 UTC+00:00
	public static String date2UTC(LocalDateTime dt, TimeZone tz) {
		int tzServerRawOffsetS = (int)(tz.getRawOffset() / (int)1000); // Смещение относительно UTC (сек)
		LocalDateTime dt2 = dt.minusSeconds(tzServerRawOffsetS);
		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		return dt2.format(df);

		//Date dtLocal2 = new Date(dtLocal.getTime() - tz.getRawOffset());
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//String sLocal2 = df.format(dtLocal2);
		//System.out.println("sLocal2=[" + sLocal2 + "]");

		//return df.format(dtLocal2);
		//return "";

		/*SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		isoFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date dtUTC = null;
		try {
			dtUTC = isoFormat.parse(sLocal2);
		}
		catch (Exception ex) {}*/


		//Date dtUTC = Date.from(dtLocal.toInstant());

		//System.out.println(LocalDateTime.ofInstant(i, ZoneOffset.UTC));

		///LocalDateTime datetime = LocalDateTime.ofInstant(i, ZoneOffset.UTC);
		//String formatted = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss").format(datetime);
		//System.out.println(formatted);

		//java.time.Instant dtUTC = Instant.ofEpochMilli(dtLocal.getTime());
		/*
		Date dtUTC = new Date(dtLocal.getTime() - tz.getRawOffset());
		dtUTC.zon
		//dfUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		isoFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = isoFormat.parse("2010-05-23T09:01:02");
		*/
	}

	// Локальное время в UTC для записи в БД
	// 28.11.2021 19:50 UTC+05:00 -> 28.11.2021 14:50 UTC+00:00
	public static LocalDateTime date2UTC2(LocalDateTime dt, TimeZone tz) {
		int tzServerRawOffsetS = (int)(tz.getRawOffset() / (int)1000); // Смещение относительно UTC (сек)
		LocalDateTime dt2 = dt.minusSeconds(tzServerRawOffsetS);
		return dt2;
	}

	// Добавить смещение offset (ms) ко времени dt
	// 28.11.2021 17:50 +02:00 -> 28.11.2021 19:50
	public static String date2TZ(LocalDateTime dt, int offset) {
		int offsetS = (int)(offset / (int)1000); // сек
		LocalDateTime dt2 = dt.plusSeconds(offsetS);
		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		return dt2.format(df);
	}

	// Добавить смещение offset (ms) ко времени dt
	// 28.11.2021 17:50 +02:00 -> 28.11.2021 19:50
	public static LocalDateTime date2TZ2(LocalDateTime dt, int offset) {
		int offsetS = (int)(offset / (int)1000); // сек
		LocalDateTime dt2 = dt.plusSeconds(offsetS);
		return dt2;
	}

	public static String date2str(LocalDateTime dt, String format) {
		format = isNull(format, "");
		if (format.isEmpty())
			format = "yyyy-MM-dd'T'HH:mm:ss";
		DateTimeFormatter df = DateTimeFormatter.ofPattern(format);
		return dt.format(df);
	}

	public static LocalDateTime str2date(String sdt, String format, LocalDateTime dtDefault) {
		sdt = isNull(sdt, "");
		if (sdt.isEmpty())
			return dtDefault;

		format = isNull(format, "");
		if (format.isEmpty())
			format = "yyyy-MM-dd'T'HH:mm:ss";

		DateTimeFormatter df = DateTimeFormatter.ofPattern(format);

		LocalDateTime dt = dtDefault;
		try {
			dt = LocalDateTime.parse(sdt, df);
		}
		catch (Exception ex) {
			dt = dtDefault;
		}

		return dt;
	}


	// Прочитать текстовый файл
	public static String readFileToStr(String fileName) {
		try {
			return Files.readString(Paths.get(fileName), StandardCharsets.UTF_8);
				/*//List<String> lst = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
			if (lst != null)
				if (lst.size() > 0)
					for (String s: lst) {
						s = isNull(s, "").trim();

						// Определить ключ и значение параметра
						Split sp = new Split(s, '=', "");
						String s1 = sp.s1.trim(); // Это искомый ключ
						String s2 = sp.s2.trim();

						if (s1.equals(sParam)) {
							// Отсечь из значения параметра комментарии #
							sp = new Split(s2, '#', "");
							res = sp.s1.trim(); // Это искомое значение
							break;
						}
					}*/
			//return res;
		}
		catch (Exception ex) {
			return "";
		}
	}

	// Прочитать текстовый файл
	public static List<String> readFileToList(String fileName) {
		try {
			return Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
		}
		catch (Exception ex) {
			return null;
		}
	}

	// Запись строки на диск
	public static boolean writeStrToFile(String sFullFileName, String sText, boolean append) {
		try {
			sText = isNull(sText, "");
			FileWriter f = new FileWriter(sFullFileName, append);
			try {
				f.write(sText);
			} finally {
				f.flush();
				f.close();
			}
			return true;
		}
		catch (Exception ex) {
			System.out.println("Utils.writeStrToFile: Error: " + ex.getMessage());
			return false;
		}
	}

	// Получить расширение файла
	public static String getFileExt(String sFileName) {
		try {
			sFileName = isNull(sFileName, "");
			String sExt = "";
			int i = sFileName.lastIndexOf('.');
			if (i > 0)
				sExt = sFileName.substring(i+1);
			return sExt;
		}
		catch (Exception ex) {
			return "";
		}
	}


	public static String convertStreamToString(InputStream is) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();

			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
			} catch (Exception ex2) {
				return null;
			} finally {
				is.close();
			}
			return sb.toString();
		}
		catch (Exception ex) {
			return null;
		}
	}

	/*public static boolean convertStreamToFile(InputStream is, String sFullFileName) {
		try {
			File file = new File(sFullFileName);
			try (FileOutputStream outputStream = new FileOutputStream(file)) {
				int read;
				byte[] bytes = new byte[1024];
				while ((read = is.read(bytes)) != -1) {
					outputStream.write(bytes, 0, read);
				}
			}
			catch (Exception ex2) {
				return false;
			}
			return true;
		}
		catch (Exception ex) {
			return false;
		}
	}*/

}


