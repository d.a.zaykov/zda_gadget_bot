package ru.zdame.zda_gadget_bot;

import sun.misc.Signal;
import sun.misc.SignalHandler;

import java.io.File;
import java.util.Locale;
import java.util.TimeZone;


public class Main {

	public static volatile boolean killed = false; // Признак завершения программы

	private static final String APP_NAME = "zda_gadget_bot";
	private static final String APP_VERSION = "dev";
	private static String APP_FULLNAME = ""; // Полное имя файла приложения
	private static String APP_PATH = ""; // Полный путь до приложения

	private static String CONF_FILENAME = ""; // Полный путь до конфига
	private static String HOST = ""; // Хост для прослушивания веб-запросов
	private static int PORT = 0; // tcp-порт для прослушивания веб-запросов
	private static String TZ_USER = ""; // TZ пользователя (учетная временная зона). Считаем, что все пользователя находятся в этой TZ
	public static volatile TimeZone tzUser = null; // TZ пользователя
	public static volatile TimeZone tzServer = java.util.TimeZone.getDefault(); // TZ сервера
	//private static int GMT = 0; // Deprecated. Учетный часовой пояс в часах X

	private static String LOG_FILENAME = ""; // Полный путь до лога
	public static String TEMP_PATH = ""; // Полный путь до папки с временными файлами
	private static String BOT_TOKEN = ""; // Токен бота 0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	//private static String BOT_URI = ""; // URL приложения "https://www.zdame.ru/zda_gadget_bot"
	public static String TM_URI = ""; // Базовый URL API "https://api.telegram.org/bot%s/" % sBOT_TOKEN
	public static String TM_FILE_URI = ""; // Базовый URL API "https://api.telegram.org/file/bot%s/" % sBOT_TOKEN

	private static String DB_HOST = ""; // Хост базы mysql
	private static String DB_NAME = ""; // Имя базы
	private static String DB_USER = ""; // Логин
	private static String DB_PASS = ""; // Пароль

	public static boolean isBOT_VCONTACT = false; // активация функции списка ВКС. Нужна БД mysql
	public static boolean isBOT_BUY = false; // активация функции списка покупок. Нужна БД mysql
	public static boolean isBOT_SPORT = false; // активация функции спорта. Нужна БД mysql
	public static boolean isBOT_FEEDBACK = false; // активация функции обратной связи Нужна БД mysql
	public static String BOT_OWNER_CHAT_ID = ""; // chat_id владелеца бота для обратной связи
	public static boolean isBOT_MESS = false; // активация функции серверных сообщений
	public static boolean isBOT_EVENT = false; // yes - активация функции "Мероприятия". Нужна БД mysql
	public static String BOT_EVENT_DOC = ""; // URL на инструкцию для организатора мероприятия

	public static Config config = null; // Главный конфиг

	public static void main(String[] args) {

		// Обработчик сигналов
		SignalHandler signalHandler = new SignalHandler() {
			@Override
			public void handle(Signal sig) {
				Logger.add("SIG" + sig.getName());
				Monitor.add("", "", "", "", "", "SIGNAL", "SIG" + sig.getName());
				killed = true;
				//try {Thread.interrupted();} catch (Exception ex) {}
				TMSend.stopSend();
				Logger.stopLogger();
				Monitor.stopMonitor();
				Feedback.stopFeedback();
				Mess.stopMess();
				Limit.stopLimit();
				Web.stopWeb();
			}
		};

		Signal signalTERM = new Signal("TERM");
		Signal signalINT = new Signal("INT");
		Signal signalABRT = new Signal("ABRT");
		SignalHandler shTERM = Signal.handle(signalTERM, signalHandler);
		SignalHandler shINT = Signal.handle(signalINT, signalHandler);
		SignalHandler shABRT = Signal.handle(signalABRT, signalHandler);

		// Старт
		Logger.add("");
		Logger.add("");
		Logger.add("==================================================================");
		Logger.add("Старт: " + APP_NAME + "-" + APP_VERSION);
		Monitor.add("", "", "", "", "", "START", "");

		try {
			//APP_PATH = System.getProperty("user.dir"); // текущий рабочий каталог
			//APP_PATH = System.getProperty("user.home"); // домашний каталог пользователя
			// Полное имя файла приложения
			APP_FULLNAME = Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
			// Полный путь до приложения
			APP_PATH = new File(APP_FULLNAME).getParent();
		}
		catch (Exception ex) {}
		Logger.add("APP_FULLNAME=" + APP_FULLNAME);
		Logger.add("APP_PATH=" + APP_PATH);

		// Параметры командной строки
		//Logger.add("args.length=" + args.length);
		if (args.length > 0) {
			CONF_FILENAME = args[0].trim();
			Logger.add("CONF_FILENAME=" + CONF_FILENAME);
		}
		if (CONF_FILENAME.isEmpty()) {
			Logger.add("main: Error: Не задан конфиг");
			return;
		}


		// Подключаем конфиг
		config = new Config(CONF_FILENAME);


		// Порт
		String sPort = config.getParam("port", "");
		Logger.add("main: port=[" + sPort + "]");
		PORT = 0;
		try {
			PORT = Integer.parseInt(sPort);
		}
		catch (Exception ex) {
			PORT = 0;
			Logger.add("main: Error: Не верный формат порта: port=[" + sPort + "]");
			return;
		}
		if (PORT <= 0) {
			Logger.add("main: Error: Не задан порт: port=[" + sPort + "]");
			return;
		}
		Logger.add("main: int port=[" + PORT + "]");

		// Хост
		HOST = config.getParam("host", "");
		Logger.add("main: host=[" + HOST + "]");

		// Полный путь до лога
		LOG_FILENAME = config.getParam("log", "");
		Logger.add("main: log=[" + LOG_FILENAME + "]");
		Logger.fileName = LOG_FILENAME;

		// Полный путь до папки с временными файлами
		TEMP_PATH = config.getParam("temp", "");
		Logger.add("main: temp=[" + TEMP_PATH + "]");
		if (TEMP_PATH.isEmpty()) {
			Logger.add("main: Error: Не задан temp");
			return;
		}
		if (!Utils.pathExists(TEMP_PATH)) {
			Logger.add("main: Error: Не найден путь temp=[" + TEMP_PATH + "]");
			return;
		}

		// Токен бота 0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		BOT_TOKEN = config.getParam("bot_token", "");
		Logger.add("main: bot_token=[" + BOT_TOKEN + "]");

		// URI приложения "https://www.zdame.ru/zda_gadget_bot"
		//BOT_URI = conf.getParam("bot_uri", "");
		//Logger.add("main: bot_uri=[" + BOT_URI + "]");

		// Базовый URI API "https://api.telegram.org/bot%s/" % sBOT_TOKEN
		TM_URI = config.getParam("tm_uri", "");
		TM_URI = TM_URI + BOT_TOKEN + "/";
		Logger.add("main: tm_uri=[" + TM_URI + "]");

		// Базовый URI API "https://api.telegram.org/file/bot%s/" % sBOT_TOKEN
		TM_FILE_URI = config.getParam("tm_file_uri", "");
		TM_FILE_URI = TM_FILE_URI + BOT_TOKEN + "/";
		Logger.add("main: tm_file_uri=[" + TM_FILE_URI + "]");

		// Хост базы mysql
		DB_HOST = config.getParam("db_host", "");
		Logger.add("main: db_host=[" + DB_HOST + "]");
		if (DB_HOST.isEmpty()) {
			Logger.add("main: Error: Не задан db_host=[" + DB_HOST + "]");
			return;
		}

		// Имя базы
		DB_NAME = config.getParam("db_name", "");
		Logger.add("main: db_name=[" + DB_NAME + "]");
		if (DB_NAME.isEmpty()) {
			Logger.add("main: Error: Не задан db_name=[" + DB_NAME + "]");
			return;
		}

		// Логин
		DB_USER = config.getParam("db_user", "");
		Logger.add("main: db_user=[" + DB_USER + "]");
		if (DB_USER.isEmpty()) {
			Logger.add("main: Error: Не задан db_user=[" + DB_USER + "]");
			return;
		}

		// Пароль
		DB_PASS = config.getParam("db_pass", "");
		Logger.add("main: db_pass=[" + DB_PASS + "]");
		if (DB_PASS.isEmpty()) {
			Logger.add("main: Error: Не задан db_pass=[" + DB_PASS + "]");
			return;
		}

		// Временная зона сервера
		tzServer = java.util.TimeZone.getDefault();
		Logger.add("main: TZ сервера: " + Utils.tz2str(tzServer) + ", (offset " + tzServer.getRawOffset() + " ms)");

		// Временная зона пользователя
		String TZ_USER = config.getParam("tz_user", "");
		Logger.add("main: tz_user=[" + TZ_USER + "]");
		try {
			tzUser = TimeZone.getTimeZone(TZ_USER);
			//GMT = Utils.tz2gmt(tzUser);
		}
		catch (Exception ex) {
			Logger.add("main: Error: Неверная временная зона: tz_user=[" + TZ_USER + "]");
			return;
		}
		Logger.add("main: TZ пользователя: " + tzUser.getDisplayName(false, TimeZone.SHORT, Locale.ENGLISH) + ", " + Utils.tz2str(tzUser) + ", (offset " + tzUser.getRawOffset() + " ms)");
		//Logger.add("main: gmt=" + Utils.gmt2str(GMT));

		/*String sGMT = conf.getParam("gmt", "");
		Logger.add("main: gmt=[" + sGMT + "]");
		GMT = 0;
		try {
			GMT = Integer.parseInt(sGMT);
		}
		catch (Exception ex) {
			GMT = 0;
			Logger.add("main: Error: Не верный формат GMT: gmt=[" + sGMT + "]");
			return;
		}
		if (GMT < -23)
			GMT = -23;
		if (GMT > 23)
			GMT = 23;
		Logger.add("main: int gmt=[" + String.valueOf(GMT) + "], " + Utils.gmt2str(GMT));*/

		// активация функции списка ВКС
		String sBOT_VCONTACT = config.getParam("bot_vcontact", "");
		Logger.add("main: bot_vcontact=[" + sBOT_VCONTACT + "]");
		if (sBOT_VCONTACT.equals("yes"))
			isBOT_VCONTACT = true;
		Logger.add("main: is bot_vcontact=[" + isBOT_VCONTACT + "]");

		// активация функции списка покупок
		String sBOT_BUY = config.getParam("bot_buy", "");
		Logger.add("main: bot_buy=[" + sBOT_BUY + "]");
		if (sBOT_BUY.equals("yes"))
			isBOT_BUY = true;
		Logger.add("main: is bot_buy=[" + isBOT_BUY + "]");

		// активация функции спорта
		String sBOT_SPORT = config.getParam("bot_sport", "");
		Logger.add("main: bot_sport=[" + sBOT_SPORT + "]");
		if (sBOT_SPORT.equals("yes"))
			isBOT_SPORT = true;
		Logger.add("main: is bot_sport=[" + isBOT_SPORT + "]");

		// активация функции обратной связи
		String sBOT_FEEDBACK = config.getParam("bot_feedback", "");
		Logger.add("main: bot_feedback=[" + sBOT_FEEDBACK + "]");
		if (sBOT_FEEDBACK.equals("yes"))
			isBOT_FEEDBACK = true;
		Logger.add("main: is bot_feedback=[" + isBOT_FEEDBACK + "]");

		// chat_id владелеца бота для сообщений
		if (isBOT_FEEDBACK || isBOT_MESS) {
			BOT_OWNER_CHAT_ID = config.getParam("owner_chat_id", "");
			Logger.add("main: owner_chat_id=[" + BOT_OWNER_CHAT_ID + "]");
			if (BOT_OWNER_CHAT_ID.isEmpty()) {
				Logger.add("main: Error: Не задан owner_chat_id");
				return;
			}
		}

		// Активация функции серверных сообщений
		String sBOT_MESS = config.getParam("bot_mess", "");
		Logger.add("main: bot_mess=[" + sBOT_MESS + "]");
		if (sBOT_MESS.equals("yes"))
			isBOT_MESS = true;
		Logger.add("main: is bot_messk=[" + isBOT_MESS + "]");

		/*if (isBOT_MESS) {
			BOT_MESS_PATH = config.getParam("bot_mess_path", "");
			Logger.add("main: bot_mess_path=[" + BOT_MESS_PATH + "]");
			if (BOT_MESS_PATH.isEmpty()) {
				Logger.add("main: Error: Не задан bot_mess_path");
				return;
			}
			if (!Utils.pathExists(BOT_MESS_PATH)) {
				Logger.add("main: Error: Не найден путь bot_mess_path=[" + BOT_MESS_PATH + "]");
				return;
			}
		}*/


		// Активация функции Мероприятия
		String sBOT_EVENT = config.getParam("bot_event", "");
		Logger.add("main: bot_event=[" + sBOT_EVENT + "]");
		if (sBOT_EVENT.equals("yes"))
			isBOT_EVENT = true;
		Logger.add("main: is bot_event=[" + isBOT_EVENT + "]");

		if (isBOT_EVENT) {
			// Инструкция организатора
			BOT_EVENT_DOC = config.getParam("bot_event_doc", "");
			Logger.add("main: bot_event_doc=[" + BOT_EVENT_DOC + "]");

			// Проверка привилегии
			// bot_event_org=probike_bzp:378246090
			//if (conf.checkValueInList("bot_event_org", "probike_bzp:378246090"))
			//	Logger.add("main: bot_event_org=probike_bzp:378246090  EXIST");
			//else
			//	Logger.add("main: bot_event_org=probike_bzp:378246090  NOT EXIST");
		}


		// Максимальное время Date
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
		//DateFormat dfUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
		//dfUTC.setTimeZone(TimeZone.getTimeZone("UTC"));

		//Date dtMax = new Date(Long.MAX_VALUE);
		//System.out.println("dtMax=[" + df.format(dtMax) + "]");

		//Date dtLocal = new Date(System.currentTimeMillis());
		//System.out.println("dtLocal=[" + df.format(dtLocal) + "]");
		//System.out.println("dtLocal in UTC=[" + Utils.dateLocal2UTC(dtLocal, tzServer) + "]");

		//Date dtUTC = Utils.dateLocal2UTC(dtLocal, tzServer);
		//System.out.println("dtUTC=[" + df.format(dtUTC) + "]");
		//System.out.println("dtUTC=[" + df.format(dtUTC) + "]");

		//LocalDateTime ldt = LocalDateTime.now(); //new LocalDateTime(LocalDate.now(), LocalTime.now());
		//System.out.println("ldt=[" + ldt.toString() + "]");

		//LocalDateTime dtLocal = LocalDateTime.now();
		///System.out.println(Utils.date2str(dtLocal) + " - " + Utils.tz2str(tzUser) +" = " + Utils.date2UTC(dtLocal, tzUser));



		// Старт логгера
		Logger logger = new Logger();
		logger.setDaemon(true);
		logger.start();

		// Старт лимитов
		Limit limit = new Limit();
		limit.setDaemon(true);
		limit.start();

		// Подключение к БД
		if (!DB.start(DB_HOST, DB_NAME, DB_USER, DB_PASS)) {
			return;
		}

		// Старт мониторинга
		Monitor monitor = new Monitor();
		monitor.setDaemon(true);
		monitor.start();

		// Старт фоновой отправки сообщений
		TMSend send = new TMSend();
		send.setDaemon(true);
		send.start();

		// Старт обработчика обратной связи
		Feedback feedback = null;
		if (isBOT_FEEDBACK) {
			feedback = new Feedback();
			feedback.setDaemon(true);
			feedback.start();
		}

		// Старт обработчика серверных сообщений
		Mess mess = null;
		if (isBOT_MESS) {
			mess = new Mess();
			mess.setDaemon(true);
			mess.start();
		}

		// Старт web-сервера
		Web.startWeb(HOST, PORT);
		try {Thread.sleep(500);} catch (Exception ex) {}

		// Сбросить метрики в базу
		Monitor.flush();

		// Сбросить лог на диск
		Logger.flush();

		System.out.println("END");
	}
}