package ru.zdame.zda_gadget_bot;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

// Запрос от телеграма
public class TMRequest {
	public String sRequest; // Тело запроса строкой для логов
	public String chat_id; // chat_id пользователя
	public String chat_type; // chat_type пользователя
	public String chat_username; // chat_username пользователя
	public String chat_name; // title + first_name + last_name пользователя
	public String from_id; // from_id пользователя
	public String from_username; // from_username пользователя
	public String from_name; // first_name + last_name пользователя
	public String sText; // Сообщение от пользователя
	public String sSRCMessageId; // id сообщение, которое отправило это сообщение
	public String sDocFileId; // id файла, который отправил пользователь
	public String sDocFileName; // Имя файла, который отправил пользователь
	public boolean isChatPrivate; // true - Личный чат с ботом
	public String sError; // Ошибка разбора запроса

	public TMRequest(HttpServletRequest request) {
		sRequest = "";
		chat_id = "";
		chat_type = "";
		chat_name = "";
		chat_username = "";
		from_id = "";
		from_username = "";
		from_name = "";
		sText = "";
		sSRCMessageId = "";
		sDocFileId = "";
		sDocFileName = "";
		isChatPrivate = false;
		sError = "";

		try {
			String contentType = Utils.isNull(request.getHeader("Content-Type"), "");
			if (!contentType.equals("application/json")) {
				sError = "TMRequest: not application/json";
				return;
			}

			BufferedReader reader = request.getReader();
			String line = null;
			while ((line = reader.readLine()) != null)
				sRequest += line;
			Logger.add("TMRequest: sRequest=" + sRequest);

			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readValue(sRequest, JsonNode.class);

			String first_name = "";
			String last_name = "";
			String chat_title = "";
			String chat_first_name = "";
			String chat_last_name = "";

			// callback_query - ответ на кнопку
			JsonNode callbackNode = rootNode.get("callback_query");
			if (callbackNode != null) {
				// from
				JsonNode fromNode = callbackNode.get("from");
				if (fromNode != null) {
					from_id = Utils.strtrunc(Utils.jsonReadStr(fromNode, "id" ,"").trim(), 255);
					Logger.add("TMRequest: from_id=" + from_id);

					from_username = Utils.strtrunc(Utils.jsonReadStr(fromNode, "username", "").trim(), 255);
					Logger.add("TMRequest: from_username=" + from_username);

					first_name =  Utils.jsonReadStr(fromNode, "first_name", "").trim();
					Logger.add("TMRequest: first_name=" + first_name);

					last_name =  Utils.jsonReadStr(fromNode, "last_name", "").trim();
					Logger.add("TMRequest: last_name=" + last_name);
				}

				// message
				JsonNode messageNode = callbackNode.get("message");
				if (messageNode != null) {
					sSRCMessageId = Utils.strtrunc(Utils.jsonReadStr(messageNode, "message_id", "").trim(), 255);
					Logger.add("TMRequest: message_id=" + sSRCMessageId);

					// chat
					JsonNode chatNode = messageNode.get("chat");
					if (chatNode != null) {
						chat_id = Utils.strtrunc(Utils.jsonReadStr(chatNode, "id", "").trim(), 255);
						Logger.add("TMRequest: chat_id=" + chat_id);

						chat_type = Utils.strtrunc(Utils.jsonReadStr(chatNode, "type", "").trim(), 255);
						Logger.add("TMRequest: chat_type=" + chat_type);

						chat_username = Utils.strtrunc(Utils.jsonReadStr(chatNode, "username", "").trim(), 255);
						Logger.add("TMRequest: chat_username=" + chat_username);

						chat_title = Utils.jsonReadStr(chatNode, "title", "").trim();
						Logger.add("TMRequest: chat_title=" + chat_title);

						chat_first_name = Utils.jsonReadStr(chatNode, "first_name", "").trim();
						Logger.add("TMRequest: chat_first_name=" + chat_first_name);

						chat_last_name = Utils.jsonReadStr(chatNode, "last_name", "").trim();
						Logger.add("TMRequest: chat_last_name=" + chat_last_name);
					}
				}

				sText =  Utils.jsonReadStr(callbackNode, "data", "").trim();
				Logger.add("TMRequest: text=" + sText);
			}
			// Обычное сообщение
			else
			{
				// message
				JsonNode messageNode = rootNode.get("message");
				if (messageNode != null) {
					sSRCMessageId = Utils.strtrunc(Utils.jsonReadStr(messageNode, "message_id", "").trim(), 255);
					Logger.add("TMRequest: message_id=" + sSRCMessageId);

					// from
					JsonNode fromNode = messageNode.get("from");
					if (fromNode != null) {
						from_id = Utils.strtrunc(Utils.jsonReadStr(fromNode, "id" ,"").trim(), 255);
						Logger.add("TMRequest: from_id=" + from_id);

						from_username = Utils.strtrunc(Utils.jsonReadStr(fromNode, "username", "").trim(), 255);
						Logger.add("TMRequest: from_username=" + from_username);

						first_name = Utils.jsonReadStr(fromNode, "first_name", "").trim();
						Logger.add("TMRequest: first_name=" + first_name);

						last_name = Utils.jsonReadStr(fromNode, "last_name", "").trim();
						Logger.add("TMRequest: last_name=" + last_name);
					}

					// chat
					JsonNode chatNode = messageNode.get("chat");
					if (chatNode != null) {
						chat_id = Utils.strtrunc(Utils.jsonReadStr(chatNode, "id", "").trim(), 255);
						Logger.add("TMRequest: chat_id=" + chat_id);

						chat_type = Utils.strtrunc(Utils.jsonReadStr(chatNode, "type", "").trim(), 255);
						Logger.add("TMRequest: chat_type=" + chat_type);

						chat_username = Utils.strtrunc(Utils.jsonReadStr(chatNode, "username", "").trim(), 255);
						Logger.add("TMRequest: chat_username=" + chat_username);

						chat_title = Utils.jsonReadStr(chatNode, "title", "").trim();
						Logger.add("TMRequest: chat_title=" + chat_title);

						chat_first_name = Utils.jsonReadStr(chatNode, "first_name", "").trim();
						Logger.add("TMRequest: chat_first_name=" + chat_first_name);

						chat_last_name = Utils.jsonReadStr(chatNode, "last_name", "").trim();
						Logger.add("TMRequest: chat_last_name=" + chat_last_name);
					}

					// document
					JsonNode documentNode = messageNode.get("document");
					if (chatNode != null) {
						sDocFileName = Utils.jsonReadStr(documentNode, "file_name", "").trim();
						Logger.add("TMRequest: sDocFileName=" + sDocFileName);

						sDocFileId =  Utils.jsonReadStr(documentNode, "file_id", "").trim();
						Logger.add("TMRequest: sDocFileId=" + sDocFileId);
					}

					sText =  Utils.jsonReadStr(messageNode, "text", "").trim();
					Logger.add("TMRequest: text=" + sText);
				}
			}

			from_name = Utils.strtrunc((first_name + " " + last_name).trim(), 255);
			Logger.add("TMRequest: from_name=" + from_name);

			chat_name = Utils.strtrunc((chat_title + " " + chat_first_name + " " + chat_last_name).trim(), 255);
			Logger.add("TMRequest: chat_name=" + chat_name);

			if (chat_type.equals("private") && chat_id.equals(from_id))
				isChatPrivate = true;
		}
		catch (Exception ex) {
			sError = "TMRequest: " + ex.getMessage();
		}
	}
}
