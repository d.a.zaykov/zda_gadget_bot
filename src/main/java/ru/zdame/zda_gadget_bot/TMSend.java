package ru.zdame.zda_gadget_bot;

import java.io.File;
import java.io.InputStream;
import java.util.*;

//import java.nio.charset.StandardCharsets;
//import java.net.URI;
//import java.net.http.HttpClient;
//import java.net.http.HttpRequest;
//import java.net.http.HttpResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;


// Отправка сообщения в телеграм.
// В отдельном потоке с целью укладываться в лимиты Телеграма на отправку сообщений
// https://core.telegram.org/bots/faq#my-bot-is-hitting-limits-how-do-i-avoid-this
// https://tlgrm.ru/docs/bots/faq
// 30 сообщений/сек - всего
// 1 сообщений/сек  - на один чат
// 20 сообщений/мин - в ту же группу
public class TMSend extends Thread {

	private static volatile boolean killed = false; // Признак завершения программы
	private static final int iPeriod = 200; // Пауза между обработкой сообщений (ms)
	private static final Object lock = new Object(); // Объект для синхронизации потоков
	private static final int capacitySend = 1000; // Начальная длина lstSend
	private static volatile ArrayList<TMMessage> lstSend = new ArrayList<TMMessage>(capacitySend);

	public static class TMButton {
		public String text;
		public String callback_data;

		public TMButton(String text, String callback_data) {
			this.text = text;
			this.callback_data = callback_data;
		}
	}

	public static class TMMessage {
		public int prior; // Приоритет отправки сообщений: 1..n - чем меньше, тем раньше
		public String to_chat_id;
		public String to_user_id;
		public String text;
		public File file;
		public ArrayList<ArrayList<TMButton>> tmButtons;

		public TMMessage() {
			this.prior = 1;
			this.to_chat_id = "";
			this.to_user_id = "";
			this.text = "";
			this.tmButtons = null;
		}
	}


	@Override
	public void run()
	{
		Logger.add("TMSend: started");
		try {Thread.sleep(iPeriod);} catch (Exception ex) {}

		ArrayList<TMMessage> lstTemp = new ArrayList<TMMessage>(capacitySend);
		ArrayList<TMMessage> lstTemp2 = new ArrayList<TMMessage>(capacitySend);
		// Регистрация общего лимита: 30 сообщений/сек
		Limit.reg("common", 30, 30);

		while (!killed) {
			try {
				// Копировать очередь сообщений
				int lenSend = 0; // Для мониторинга нагрузки
				synchronized (lock) {
					lenSend = lstSend.size();
					if (lenSend > 0) {
						lstTemp.addAll(lstSend);
						lstSend = new ArrayList<TMMessage>(capacitySend);
					}
				}

				// Мониторинг нагрузки
				if (lenSend > capacitySend) {
					Logger.add("TMSend.run: Warning: length=" + lenSend + ", capacityLog=" + capacitySend);
					Monitor.add("", "", "", "", "", "TM_SENDMESSAGE_WARN", "length=" + lenSend + ", capacityLog=" + capacitySend);
				}

				if (lstTemp.size() > 0) {
					// Определить минимальный приоритет сообщений в очереди
					int minPrior = 1000;
					for (TMMessage tmMessage : lstTemp)
						if (tmMessage.prior < minPrior) {
							minPrior = tmMessage.prior;
						}

					// Отправка сообщений с приоритетом minPrior
					boolean isFirst = true; // true - нашли первое сообщение с приоритетом minPrior. Это чтоб отправлять только одно сообщение с низким приоритетом за раз
					lstTemp2 = new ArrayList<TMMessage>(capacitySend); // Сюда будем перекладывать пропущенные сообщения
					for (TMMessage tmMessage : lstTemp) {
						// Пропустить сообщения с более низким приоритетом
						if (tmMessage.prior > minPrior) {
							lstTemp2.add(tmMessage);
						}
						else {
							// Пропустить сообщения с низким приоритетом, если оно уже не первое
							if (minPrior > 1 && !isFirst) {
								lstTemp2.add(tmMessage);
							}
							else
							{
								// Регистрация лимита на один чат: 1 сообщений/сек
								Limit.reg("chat_id_" + tmMessage.to_chat_id, 1, 1);
								// Проверить лимит чата
								if (!Limit.check("chat_id_" + tmMessage.to_chat_id, 1)) {
									lstTemp2.add(tmMessage);
									Monitor.add("", "", "", "", "", "TM_SENDMESSAGE_LIMITWARNING", "chat_id_" + tmMessage.to_chat_id); // Предупреждение по перелимиту
								}
								else
								{
									// Проверить общий лимит
									if (!Limit.check("common", 1)) {
										lstTemp2.add(tmMessage);
										Monitor.add("", "", "", "", "", "TM_SENDMESSAGE_LIMITWARNING", "common"); // Предупреждение по перелимиту
									}
									else
									{
										// Теперь можно отправлять сообщение
										isFirst = false;
										TM_sendMessage(tmMessage);
										Limit.add("chat_id_" + tmMessage.to_chat_id, 1);
										Limit.add("common", 1);
									}
								}
							}
						}
					}
					lstTemp = lstTemp2;
				}
			}
			catch (Exception ex)
			{
				Logger.add("TMSend.run: Error: " + ex.getMessage());
			}

			if (!killed)
				try {Thread.sleep(iPeriod);} catch (Exception ex) {}
		}
		Logger.add("TMSend: stopped");
	}



	public static void stopSend() {
		killed = true;
	}


	// Добавить сообщение в очередь на отправку
	// priority - приоритет отправки сообщения. У кого меньше приоритет, тот отправляется раньше.
	public static void send(TMMessage tmMessage)
	{
		if (tmMessage == null)
			return;
		Logger.add("TMSend.send: \n" + tmMessage.text);
		if (!tmMessage.text.isEmpty() || tmMessage.tmButtons != null) {
			// Добавить в список
			synchronized (lock) {
				lstSend.add(tmMessage);
			}
		}
	}

	// Отправить сообщение в чат
	private static boolean TM_sendMessage(TMMessage tmMessage)
	{
		tmMessage.to_chat_id = Utils.isNull(tmMessage.to_chat_id, "");
		tmMessage.to_user_id = Utils.isNull(tmMessage.to_user_id, "");
		tmMessage.text = Utils.strtrunc(tmMessage.text, 4000);

		//Logger.add("TMSend.TM_sendMessage: " + tmMessage.text);
		Monitor.add("", "", "", "", "", "TM_SENDMESSAGE_TOTAL", "");
		try {
			// Сформировать json
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode rootNode = mapper.createObjectNode();
			rootNode.put("chat_id", tmMessage.to_chat_id);
			rootNode.put("text", tmMessage.text);
			rootNode.put("disable_web_page_preview", true);
			if (tmMessage.to_user_id.length() > 0)
				rootNode.put("disable_notification", false);
			else
				rootNode.put("disable_notification", true);
			if (tmMessage.tmButtons != null) {
				ObjectNode reply_markup = rootNode.putObject("reply_markup");
				ArrayNode inline_keyboard = reply_markup.putArray("inline_keyboard");

				for (ArrayList<TMSend.TMButton> listButton: tmMessage.tmButtons) {
					ArrayNode row = mapper.createArrayNode();
					for (TMSend.TMButton tmButton : listButton) {

						ObjectNode buttonNode = mapper.createObjectNode();
						buttonNode.put("text", tmButton.text);
						buttonNode.put("callback_data", tmButton.callback_data);
						row.add(buttonNode);
					}
					inline_keyboard.add(row);
				}
			}
			String json = mapper.writeValueAsString(rootNode);
			Logger.add("TMSend.TM_sendMessage: json=" + json);

			// Отправить json
			/*HttpClient httpClient = HttpClient.newBuilder()
				.version(HttpClient.Version.HTTP_2)
				.build();
			HttpRequest request = HttpRequest.newBuilder()
				.uri(URI.create(Main.TM_URL + "sendMessage"))
				.POST(HttpRequest.BodyPublishers.ofString(json))
				.header("Content-Type", "application/json")
				.build();
			HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));

			int iStatusCode = response.statusCode();
			if (iStatusCode != 200) {
				Logger.add("TMSend.TM_sendMessage: Error: iStatusCode=" + iStatusCode + ", " + response.body());
				Monitor.add("", "", "", "", "", "TM_SENDMESSAGE_FAIL", "iStatusCode=" + iStatusCode + ", " + response.body());
				if (iStatusCode == 429)
					Monitor.add("", "", "", "", "", "TM_SENDMESSAGE_LIMITFAIL", "");
				return false;
			}*/

			HttpPost post = new HttpPost(Main.TM_URI + "sendMessage");
			HttpEntity entity = new StringEntity(json, ContentType.APPLICATION_JSON);
			post.setEntity(entity);
			CloseableHttpClient client = HttpClients.createDefault();
			CloseableHttpResponse response = client.execute(post);

			int iStatusCode = response.getStatusLine().getStatusCode();
			if (iStatusCode != 200) {
				String statusLine = response.getStatusLine().toString();
				Logger.add("TMSend.TM_sendMessage: Error: iStatusCode=" + iStatusCode + ", " + statusLine);
				Monitor.add("", "", "", "", "", "TM_SENDMESSAGE_FAIL", "iStatusCode=" + iStatusCode + ", " + statusLine);
				if (iStatusCode == 429)
					Monitor.add("", "", "", "", "", "TM_SENDMESSAGE_LIMITFAIL", "");
				return false;
			}

			Monitor.add("", "", "", "", "", "TM_SENDMESSAGE_SUCCESS", "");
			return true;
		}
		catch (Exception ex) {
			Logger.add("TMSend.TM_sendMessage: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "TM_SENDMESSAGE_FAIL", ex.getMessage());
			return false;
		}
	}

	// Отправить файл в чат
	public static boolean TM_sendDocument(TMMessage tmMessage)
	{
		tmMessage.to_chat_id = Utils.isNull(tmMessage.to_chat_id, "");

		Logger.add("TMSend.TM_sendDocument: " + tmMessage.text);
		Monitor.add("", "", "", "", "", "TM_SENDDOCUMENT_TOTAL", "");

		try {
			if (tmMessage.file == null)
			{
				Logger.add("TMSend.TM_sendDocument: tmMessage.file == null");
				Monitor.add("", "", "", "", "", "TM_SENDDOCUMENT_FAIL", "tmMessage.file == null");
				return false;
			}
			Logger.add("TMSend.TM_sendDocument: " + tmMessage.file.getName());

			/*HttpPost post = new HttpPost(Main.TM_URL + "sendDocument");
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addTextBody("json_text", json, ContentType.APPLICATION_JSON);
			builder.addBinaryBody("document", tmMessage.file, ContentType.DEFAULT_BINARY, sFileName);
			HttpEntity entity = builder.build();
			post.setEntity(entity);
			CloseableHttpClient client = HttpClients.createDefault();
			CloseableHttpResponse response = client.execute(post);*/

			HttpPost post = new HttpPost(Main.TM_URI + "sendDocument?chat_id=" + tmMessage.to_chat_id);
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addBinaryBody("document", tmMessage.file, ContentType.DEFAULT_BINARY, tmMessage.file.getName());
			HttpEntity entity = builder.build();
			post.setEntity(entity);
			CloseableHttpClient client = HttpClients.createDefault();
			CloseableHttpResponse response = client.execute(post);

			int iStatusCode = response.getStatusLine().getStatusCode();
			if (iStatusCode != 200) {
				String statusLine = response.getStatusLine().toString();
				Logger.add("TMSend.TM_sendDocument: Error: iStatusCode=" + iStatusCode + ", " + statusLine);
				Monitor.add("", "", "", "", "", "TM_SENDDOCUMENT_FAIL", "iStatusCode=" + iStatusCode + ", " + statusLine);
				if (iStatusCode == 429)
					Monitor.add("", "", "", "", "", "TM_SENDDOCUMENT_LIMITFAIL", "");
				return false;
			}

			Monitor.add("", "", "", "", "", "TM_SENDDOCUMENT_SUCCESS", "");
			return true;
		}
		catch (Exception ex) {
			Logger.add("TMSend.TM_sendDocument: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "TM_SENDDOCUMENT_FAIL", ex.getMessage());
			return false;
		}
	}

	// Получить файл
	public static String TM_getFile(TMRequest tmReq, String sDocFileId)
	{
		sDocFileId = Utils.isNull(sDocFileId, "").trim();

		Logger.add("TMSend.TM_getFile: " + sDocFileId);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "TM_GETFILE_TOTAL", "");

		try {
			if (sDocFileId.isEmpty())
			{
				Logger.add("TMSend.TM_getFile: sDocFileId.isEmpty()");
				Monitor.add("", "", "", "", "", "TM_GETFILE_FAIL", "sDocFileId.isEmpty()");
				return null;
			}

			// Получить ссылку на файл
			String json = "{\"file_id\": \"" + sDocFileId + "\"}";
			Logger.add("TMSend.TM_getFile: json=" + json);

			HttpPost post = new HttpPost(Main.TM_URI + "getFile");
			HttpEntity entity = new StringEntity(json, ContentType.APPLICATION_JSON);
			post.setEntity(entity);
			CloseableHttpClient client = HttpClients.createDefault();
			CloseableHttpResponse response = client.execute(post);

			int iStatusCode = response.getStatusLine().getStatusCode();
			if (iStatusCode != 200) {
				String statusLine = response.getStatusLine().toString();
				Logger.add("TMSend.TM_getFile: Error: POST iStatusCode=" + iStatusCode + ", " + statusLine);
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "TM_GETFILE_FAIL", "POST iStatusCode=" + iStatusCode + ", " + statusLine);
				if (iStatusCode == 429)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "TM_GETFILE_LIMITFAIL", "");
				return null;
			}

			json = "";
			HttpEntity responseEntity = response.getEntity();
			if (responseEntity != null) {
				InputStream instream = responseEntity.getContent();
				try {
					json = Utils.isNull(Utils.convertStreamToString(instream), "");
				}
				finally {
					instream.close();
				}
			}
			Logger.add("TMSend.TM_getFile: response=" + json);

			String file_path = "";
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readValue(json, JsonNode.class);
			if (rootNode != null) {
				JsonNode resultNode = rootNode.get("result");
				if (resultNode != null)
					file_path = Utils.jsonReadStr(resultNode, "file_path" ,"").trim();
			}
			Logger.add("TMSend.TM_getFile: file_path=" + file_path);


			// Скачать файл
			HttpGet get = new HttpGet(Main.TM_FILE_URI + file_path);
			CloseableHttpResponse responseGet = client.execute(get);

			iStatusCode = responseGet.getStatusLine().getStatusCode();
			String statusLine = responseGet.getStatusLine().toString();
			if (iStatusCode != 200) {
				Logger.add("TMSend.TM_getFile: Error: GET iStatusCode=" + iStatusCode + ", " + statusLine);
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "TM_GETFILE_FAIL", "GET iStatusCode=" + iStatusCode + ", " + statusLine);
				if (iStatusCode == 429)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "TM_GETFILE_LIMITFAIL", "");
				return null;
			}
			Logger.add("TMSend.TM_getFile: GET " + statusLine);

			String sDoc = "";
			responseEntity = responseGet.getEntity();
			if (responseEntity != null) {
				InputStream instream = responseEntity.getContent();
				try {
					sDoc = Utils.isNull(Utils.convertStreamToString(instream), "");
				}
				finally {
					instream.close();
				}
			}

			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "TM_GETFILE_SUCCESS", "");
			return sDoc;
		}
		catch (Exception ex) {
			Logger.add("TMSend.TM_getFile: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "TM_GETFILE_FAIL", ex.getMessage());
			return null;
		}
	}

}
