package ru.zdame.zda_gadget_bot;

import java.time.LocalDateTime;
import java.util.HashMap;

// Ожидание пользовательского ввода
public class InputWait {

	private static final Object lock = new Object(); // Объект для синхронизации потоков
	private static volatile HashMap<String, String> mapInput = new HashMap<String, String>();

	// Добавить признак ожидания ввода от пользователя
	public static void add(String chat_id, String from_id, String sText) {
		chat_id = Utils.strtrunc(chat_id, 255);
		from_id = Utils.strtrunc(from_id, 255);
		sText = Utils.strtrunc(sText, 255);
		String key = chat_id + "+" + from_id;

		try {
			synchronized (lock) {
				if (mapInput == null)
					mapInput = new HashMap<String, String>();
				mapInput.put(key, sText);
			}
		}
		catch (Exception ex) {}
	}

	// Проверить признак ожидания ввода от пользователя
	public static String check(String chat_id, String from_id) {
		chat_id = Utils.strtrunc(chat_id, 255);
		from_id = Utils.strtrunc(from_id, 255);
		String key = chat_id + "+" + from_id;
		String sText = "";

		try {
			synchronized (lock) {
				if (mapInput == null)
					mapInput = new HashMap<String, String>();
				if (mapInput.containsKey(key))
					sText = mapInput.get(key);
			}
		}
		catch (Exception ex) {}
		return sText;
	}

}