package ru.zdame.zda_gadget_bot;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;

import java.net.InetSocketAddress;
import java.net.InetAddress;

public class Web {

	private static volatile boolean killed = false; // Признак завершения программы
	private static volatile Server server = null;

	public static void startWeb(String host, int port) {
		try {
			server = null;
			if (host.isEmpty()){
				Logger.add("Web: starting on 0.0.0.0:" + port);
				server = new Server(port);
			}
			else
			{
				Logger.add("Web: starting on " + host + ":" + port);
				InetSocketAddress addr = new InetSocketAddress(InetAddress.getByName(host), port);
				server = new Server(addr);
			}

			ServletContextHandler context = new ServletContextHandler();
			context.setContextPath("/");
			context.addServlet(WebServlet.class, "/*");

			HandlerList handlers = new HandlerList();
			handlers.setHandlers(new Handler[] { context });
			server.setHandler(handlers);

			server.start();

			Logger.add("Web: started");
			while (true) {
				if (Web.killed) {
					try { server.stop(); } catch (Exception ex) {}
					break;
				}
				else {
					try {Thread.sleep(100);} catch (Exception ex) {}
				}
			}
			Logger.add("Web: stopped");
		}
		catch (Exception ex) {
			Logger.add("Web.startWeb: Error: " + ex.getMessage());
		}
	}

	public static void stopWeb() {
		Web.killed = true;
		try {
			server.stop();
		}
		catch (Exception ex) {}
	}

}