package ru.zdame.zda_gadget_bot;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

// Мониторинг
public class Monitor extends Thread {

	private static volatile boolean killed = false; // Признак завершения программы
	private static final int iPeriod = 11; // Период сброса метрик в базу (сек)
	private static final Object lock = new Object(); // Объект для синхронизации потоков
	private static final int capacityMonitor = 1000; // Начальная длина mapMonitor
	private static volatile HashMap<String, MonitorRow> mapMonitor = new HashMap<String, MonitorRow>(capacityMonitor);

	// Класс для одной записи таблицы monitor
	public static class MonitorRow {
		public String chat_id;
		public String chat_name;
		public String from_id;
		public String from_name;
		public String from_user;
		public String name;
		public String description;
		public int count;
		public java.time.LocalDateTime start_date;	// TZ сервера
		public java.time.LocalDateTime end_date;	// TZ сервера
	}


	@Override
	public void run()
	{
		Logger.add("Monitor: started");
		try {Thread.sleep(iPeriod*1000);} catch (Exception ex) {}

		HashMap<String, MonitorRow> mapTemp = new HashMap<String, MonitorRow>(capacityMonitor);
		while (!killed) {
			String sError = "";
			try {
				// Копировать список метрик
				int lenMonitor = 0; // Для мониторинга нагрузки на мониторинг
				synchronized (lock) {
					lenMonitor = mapMonitor.size();
					// Проверить можем ли мы еще держать незаписанные метрики в памяти. 10000 записей
					if (mapTemp.size() < 10000)
						mapTemp.putAll(mapMonitor);
					mapMonitor = new HashMap<String, MonitorRow>(capacityMonitor);
				}

				// Пытаемся записать в базу что сумеем
				HashMap<String, MonitorRow> mapTemp2 = new HashMap<String, MonitorRow>(capacityMonitor); // для неуспешнозаписанных метрик
				if (mapTemp.size() > 0) {
					Set<Map.Entry<String, MonitorRow>> set = mapTemp.entrySet();
					for (Map.Entry<String, MonitorRow> me : set) {
						MonitorRow row = me.getValue();
						if (!DB.monitor_insert(row))
							mapTemp2.put(me.getKey(), row);
					}
				}
				mapTemp = mapTemp2;

				// Мониторинг нагрузки на мониторинг
				if (lenMonitor > capacityMonitor) {
					Logger.add("Monitor.run: Warning: length=" + lenMonitor + ", capacityLog=" + capacityMonitor);
					Monitor.add("", "", "", "", "", "MONITOR_WARN", "length=" + lenMonitor + ", capacityLog=" + capacityMonitor);
				}
			}
			catch (Exception ex)
			{
				Logger.add("Monitor.run: Error: " + ex.getMessage());
			}

			if (!killed)
				try {Thread.sleep(iPeriod*1000);} catch (Exception ex) {}
		}
		Logger.add("Monitor: stopped");
	}

	public static void stopMonitor() {
		Monitor.killed = true;
		flush();
	}

	// Сброс логов на диск
	public static void flush() {
		Logger.add("Monitor.flush");

		try {
			// Копировать список метрик
			HashMap<String, MonitorRow> mapTemp = null;
			synchronized (lock) {
				mapTemp = mapMonitor;
				mapMonitor = new HashMap<String, MonitorRow>(capacityMonitor);
			}

			// Пытаемся записать в базу что сумеем
			if (mapTemp.size() > 0) {
				Set<Map.Entry<String, MonitorRow>> set = mapTemp.entrySet();
				for (Map.Entry<String, MonitorRow> me : set) {
					MonitorRow row = me.getValue();
					DB.monitor_insert(row);
				}
			}
		}
		catch (Exception ex)
		{
			Logger.add("Monitor.flush: Error: " + ex.getMessage());
		}
	}

	// Добавить метрику в список
	public static void add(String chat_id, String chat_name, String from_id, String from_user, String from_name, String name, String description) {
		name = Utils.strtrunc(Utils.isNull(name, "").trim(), 255);
		description = Utils.strtrunc(Utils.isNull(description, "").trim(), 4000);
		String key = chat_id + "+" + from_id + "+" + name + "+" + description;

		// Добавить в список метрик
		synchronized (lock) {
			// Найти такую же метрики и увеличить iCount
			int count = 0;
			if (mapMonitor.containsKey(key)) {
				MonitorRow row = mapMonitor.get(key);
				row.count += 1;
				row.end_date = LocalDateTime.now(); // TZ сервера
				mapMonitor.replace(key, row);
			}
			else {
				MonitorRow row = new MonitorRow();
				row.chat_id = chat_id;
				row.chat_name = chat_name;
				row.from_id = from_id;
				row.from_user = from_user;
				row.from_name = from_name;
				row.name = name;
				row.description = description;
				row.count = 1;
				row.start_date = LocalDateTime.now(); // TZ сервера
				row.end_date = LocalDateTime.now(); // TZ сервера
				mapMonitor.put(key, row);
			}
		}
	}

}