package ru.zdame.zda_gadget_bot;

//import com.mysql.cj.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.ArrayList;

public class DB {

	//private static volatile boolean killed = false; // Признак завершения программы
	private static final Object lock = new Object(); // Объект для синхронизации потоков
	private static volatile String db_host = ""; // Хост базы mysql
	private static volatile String db_name = ""; // Имя базы
	private static volatile String db_user = ""; // Логин
	private static volatile String db_pass = ""; // Пароль
	private static volatile Connection connection = null; // Коннект к базе
	private static volatile Date dtConnection = null; // Время последнего успешного подключения к базе
	private static final long longtimeDB = 1000; // Время на исполнение запроса в БД (мс)

	// Инициализация
	public static boolean start(String db_host, String db_name, String db_user, String db_pass)
	{
		DB.db_host = Utils.isNull(db_host, "");
		DB.db_name = Utils.isNull(db_name, "");
		DB.db_user = Utils.isNull(db_user, "");
		DB.db_pass = Utils.isNull(db_pass, "");

		// Проверить доступность драйвера
		Logger.add("DB.start: Проверка драйвера mysql");
		try{
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
		}
		catch (Exception ex){
			Logger.add("DB.start: Error: Драйвер mysql отсутствует: " + ex.getMessage());
			return false;
		}

		// Подключение к БД
		Logger.add("DB.start: Подключение к БД");
		if (!connect())
			return false;

		// Обновление структуры БД
		if (!update())
			return false;

		return true;
	}

	// Инициализация
	/*public static void stop()
	{
		killed = true;
		try {
			synchronized (lock) {
				// Закрытие старого подключения
				try {
					if (connection != null)
						connection.close();
				}
				catch (Exception ex2) {}
			}
		}
		catch (Exception ex) {}
	}*/

	private static boolean connect()
	{
		Logger.add("DB.connect");
		Monitor.add("", "", "", "", "", "DB_CONNECT_TOTAL", "");

		if (db_host.isEmpty())
			return false;

		String sVersion = "";
		String sError = "";
		try {
			synchronized (lock) {
				// Закрытие старого подключения
				try {
					if (connection != null)
						connection.close();
				}
				catch (Exception ex2) {}

				// Подключение
				try {
					connection = DriverManager.getConnection("jdbc:mysql://" + db_host + "/" + db_name, db_user, db_pass);
				}
				catch (Exception ex2)
				{
					sError = ex2.getMessage();
				}

				// Получение версии
				try {
					Statement statement = connection.createStatement();
					ResultSet resultSet = statement.executeQuery("select version()");
					while (resultSet.next()) {
						sVersion = resultSet.getString(1);
						break;
					}
					if (!sVersion.isEmpty())
						dtConnection = new Date();
				}
				catch (Exception ex2)
				{
					sVersion = "";
					sError = ex2.getMessage();
				}
			}

			if (!sError.isEmpty())
			{
				Logger.add("DB.connect: Error: " + sError);
				Monitor.add("", "", "", "", "", "DB_CONNECT_FAIL", sError);
				return false;
			}

			if (sVersion.isEmpty())
			{
				Logger.add("DB.connect: Error: version is empty");
				Monitor.add("", "", "", "", "", "DB_CONNECT_FAIL", "version is empty");
				return false;
			}
			else
			{
				Logger.add("DB.connect: version=" + sVersion);
				Monitor.add("", "", "", "", "", "DB_CONNECT_SUCCESS", "");
				return true;
			}
		}
		catch (Exception ex)
		{
			Logger.add("DB.connect: Error: " + ex.getMessage());
			return false;
		}
	}


	// Проверка соединения и переподключение
	private static boolean check()
	{
		//Logger.add("DB.check");

		if (db_host.isEmpty())
			return false;

		try {
			// Еще не было ни одного соединения
			if (connection == null) {
				if (!connect()) {
					try {Thread.sleep(2000);} catch (Exception ex2) {}
					if (!connect()) {
						Logger.add("DB.check: Не удалось подключиться к базе 1");
						Monitor.add("", "", "", "", "", "DB_CONNECT_CHECK_FAIL", "1");
						return false;
					}
					else {
						return true;
					}
				}
				else {
					return true;
				}
			}

			// Проверяем наличие соединения
			boolean isConnected = false;

			// Будем просто смотреть на вермя последнего подключения и переподключаться
			if (dtConnection != null) {
				Date dtCurr = new Date();
				long delta = dtCurr.getTime() - dtConnection.getTime();
				// Каждые 60 сек должны переподключаться
				if (delta <= 60000)
					isConnected = true;
			}

			if (!isConnected) {
				if (!connect()) {
					try {Thread.sleep(2000);} catch (Exception ex2) {}
					if (!connect()) {
						Logger.add("DB.check: Не удалось подключиться к базе 2");
						Monitor.add("", "", "", "", "", "DB_CONNECT_CHECK_FAIL", "2");
						return false;
					}
					else {
						return true;
					}
				}
				else {
					return true;
				}
			}

			return true;
		}
		catch (Exception ex) {
			Logger.add("DB.check: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "DB_CONNECT_CHECK_FAIL",  ex.getMessage());
			return false;
		}
	}


	// Проверка работы БД путем получения версии БД
	public static boolean hc()
	{
		Logger.add("DB.hc");
		Monitor.add("", "", "", "", "", "DB_HC_TOTAL", "");

		if (!check())
			return false;

		String sVersion = "";
		String sError = "";
		try {
			synchronized (lock) {
				// Получение версии
				try {
					Statement statement = connection.createStatement();
					ResultSet resultSet = statement.executeQuery("select version()");
					while (resultSet.next()) {
						sVersion = resultSet.getString(1);
						break;
					}
				}
				catch (Exception ex2)
				{
					sVersion = "";
					sError = ex2.getMessage();
				}
			}

			if (!sError.isEmpty())
			{
				Logger.add("DB.hc: Error: " + sError);
				Monitor.add("", "", "", "", "", "DB_HC_FAIL", sError);
				return false;
			}

			if (sVersion.isEmpty())
			{
				Logger.add("DB.hc: Error: version is empty");
				Monitor.add("", "", "", "", "", "DB_HC_FAIL", "version is empty");
				return false;
			}
			else
			{
				Logger.add("DB.hc: version=" + sVersion);
				Monitor.add("", "", "", "", "", "DB_HC_SUCCESS", "");
				return true;
			}
		}
		catch (Exception ex)
		{
			Logger.add("DB.hc: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "DB_HC_FAIL", ex.getMessage());
			return false;
		}
	}


	// Обновить структура данных в базе
	private static boolean update()
	{
		Logger.add("DB.update");

		if (!check())
			return false;

		String sError = "";
		try {
			synchronized (lock) {
				try {
					Statement statement = connection.createStatement();
					String sql = "";

					// Таблица vcontact - Список ВКС
					Logger.add("DB.update: create table vcontact");
					sql =
						"CREATE TABLE IF NOT EXISTS vcontact ( \n" +
						"	id bigint(20) unsigned NOT NULL AUTO_INCREMENT, \n" +
						"	chat_id varchar(255) NOT NULL, \n" +		// Автор записи
						"	chat_name varchar(255) NOT NULL, \n" +		// ...
						"	from_id varchar(255) NOT NULL, \n" +		// ...
						"	from_name varchar(255) NOT NULL, \n" +		// ...
						"	from_user varchar(255) NOT NULL, \n" +		// ...
						"	sort varchar(255) NOT NULL, \n" +			// Строка для сортировки записей при выводе списка
						"	name varchar(255) NOT NULL, \n" +			// Краткое название. Отображается на кнопке
						"	description varchar(4000) NOT NULL, \n" +	// Описание. Например, ссылка на ВКС.
						"	create_date DATETIME NOT NULL, \n" +		// Время создания записи в UTC
						"	PRIMARY KEY (id) \n" +
						")";
					statement.executeUpdate(sql);
					// Индексы
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_id ON vcontact (chat_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_name ON vcontact (chat_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_id ON vcontact (from_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_name ON vcontact (from_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_user ON vcontact (from_user)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS sort ON vcontact (sort)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date ON vcontact (create_date)");

					// Таблица buy - Список покупок
					Logger.add("DB.update: create table buy");
					sql =
						"CREATE TABLE IF NOT EXISTS buy ( \n" +
						"	id bigint(20) unsigned NOT NULL AUTO_INCREMENT, \n" +
						"	chat_id varchar(255) NOT NULL, \n" +		// Автор записи
						"	chat_name varchar(255) NOT NULL, \n" +		// ...
						"	from_id varchar(255) NOT NULL, \n" +		// ...
						"	from_name varchar(255) NOT NULL, \n" +		// ...
						"	from_user varchar(255) NOT NULL, \n" +		// ...
						"	sort varchar(255) NOT NULL, \n" +			// Строка для сортировки записей при выводе списка
						"	name varchar(255) NOT NULL, \n" +			// Краткое название. Отображается на кнопке
						"	description varchar(4000) NOT NULL, \n" +	// Описание
						"	create_date DATETIME NOT NULL, \n" +		// Время создания записи в UTC
						"	PRIMARY KEY (id) \n" +
						")";
					statement.executeUpdate(sql);
					// Индексы
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_id ON buy (chat_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_name ON buy (chat_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_id ON buy (from_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_name ON buy (from_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_user ON buy (from_user)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS sort ON buy (sort)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date ON buy (create_date)");

					// Таблица sport - Тренировки
					Logger.add("DB.update: create table sport");
					sql =
						"CREATE TABLE IF NOT EXISTS sport ( \n" +
						"	id bigint(20) unsigned NOT NULL AUTO_INCREMENT, \n" +
						"	chat_id varchar(255) NOT NULL, \n" +		// Автор записи
						"	chat_name varchar(255) NOT NULL, \n" +		// ...
						"	from_id varchar(255) NOT NULL, \n" +		// ...
						"	from_name varchar(255) NOT NULL, \n" +		// ...
						"	from_user varchar(255) NOT NULL, \n" +		// ...
						"	name varchar(255) NOT NULL, \n" +			// Название вида спорта: RUN, SKY, WALK, BIKE, SWIM, OTHER
						"	description varchar(4000) NOT NULL, \n" +	// Описание. Зарезервировано.
						"	count DECIMAL(10, 1) NOT NULL, \n" +		// Объем выполненной тренировки в исходной ЕИ
						"	koeff DECIMAL(10, 1) NOT NULL, \n" +		// Коэффициент для пересчета в учетную ЕИ
						"	km DECIMAL(10, 1) NOT NULL, \n" +			// Объем выполненной тренировки в учетной ЕИ
						"	create_date DATETIME NOT NULL, \n" +		// Время создания записи в UTC
						"	gmt TINYINT NOT NULL, \n" +					// Deprecated. Учетный часовой пояс
						"	create_date_local DATETIME NOT NULL, \n" +	// Deprecated. Время создания записи в учетном часовом поясе
						"	PRIMARY KEY (id) \n" +
						")";
					statement.executeUpdate(sql);
					// Индексы
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_id ON sport (chat_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_name ON sport (chat_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_id ON sport (from_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_name ON sport (from_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_user ON sport (from_user)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date ON sport (create_date)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date_local ON sport (create_date_local)");

					// Таблица feedback - Обратная связь
					Logger.add("DB.update: create table feedback");
					sql =
						"CREATE TABLE IF NOT EXISTS feedback ( \n" +
						"	id bigint(20) unsigned NOT NULL AUTO_INCREMENT, \n" +
						"	chat_id varchar(255) NOT NULL, \n" +		// Автор записи
						"	chat_name varchar(255) NOT NULL, \n" +		// ...
						"	from_id varchar(255) NOT NULL, \n" +		// ...
						"	from_name varchar(255) NOT NULL, \n" +		// ...
						"	from_user varchar(255) NOT NULL, \n" +		// ...
						"	message varchar(4000) NOT NULL, \n" +		// Текст сообщения
						"	folder varchar(255) NOT NULL, \n" +			// Условная папка сообщения: IN, OUT
						"	status varchar(255) NOT NULL, \n" +			// Статус сообщения: "" - отправлено, SENT - доставлено
						"	reply_to_id bigint(20) unsigned, \n" +		// Ответ на сообщение с id
						"	create_date DATETIME NOT NULL, \n" +		// Время создания записи в UTC
						"	gmt TINYINT NOT NULL, \n" +					// Deprecated. Учетный часовой пояс
						"	create_date_local DATETIME NOT NULL, \n" +	// Deprecated. Время создания записи в учетном часовом поясе
						"	PRIMARY KEY (id) \n" +
						")";
					statement.executeUpdate(sql);
					// Индексы
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_id ON feedback (chat_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_name ON feedback (chat_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_id ON feedback (from_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_name ON feedback (from_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_user ON feedback (from_user)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS folder ON feedback (folder)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS status ON feedback (status)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS reply_to_id ON feedback (reply_to_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date ON feedback (create_date)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date_local ON feedback (create_date_local)");

					// Таблица event - Мероприятие
					Logger.add("DB.update: create table event");
					sql =
						"CREATE TABLE IF NOT EXISTS event ( \n" +
						"	id bigint(20) unsigned NOT NULL AUTO_INCREMENT, \n" +
						"	chat_id varchar(255) NOT NULL, \n" +		// Автор записи
						"	chat_name varchar(255) NOT NULL, \n" +		// ...
						"	from_id varchar(255) NOT NULL, \n" +		// ...
						"	from_name varchar(255) NOT NULL, \n" +		// ...
						"	from_user varchar(255) NOT NULL, \n" +		// ...
						"	event varchar(255) NOT NULL, \n" +			// Обозначение мероприятия
						"	title varchar(255) NOT NULL, \n" +			// Заголовок мероприятия. Отображается на кнопке.
						"	description varchar(4000) NOT NULL, \n" +	// Описание мероприятия
						"	status varchar(255) NOT NULL, \n" +			// Статус мероприятия: "" - Закрыто, open - открыто, open_req - открыта регистрация
						"	create_date DATETIME NOT NULL, \n" +		// Время создания записи в UTC
						"	gmt TINYINT NOT NULL, \n" +					// Deprecated. Учетный часовой пояс
						"	create_date_local DATETIME NOT NULL, \n" +	// Deprecated. Время создания записи в учетном часовом поясе
						"	PRIMARY KEY (id) \n" +
						")";
					statement.executeUpdate(sql);
					// Индексы
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_id ON event (chat_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_name ON event (chat_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_id ON event (from_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_name ON event (from_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_user ON event (from_user)");
					statement.executeUpdate("CREATE UNIQUE INDEX IF NOT EXISTS event ON event (event)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS status ON event (status)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date ON event (create_date)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date_local ON event (create_date_local)");

					// Таблица event_request - Заявка на мероприятие
					Logger.add("DB.update: create table event_request");
					sql =
						"CREATE TABLE IF NOT EXISTS event_request ( \n" +
						"	id bigint(20) unsigned NOT NULL AUTO_INCREMENT, \n" +
						"	chat_id varchar(255) NOT NULL, \n" +		// Автор записи
						"	chat_name varchar(255) NOT NULL, \n" +		// ...
						"	from_id varchar(255) NOT NULL, \n" +		// ...
						"	from_name varchar(255) NOT NULL, \n" +		// ...
						"	from_user varchar(255) NOT NULL, \n" +		// ...
						"	event varchar(255) NOT NULL, \n" +			// Обозначение мероприятия
						"	title varchar(255) NOT NULL, \n" +			// Ключ (см. инструкцию организтора)
						"	description varchar(4000) NOT NULL, \n" +	// Значение (см. инструкцию организтора)
						"	status varchar(255) NOT NULL, \n" +			// Статус заявки: "" - Отправлена, accepted - принята, rejected - отклонена, canceled - отменена участником, result - результат
						"	feedback varchar(4000) NOT NULL, \n" +		// Сообщение участнику
						"	create_date DATETIME NOT NULL, \n" +		// Время создания записи в UTC
						"	gmt TINYINT NOT NULL, \n" +					// Deprecated. Учетный часовой пояс
						"	create_date_local DATETIME NOT NULL, \n" +	// Deprecated. Время создания записи в учетном часовом поясе
						"	PRIMARY KEY (id) \n" +
						")";
					statement.executeUpdate(sql);
					// Индексы
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_id ON event_request (chat_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_name ON event_request (chat_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_id ON event_request (from_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_name ON event_request (from_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_user ON event_request (from_user)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS event ON event_request (event)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS status ON event_request (status)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date ON event_request (create_date)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date_local ON event_request (create_date_local)");


					// Таблица monitor - Мониторинг
					Logger.add("DB.update: create table monitor");
					sql =
						"CREATE TABLE IF NOT EXISTS monitor ( \n" +
						"	id bigint(20) unsigned NOT NULL AUTO_INCREMENT, \n" +
						"	chat_id varchar(255) NOT NULL, \n" +		// Автор записи
						"	chat_name varchar(255) NOT NULL, \n" +		// ...
						"	from_id varchar(255) NOT NULL, \n" +		// ...
						"	from_name varchar(255) NOT NULL, \n" +		// ...
						"	from_user varchar(255) NOT NULL, \n" +		// ...
						"	name varchar(255) NOT NULL, \n" +			// Обозначение метрики
						"	description varchar(4000) NOT NULL, \n" +	// Описание. Например, можно писать сюда тексты ошибок и прочую дполнительную информацию. Т.о. Мониторинг совмежается с Журналированием.
						"	count SMALLINT UNSIGNED NOT NULL, \n" +		// Количество отброшенных метрик за отведенный интервал start_date - end_date
						"	start_date DATETIME NOT NULL, \n" +			// Время открытия интервала в UTC
						"	end_date DATETIME NOT NULL, \n" +			// Время закрытия интервала в UTC
						"	create_date DATETIME NOT NULL, \n" +		// Время создания записи в UTC
						"	PRIMARY KEY (id) \n" +
						")";
					statement.executeUpdate(sql);
					// Индексы
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_id ON monitor (chat_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS chat_name ON monitor (chat_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_id ON monitor (from_id)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_name ON monitor (from_name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS from_user ON monitor (from_user)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS name ON monitor (name)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS start_date ON monitor (start_date)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS end_date ON monitor (end_date)");
					statement.executeUpdate("CREATE INDEX IF NOT EXISTS create_date ON monitor (create_date)");
				}
				catch (Exception ex2)
				{
					sError = ex2.getMessage();
				}
			}

			if (!sError.isEmpty())
			{
				Logger.add("DB.update: Error: " + sError);
				return false;
			}

			Logger.add("DB.update: ok");
			return true;
		}
		catch (Exception ex)
		{
			Logger.add("DB.update: Error: " + ex.getMessage());
			return false;
		}
	}


	// Мониторинг
	// Добавить записи
	public static boolean monitor_insert(Monitor.MonitorRow monitorRow)
	{
		//Logger.add("DB.monitor_insert");

		if (!check())
			return false;

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"INSERT INTO monitor (chat_id, chat_name, from_id, from_name, from_user, name, description, count, start_date, end_date, create_date) \n" +
								"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, monitorRow.chat_id);
						statement.setString(2, monitorRow.chat_name);
						statement.setString(3, monitorRow.from_id);
						statement.setString(4, monitorRow.from_name);
						statement.setString(5, monitorRow.from_user);
						statement.setString(6, monitorRow.name);
						statement.setString(7, monitorRow.description);
						statement.setInt(8, monitorRow.count);
						statement.setString(9, Utils.date2UTC(monitorRow.start_date, Main.tzServer)); // Перевод серверного времени в UTC
						statement.setString(10, Utils.date2UTC(monitorRow.end_date, Main.tzServer)); // Перевод серверного времени в UTC
						statement.setString(11, Utils.date2UTC(LocalDateTime.now(), Main.tzServer)); // Перевод серверного времени в UTC

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					} catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.monitor_insert: Error: INSERT: " + sError);
					Monitor.add("", "", "", "", "", "DB_MONITOR_INSERT_FAIL", "INSERT: " + sError);
				}

				if (ticker > longtimeDB)
					Monitor.add("", "", "", "", "", "DB_MONITOR_INSERT_LONGTIME", "" + ticker);
			}
			catch(Exception ex2)
			{
				Logger.add("DB.monitor_insert: Error: 2: " + ex2.getMessage());
				Monitor.add("", "", "", "", "", "DB_MONITOR_INSERT_FAIL", "2: " + ex2.getMessage());
			}

			return true;
		}
		catch (Exception ex) {
			Logger.add("DB.monitor_insert: Error: 3: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "DB_MONITOR_INSERT_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}


	// ВКС
	// Добавить запись
	public static boolean vcontact_insert(TMRequest tmReq, String sText)
	{
		sText = Utils.isNull(sText, "").trim();
		Split sp = new Split(sText, '.', "");
		String sName = Utils.strtrunc(Utils.isNull(sp.s1, "").trim(), 255);
		String sDescription = Utils.strtrunc(Utils.isNull(sp.s2, "").trim(), 4000);
		String sSort = sName.toLowerCase();

		Logger.add("DB.vcontact_insert: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", sName=" + sName +
			", sDescription=" + sDescription);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_INSERT_TOTAL", "");

		if (!check()) {
			Logger.add("DB.vcontact_insert: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_INSERT_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"INSERT INTO vcontact (chat_id, chat_name, from_id, from_name, from_user, sort, name, description, create_date) \n" +
							"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, tmReq.chat_id);
						statement.setString(2, tmReq.chat_name);
						statement.setString(3, tmReq.from_id);
						statement.setString(4, tmReq.from_name);
						statement.setString(5, tmReq.from_username);
						statement.setString(6, sSort);
						statement.setString(7, sName);
						statement.setString(8, sDescription);
						statement.setString(9, Utils.date2UTC(LocalDateTime.now(), Main.tzServer)); // Перевод серверного времени в UTC

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.vcontact_insert: Error: INSERT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_INSERT_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_INSERT_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_INSERT_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.vcontact_insert: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_INSERT_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.vcontact_insert: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_INSERT_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

	// ВКС
	// Класс для возрата результатов
	public static class vcontact_row {
		public int id;
		public String chat_id;
		public String from_name;
		public String name;
		public String description;

		public vcontact_row() {
			this.id = 0;
			this.chat_id = "";
			this.from_name = "";
			this.name = "";
			this.description = "";
		}
	}

	// ВКС
	// Выбрать по chat_id
	public static ArrayList<vcontact_row> vcontact_selectByChatId(TMRequest tmReq, String chat_id)
	{
		Logger.add("DB.vcontact_selectByChatId: chat_id=" + chat_id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTBYCHATID_TOTAL", "");

		if (!check()) {
			Logger.add("DB.vcontact_selectByChatId: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTBYCHATID_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				ArrayList<vcontact_row> rows = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id, from_name, name, description \n" +
							"FROM vcontact \n" +
							"WHERE chat_id = ? \n" +
							"ORDER BY sort";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						rows = new ArrayList<vcontact_row>();
						while (resultSet.next()) {
							vcontact_row row = new vcontact_row();
							row.id = resultSet.getInt(1);
							row.chat_id = chat_id;
							row.from_name = resultSet.getString(2);
							row.name = resultSet.getString(3);
							row.description = resultSet.getString(4);
							rows.add(row);
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.vcontact_selectByChatId: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTBYCHATID_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTBYCHATID_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTBYCHATID_SUCCESS", "");
				return rows;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.vcontact_selectByChatId: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTBYCHATID_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.vcontact_selectByChatId: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTBYCHATID_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// ВКС
	// Выбрать по chat_id, id
	public static vcontact_row vcontact_selectOne(TMRequest tmReq, String chat_id, int id)
	{
		Logger.add("DB.vcontact_selectOne: chat_id=" + chat_id + ", id=" + id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTONE_TOTAL", "");

		if (!check()) {
			Logger.add("DB.vcontact_selectOne: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTONE_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				vcontact_row row = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id, from_name, name, description \n" +
								"FROM vcontact \n" +
								"WHERE id = ? and chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setInt(1, id);
						statement.setString(2, chat_id);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						while (resultSet.next()) {
							row = new vcontact_row();
							row.id = resultSet.getInt(1);
							row.chat_id = chat_id;
							row.from_name = resultSet.getString(2);
							row.name = resultSet.getString(3);
							row.description = resultSet.getString(4);
							break;
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.vcontact_selectOne: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTONE_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTONE_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTONE_SUCCESS", "");
				return row;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.vcontact_selectOne: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTONE_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.vcontact_selectOne: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_SELECTONE_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// ВКС
	// Удалить запись
	public static boolean vcontact_delete(TMRequest tmReq, String chat_id, int id)
	{
		Logger.add("DB.vcontact_delete: chat_id=" + chat_id + ", id=" + id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETE_TOTAL", "");

		if (!check()) {
			Logger.add("DB.vcontact_delete: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETE_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"DELETE FROM vcontact \n" +
							"WHERE id = ? and chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setInt(1, id);
						statement.setString(2, chat_id);

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.vcontact_delete: Error: DELETE: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETE_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETE_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETE_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.vcontact_delete: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETE_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.vcontact_delete: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETE_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}


	// ВКС
	// Удалить все записи пользователя
	public static boolean vcontact_deleteAll(TMRequest tmReq, String chat_id)
	{
		Logger.add("DB.vcontact_deleteAll: chat_id=" + chat_id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETEALL_TOTAL", "");

		if (!check()) {
			Logger.add("DB.vcontact_deleteAll: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETEALL_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"DELETE FROM vcontact \n" +
								"WHERE chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.vcontact_deleteAll: Error: DELETE: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETEALL_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETEALL_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETEALL_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.vcontact_deleteAll: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETEALL_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.vcontact_deleteAll: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_VCONTACT_DELETEALL_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}


	// Покупки
	// Добавить запись
	public static boolean buy_insert(TMRequest tmReq, String sText)
	{
		sText = Utils.isNull(sText, "").trim();
		Split sp = new Split(sText, '.', "");
		String sName = Utils.strtrunc(Utils.isNull(sp.s1, "").trim(), 255);
		String sDescription = Utils.strtrunc(Utils.isNull(sp.s2, "").trim(), 4000);
		String sSort = (tmReq.from_name.toLowerCase()  + "+" + sName.toLowerCase()).trim();
		sSort = Utils.strtrunc(sSort, 255);

		Logger.add("DB.buy_insert: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", sName=" + sName +
			", sDescription=" + sDescription);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_INSERT_TOTAL", "");

		if (!check()) {
			Logger.add("DB.buy_insert: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_INSERT_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"INSERT INTO buy (chat_id, chat_name, from_id, from_name, from_user, sort, name, description, create_date) \n" +
								"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, tmReq.chat_id);
						statement.setString(2, tmReq.chat_name);
						statement.setString(3, tmReq.from_id);
						statement.setString(4, tmReq.from_name);
						statement.setString(5, tmReq.from_username);
						statement.setString(6, sSort);
						statement.setString(7, sName);
						statement.setString(8, sDescription);
						statement.setString(9, Utils.date2UTC(LocalDateTime.now(), Main.tzServer)); // Перевод серверного времени в UTC

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.buy_insert: Error: INSERT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_INSERT_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_INSERT_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_INSERT_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.buy_insert: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_INSERT_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.buy_insert: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_INSERT_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

	// Покупки
	// Класс для возрата результатов
	public static class buy_row {
		public int id;
		public String chat_id;
		public String from_name;
		public String name;
		public String description;

		public buy_row() {
			this.id = 0;
			this.chat_id = "";
			this.from_name = "";
			this.name = "";
			this.description = "";
		}
	}

	// Покупки
	// Выбрать по chat_id
	public static ArrayList<buy_row> buy_selectByChatId(TMRequest tmReq, String chat_id)
	{
		Logger.add("DB.buy_selectByChatId: chat_id=" + chat_id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTBYCHATID_TOTAL", "");

		if (!check()) {
			Logger.add("DB.buy_selectByChatId: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTBYCHATID_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				ArrayList<buy_row> rows = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id, from_name, name, description \n" +
								"FROM buy \n" +
								"WHERE chat_id = ? \n" +
								"ORDER BY sort";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						rows = new ArrayList<buy_row>();
						while (resultSet.next()) {
							buy_row row = new buy_row();
							row.id = resultSet.getInt(1);
							row.chat_id = chat_id;
							row.from_name = resultSet.getString(2);
							row.name = resultSet.getString(3);
							row.description = resultSet.getString(4);
							rows.add(row);
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.buy_selectByChatId: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTBYCHATID_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTBYCHATID_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTBYCHATID_SUCCESS", "");
				return rows;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.buy_selectByChatId: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTBYCHATID_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.buy_selectByChatId: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTBYCHATID_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// Покупки
	// Выбрать по chat_id, id
	public static buy_row buy_selectOne(TMRequest tmReq, String chat_id, int id)
	{
		Logger.add("DB.buy_selectOne: chat_id=" + chat_id + ", id=" + id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTONE_TOTAL", "");

		if (!check()) {
			Logger.add("DB.buy_selectOne: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTONE_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				buy_row rowBuy = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id, from_name, name, description \n" +
							"FROM buy \n" +
							"WHERE id = ? and chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setInt(1, id);
						statement.setString(2, chat_id);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						while (resultSet.next()) {
							rowBuy = new buy_row();
							rowBuy.id = resultSet.getInt(1);
							rowBuy.chat_id = chat_id;
							rowBuy.from_name = resultSet.getString(2);
							rowBuy.name = resultSet.getString(3);
							rowBuy.description = resultSet.getString(4);
							break;
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.buy_selectOne: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTONE_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTONE_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTONE_SUCCESS", "");
				return rowBuy;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.buy_selectOne: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTONE_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.buy_selectOne: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_SELECTONE_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// Покупки
	// Удалить запись
	public static boolean buy_delete(TMRequest tmReq, String chat_id, int id)
	{
		Logger.add("DB.buy_delete: chat_id=" + chat_id + ", id=" + id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETE_TOTAL", "");

		if (!check()) {
			Logger.add("DB.buy_delete: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETE_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"DELETE FROM buy \n" +
								"WHERE id = ? and chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setInt(1, id);
						statement.setString(2, chat_id);

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.buy_delete: Error: DELETE: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETE_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETE_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETE_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.buy_delete: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETE_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.buy_delete: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETE_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

	// Покупки
	// Удалить все записи пользователя
	public static boolean buy_deleteAll(TMRequest tmReq, String chat_id)
	{
		Logger.add("DB.buy_deleteAll: chat_id=" + chat_id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETEALL_TOTAL", "");

		if (!check()) {
			Logger.add("DB.buy_deleteAll: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETEALL_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"DELETE FROM buy \n" +
								"WHERE chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.buy_deleteAll: Error: DELETE: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETEALL_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETEALL_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETEALL_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.buy_deleteAll: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETEALL_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.buy_deleteAll: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_BUY_DELETEALL_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

	// Спорт
	// Добавить запись
	public static boolean sport_insert(TMRequest tmReq, String sName, String sDescription, double fCount)
	{
		sName = Utils.strtrunc(Utils.isNull(sName, "").trim(), 255);
		sDescription = Utils.strtrunc(Utils.isNull(sDescription, "").trim(), 4000);

		Logger.add("DB.sport_insert: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", sName=" + sName +
			", sDescription=" + sDescription +
			", fCount=" + fCount);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_INSERT_TOTAL", "");

		if (fCount <= 0) {
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_INSERT_FAIL", "fCount <= 0");
			return false;
		}

		// Перевод в км
		double fkoeff = 1.0;
		if (sName.equals("RUN")) // Бег
			fkoeff = 1.0;
		else if (sName.equals("SKY")) // Лыжи
			fkoeff = 1.0;
		else if (sName.equals("WALK")) // Ходьба
			fkoeff = 0.7;
		else if (sName.equals("BIKE")) // Велосипед
			fkoeff = 0.5;
		else if (sName.equals("SWIM")) // Плавание
			fkoeff = 5.0;
		else if (sName.equals("OTHER")) // Прочие активности в минутах
			fkoeff = 0.1;
		double fkm = fCount * fkoeff;

		if (!check()) {
			Logger.add("DB.sport_insert: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_INSERT_FAIL", "not DB.check()");
			return false;
		}

		LocalDateTime dt = LocalDateTime.now(); // Текущее время в tzServer
		String create_date = Utils.date2UTC(dt, Main.tzServer); // Время в UTC
		String create_date_local = Utils.date2TZ(dt, Main.tzUser.getRawOffset() - Main.tzServer.getRawOffset()); // Время в tzUser

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"INSERT INTO sport (chat_id, chat_name, from_id, from_name, from_user, name, description, count, koeff, km, create_date, gmt, create_date_local) \n" +
							"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, tmReq.chat_id);
						statement.setString(2, tmReq.chat_name);
						statement.setString(3, tmReq.from_id);
						statement.setString(4, tmReq.from_name);
						statement.setString(5, tmReq.from_username);
						statement.setString(6, sName);
						statement.setString(7, sDescription);
						statement.setDouble(8, fCount);
						statement.setDouble(9, fkoeff);
						statement.setDouble(10, fkm);
						statement.setString(11, create_date); // UTC
						statement.setInt(12, Utils.tz2gmt(Main.tzUser));
						statement.setString(13, create_date_local); // tzUser

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.sport_insert: Error: INSERT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_INSERT_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_INSERT_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_INSERT_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.sport_insert: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_INSERT_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.sport_insert: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_INSERT_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}


	// Спорт
	// Выбрать статистику за период
	public static String sport_selectStatPeriod(TMRequest tmReq, String chat_id, String startDate, String endDate, String sTitle)
	{
		Logger.add("DB.sport_selectStatPeriod: chat_id=" + chat_id + ", Период: с " + startDate + " по " + endDate);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTPERIOD_TOTAL", "");

		if (!check()) {
			Logger.add("DB.sport_selectStatPeriod: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTPERIOD_FAIL", "not DB.check()");
			return "";
		}

		try {
			String sError = "";
			try {
				String sRes = "";
				double dKmAll = 0.0;
				long ticker = 0;
				synchronized (lock) {
					// Бег
					double dCount = 0.0;
					double dKm = 0.0;
					try {
						String sql =
							"SELECT sum(count), sum(km) \n" +
							"FROM sport \n" +
							"WHERE chat_id = ? and create_date >= ? and create_date <= ? AND name = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);
						statement.setString(2, startDate);
						statement.setString(3, endDate);
						statement.setString(4, "RUN");

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						long ticker2 = dtEndTicker.getTime() - dtStartTicker.getTime();
						if (ticker2 > ticker)
							ticker = ticker2;

						while (resultSet.next()) {
							dCount = resultSet.getDouble(1);
							dKm = resultSet.getDouble(2);
							break;
						}
					}
					catch (Exception ex2) {
						sError += "SELECT RUN: " + ex2.getMessage() + " \n";
					}
					if (dKm > 0) {
						dKmAll += dKm;
						sRes += "\n  — Бег " + dCount + " км * 1 = " + dKm + " км";
					}

					// Лыжи
					dCount = 0.0;
					dKm = 0.0;
					try {
						String sql =
							"SELECT sum(count), sum(km) \n" +
							"FROM sport \n" +
							"WHERE chat_id = ? and create_date >= ? and create_date <= ? AND name = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);
						statement.setString(2, startDate);
						statement.setString(3, endDate);
						statement.setString(4, "SKY");

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						long ticker2 = dtEndTicker.getTime() - dtStartTicker.getTime();
						if (ticker2 > ticker)
							ticker = ticker2;

						while (resultSet.next()) {
							dCount = resultSet.getDouble(1);
							dKm = resultSet.getDouble(2);
							break;
						}
					}
					catch (Exception ex2) {
						sError += "SELECT SKY: " + ex2.getMessage() + " \n";
					}
					if (dKm > 0) {
						dKmAll += dKm;
						sRes += "\n  — Лыжи " + dCount + " км * 1 = " + dKm + " км";
					}

					// Ходьба
					dCount = 0.0;
					dKm = 0.0;
					try {
						String sql =
							"SELECT sum(count), sum(km) \n" +
							"FROM sport \n" +
							"WHERE chat_id = ? and create_date >= ? and create_date <= ? AND name = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);
						statement.setString(2, startDate);
						statement.setString(3, endDate);
						statement.setString(4, "WALK");

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						long ticker2 = dtEndTicker.getTime() - dtStartTicker.getTime();
						if (ticker2 > ticker)
							ticker = ticker2;

						while (resultSet.next()) {
							dCount = resultSet.getDouble(1);
							dKm = resultSet.getDouble(2);
							break;
						}
					}
					catch (Exception ex2) {
						sError += "SELECT WALK: " + ex2.getMessage() + " \n";
					}
					if (dKm > 0) {
						dKmAll += dKm;
						sRes += "\n  — Ходьба " + dCount + " км * 0.7 = " + dKm + " км";
					}

					// Велик
					dCount = 0.0;
					dKm = 0.0;
					try {
						String sql =
							"SELECT sum(count), sum(km) \n" +
							"FROM sport \n" +
							"WHERE chat_id = ? and create_date >= ? and create_date <= ? AND name = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);
						statement.setString(2, startDate);
						statement.setString(3, endDate);
						statement.setString(4, "BIKE");

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						long ticker2 = dtEndTicker.getTime() - dtStartTicker.getTime();
						if (ticker2 > ticker)
							ticker = ticker2;

						while (resultSet.next()) {
							dCount = resultSet.getDouble(1);
							dKm = resultSet.getDouble(2);
							break;
						}
					}
					catch (Exception ex2) {
						sError += "SELECT BIKE: " + ex2.getMessage() + " \n";
					}
					if (dKm > 0) {
						dKmAll += dKm;
						sRes += "\n  — Велик " + dCount + " км * 0.5 = " + dKm + " км";
					}

					// Плавание
					dCount = 0.0;
					dKm = 0.0;
					try {
						String sql =
							"SELECT sum(count), sum(km) \n" +
							"FROM sport \n" +
							"WHERE chat_id = ? and create_date >= ? and create_date <= ? AND name = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);
						statement.setString(2, startDate);
						statement.setString(3, endDate);
						statement.setString(4, "SWIM");

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						long ticker2 = dtEndTicker.getTime() - dtStartTicker.getTime();
						if (ticker2 > ticker)
							ticker = ticker2;

						while (resultSet.next()) {
							dCount = resultSet.getDouble(1);
							dKm = resultSet.getDouble(2);
							break;
						}
					}
					catch (Exception ex2) {
						sError += "SELECT SWIM: " + ex2.getMessage() + " \n";
					}
					if (dKm > 0) {
						dKmAll += dKm;
						sRes += "\n  — Плавание " + dCount + " км * 5 = " + dKm + " км";
					}

					// Пр. активности
					dCount = 0.0;
					dKm = 0.0;
					try {
						String sql =
							"SELECT sum(count), sum(km) \n" +
							"FROM sport \n" +
							"WHERE chat_id = ? and create_date >= ? and create_date <= ? AND name = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);
						statement.setString(2, startDate);
						statement.setString(3, endDate);
						statement.setString(4, "OTHER");

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						long ticker2 = dtEndTicker.getTime() - dtStartTicker.getTime();
						if (ticker2 > ticker)
							ticker = ticker2;

						while (resultSet.next()) {
							dCount = resultSet.getDouble(1);
							dKm = resultSet.getDouble(2);
							break;
						}
					}
					catch (Exception ex2) {
						sError += "SELECT OTHER: " + ex2.getMessage() + " \n";
					}
					if (dKm > 0) {
						dKmAll += dKm;
						sRes += "\n  — Пр. активности " + dCount + " мин * 0,1 = " + dKm + " км";
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.sport_selectStatPeriod: Error: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTPERIOD_FAIL", sError);
					return "";
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTPERIOD_LONGTIME", "" + ticker);

				int iKmAll = (int)Math.round(dKmAll * 10);
				dKmAll = (double)((double)iKmAll / (double)10.0);
				if (dKmAll > 0.0)
					sRes = "\n" + sTitle + " = " + dKmAll + " км:" + sRes;

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTPERIOD_SUCCESS", "");
				return sRes;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.sport_selectStatPeriod: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTPERIOD_FAIL", "2: " + ex2.getMessage());
				return "";
			}
		}
		catch (Exception ex) {
			Logger.add("DB.sport_selectStatPeriod: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTPERIOD_FAIL", "3: " + ex.getMessage());
			return "";
		}
	}

	// Спорт
	// Определить дату первой записи в UTC
	public static LocalDateTime sport_selectMinDate(TMRequest tmReq, String chat_id)
	{
		Logger.add("DB.sport_selectMinDate: chat_id=" + chat_id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTMINDATE_TOTAL", "");

		LocalDateTime dt = LocalDateTime.now(); // Текущее время в tzServer
		LocalDateTime dtRes = Utils.date2UTC2(dt, Main.tzServer); // UTC

		if (!check()) {
			Logger.add("DB.sport_selectMinDate: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTMINDATE_FAIL", "not DB.check()");
			return dtRes;
		}

		try {
			String sError = "";
			String sdt = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT min(create_date) \n" +
							"FROM sport \n" +
							"WHERE chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						while (resultSet.next()) {
							sdt = resultSet.getString(1);
							break;
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.sport_selectMinDate: Error: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTMINDATE_FAIL", sError);
					return dtRes;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTMINDATE_LONGTIME", "" + ticker);

				dtRes = Utils.str2date(sdt, "yyyy-MM-dd HH:mm:ss", dtRes);
				//Logger.add("DB.sport_selectMinDate: sdt=" + sdt);
				//Logger.add("DB.sport_selectMinDate: dtRes=" + Utils.date2str(dtRes, ""));

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTMINDATE_SUCCESS", "");
				return dtRes;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.sport_selectMinDate: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTMINDATE_FAIL", "2: " + ex2.getMessage());
				return dtRes;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.sport_selectMinDate: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_SELECTMINDATE_FAIL", "3: " + ex.getMessage());
			return dtRes;
		}
	}

	// Спорт
	// Выбрать статистику за текущий месяц
	public static String sport_selectStatMonth(TMRequest tmReq, String chat_id)
	{
		Logger.add("DB.sport_selectStatMonth: chat_id=" + chat_id);

		try {
			// Текущий месяц
			String sRes = "";
			LocalDateTime dt = LocalDateTime.now(); // Текущее время в tzServer
			LocalDateTime dtToday = Utils.date2TZ2(dt, Main.tzUser.getRawOffset() - Main.tzServer.getRawOffset()); // Текущее время в tzUser
			LocalDateTime dtStart = LocalDateTime.of(dtToday.getYear(), dtToday.getMonth(), 1, 0, 0, 0); // tzUser
			LocalDateTime dtEnd = dtToday; // tzUser
			sRes = DB.sport_selectStatPeriod(tmReq, chat_id, Utils.date2UTC(dtStart, Main.tzUser), Utils.date2UTC(dtEnd, Main.tzUser), "Месяц " + Utils.date2str(dtStart, "yyyy.MM"));

			if (sRes.isEmpty())
				sRes = "\nПусто";
			return sRes;
		}
		catch (Exception ex) {
			Logger.add("DB.sport_selectStatMonth: Error: " + ex.getMessage());
			return "";
		}
	}


	// Спорт
	// Выбрать статистику полную
	public static String sport_selectStatFull(TMRequest tmReq, String chat_id)
	{
		Logger.add("DB.sport_selectStatFull: chat_id=" + chat_id);

		try {
			String sRes = "";

			// Текущая дата
			LocalDateTime dt = LocalDateTime.now(); // Текущее время в tzServer
			LocalDateTime dtToday = Utils.date2TZ2(dt, Main.tzUser.getRawOffset() - Main.tzServer.getRawOffset()); // Текущее время в tzUser
			int iYearCur = dtToday.getYear(); // Текущий год
			LocalDateTime dtYearCur = LocalDateTime.of(dtToday.getYear(), 1, 1, 0, 0, 0); // tzUser
			int iMonthCur = dtToday.getMonthValue(); // Текущий месяц
			Logger.add("DB.sport_selectStatFull: dtToday=" + Utils.date2str(dtToday, "") + ", iYearCur=" + iYearCur + ", iMonthCur=" + iMonthCur);

			// Стартовая дата статистики
			LocalDateTime dtMinUTC = DB.sport_selectMinDate(tmReq, chat_id); // UTC
			LocalDateTime dtMin = Utils.date2TZ2(dtMinUTC, Main.tzUser.getRawOffset()); // tzUser
			Logger.add("DB.sport_selectStatFull: dtMinUTC=" + Utils.date2str(dtMinUTC, "") + ", dtMin=" + Utils.date2str(dtMin, ""));


			// Стартовый год
			int iYearStart = dtMin.getYear();

			// Пройти по всем годам от начального до текущего
			for (int i = iYearStart; i <= iYearCur; i++) {
				LocalDateTime dtStart = LocalDateTime.of(i, 1, 1, 0, 0, 0); // tzUser
				LocalDateTime dtEnd = LocalDateTime.of(i, 12, 31, 23, 59, 59); // tzUser
				String s = DB.sport_selectStatPeriod(tmReq, chat_id, Utils.date2UTC(dtStart, Main.tzUser), Utils.date2UTC(dtEnd, Main.tzUser), "Год " + Utils.date2str(dtStart, "yyyy"));
				sRes += s;
			}


			// Стартовый месяц в текущем году
			LocalDateTime dtMonthStart = dtMin;
			if (dtMin.compareTo(dtYearCur) < 0)
				dtMonthStart = dtYearCur; // Взять текущий месяц из dtYearCur
			else
				dtMonthStart = LocalDateTime.of(dtMin.getYear(), dtMin.getMonth(), 1, 0, 0, 0); // Взять текущий месяц из dtMin
			int iMonthStart = dtMonthStart.getMonthValue();

			// Пройти по всем месяцам текущего года
			for (int i = iMonthStart; i <= 12; i++) {
				LocalDateTime dtStart = LocalDateTime.of(iYearCur, i, 1, 0, 0, 0); // tzUser
				LocalDate dtEndM = LocalDate.of(iYearCur, i, 1); // tzUser
				LocalDateTime dtEnd = LocalDateTime.of(iYearCur, i, dtEndM.lengthOfMonth(), 23, 59, 59); // tzUser
				String s = DB.sport_selectStatPeriod(tmReq, chat_id, Utils.date2UTC(dtStart, Main.tzUser), Utils.date2UTC(dtEnd, Main.tzUser), "Месяц " + Utils.date2str(dtStart, "yyyy.MM"));
				sRes += s;
			}

			if (sRes.isEmpty())
				sRes = "\nПусто";
			return sRes;
		}
		catch (Exception ex) {
			Logger.add("DB.sport_selectStatFull: Error: " + ex.getMessage());
			return "";
		}
	}

	// Спорт
	// Очистить данные пользователя
	public static boolean sport_deleteAll(TMRequest tmReq, String chat_id)
	{
		Logger.add("DB.sport_deleteAll: chat_id=" + chat_id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_DELETEALL_TOTAL", "");

		if (!check()) {
			Logger.add("DB.sport_deleteAll: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_DELETEALL_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"DELETE FROM sport  \n" +
							"WHERE chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, chat_id);

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.sport_deleteAll: Error: DELETE: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_DELETEALL_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_DELETEALL_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_DELETEALL_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.sport_deleteAll: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_DELETEALL_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.sport_deleteAll: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_SPORT_DELETEALL_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

	// Обратная связь
	// Добавить запись
	public static boolean feedback_insert(TMRequest tmReq, String sMessage, String sFolder, String sStatus, int reply_to_id)
	{
		sMessage = Utils.strtrunc(Utils.isNull(sMessage, "").trim(), 4000);
		sFolder = Utils.strtrunc(Utils.isNull(sFolder, "").trim(), 255);
		sStatus = Utils.strtrunc(Utils.isNull(sStatus, "").trim(), 255);

		Logger.add("DB.feedback_insert: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", sMessage=" + sMessage +
			", sFolder=" + sFolder +
			", sStatus=" + sStatus +
			", reply_to_id=" + reply_to_id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_INSERT_TOTAL", "");

		if (!check()) {
			Logger.add("DB.feedback_insert: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_INSERT_FAIL", "not DB.check()");
			return false;
		}

		LocalDateTime dt = LocalDateTime.now(); // Текущее время в tzServer
		String create_date = Utils.date2UTC(dt, Main.tzServer); // Время в UTC
		String create_date_local = Utils.date2TZ(dt, Main.tzUser.getRawOffset() - Main.tzServer.getRawOffset()); // Время в tzUser

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql = "";
						PreparedStatement statement = null;
						if (reply_to_id > 0)
						{
							sql = "INSERT INTO feedback (chat_id, chat_name, from_id, from_name, from_user, message, folder, status, reply_to_id, create_date, gmt, create_date_local) \n" +
								"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
							statement = connection.prepareStatement(sql);
							statement.setString(1, tmReq.chat_id);
							statement.setString(2, tmReq.chat_name);
							statement.setString(3, tmReq.from_id);
							statement.setString(4, tmReq.from_name);
							statement.setString(5, tmReq.from_username);
							statement.setString(6, sMessage);
							statement.setString(7, sFolder);
							statement.setString(8, sStatus);
							statement.setInt(9, reply_to_id);
							statement.setString(10, create_date); // UTC
							statement.setInt(11, Utils.tz2gmt(Main.tzUser));
							statement.setString(12, create_date_local); // tzUser
						}
						else
						{
							sql = "INSERT INTO feedback (chat_id, chat_name, from_id, from_name, from_user, message, folder, status, create_date, gmt, create_date_local) \n" +
								"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
							statement = connection.prepareStatement(sql);
							statement.setString(1, tmReq.chat_id);
							statement.setString(2, tmReq.chat_name);
							statement.setString(3, tmReq.from_id);
							statement.setString(4, tmReq.from_name);
							statement.setString(5, tmReq.from_username);
							statement.setString(6, sMessage);
							statement.setString(7, sFolder);
							statement.setString(8, sStatus);
							statement.setString(9, create_date); // UTC
							statement.setInt(10, Utils.tz2gmt(Main.tzUser));
							statement.setString(11, create_date_local); // tzUser
						}

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.feedback_insert: Error: INSERT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_INSERT_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_INSERT_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_INSERT_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.feedback_insert: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_INSERT_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.feedback_insert: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_INSERT_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}


	// Обратная связь
	// Обновить статус
	public static boolean feedback_updateStatus(String chat_id, String chat_name, String from_id, String from_username, String from_name, int id, String sStatus)
	{
		sStatus = Utils.strtrunc(Utils.isNull(sStatus, "").trim(), 255);

		Logger.add("DB.feedback_updateStatus: chat_id=" + chat_id +
			", chat_name=" + chat_name +
			", from_id=" + from_id +
			", from_user=" + from_username +
			", from_name=" + from_name +
			", sStatus=" + sStatus +
			", id=" + id);
		Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_UPDATESTATUS_TOTAL", "");

		if (!check()) {
			Logger.add("DB.feedback_updateStatus: Error: not DB.check()");
			Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_UPDATESTATUS_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"UPDATE feedback \n" +
							"SET status = ? \n" +
							"WHERE id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, sStatus);
						statement.setInt(2, id);

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.feedback_updateStatus: Error: UPDATE: " + sError);
					Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_UPDATESTATUS_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_UPDATESTATUS_LONGTIME", "" + ticker);

				Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_UPDATESTATUS_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.feedback_updateStatus: Error: 2: " + ex2.getMessage());
				Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_UPDATESTATUS_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.feedback_updateStatus: Error: 3: " + ex.getMessage());
			Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_UPDATESTATUS_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

	// Обратная связь
	// Класс для возрата результатов
	public static class feedback_row {
		public int id;
		public String chat_id;
		public String chat_name;
		public String from_id;
		public String from_name;
		public String from_username;
		public String message;
		public int reply_to_id;

		public feedback_row() {
			this.id = 0;
			this.chat_id = "";
			this.chat_name = "";
			this.from_id = "";
			this.from_name = "";
			this.from_username = "";
			this.message = "";
			this.reply_to_id = 0;
		}
	}

	// Обратная связь
	// Выбрать сообщения по статусу
	public static ArrayList<feedback_row> feedback_selectByStatus(String sFolder, String sStatus)
	{
		sFolder = Utils.strtrunc(Utils.isNull(sFolder, "").trim(), 255);
		sStatus = Utils.strtrunc(Utils.isNull(sStatus, "").trim(), 255);

		//Logger.add("DB.feedback_selectByStatus: sFolder=" + sFolder + ", sStatus=" + sStatus);
		Monitor.add("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_TOTAL", "");

		if (!check()) {
			Logger.add("DB.feedback_selectByStatus: Error: not DB.check()");
			Monitor.add("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				ArrayList<feedback_row> rows = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id, chat_id, chat_name, from_id, from_name, from_user, message, reply_to_id \n" +
								"FROM feedback \n" +
								"WHERE status = ? AND folder = ? \n" +
								"ORDER BY id";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, sStatus);
						statement.setString(2, sFolder);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						rows = new ArrayList<feedback_row>();
						while (resultSet.next()) {
							feedback_row row = new feedback_row();
							row.id = resultSet.getInt(1);
							row.chat_id = resultSet.getString(2);
							row.chat_name = resultSet.getString(3);
							row.from_id = resultSet.getString(4);
							row.from_name = resultSet.getString(5);
							row.from_username = resultSet.getString(6);
							row.message = resultSet.getString(7);
							row.reply_to_id = resultSet.getInt(8);
							rows.add(row);
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.feedback_selectByStatus: Error: SELECT: " + sError);
					Monitor.add("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_LONGTIME", "" + ticker);

				Monitor.add("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_SUCCESS", "");
				return rows;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.feedback_selectByStatus: Error: 2: " + ex2.getMessage());
				Monitor.add("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.feedback_selectByStatus: Error: 3: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// Обратная связь
	// Вернуть chat_id и from_user по id
	public static ArrayList<feedback_row> feedback_chatById(String chat_id, String chat_name, String from_id, String from_username, String from_name, int id)
	{
		Logger.add("DB.feedback_chatById: id=" + id);
		Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_CHATBYID_TOTAL", "");

		if (!check()) {
			Logger.add("DB.feedback_chatById: Error: not DB.check()");
			Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_CHATBYID_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				ArrayList<feedback_row> rows = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT chat_id, from_user \n" +
								"FROM feedback \n" +
								"WHERE id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setInt(1, id);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						rows = new ArrayList<feedback_row>();
						while (resultSet.next()) {
							feedback_row row = new feedback_row();
							row.chat_id = resultSet.getString(1);
							row.from_username = resultSet.getString(2);
							rows.add(row);
							break;
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.feedback_chatById: Error: SELECT: " + sError);
					Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_CHATBYID_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_CHATBYID_LONGTIME", "" + ticker);

				Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_CHATBYID_SUCCESS", "");
				return rows;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.feedback_chatById: Error: 2: " + ex2.getMessage());
				Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_CHATBYID_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.feedback_chatById: Error: 3: " + ex.getMessage());
			Monitor.add(chat_id, chat_name, from_id, from_username, from_name, "DB_FEEDBACK_CHATBYID_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// Обратная связь
	// Проверить существование сообщения по id
	public static boolean feedback_checkById(TMRequest tmReq, int id)
	{
		Logger.add("DB.feedback_checkById: id=" + id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_CHECKBYID_TOTAL", "");

		if (!check()) {
			Logger.add("DB.feedback_checkById: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_CHECKBYID_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				int id2 = 0;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id \n" +
								"FROM feedback \n" +
								"WHERE id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setInt(1, id);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						while (resultSet.next()) {
							id2 = resultSet.getInt(1);
							break;
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.feedback_checkById: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_CHECKBYID_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_CHECKBYID_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_CHECKBYID_SUCCESS", "");
				if (id2 > 0)
					return true;
				else
					return false;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.feedback_checkById: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_CHECKBYID_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.feedback_checkById: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_FEEDBACK_CHECKBYID_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}


	// Мероприятия
	// Класс для возрата результатов
	public static class event_row {
		public int id;
		public String event;
		public String title;
		public String description;
		public String status;
		public String icon;

		public event_row() {
			this.id = 0;
			this.event = "";
			this.title = "";
			this.description = "";
			this.status = "";
			this.icon = "";
		}
	}

	// Заявки на мероприятия
	// Класс для возрата результатов
	public static class event_request_row {
		public int id;
		public String event;
		public String title;
		public String description;
		public String status;
		public String feedback;
		public String icon;

		public event_request_row() {
			this.id = 0;
			this.event = "";
			this.title = "";
			this.description = "";
			this.status = "";
			this.feedback = "";
			this.icon = "";
		}
	}

	// Мероприятия
	// Выбрать по chat_id список доступных мероприятий
	public static ArrayList<event_row> event_selectEvents(TMRequest tmReq, String chat_id)
	{
		Logger.add("DB.event_selectEvents: chat_id=" + chat_id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTEVENTS_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_selectEvents: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTEVENTS_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				// Сначала выбрать из базы все мероприятия
				ArrayList<event_row> rowsAll = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id, event, title, status \n" +
							"FROM event \n" +
							"ORDER BY title";
						PreparedStatement statement = connection.prepareStatement(sql);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						rowsAll = new ArrayList<event_row>();
						while (resultSet.next()) {
							event_row row = new event_row();
							row.id = resultSet.getInt(1);
							row.event = Utils.isNull(resultSet.getString(2), "");
							row.title = Utils.isNull(resultSet.getString(3), "");
							row.status = Utils.isNull(resultSet.getString(4), "");
							// Иконка статуса
							// open - нет иконки
							// open_reg - ➕
							// иначе - 🚫
							if (row.status.equals("open"))
								row.icon = ""; // нет иконки
							else if (row.status.equals("open_reg"))
								row.icon = "➕ "; // Регистрация открыта
							else
								row.icon = "🚫 "; // Мероприятие закрыто
							rowsAll.add(row);
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.event_selectEvents: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTEVENTS_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTEVENTS_LONGTIME", "" + ticker);

				// Отфильтровать мероприятия
				ArrayList<event_row> rows = new ArrayList<event_row>();
				if (rowsAll != null)
					for (event_row row : rowsAll) {
						// Определить является ли chat_id организатором
						boolean isOrg = DB.event_isOrg(chat_id, row.event);

						// Если организатор, то показать ему мероприятие в любом случае
						if (isOrg)
							rows.add(row);
						// Если не организатор, то показать ему мероприятие только если оно запущено
						else if (row.status.equals("open") || row.status.equals("open_reg"))
							rows.add(row);
					}

				// Выбрать все мероприятия из конфига по организатору chat_id
				ArrayList<String> list = Main.config.getEventsInList("bot_event_org", chat_id);
				if (list != null)
					for (String event : list) {
						// Найти event в rows
						boolean isFind = false;
						for (event_row row2 : rows)
							if (row2.event.equals(event)) {
								isFind = true;
								break;
							}
						// Если не найден, то добавить
						if (!isFind)
						{
							event_row row3 = new event_row();
							row3.event = event;
							row3.title = event;
							row3.icon = "🚫 ";
							rows.add(row3);
						}
					}

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTEVENTS_SUCCESS", "");
				return rows;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.event_selectEvents: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTEVENTS_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_selectEvents: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTEVENTS_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// Мероприятия
	// Является ли chat_id организатором мероприятия event
	public static boolean event_isOrg(String chat_id, String event) {
		boolean isOrg = false;
		try {
			// Проверить по конкретному event
			isOrg = Main.config.checkValueInList("bot_event_org", event + ":" + chat_id);

			// Проверить по всем event
			if (!isOrg)
				isOrg = Main.config.matchValueInList("bot_event_org", ".+:" + chat_id);

			return isOrg;
		}
		catch (Exception ex) {
			return false;
		}
	}


	// Мероприятия
	// Выбрать описание мероприятия
	public static event_row event_selectDescription(TMRequest tmReq, String chat_id, String event)
	{
		Logger.add("DB.event_selectDescription: chat_id=" + chat_id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTDESCRIPTION_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_selectDescription: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTDESCRIPTION_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				// Сначала выбрать из базы все мероприятия
				event_row rowEvent = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id, event, title, status, description \n" +
							"FROM event \n" +
							"WHERE event = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, event);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						while (resultSet.next()) {
							rowEvent = new event_row();
							rowEvent.id = resultSet.getInt(1);
							rowEvent.event = Utils.isNull(resultSet.getString(2), "");
							rowEvent.title = Utils.isNull(resultSet.getString(3), "");
							rowEvent.status = Utils.isNull(resultSet.getString(4), "");
							rowEvent.description = Utils.isNull(resultSet.getString(5), "");
							// Иконка статуса
							// open - нет иконки
							// open_reg - ➕
							// иначе - 🚫
							if (rowEvent.status.equals("open"))
								rowEvent.icon = ""; // нет иконки
							else if (rowEvent.status.equals("open_reg"))
								rowEvent.icon = "➕ "; // Регистрация открыта
							else
								rowEvent.icon = "🚫 "; // Мероприятие закрыто
							break;
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.event_selectDescription: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTDESCRIPTION_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTDESCRIPTION_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTDESCRIPTION_SUCCESS", "");
				return rowEvent;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.event_selectDescription: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTDESCRIPTION_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_selectDescription: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTDESCRIPTION_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// Мероприятия
	// Найти мероприятие
	public static event_row event_selectOne(TMRequest tmReq, String event)
	{
		Logger.add("DB.event_selectOne: event=" + event);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTONE_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_selectOne: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTONE_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				event_row rowEvent = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id, event, title, status, description \n" +
							"FROM event \n" +
							"WHERE event = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, event);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						while (resultSet.next()) {
							rowEvent = new event_row();
							rowEvent.id = resultSet.getInt(1);
							rowEvent.event = Utils.isNull(resultSet.getString(2), "");
							rowEvent.title = Utils.isNull(resultSet.getString(3), "");
							rowEvent.status = Utils.isNull(resultSet.getString(4), "");
							rowEvent.description = Utils.isNull(resultSet.getString(5), "");
							break;
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.event_selectOne: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTONE_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTONE_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTONE_SUCCESS", "");
				return rowEvent;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.event_selectOne: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTONE_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_selectOne: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_SELECTONE_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}


	// Мероприятия
	// Ввести описание
	public static boolean event_updateDescription(TMRequest tmReq, String event, String sText)
	{
		sText = Utils.isNull(sText, "").trim();
		Split sp = new Split(sText, '.', "");
		String sTitle = Utils.isNull(sp.s1, "").trim();
		String sDescription = Utils.isNull(sp.s2, "").trim();
		sTitle = Utils.strtrunc(Utils.isNull(sTitle, "").trim(), 255);
		sDescription = Utils.strtrunc(Utils.isNull(sDescription, "").trim(), 4000);

		Logger.add("DB.event_updateDescription: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", event=" + event +
			", sTitle=" + sTitle +
			", sDescription=" + sDescription);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_updateDescription: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_FAIL", "not DB.check()");
			return false;
		}

		LocalDateTime dt = LocalDateTime.now(); // Текущее время в tzServer
		String create_date = Utils.date2UTC(dt, Main.tzServer); // Время в UTC
		String create_date_local = Utils.date2TZ(dt, Main.tzUser.getRawOffset() - Main.tzServer.getRawOffset()); // Время в tzUser

		try {
			// Найти уже существующую запись мероприятия
			event_row rowEvent = event_selectOne(tmReq, event);

			// Добавление мероприятия
			if (rowEvent == null)
			{
				String sError = "";
				try {
					long ticker = 0;
					synchronized (lock) {
						try {
							String sql =
								"INSERT INTO event (chat_id, chat_name, from_id, from_name, from_user, event, title, description, status, create_date, gmt, create_date_local) \n" +
								"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
							PreparedStatement statement = connection.prepareStatement(sql);
							statement.setString(1, tmReq.chat_id);
							statement.setString(2, tmReq.chat_name);
							statement.setString(3, tmReq.from_id);
							statement.setString(4, tmReq.from_name);
							statement.setString(5, tmReq.from_username);
							statement.setString(6, event);
							statement.setString(7, sTitle);
							statement.setString(8, sDescription);
							statement.setString(9, "");
							statement.setString(10, create_date); // UTC
							statement.setInt(11, Utils.tz2gmt(Main.tzUser));
							statement.setString(12, create_date_local); // tzUser

							Date dtStartTicker = new Date();
							statement.executeUpdate();
							Date dtEndTicker = new Date();
							ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
						}
						catch (Exception ex2) {
							sError = ex2.getMessage();
						}
					}

					if (!sError.isEmpty()) {
						Logger.add("DB.event_updateDescription: Error: INSERT: " + sError);
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_FAIL", "INSERT: " + sError);
						return false;
					}

					if (ticker > longtimeDB)
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_LONGTIME", "" + ticker);

					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_SUCCESS", "");
					return true;
				}
				catch(Exception ex2)
				{
					Logger.add("DB.event_updateDescription: Error: 2: " + ex2.getMessage());
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_FAIL", "2: " + ex2.getMessage());
					return false;
				}
			}

			// Обновление описания мероприятия
			else
			{
				String sError = "";
				try {
					long ticker = 0;
					synchronized (lock) {
						try {
							String sql =
								"UPDATE event \n" +
								"SET title = ?, description = ? \n" +
								"WHERE id = ?";
							PreparedStatement statement = connection.prepareStatement(sql);
							statement.setString(1, sTitle);
							statement.setString(2, sDescription);
							statement.setInt(3, rowEvent.id);

							Date dtStartTicker = new Date();
							statement.executeUpdate();
							Date dtEndTicker = new Date();
							ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
						}
						catch (Exception ex2) {
							sError = ex2.getMessage();
						}
					}

					if (!sError.isEmpty()) {
						Logger.add("DB.event_updateDescription: Error: UPDATE: " + sError);
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_FAIL", "INSERT: " + sError);
						return false;
					}

					if (ticker > longtimeDB)
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_LONGTIME", "" + ticker);

					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_SUCCESS", "");
					return true;
				}
				catch(Exception ex2)
				{
					Logger.add("DB.event_updateDescription: Error: 2: " + ex2.getMessage());
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_FAIL", "2: " + ex2.getMessage());
					return false;
				}
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_updateDescription: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATEDESCRIPTION_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}


	// Мероприятия
	// Обновить статус
	public static boolean event_updateStatus(TMRequest tmReq, String event, String sStatus)
	{
		sStatus = Utils.strtrunc(Utils.isNull(sStatus, "").trim(), 255);

		Logger.add("DB.event_updateStatus: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", event=" + event +
			", sStatus=" + sStatus);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATESTATUS_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_updateStatus: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATESTATUS_FAIL", "not DB.check()");
			return false;
		}

		LocalDateTime dt = LocalDateTime.now(); // Текущее время в tzServer
		String create_date = Utils.date2UTC(dt, Main.tzServer); // Время в UTC
		String create_date_local = Utils.date2TZ(dt, Main.tzUser.getRawOffset() - Main.tzServer.getRawOffset()); // Время в tzUser

		try {
			// Найти уже существующую запись мероприятия
			event_row rowEvent = event_selectOne(tmReq, event);

			// Не найдено мероприятие
			if (rowEvent == null)
			{
				Logger.add("DB.event_updateStatus: Error: Не найдено мероприятие " + event);
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATESTATUS_FAIL", "Не найдено мероприятие " + event);
				return false;
			}

			// Обновление статуса
			else
			{
				String sError = "";
				try {
					long ticker = 0;
					synchronized (lock) {
						try {
							String sql =
								"UPDATE event \n" +
								"SET status = ? \n" +
								"WHERE id = ?";
							PreparedStatement statement = connection.prepareStatement(sql);
							statement.setString(1, sStatus);
							statement.setInt(2, rowEvent.id);

							Date dtStartTicker = new Date();
							statement.executeUpdate();
							Date dtEndTicker = new Date();
							ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
						}
						catch (Exception ex2) {
							sError = ex2.getMessage();
						}
					}

					if (!sError.isEmpty()) {
						Logger.add("DB.event_updateStatus: Error: UPDATE: " + sError);
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATESTATUS_FAIL", "UPDATE: " + sError);
						return false;
					}

					if (ticker > longtimeDB)
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATESTATUS_LONGTIME", "" + ticker);

					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATESTATUS_SUCCESS", "");
					return true;
				}
				catch(Exception ex2)
				{
					Logger.add("DB.event_updateStatus: Error: 2: " + ex2.getMessage());
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATESTATUS_FAIL", "2: " + ex2.getMessage());
					return false;
				}
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_updateStatus: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_UPDATESTATUS_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

	// Мероприятия
	// Ввести данные по заявке
	public static boolean event_request_updateRequest(TMRequest tmReq, String chat_id, String event, String sText)
	{
		sText = Utils.isNull(sText, "").trim();
		Split sp = new Split(sText, '.', "");
		String sTitle = Utils.isNull(sp.s1, "").trim();
		String sDescription = Utils.isNull(sp.s2, "").trim();
		sTitle = Utils.strtrunc(Utils.isNull(sTitle, "").trim(), 255);
		sDescription = Utils.strtrunc(Utils.isNull(sDescription, "").trim(), 4000);

		Logger.add("DB.event_request_updateRequest: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", chat_id=" + chat_id +
			", event=" + event +
			", sTitle=" + sTitle +
			", sDescription=" + sDescription);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_request_updateRequest: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_FAIL", "not DB.check()");
			return false;
		}

		LocalDateTime dt = LocalDateTime.now(); // Текущее время в tzServer
		String create_date = Utils.date2UTC(dt, Main.tzServer); // Время в UTC
		String create_date_local = Utils.date2TZ(dt, Main.tzUser.getRawOffset() - Main.tzServer.getRawOffset()); // Время в tzUser

		try {
			// Найти уже существующую заявку на мероприятие
			event_request_row row = DB.event_request_selectOne(tmReq, chat_id, event);

			// Добавление мероприятия
			if (row == null)
			{
				String sError = "";
				try {
					long ticker = 0;
					synchronized (lock) {
						try {
							String sql =
								"INSERT INTO event_request (chat_id, chat_name, from_id, from_name, from_user, event, title, description, status, feedback, create_date, gmt, create_date_local) \n" +
								"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
							PreparedStatement statement = connection.prepareStatement(sql);
							statement.setString(1, tmReq.chat_id);
							statement.setString(2, tmReq.chat_name);
							statement.setString(3, tmReq.from_id);
							statement.setString(4, tmReq.from_name);
							statement.setString(5, tmReq.from_username);
							statement.setString(6, event);
							statement.setString(7, sTitle);
							statement.setString(8, sDescription);
							statement.setString(9, "");
							statement.setString(10, "");
							statement.setString(11, create_date); // UTC
							statement.setInt(12, Utils.tz2gmt(Main.tzUser));
							statement.setString(13, create_date_local); // tzUser

							Date dtStartTicker = new Date();
							statement.executeUpdate();
							Date dtEndTicker = new Date();
							ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
						}
						catch (Exception ex2) {
							sError = ex2.getMessage();
						}
					}

					if (!sError.isEmpty()) {
						Logger.add("DB.event_request_updateRequest: Error: INSERT: " + sError);
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_FAIL", "INSERT: " + sError);
						return false;
					}

					if (ticker > longtimeDB)
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_LONGTIME", "" + ticker);

					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_SUCCESS", "");
					return true;
				}
				catch(Exception ex2)
				{
					Logger.add("DB.event_request_updateRequest: Error: 2: " + ex2.getMessage());
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_FAIL", "2: " + ex2.getMessage());
					return false;
				}
			}

			// Обновление описания мероприятия
			else
			{
				String sError = "";
				try {
					long ticker = 0;
					synchronized (lock) {
						try {
							String sql =
								"UPDATE event_request \n" +
								"SET title = ?, description = ?, status = ? \n" +
								"WHERE id = ?";
							PreparedStatement statement = connection.prepareStatement(sql);
							statement.setString(1, sTitle);
							statement.setString(2, sDescription);
							statement.setString(3, "");
							statement.setInt(4, row.id);

							Date dtStartTicker = new Date();
							statement.executeUpdate();
							Date dtEndTicker = new Date();
							ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
						}
						catch (Exception ex2) {
							sError = ex2.getMessage();
						}
					}

					if (!sError.isEmpty()) {
						Logger.add("DB.event_request_updateRequest: Error: UPDATE: " + sError);
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_FAIL", "INSERT: " + sError);
						return false;
					}

					if (ticker > longtimeDB)
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_LONGTIME", "" + ticker);

					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_SUCCESS", "");
					return true;
				}
				catch(Exception ex2)
				{
					Logger.add("DB.event_request_updateRequest: Error: 2: " + ex2.getMessage());
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_FAIL", "2: " + ex2.getMessage());
					return false;
				}
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_request_updateRequest: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUEST_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

	// Мероприятия
	// Найти заявку на мероприятие
	public static event_request_row event_request_selectOne(TMRequest tmReq, String chat_id, String event)
	{
		Logger.add("DB.event_reques_selectOne: event=" + event);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTONE_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_reques_selectOne: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTONE_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				event_request_row row = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id, event, title, status, description, feedback \n" +
							"FROM event_request \n" +
							"WHERE event = ? AND chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, event);
						statement.setString(2, chat_id);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						while (resultSet.next()) {
							row = new event_request_row();
							row.id = resultSet.getInt(1);
							row.event = Utils.isNull(resultSet.getString(2), "");
							row.title = Utils.isNull(resultSet.getString(3), "");
							row.status = Utils.isNull(resultSet.getString(4), "");
							row.description = Utils.isNull(resultSet.getString(5), "");
							row.feedback = Utils.isNull(resultSet.getString(6), "");
							break;
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.event_reques_selectOne: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTONE_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTONE_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTONE_SUCCESS", "");
				return row;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.event_reques_selectOne: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTONE_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_reques_selectOne: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTONE_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// Мероприятия
	// Выбрать описание заявки на мероприятие
	public static event_request_row event_request_selectDescription(TMRequest tmReq, String chat_id, String event)
	{
		Logger.add("DB.event_request_selectDescription: chat_id=" + chat_id);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTDESCRIPTION_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_request_selectDescription: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTDESCRIPTION_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				event_request_row row = null;
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT id, event, title, status, description, feedback \n" +
							"FROM event_request \n" +
							"WHERE event = ? and chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, event);
						statement.setString(2, chat_id);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						while (resultSet.next()) {
							row = new event_request_row();
							row.id = resultSet.getInt(1);
							row.event = Utils.isNull(resultSet.getString(2), "");
							row.title = Utils.isNull(resultSet.getString(3), "");
							row.status = Utils.isNull(resultSet.getString(4), "");
							row.description = Utils.isNull(resultSet.getString(5), "");
							row.feedback = Utils.isNull(resultSet.getString(6), "");
							// Иконка статуса
							if (row.status.equals("accepted"))
								row.icon = "✅ "; // Заявка принята организатором
							else if (row.status.equals("rejected"))
								row.icon = "🚫 "; // Заявка отклонена организатором
							else if (row.status.equals("canceled"))
								row.icon = "🚫 "; // Заявка отменена участником
							else if (row.status.equals("result"))
								row.icon = "✅ "; // Результаты
							else
								row.icon = "➕ "; // Заявка создана
							break;
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.event_request_selectDescription: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTDESCRIPTION_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTDESCRIPTION_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTDESCRIPTION_SUCCESS", "");
				return row;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.event_request_selectDescription: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTDESCRIPTION_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_request_selectDescription: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_SELECTDESCRIPTION_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// Мероприятия
	// Удалить заявки мероприятия
	public static boolean event_request_delete(TMRequest tmReq, String event)
	{
		Logger.add("DB.event_request_delete: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", event=" + event);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_DELETE_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_request_delete: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_DELETE_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"DELETE FROM event_request \n" +
							"WHERE event = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, event);

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.event_request_delete: Error: DELETE: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_DELETE_FAIL", "DELETE: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_DELETE_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_DELETE_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.event_request_delete: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_DELETE_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_request_delete: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_DELETE_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

	// Мероприятия
	// Обновить заявку по chat_id участника
	public static boolean event_request_updateRequestByChatId(TMRequest tmReq, String chat_id, String event, String sStatus, String sFeedback, String sTitle, String sDescription)
	{
		sStatus = Utils.strtrunc(Utils.isNull(sStatus, "").trim(), 255);
		sFeedback = Utils.strtrunc(Utils.isNull(sFeedback, "").trim(), 4000);
		sTitle = Utils.strtrunc(Utils.isNull(sTitle, "").trim(), 255);
		sDescription = Utils.strtrunc(Utils.isNull(sDescription, "").trim(), 4000);

		Logger.add("DB.event_request_updateRequestByChatId: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", chat_id=" + chat_id +
			", event=" + event +
			", sStatus=" + sStatus +
			", sFeedback=" + sFeedback +
			", sTitle=" + sTitle +
			", sDescription=" + sDescription);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_request_updateRequestByChatId: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"UPDATE event_request \n" +
							"SET status = ?, feedback = ?, title = ?, description = ? \n" +
							"WHERE event = ? and chat_id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, sStatus);
						statement.setString(2, sFeedback);
						statement.setString(3, sTitle);
						statement.setString(4, sDescription);
						statement.setString(5, event);
						statement.setString(6, chat_id);

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.event_request_updateRequestByChatId: Error: UPDATE: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.event_request_updateRequestByChatId: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_request_updateRequestByChatId: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

	// Мероприятия
	// Обновить заявку по id
	public static boolean event_request_updateRequestById(TMRequest tmReq, int id, String sStatus, String sFeedback, String sTitle, String sDescription)
	{
		sStatus = Utils.strtrunc(Utils.isNull(sStatus, "").trim(), 255);
		sFeedback = Utils.strtrunc(Utils.isNull(sFeedback, "").trim(), 4000);
		sTitle = Utils.strtrunc(Utils.isNull(sTitle, "").trim(), 255);
		sDescription = Utils.strtrunc(Utils.isNull(sDescription, "").trim(), 4000);

		Logger.add("DB.event_request_updateRequestById: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", id=" + id +
			", sStatus=" + sStatus +
			", sFeedback=" + sFeedback +
			", sTitle=" + sTitle +
			", sDescription=" + sDescription);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYID_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_request_updateRequestById: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYID_FAIL", "not DB.check()");
			return false;
		}

		try {
			String sError = "";
			try {
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"UPDATE event_request \n" +
							"SET status = ?, feedback = ?, title = ?, description = ? \n" +
							"WHERE id = ?";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, sStatus);
						statement.setString(2, sFeedback);
						statement.setString(3, sTitle);
						statement.setString(4, sDescription);
						statement.setInt(5, id);

						Date dtStartTicker = new Date();
						statement.executeUpdate();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.event_request_updateRequestById: Error: UPDATE: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYID_FAIL", "INSERT: " + sError);
					return false;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYID_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYIDD_SUCCESS", "");
				return true;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.event_request_updateRequestById: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYID_FAIL", "2: " + ex2.getMessage());
				return false;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_request_updateRequestById: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}


	// Мероприятия
	// Выгрузить заявки мероприятия
	public static String event_request_getreq(TMRequest tmReq, String event)
	{
		Logger.add("DB.event_request_getreq: event=" + event);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_GETREQ_TOTAL", "");

		if (!check()) {
			Logger.add("DB.event_request_getreq: Error: not DB.check()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_GETREQ_FAIL", "not DB.check()");
			return null;
		}

		try {
			String sError = "";
			try {
				String sDoc = "";
				long ticker = 0;
				synchronized (lock) {
					try {
						String sql =
							"SELECT from_id, from_user, from_name, title, description, status, feedback \n" +
							"FROM event_request \n" +
							"WHERE event = ? \n" +
							"ORDER BY id";
						PreparedStatement statement = connection.prepareStatement(sql);
						statement.setString(1, event);

						Date dtStartTicker = new Date();
						ResultSet resultSet = statement.executeQuery();
						Date dtEndTicker = new Date();
						ticker = dtEndTicker.getTime() - dtStartTicker.getTime();

						while (resultSet.next()) {
							String sFromId = Utils.isNull(resultSet.getString(1), "");
							String sFromUser = Utils.isNull(resultSet.getString(2), "");
							String sFromName = Utils.isNull(resultSet.getString(3), "");
							String sTitle = Utils.isNull(resultSet.getString(4), "");
							String sDescription = Utils.isNull(resultSet.getString(5), "");
							String sStatus = Utils.isNull(resultSet.getString(6), "");
							String sFeedback = Utils.isNull(resultSet.getString(7), "");
							sDoc += event + "\t" + sFromId + "\t" + sStatus + "\t" + sFeedback + "\t" + sTitle + "\t" + sDescription + "\t@" + sFromUser + "\t" + sFromName + "\n";
						}
					}
					catch (Exception ex2) {
						sError = ex2.getMessage();
					}
				}

				if (!sError.isEmpty()) {
					Logger.add("DB.event_request_getreq: Error: SELECT: " + sError);
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_GETREQ_FAIL", "INSERT: " + sError);
					return null;
				}

				if (ticker > longtimeDB)
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_GETREQ_LONGTIME", "" + ticker);

				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_GETREQ_SUCCESS", "");
				return sDoc;
			}
			catch(Exception ex2)
			{
				Logger.add("DB.event_request_getreq: Error: 2: " + ex2.getMessage());
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_GETREQ_FAIL", "2: " + ex2.getMessage());
				return null;
			}
		}
		catch (Exception ex) {
			Logger.add("DB.event_request_getreq: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_GETREQ_FAIL", "3: " + ex.getMessage());
			return null;
		}
	}

	// Мероприятия
	// Обновить все заявки по загруженным данным от Организатора
	public static boolean event_request_updateRequestList(TMRequest tmReq, String event, String sText)
	{
		sText = Utils.isNull(sText, "").trim();

		Logger.add("DB.event_request_updateRequestList: chat_id=" + tmReq.chat_id +
			", chat_name=" + tmReq.chat_name +
			", from_id=" + tmReq.from_id +
			", from_user=" + tmReq.from_username +
			", from_name=" + tmReq.from_name +
			", event=" + event);
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTLIST_TOTAL", "");

		try {
			int iCountOk = 0; // Количество успешно обработанных заявок
			int iCountUpdated = 0; // Количество успешно обновленных заявок
			int iCountError = 0; // Количество ошибок
			String sError = ""; // Все ошибки

			event_row rowEvent = DB.event_selectOne(tmReq, event);
			if (rowEvent == null) {
				Logger.add("DB.event_request_updateRequestList: Error: rowEvent==null");
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTLIST_FAIL", "rowEvent==null");
				return false;
			}

			String[] lstReq = sText.split("\n");
			Logger.add("DB.event_request_updateRequestList: lstReq.length=" + lstReq.length);
			for (String sReq : lstReq) {
				//Logger.add("DB.event_request_updateRequestList: sReq=" + sReq);
				// probike_bzp	481648490			12	Зайков Дмитрий	@dazaykov	Дмитрий Зайков
				String sReq2 = Utils.isNull(sReq, "").trim();
				if (sReq2.isEmpty())
					continue;

				String[] lstRecord = sReq2.split("\t");
				if (lstRecord == null)
					continue;
				if (lstRecord.length == 0)
					continue;

				String sREvent = "";
				if (lstRecord.length >= 1)
					sREvent = Utils.isNull(lstRecord[0], "").trim();
				if (!sREvent.equals(event))
					continue;

				String sRUserId = "";
				if (lstRecord.length >= 2)
					sRUserId = Utils.isNull(lstRecord[1], "").trim();
				if (sRUserId.isEmpty())
					continue;

				event_request_row rowREvent = DB.event_request_selectOne(tmReq, sRUserId, sREvent);
				if (rowREvent == null)
					continue;
				Logger.add("DB.event_request_updateRequestList: rowREvent.id=" + rowREvent.id);
				Logger.add("sDBStatus=" + rowREvent.status + ", sDBFeedback=" + rowREvent.feedback + ", sDBTitle=" + rowREvent.title + ", sDBDescription=" + rowREvent.description);

				String sRStatus = "";
				if (lstRecord.length >= 3)
					sRStatus = Utils.isNull(lstRecord[2], "").trim();

				String sRFeedback = "";
				if (lstRecord.length >= 4)
					sRFeedback = Utils.isNull(lstRecord[3], "").trim();

				String sRTitle = "";
				if (lstRecord.length >= 5)
					sRTitle = Utils.isNull(lstRecord[4], "").trim();
				if (!sRTitle.isEmpty())
					sRTitle = rowREvent.title;

				String sRDescription = "";
				if (lstRecord.length >= 6)
					sRDescription = Utils.isNull(lstRecord[5], "").trim();
				if (!sRDescription.isEmpty())
					sRDescription = rowREvent.description;

				Logger.add("sRStatus=" + sRStatus + ", sRFeedback=" + sRFeedback + ", sRTitle=" + sRTitle + ", sRDescription=" + sRDescription);

				if ( !sRStatus.equals(rowREvent.status) ||
					!sRFeedback.equals(rowREvent.feedback) ||
					!sRTitle.equals(rowREvent.title) ||
					!sRDescription.equals(rowREvent.description)
				) {
					Logger.add("DB.event_request_updateRequestList: NEED UPDATE");
					if (DB.event_request_updateRequestById(tmReq, rowREvent.id, sRStatus, sRFeedback, sRTitle, sRDescription)) {
						iCountUpdated += 1;
						if (!sRStatus.equals(rowREvent.status)) {
							Logger.add("DB.event_request_updateRequestList: NEED MESSAGE status");
							TMSend.TMMessage tmMessage = new TMSend.TMMessage();
							tmMessage.to_chat_id = sRUserId;
							tmMessage.to_user_id = sRUserId;
							tmMessage.prior = 3;
							tmMessage.text = "🏆 " + rowEvent.title + ": Статус заявки обновлен";
							TMSend.send(tmMessage);
						}
						else if (!sRFeedback.equals(rowREvent.feedback)) {
							Logger.add("DB.event_request_updateRequestList: NEED MESSAGE feedback");
							TMSend.TMMessage tmMessage = new TMSend.TMMessage();
							tmMessage.to_chat_id = sRUserId;
							tmMessage.to_user_id = sRUserId;
							tmMessage.prior = 3;
							tmMessage.text = "🏆 " + rowEvent.title + ": Пришло сообщение по заявке";
							TMSend.send(tmMessage);
						}
					}
					else
					{
						iCountError += 1;
						sError += "Ошибка обновления: Event=" + event + ", UserId=" + sRUserId + ", Status=" + sRStatus + ", Feedback=" + sRFeedback + ", Key=" + sRTitle + ", Value=" + sRDescription + "\n";
					}
				}

				iCountOk += 1;
			}


			// Результат
			TMSend.TMMessage tmMessage = new TMSend.TMMessage();
			tmMessage.to_chat_id = tmReq.chat_id;
			tmMessage.text = "🏆 " + rowEvent.title + ":";
			Logger.add("Обработано записей: " + iCountOk);
			tmMessage.text += "\nОбработано записей: " + iCountOk;
			Logger.add("Обновлено записей: " + iCountUpdated);
			tmMessage.text += "\nОбновлено записей: " + iCountUpdated;
			Logger.add("Ошибок: " + iCountError);
			tmMessage.text += "\nОшибок: " + iCountError;
			if (!sError.isEmpty()) {
				Logger.add("sError=" + sError);
				tmMessage.text += "\n" + sError;
			}
			TMSend.send(tmMessage);

			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTLIST_SUCCESS", "");
			return true;
		}
		catch (Exception ex) {
			Logger.add("DB.event_request_updateRequestList: Error: 3: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "DB_EVENT_REQUEST_UPDATEREQUESTLIST_FAIL", "3: " + ex.getMessage());
			return false;
		}
	}

}