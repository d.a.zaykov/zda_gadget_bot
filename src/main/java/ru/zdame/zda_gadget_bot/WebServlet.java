package ru.zdame.zda_gadget_bot;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;


public class WebServlet extends HttpServlet {

	@Override
	protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger.add("");
		Logger.add("------------------------------------------------------------------");
		String sIP = "";
		String sPath = "";
		try {
			sPath = Utils.isNull(request.getPathInfo(), "");
			Logger.add("OPTIONS " + sPath);

			sIP = Utils.isNull(request.getHeader("X-Real-IP"), "");
			if (sIP.isEmpty())
				sIP = Utils.isNull(request.getRemoteAddr(), "");
			Logger.add("IP=" + sIP);
			Monitor.add("", "", "", "", "", "WEB_OPTIONS_TOTAL", sIP + sPath);

			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println("");
			Logger.add("ok");
			Monitor.add("", "", "", "", "", "WEB_OPTIONS_SUCCESS", sIP + sPath);
		}
		catch (Exception ex) {
			//response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			//response.getWriter().println("505");
			response.getWriter().println("");
			response.setStatus(HttpServletResponse.SC_OK);
			Logger.add("WebServletBOT.doGet: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "WEB_OPTIONS_FAIL", sIP + sPath + ": " + ex.getMessage());
		}
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger.add("");
		Logger.add("------------------------------------------------------------------");
		String sIP = "";
		String sPath = "";
		try {
			sPath = Utils.isNull(request.getPathInfo(), "");
			Logger.add("DELETE " + sPath);

			sIP = Utils.isNull(request.getHeader("X-Real-IP"), "");
			if (sIP.isEmpty())
				sIP = Utils.isNull(request.getRemoteAddr(), "");
			Logger.add("IP=" + sIP);
			Monitor.add("", "", "", "", "", "WEB_DELETE_TOTAL", sIP + sPath);

			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println("");
			Logger.add("ok");
			Monitor.add("", "", "", "", "", "WEB_DELETE_SUCCESS", sIP + sPath);
		}
		catch (Exception ex) {
			//response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			//response.getWriter().println("505");
			response.getWriter().println("");
			response.setStatus(HttpServletResponse.SC_OK);
			Logger.add("WebServletBOT.doDelete: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "WEB_DELETE_FAIL", sIP + sPath + ": " + ex.getMessage());
		}
	}

	@Override
	protected void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger.add("");
		Logger.add("------------------------------------------------------------------");
		String sIP = "";
		String sPath = "";
		try {
			sPath = Utils.isNull(request.getPathInfo(), "");
			Logger.add("HEAD " + sPath);

			sIP = Utils.isNull(request.getHeader("X-Real-IP"), "");
			if (sIP.isEmpty())
				sIP = Utils.isNull(request.getRemoteAddr(), "");
			Logger.add("IP=" + sIP);
			Monitor.add("", "", "", "", "", "WEB_HEAD_TOTAL", sIP + sPath);

			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println("");
			Logger.add("ok");
			Monitor.add("", "", "", "", "", "WEB_HEAD_SUCCESS", sIP + sPath);
		}
		catch (Exception ex) {
			//response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			//response.getWriter().println("505");
			response.getWriter().println("");
			response.setStatus(HttpServletResponse.SC_OK);
			Logger.add("WebServletBOT.doHead: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "WEB_HEAD_FAIL", sIP + sPath + ": " + ex.getMessage());
		}
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger.add("");
		Logger.add("------------------------------------------------------------------");
		String sIP = "";
		String sPath = "";
		try {
			sPath = Utils.isNull(request.getPathInfo(), "");
			Logger.add("PUT " + sPath);

			sIP = Utils.isNull(request.getHeader("X-Real-IP"), "");
			if (sIP.isEmpty())
				sIP = Utils.isNull(request.getRemoteAddr(), "");
			Logger.add("IP=" + sIP);
			Monitor.add("", "", "", "", "", "WEB_PUT_TOTAL", sIP + sPath);

			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println("");
			Logger.add("ok");
			Monitor.add("", "", "", "", "", "WEB_PUT_SUCCESS", sIP + sPath);
		}
		catch (Exception ex) {
			//response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			//response.getWriter().println("505");
			response.getWriter().println("");
			response.setStatus(HttpServletResponse.SC_OK);
			Logger.add("WebServletBOT.doPut: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "WEB_PUT_FAIL", sIP + sPath + ": " + ex.getMessage());
		}
	}

	@Override
	protected void doTrace(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger.add("");
		Logger.add("------------------------------------------------------------------");
		String sIP = "";
		String sPath = "";
		try {
			sPath = Utils.isNull(request.getPathInfo(), "");
			Logger.add("TRACE " + sPath);

			sIP = Utils.isNull(request.getHeader("X-Real-IP"), "");
			if (sIP.isEmpty())
				sIP = Utils.isNull(request.getRemoteAddr(), "");
			Logger.add("IP=" + sIP);
			Monitor.add("", "", "", "", "", "WEB_TRACE_TOTAL", sIP + sPath);

			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println("");
			Logger.add("ok");
			Monitor.add("", "", "", "", "", "WEB_TRACE_SUCCESS", sIP + sPath);
		}
		catch (Exception ex) {
			//response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			//response.getWriter().println("505");
			response.getWriter().println("");
			response.setStatus(HttpServletResponse.SC_OK);
			Logger.add("WebServletBOT.doTrace: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "WEB_TRACE_FAIL", sIP + sPath + ": " + ex.getMessage());
		}
	}


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger.add("");
		Logger.add("------------------------------------------------------------------");
		String sIP = "";
		String sPath = "";
		try {
			sPath = Utils.isNull(request.getPathInfo(), "");
			Logger.add("GET " + sPath);

			sIP = Utils.isNull(request.getHeader("X-Real-IP"), "");
			if (sIP.isEmpty())
				sIP = Utils.isNull(request.getRemoteAddr(), "");
			Logger.add("IP=" + sIP);
			Monitor.add("", "", "", "", "", "WEB_GET_HC_TOTAL", sIP + sPath);

			if (!sPath.equals("/hc"))
			{
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				response.getWriter().println("404");
				Logger.add("404");
				Monitor.add("", "", "", "", "", "WEB_GET_HC_FAIL", sIP + sPath + ": 404");
				return;
			}

			if (DB.hc()) {
				response.getWriter().println("ok");
				Logger.add("ok");
				Monitor.add("", "", "", "", "", "WEB_GET_HC_SUCCESS", sIP + sPath);
			}
			else
			{
				response.getWriter().println("DB.hc: Error");
				Logger.add("DB.hc: Error");
				Monitor.add("", "", "", "", "", "WEB_GET_HC_FAIL", sIP + sPath + ": DB.hc: Error");
			}
			response.setStatus(HttpServletResponse.SC_OK);
		}
		catch (Exception ex) {
			//response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			//response.getWriter().println("505");
			response.getWriter().println("");
			response.setStatus(HttpServletResponse.SC_OK);
			Logger.add("WebServletBOT.doGet: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "WEB_GET_HC_FAIL", sIP + sPath + ": " + ex.getMessage());
		}
	}



	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger.add("");
		Logger.add("------------------------------------------------------------------");
		String sIP = "";
		String sPath = "";
		try {
			sPath = Utils.isNull(request.getPathInfo(), "");
			Logger.add("POST " + sPath);

			sIP = Utils.isNull(request.getHeader("X-Real-IP"), "");
			if (sIP.isEmpty())
				sIP = Utils.isNull(request.getRemoteAddr(), "");
			Logger.add("IP=" + sIP);
			Monitor.add("", "", "", "", "", "WEB_POST_ROOT_TOTAL", sIP + sPath);

			//String str_a = request.getParameter("a");

			/*if (!sPath.equals("/zda_gadget_bot"))
			{
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				response.getWriter().println("404");
				Logger.add("404");
				return;
			}*/

			String contentType = Utils.isNull(request.getHeader("Content-Type"), "");
			if (contentType.equals("application/json")) {
				// Разобрать сообщение от телеграма
				TMRequest tmReq = new TMRequest(request);
				if (tmReq.sError.length() > 0) {
					Logger.add("request error: " + tmReq.sError);
					// Если не смогли разобрать, то ничего не делаем.
					// И на всякий случай отдадим ответ, что все хорошо
				}
				else {
					Logger.add("request ok");
					TMSend.TMMessage tmMessage = null;

					// Главное меню
					if ( !tmReq.chat_id.isEmpty()
						&& !tmReq.from_id.isEmpty()
						&& ( tmReq.sText.startsWith("/start") || tmReq.sText.startsWith("/menu") )
					) {
						tmMessage = menuHandler(tmReq);
					}

					// ВКС
					else if ( !tmReq.chat_id.isEmpty()
						&& !tmReq.from_id.isEmpty()
						&& ( tmReq.sText.startsWith("/vcontact") )
					) {
						tmMessage = vcontactHandler(tmReq);
					}

					// Покупки
					else if ( !tmReq.chat_id.isEmpty()
						&& !tmReq.from_id.isEmpty()
						&& ( tmReq.sText.startsWith("/buy") )
					) {
						tmMessage = buyHandler(tmReq);
					}

					// Спорт
					else if ( !tmReq.chat_id.isEmpty()
						&& !tmReq.from_id.isEmpty()
						&& ( tmReq.sText.startsWith("/sport") )
					) {
						tmMessage = sportHandler(tmReq);
					}

					// Мероприятия
					else if ( !tmReq.chat_id.isEmpty()
						&& !tmReq.from_id.isEmpty()
						&& ( tmReq.sText.startsWith("/event") )
					) {
						tmMessage = eventHandler(tmReq);
					}

					// Обратная связь
					else if ( !tmReq.chat_id.isEmpty()
						&& !tmReq.from_id.isEmpty()
						&& ( tmReq.sText.startsWith("/feedback") )
					) {
						tmMessage = feedbackHandler(tmReq);
					}

					// Справка
					else if ( !tmReq.chat_id.isEmpty()
						&& !tmReq.from_id.isEmpty()
						&& ( tmReq.sText.startsWith("/help") )
					) {
						tmMessage = helpHandler(tmReq);
					}

					// Прочие сообщения, включая ввод данных пользователем
					else {
						tmMessage = otherHandler(tmReq);
					}

					// Отправить сообщение пользователю
					if (tmMessage != null) {
						TMSend.send(tmMessage);
					}
				}
			}

			response.getWriter().println("");
			response.setStatus(HttpServletResponse.SC_OK);
			Monitor.add("", "", "", "", "", "WEB_POST_ROOT_SUCCESS", sIP + sPath);

			//String res = "{ \"status\": \"ok\"}";
			//response.setContentType("application/json");
			//response.setStatus(HttpServletResponse.SC_OK);
			//response.getWriter().println(res);
			//Logger.add(res);

			//String reply = "{\"error\":0,\"result\":" + Double.toString(result) + "}";
			//response.getOutputStream().write( reply.getBytes("UTF-8") );
			//response.setContentType("application/json; charset=UTF-8");
			//response.setHeader("Access-Control-Allow-Origin", "*");

			//String reply = "{\"error\":1}";
			//response.getOutputStream().write( reply.getBytes("UTF-8") );
			//response.setContentType("application/json; charset=UTF-8");
			//response.setHeader("Access-Control-Allow-Origin", "*");

		}
		catch (Exception ex) {
			//response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			//response.getWriter().println("505");
			response.getWriter().println("");
			response.setStatus(HttpServletResponse.SC_OK); // Надо отвечать, что сигнал обработан успешно, иначе телега будет бесконечно присылать одно и то же сообщение
			Logger.add("WebServletBOT.doPost: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "WEB_POST_ROOT_FAIL", sIP + sPath + ": " + ex.getMessage());
		}
	}


	// Главное меню
	// Обработать текст сообщения и вернуть ответный текст
	private TMSend.TMMessage menuHandler(TMRequest tmReq) {
		TMSend.TMMessage tmMessage = new TMSend.TMMessage();
		tmMessage.to_chat_id = tmReq.chat_id;
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_MENU_TOTAL", "");

		if (tmReq.chat_id.isEmpty()) {
			Logger.add("menuHandler: Error: chat_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_MENU_FAIL", "chat_id.isEmpty()");
			return null;
		}

		if (tmReq.from_id.isEmpty()) {
			Logger.add("menuHandler: Error: from_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_MENU_FAIL", "from_id.isEmpty()");
			return null;
		}

		try {
			// Главное меню
			if (tmReq.sText.startsWith("/start") || tmReq.sText.startsWith("/menu")) {
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "MENU_TOTAL", "");
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "MENU_SUCCESS", "");
				InputWait.add(tmReq.chat_id, tmReq.from_id, "");

				// Сформировать главное меню
				tmMessage.text = "Главное меню:";
				tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>(6);

				if (Main.isBOT_VCONTACT) {
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🔵 ВКС", "/vcontact"));
					tmMessage.tmButtons.add(listButton);
				}

				if (Main.isBOT_BUY) {
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🧀 Покупки", "/buy"));
					tmMessage.tmButtons.add(listButton);
				}

				if (Main.isBOT_SPORT && tmReq.isChatPrivate) {
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ Спорт", "/sport"));
					tmMessage.tmButtons.add(listButton);
				}

				if (Main.isBOT_EVENT && tmReq.isChatPrivate) {
					ArrayList<DB.event_row> rows = DB.event_selectEvents(tmReq, tmReq.chat_id);
					if (rows != null)
						if (rows.size() > 0) {
							ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🏆 Мероприятия", "/event"));
							tmMessage.tmButtons.add(listButton);
						}
				}

				if (Main.isBOT_FEEDBACK && tmReq.isChatPrivate) {
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("✉️ Обратная связь", "/feedback"));
					if (tmReq.chat_id.equals(Main.BOT_OWNER_CHAT_ID)) {
						listButton.add(new TMSend.TMButton("✉️ Ответить", "/feedback_replyto"));
					}
					tmMessage.tmButtons.add(listButton);
				}

				// Справка для всех
				{
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("❔ Справка", "/help"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_MENU_SUCCESS", "");
			return tmMessage;
		}
		catch (Exception ex) {
			Logger.add("menuHandler: Error: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_MENU_FAIL", ex.getMessage());
			return null;
		}
	}


	// ВКС
	// Обработать текст сообщения и вернуть ответный текст
	private TMSend.TMMessage vcontactHandler(TMRequest tmReq) {
		TMSend.TMMessage tmMessage = new TMSend.TMMessage();
		tmMessage.to_chat_id = tmReq.chat_id;
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_VCONTACT_TOTAL", "");

		if (tmReq.chat_id.isEmpty()) {
			Logger.add("vcontactHandler: Error: chat_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_VCONTACT_FAIL", "chat_id.isEmpty()");
			return null;
		}

		if (tmReq.from_id.isEmpty()) {
			Logger.add("vcontactHandler: Error: from_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_VCONTACT_FAIL", "from_id.isEmpty()");
			return null;
		}

		try {
			// Очистить список
			if (tmReq.sText.startsWith("/vcontact_del_all")) {
				if (Main.isBOT_VCONTACT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_DEL_ALL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_DEL_ALL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Удалить исходное сообщение
					//if len(sSRCMessageId) > 0:
					//	API_deleteMessage(sChatId, sSRCMessageId)
					// Удалить
					DB.vcontact_deleteAll(tmReq, tmReq.chat_id);
					// Показать все ВКС в форме кнопок
					tmMessage.text = "🔵 Список пуст";
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🔵 ➕ Добавить ВКС", "/vcontact_add"));
					tmMessage.tmButtons.add(listButton);
					ArrayList<DB.vcontact_row> rows = DB.vcontact_selectByChatId(tmReq, tmReq.chat_id);
					if (rows != null)
						if (rows.size() > 0) {
							tmMessage.text = "🔵 Текущие ВКС:";
							listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🔵 ❌ Очистить список", "/vcontact_confirm_all"));
							tmMessage.tmButtons.add(listButton);
							for (DB.vcontact_row row : rows) {
								listButton = new ArrayList<TMSend.TMButton>();
								listButton.add(new TMSend.TMButton("🔵 " + row.name, "/vcontact_confirm " + row.id));
								tmMessage.tmButtons.add(listButton);
							}
						}
				}
			}

			// Подтвердить очистку списка 3
			else if (tmReq.sText.startsWith("/vcontact_confirm_all3")) {
				if (Main.isBOT_VCONTACT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_CONFIRM_ALL3_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_CONFIRM_ALL3_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать вопрос
					tmMessage.text = "🔵 ❌ Я ведь очищу!";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🔵 ДА!", "/vcontact_del_all"));
					listButton.add(new TMSend.TMButton("🔵 Нет", "/vcontact"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Подтвердить очистку списка 2
			else if (tmReq.sText.startsWith("/vcontact_confirm_all2")) {
				if (Main.isBOT_VCONTACT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_CONFIRM_ALL2_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_CONFIRM_ALL2_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать вопрос
					tmMessage.text = "🔵 ❌ Точно очистить?";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🔵 Да", "/vcontact_confirm_all3"));
					listButton.add(new TMSend.TMButton("🔵 Нет", "/vcontact"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Подтвердить очистку списка
			else if (tmReq.sText.startsWith("/vcontact_confirm_all")) {
				if (Main.isBOT_VCONTACT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_CONFIRM_ALL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_CONFIRM_ALL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать вопрос
					tmMessage.text = "🔵 ❌ Очистить список?";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🔵 Да", "/vcontact_confirm_all2"));
					listButton.add(new TMSend.TMButton("🔵 Нет", "/vcontact"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Удалить по id
			else if (tmReq.sText.startsWith("/vcontact_del")) {
				if (Main.isBOT_VCONTACT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_DEL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_DEL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Удалить исходное сообщение
					//if len(sSRCMessageId) > 0:
					//	API_deleteMessage(sChatId, sSRCMessageId)
					// Определить id
					Split sp = new Split(tmReq.sText, ' ', "");
					String sid = Utils.isNull(sp.s2, "").trim();
					int id = Integer.parseInt(sid);
					// Удалить
					DB.vcontact_delete(tmReq, tmReq.chat_id, id);
					// Показать все ВКС в форме кнопок
					tmMessage.text = "🔵 Список пуст";
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🔵 ➕ Добавить ВКС", "/vcontact_add"));
					tmMessage.tmButtons.add(listButton);
					ArrayList<DB.vcontact_row> rows = DB.vcontact_selectByChatId(tmReq, tmReq.chat_id);
					if (rows != null)
						if (rows.size() > 0) {
							tmMessage.text = "🔵 Текущие ВКС:";
							listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🔵 ❌ Очистить список", "/vcontact_confirm_all"));
							tmMessage.tmButtons.add(listButton);
							for (DB.vcontact_row row : rows) {
								listButton = new ArrayList<TMSend.TMButton>();
								listButton.add(new TMSend.TMButton("🔵 " + row.name, "/vcontact_confirm " + row.id));
								tmMessage.tmButtons.add(listButton);
							}
						}
				}
			}

			// Подтвердить удаление по id
			else if (tmReq.sText.startsWith("/vcontact_confirm")) {
				if (Main.isBOT_VCONTACT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_CONFIRM_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_CONFIRM_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить id
					Split sp = new Split(tmReq.sText, ' ', "");
					String sid = Utils.isNull(sp.s2, "").trim();
					int id = Integer.parseInt(sid);
					// Показать ВКС и вопрос
					tmMessage.text = "------------------------------------------------";
					DB.vcontact_row row = DB.vcontact_selectOne(tmReq, tmReq.chat_id, id);
					if (row != null) {
						tmMessage.text += "\n🔵 " + row.name;
						if (!row.description.isEmpty())
							tmMessage.text += "\n" + row.description;
					}
					tmMessage.text += "\n------------------------------------------------";
					tmMessage.text += "\n🔵 ❌ Удалить ВКС?";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🔵 Да", "/vcontact_del " + sid));
					listButton.add(new TMSend.TMButton("🔵 Нет", "/vcontact"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Ввод новой записи
			else if (tmReq.sText.startsWith("/vcontact_add")) {
				if (Main.isBOT_VCONTACT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_ADD_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_ADD_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "vcontact_add");
					tmMessage.text = "🔵 ➕ Введи новую ВКС:\nНазвание. Ссылка";
				}
			}

			// Список
			else if (tmReq.sText.startsWith("/vcontact")) {
				if (Main.isBOT_VCONTACT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "VCONTACT_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать все ВКС в форме кнопок
					tmMessage.text = "🔵 Список пуст";
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🔵 ➕ Добавить ВКС", "/vcontact_add"));
					tmMessage.tmButtons.add(listButton);
					ArrayList<DB.vcontact_row> rows = DB.vcontact_selectByChatId(tmReq, tmReq.chat_id);
					if (rows != null)
						if (rows.size() > 0) {
							tmMessage.text = "🔵 Текущие ВКС:";
							listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🔵 ❌ Очистить список", "/vcontact_confirm_all"));
							tmMessage.tmButtons.add(listButton);
							for (DB.vcontact_row row : rows) {
								listButton = new ArrayList<TMSend.TMButton>();
								listButton.add(new TMSend.TMButton("🔵 " + row.name, "/vcontact_confirm " + row.id));
								tmMessage.tmButtons.add(listButton);
							}
						}
				}
			}

			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_VCONTACT_SUCCESS", "");
			return tmMessage;
		}
		catch (Exception ex) {
			Logger.add("vcontactHandler: Error: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_VCONTACT_FAIL", ex.getMessage());
			return null;
		}
	}


	// Покупки
	// Обработать текст сообщения и вернуть ответный текст
	private TMSend.TMMessage buyHandler(TMRequest tmReq) {
		TMSend.TMMessage tmMessage = new TMSend.TMMessage();
		tmMessage.to_chat_id = tmReq.chat_id;
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_BUY_TOTAL", "");

		if (tmReq.chat_id.isEmpty()) {
			Logger.add("buyHandler: Error: chat_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_BUY_FAIL", "chat_id.isEmpty()");
			return null;
		}

		if (tmReq.from_id.isEmpty()) {
			Logger.add("buyHandler: Error: from_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_BUY_FAIL", "from_id.isEmpty()");
			return null;
		}

		try {
			// Очистить список
			if (tmReq.sText.startsWith("/buy_del_all")) {
				if (Main.isBOT_BUY) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_DEL_ALL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_DEL_ALL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Удалить исходное сообщение
					//if len(sSRCMessageId) > 0:
					//	API_deleteMessage(sChatId, sSRCMessageId)
					// Удалить
					DB.buy_deleteAll(tmReq, tmReq.chat_id);
					// Показать все покупки в форме кнопок
					tmMessage.text = "🧀 Список пуст";
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🧀 ➕ Добавить покупку", "/buy_add"));
					tmMessage.tmButtons.add(listButton);
					ArrayList<DB.buy_row> rows = DB.buy_selectByChatId(tmReq, tmReq.chat_id);
					if (rows != null)
						if (rows.size() > 0) {
							tmMessage.text = "🧀 Текущие покупки:";
							listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🧀 ❌ Очистить список", "/buy_confirm_all"));
							tmMessage.tmButtons.add(listButton);
							for (DB.buy_row row : rows) {
								listButton = new ArrayList<TMSend.TMButton>();
								if (tmReq.isChatPrivate)
									listButton.add(new TMSend.TMButton("🧀 " + row.name, "/buy_confirm " + row.id));
								else
									listButton.add(new TMSend.TMButton("🧀 " + row.from_name + ": " + row.name, "/buy_confirm " + row.id));
								tmMessage.tmButtons.add(listButton);
							}
						}
				}
			}

			// Подтвердить очистку списка 3
			else if (tmReq.sText.startsWith("/buy_confirm_all3")) {
				if (Main.isBOT_BUY) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_CONFIRM_ALL3_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_CONFIRM_ALL3_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать вопрос
					tmMessage.text = "🧀 ❌ Я ведь очищу!";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🧀 ДА!", "/buy_del_all"));
					listButton.add(new TMSend.TMButton("🧀 Нет", "/buy"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Подтвердить очистку списка 2
			else if (tmReq.sText.startsWith("/buy_confirm_all2")) {
				if (Main.isBOT_BUY) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_CONFIRM_ALL2_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_CONFIRM_ALL2_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать вопрос
					tmMessage.text = "🧀 ❌ Точно очистить?";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🧀 Да", "/buy_confirm_all3"));
					listButton.add(new TMSend.TMButton("🧀 Нет", "/buy"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Подтвердить очистку списка
			else if (tmReq.sText.startsWith("/buy_confirm_all")) {
				if (Main.isBOT_BUY) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_CONFIRM_ALL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_CONFIRM_ALL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать вопрос
					tmMessage.text = "🧀 ❌ Очистить список?";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🧀 Да", "/buy_confirm_all2"));
					listButton.add(new TMSend.TMButton("🧀 Нет", "/buy"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Удалить по id
			else if (tmReq.sText.startsWith("/buy_del")) {
				if (Main.isBOT_BUY) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_DEL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_DEL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Удалить исходное сообщение
					//if len(sSRCMessageId) > 0:
					//	API_deleteMessage(sChatId, sSRCMessageId)
					// Определить id
					Split sp = new Split(tmReq.sText, ' ', "");
					String sid = Utils.isNull(sp.s2, "").trim();
					int id = Integer.parseInt(sid);
					// Удалить
					DB.buy_delete(tmReq, tmReq.chat_id, id);
					// Показать все покупки в форме кнопок
					tmMessage.text = "🧀 Список пуст";
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🧀 ➕ Добавить покупку", "/buy_add"));
					tmMessage.tmButtons.add(listButton);
					ArrayList<DB.buy_row> rows = DB.buy_selectByChatId(tmReq, tmReq.chat_id);
					if (rows != null)
						if (rows.size() > 0) {
							tmMessage.text = "🧀 Текущие покупки:";
							listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🧀 ❌ Очистить список", "/buy_confirm_all"));
							tmMessage.tmButtons.add(listButton);
							for (DB.buy_row row : rows) {
								listButton = new ArrayList<TMSend.TMButton>();
								if (tmReq.isChatPrivate)
									listButton.add(new TMSend.TMButton("🧀 " + row.name, "/buy_confirm " + row.id));
								else
									listButton.add(new TMSend.TMButton("🧀 " + row.from_name + ": " + row.name, "/buy_confirm " + row.id));
								tmMessage.tmButtons.add(listButton);
							}
						}
				}
			}

			// Подтвердить удаление по id
			else if (tmReq.sText.startsWith("/buy_confirm")) {
				if (Main.isBOT_BUY) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_CONFIRM_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_CONFIRM_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить id
					Split sp = new Split(tmReq.sText, ' ', "");
					String sid = Utils.isNull(sp.s2, "").trim();
					int id = Integer.parseInt(sid);
					// Показать покупку и вопрос
					tmMessage.text = "------------------------------------------------";
					DB.buy_row rowBuy = DB.buy_selectOne(tmReq, tmReq.chat_id, id);
					if (rowBuy != null) {
						if (tmReq.isChatPrivate)
							tmMessage.text += "\n🧀 " + rowBuy.name;
						else
							tmMessage.text += "\n🧀 " + rowBuy.from_name + ": " + rowBuy.name;
						if (!rowBuy.description.isEmpty())
							tmMessage.text += "\n" + rowBuy.description;
					}
					tmMessage.text += "\n------------------------------------------------";
					tmMessage.text += "\n🧀 ❌ Удалить покупку?";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🧀 Да", "/buy_del " + sid));
					listButton.add(new TMSend.TMButton("🧀 Нет", "/buy"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Ввод новой записи
			else if (tmReq.sText.startsWith("/buy_add")) {
				if (Main.isBOT_BUY) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_ADD_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_ADD_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "buy_add");
					tmMessage.text = "🧀 ➕ Введи новую покупку:\nНазвание. Описание";
				}
			}

			// Список
			else if (tmReq.sText.startsWith("/buy")) {
				if (Main.isBOT_BUY) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BUY_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать все покупки в форме кнопок
					tmMessage.text = "🧀 Список пуст";
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🧀 ➕ Добавить покупку", "/buy_add"));
					tmMessage.tmButtons.add(listButton);
					ArrayList<DB.buy_row> rows = DB.buy_selectByChatId(tmReq, tmReq.chat_id);
					if (rows != null)
						if (rows.size() > 0) {
							tmMessage.text = "🧀 Текущие покупки:";
							listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🧀 ❌ Очистить список", "/buy_confirm_all"));
							tmMessage.tmButtons.add(listButton);
							for (DB.buy_row row : rows) {
								listButton = new ArrayList<TMSend.TMButton>();
								if (tmReq.isChatPrivate)
									listButton.add(new TMSend.TMButton("🧀 " + row.name, "/buy_confirm " + row.id));
								else
									listButton.add(new TMSend.TMButton("🧀 " + row.from_name + ": " + row.name, "/buy_confirm " + row.id));
								tmMessage.tmButtons.add(listButton);
							}
						}
				}
			}

			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_BUY_SUCCESS", "");
			return tmMessage;
		}
		catch (Exception ex) {
			Logger.add("buyHandler: Error: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_BUY_FAIL", ex.getMessage());
			return null;
		}
	}


	// Спорт
	// Обработать текст сообщения и вернуть ответный текст
	private TMSend.TMMessage sportHandler(TMRequest tmReq) {
		TMSend.TMMessage tmMessage = new TMSend.TMMessage();
		tmMessage.to_chat_id = tmReq.chat_id;
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_SPORT_TOTAL", "");

		if (tmReq.chat_id.isEmpty()) {
			Logger.add("sportHandler: Error: chat_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_SPORT_FAIL", "chat_id.isEmpty()");
			return null;
		}

		if (tmReq.from_id.isEmpty()) {
			Logger.add("sportHandler: Error: from_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_SPORT_FAIL", "from_id.isEmpty()");
			return null;
		}

		if (!tmReq.isChatPrivate) {
			Logger.add("sportHandler: Error: not isChatPrivate");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_SPORT_FAIL", "not isChatPrivate");
			return null;
		}

		try {
			// Очистить данные
			if (tmReq.sText.startsWith("/sport_del_all")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_DEL_ALL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_DEL_ALL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Удалить исходное сообщение
					//if len(sSRCMessageId) > 0:
					//	API_deleteMessage(sChatId, sSRCMessageId)
					// Удалить
					DB.sport_deleteAll(tmReq, tmReq.chat_id);
					// Статистика
					tmMessage.text = "⛷ Статистика:";
					tmMessage.text += DB.sport_selectStatMonth(tmReq, tmReq.chat_id);
					// Кнопки
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ❔ Справка", "/sport_help"));
					listButton.add(new TMSend.TMButton("⛷ 📘 Статистика", "/sport_stat"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ❌ Очистить данные", "/sport_confirm_all"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Бег", "/sport_add_run"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Лыжи", "/sport_add_sky"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Ходьба", "/sport_add_walk"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Велик", "/sport_add_bike"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Плавание", "/sport_add_swim"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Пр. активности", "/sport_add_other"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Подтвердить очистку данных 3
			else if (tmReq.sText.startsWith("/sport_confirm_all3")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_CONFIRM_ALL3_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_CONFIRM_ALL3_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать вопрос
					tmMessage.text = "⛷ ❌ Я ведь очищу!";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ДА!", "/sport_del_all"));
					listButton.add(new TMSend.TMButton("⛷ Нет", "/sport"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Подтвердить очистку данных 2
			else if (tmReq.sText.startsWith("/sport_confirm_all2")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_CONFIRM_ALL2_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_CONFIRM_ALL2_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать вопрос
					tmMessage.text = "⛷ ❌ Точно очистить?";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ Да", "/sport_confirm_all3"));
					listButton.add(new TMSend.TMButton("⛷ Нет", "/sport"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Подтвердить очистку данных
			else if (tmReq.sText.startsWith("/sport_confirm_all")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_CONFIRM_ALL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_CONFIRM_ALL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать вопрос
					tmMessage.text = "⛷ ❌ Очистить данные?";
					// Показать кнопки Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ Да", "/sport_confirm_all2"));
					listButton.add(new TMSend.TMButton("⛷ Нет", "/sport"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Ввод новой записи: Бег
			else if (tmReq.sText.startsWith("/sport_add_run")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_RUN_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_RUN_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "sport_add_run");
					tmMessage.text = "⛷ ➕ Добавить Бег 1 км = 1 км:";
				}
			}

			// Ввод новой записи: Лыжи
			else if (tmReq.sText.startsWith("/sport_add_sky")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_SKY_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_SKY_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "sport_add_sky");
					tmMessage.text = "⛷ ➕ Добавить Лыжи 1 км = 1 км:";
				}
			}

			// Ввод новой записи: Ходьба
			else if (tmReq.sText.startsWith("/sport_add_walk")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_WALK_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_WALK_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "sport_add_walk");
					tmMessage.text = "⛷ ➕ Добавить Ходьба 1 км = 0,7 км:";
				}
			}

			// Ввод новой записи: Велик
			else if (tmReq.sText.startsWith("/sport_add_bike")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_BIKE_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_BIKE_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "sport_add_bike");
					tmMessage.text = "⛷ ➕ Добавить Велик 1 км = 0,5 км:";
				}
			}

			// Ввод новой записи: Плавание
			else if (tmReq.sText.startsWith("/sport_add_swim")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_SWIM_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_SWIM_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "sport_add_swim");
					tmMessage.text = "⛷ ➕ Добавить Плавание 1 км = 5 км:";
				}
			}

			// Ввод новой записи: Пр. активности
			else if (tmReq.sText.startsWith("/sport_add_other")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_OTHER_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_ADD_OTHER_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "sport_add_other");
					tmMessage.text = "⛷ ➕ Добавить Пр. активности 1 мин = 0,1 км:";
				}
			}

			// Справка
			else if (tmReq.sText.startsWith("/sport_help")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_HELP_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_HELP_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать справку
					tmMessage.text = "----------------------------------------------------";
					tmMessage.text += "\n⛷ ❔ Учёт тренировок:";
					tmMessage.text += "\nУчетная временная зона — " + Main.tzUser.getDisplayName(false, TimeZone.SHORT, Locale.ENGLISH);
					tmMessage.text += "\nУчетная единица измеренния — км";
					tmMessage.text += "\nКоэффициенты:";
					tmMessage.text += "\n  — Бег 1 км = 1 км";
					tmMessage.text += "\n  — Лыжи 1 км = 1 км";
					tmMessage.text += "\n  — Ходьба 1 км = 0,7 км";
					tmMessage.text += "\n  — Велик 1 км = 0,5 км";
					tmMessage.text += "\n  — Плавание 1 км = 5 км";
					tmMessage.text += "\n  — Пр. активности 1 мин = 0,1 км";
					tmMessage.text += "\n----------------------------------------------------";
				}
			}

			// Статистика
			else if (tmReq.sText.startsWith("/sport_stat")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_STAT_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_STAT_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать статистику
					tmMessage.text = "⛷ Статистика:";
					tmMessage.text += DB.sport_selectStatFull(tmReq, tmReq.chat_id);
				}
			}

			// Меню
			else if (tmReq.sText.startsWith("/sport")) {
				if (Main.isBOT_SPORT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "SPORT_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Статистика
					tmMessage.text = "⛷ Статистика:";
					tmMessage.text += DB.sport_selectStatMonth(tmReq, tmReq.chat_id);
					// Кнопки
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ❔ Справка", "/sport_help"));
					listButton.add(new TMSend.TMButton("⛷ 📘 Статистика", "/sport_stat"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ❌ Очистить данные", "/sport_confirm_all"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Бег", "/sport_add_run"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Лыжи", "/sport_add_sky"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Ходьба", "/sport_add_walk"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Велик", "/sport_add_bike"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Плавание", "/sport_add_swim"));
					tmMessage.tmButtons.add(listButton);
					listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("⛷ ➕ Пр. активности", "/sport_add_other"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_SPORT_SUCCESS", "");
			return tmMessage;
		}
		catch (Exception ex) {
			Logger.add("sportHandler: Error: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_SPORT_FAIL", ex.getMessage());
			return null;
		}
	}


	// Мероприятия
	// Обработать текст сообщения и вернуть ответный текст
	private TMSend.TMMessage eventHandler(TMRequest tmReq) {
		TMSend.TMMessage tmMessage = new TMSend.TMMessage();
		tmMessage.to_chat_id = tmReq.chat_id;
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_BUY_TOTAL", "");

		if (tmReq.chat_id.isEmpty()) {
			Logger.add("eventHandler: Error: chat_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_EVENT_FAIL", "chat_id.isEmpty()");
			return null;
		}

		if (tmReq.from_id.isEmpty()) {
			Logger.add("eventHandler: Error: from_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_EVENT_FAIL", "from_id.isEmpty()");
			return null;
		}

		if (!tmReq.isChatPrivate) {
			Logger.add("eventHandler: Error: not isChatPrivate");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_EVENT_FAIL", "not isChatPrivate");
			return null;
		}

		try {
			// Загрузить результаты
			if (tmReq.sText.startsWith("/event_postreq")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_POSTREQ_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_POSTREQ_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					//
					tmMessage.text = "🏆 🚫 Что-то пошло не так";
					// Для организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						tmMessage.text = "🏆 📥 Отправьте текстовый файл:";
						InputWait.add(tmReq.chat_id, tmReq.from_id, "event_postreq " + event);
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Выгрузить заявки
			else if (tmReq.sText.startsWith("/event_getreq")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_GETREQ_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_GETREQ_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					//
					tmMessage.text = "🏆 🚫 Что-то пошло не так";
					// Для организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						// Выгрузить
						String sDoc = DB.event_request_getreq(tmReq, event);
						if (!sDoc.isEmpty()) {
							String sFileName = event + "_out.txt";
							String sFullFileName = Main.TEMP_PATH + "/" + sFileName;
							Utils.writeStrToFile(sFullFileName, sDoc, false);
							// Отправить файл
							tmMessage = new TMSend.TMMessage();
							tmMessage.to_chat_id = tmReq.chat_id;
							tmMessage.file = new File(sFullFileName);
							TMSend.TM_sendDocument(tmMessage);
							tmMessage.text = "🏆 📤 Файл отправлен";
						}
						else
						{
							tmMessage.text = "🏆 📤 Заявок нет";
						}
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Отмена регистрации
			else if (tmReq.sText.startsWith("/event_req_cancel")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_REQ_CANCEL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_REQ_CANCEL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					//
					tmMessage.text = "🏆 🚫 Что-то пошло не так";
					if (DB.event_request_updateRequestByChatId(tmReq, tmReq.chat_id, event, "canceled", "", "", "")) {
						tmMessage.text = "🏆 Регистрация отменена";
					}
				}
			}

			// Подтвердить отмену регистрации 3
			else if (tmReq.sText.startsWith("/event_req_confirm_cancel3")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_REQ_CONFIRM_CANCEL3_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_REQ_CONFIRM_CANCEL3_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// Показать вопрос
					tmMessage.text = "🏆 ❌ Я ведь отменю!";
					// Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🏆 ДА!", "/event_req_cancel " + event));
					listButton.add(new TMSend.TMButton("🏆 Нет", "/event"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Подтвердить отмену регистрации 2
			else if (tmReq.sText.startsWith("/event_req_confirm_cancel2")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_REQ_CONFIRM_CANCEL2_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_REQ_CONFIRM_CANCEL2_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// Показать вопрос
					tmMessage.text = "🏆 ❌ Точно отменить?";
					// Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🏆 Да", "/event_req_confirm_cancel3 " + event));
					listButton.add(new TMSend.TMButton("🏆 Нет", "/event"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Подтвердить отмену регистрации
			else if (tmReq.sText.startsWith("/event_req_confirm_cancel")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_REQ_CONFIRM_CANCEL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_REQ_CONFIRM_CANCEL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// Показать вопрос
					tmMessage.text = "🏆 ❌ Отменить регистрацию?";
					// Да Нет
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🏆 Да", "/event_req_confirm_cancel2 " + event));
					listButton.add(new TMSend.TMButton("🏆 Нет", "/event"));
					tmMessage.tmButtons.add(listButton);
				}
			}

			// Очистка очистку заявок мерпориятия
			else if (tmReq.sText.startsWith("/event_del_all")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_DEL_ALL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_DEL_ALL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					//
					tmMessage.text = "🏆 🚫 Что-то пошло не так";
					// Для организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						//if DB_EVENT_REQUEST_delete(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent):
						if (DB.event_request_delete(tmReq, event)) {
							// Показать список мероприятий
							tmMessage.text = "🏆 Нет доступных мероприятий";
							tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
							ArrayList<DB.event_row> rows = DB.event_selectEvents(tmReq, tmReq.chat_id);
							if (rows != null)
								if (rows.size() > 0) {
									tmMessage.text = "🏆 Заявки очищены";
									for (DB.event_row row : rows) {
										ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
										listButton.add(new TMSend.TMButton("🏆 " + row.icon + row.title, "/event_show " + row.event));
										tmMessage.tmButtons.add(listButton);
									}
								}
							// Справка
							ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🏆 ❔ Справка", "/event_help"));
							tmMessage.tmButtons.add(listButton);
						}
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Подтвердить очистку заявок мерпориятия 3
			else if (tmReq.sText.startsWith("/event_confirm_all3")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_CONFIRM_ALL3_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_CONFIRM_ALL3_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// Показать вопрос
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						tmMessage.text = "🏆 ❌ Я ведь очищу!";
						// Да Нет
						tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
						ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
						listButton.add(new TMSend.TMButton("🏆 ДА!", "/event_del_all " + event));
						listButton.add(new TMSend.TMButton("🏆 Нет", "/event"));
						tmMessage.tmButtons.add(listButton);
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Подтвердить очистку заявок мерпориятия 2
			else if (tmReq.sText.startsWith("/event_confirm_all2")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_CONFIRM_ALL2_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_CONFIRM_ALL2_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// Показать вопрос
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						tmMessage.text = "🏆 ❌ Точно очистить?";
						// Да Нет
						tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
						ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
						listButton.add(new TMSend.TMButton("🏆 Да", "/event_confirm_all3 " + event));
						listButton.add(new TMSend.TMButton("🏆 Нет", "/event"));
						tmMessage.tmButtons.add(listButton);
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Подтвердить очистку заявок мерпориятия
			else if (tmReq.sText.startsWith("/event_confirm_all")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_CONFIRM_ALL_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_CONFIRM_ALL_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// Показать вопрос
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						tmMessage.text = "🏆 ❌ Очистить заявки мероприятия?";
						// Да Нет
						tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
						ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
						listButton.add(new TMSend.TMButton("🏆 Да", "/event_confirm_all2 " + event));
						listButton.add(new TMSend.TMButton("🏆 Нет", "/event"));
						tmMessage.tmButtons.add(listButton);
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Регистрация на мероприятие (Участник)
			else if (tmReq.sText.startsWith("/event_req ")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_REG_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_REG_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					//
					tmMessage.text = "🏆 ➕ Введите свои данные:";
					InputWait.add(tmReq.chat_id, tmReq.from_id, "event_req " + event);
				}
			}

			// Остановить регистрацию (Организатор)
			else if (tmReq.sText.startsWith("/event_close_reg")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_CLOSEREG_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_CLOSEREG_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					//
					tmMessage.text = "🏆 🚫 Что-то пошло не так";
					// Для организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						if (DB.event_updateStatus(tmReq, event, "open")) {
							// Показать список мероприятий
							tmMessage.text = "🏆 Нет доступных мероприятий";
							tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
							ArrayList<DB.event_row> rows = DB.event_selectEvents(tmReq, tmReq.chat_id);
							if (rows != null)
								if (rows.size() > 0) {
									tmMessage.text = "🏆 Доступные мероприятия:";
									for (DB.event_row row : rows) {
										ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
										listButton.add(new TMSend.TMButton("🏆 " + row.icon + row.title, "/event_show " + row.event));
										tmMessage.tmButtons.add(listButton);
									}
								}
							// Справка
							ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🏆 ❔ Справка", "/event_help"));
							tmMessage.tmButtons.add(listButton);
						}
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Стартовать регистрацию (Организатор)
			else if (tmReq.sText.startsWith("/event_open_reg")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_OPENREG_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_OPENREG_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					//
					tmMessage.text = "🏆 🚫 Что-то пошло не так";
					// Для организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						if (DB.event_updateStatus(tmReq, event, "open_reg")) {
							// Показать список мероприятий
							tmMessage.text = "🏆 Нет доступных мероприятий";
							tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
							ArrayList<DB.event_row> rows = DB.event_selectEvents(tmReq, tmReq.chat_id);
							if (rows != null)
								if (rows.size() > 0) {
									tmMessage.text = "🏆 Доступные мероприятия:";
									for (DB.event_row row : rows) {
										ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
										listButton.add(new TMSend.TMButton("🏆 " + row.icon + row.title, "/event_show " + row.event));
										tmMessage.tmButtons.add(listButton);
									}
								}
							// Справка
							ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🏆 ❔ Справка", "/event_help"));
							tmMessage.tmButtons.add(listButton);
						}
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Остановить мероприятие (Организатор)
			else if (tmReq.sText.startsWith("/event_close")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_CLOSE_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_CLOSE_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					//
					tmMessage.text = "🏆 🚫 Что-то пошло не так";
					// Для организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						if (DB.event_updateStatus(tmReq, event, "")) {
							// Показать список мероприятий
							tmMessage.text = "🏆 Нет доступных мероприятий";
							tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
							ArrayList<DB.event_row> rows = DB.event_selectEvents(tmReq, tmReq.chat_id);
							if (rows != null)
								if (rows.size() > 0) {
									tmMessage.text = "🏆 Доступные мероприятия:";
									for (DB.event_row row : rows) {
										ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
										listButton.add(new TMSend.TMButton("🏆 " + row.icon + row.title, "/event_show " + row.event));
										tmMessage.tmButtons.add(listButton);
									}
								}
							// Справка
							ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🏆 ❔ Справка", "/event_help"));
							tmMessage.tmButtons.add(listButton);
						}
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Стартовать мероприятие (Организатор)
			else if (tmReq.sText.startsWith("/event_open")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_OPEN_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_OPEN_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					//
					tmMessage.text = "🏆 🚫 Что-то пошло не так";
					// Для организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						if (DB.event_updateStatus(tmReq, event, "open")) {
							// Показать список мероприятий
							tmMessage.text = "🏆 Нет доступных мероприятий";
							tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
							ArrayList<DB.event_row> rows = DB.event_selectEvents(tmReq, tmReq.chat_id);
							if (rows != null)
								if (rows.size() > 0) {
									tmMessage.text = "🏆 Доступные мероприятия:";
									for (DB.event_row row : rows) {
										ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
										listButton.add(new TMSend.TMButton("🏆 " + row.icon + row.title, "/event_show " + row.event));
										tmMessage.tmButtons.add(listButton);
									}
								}
							// Справка
							ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🏆 ❔ Справка", "/event_help"));
							tmMessage.tmButtons.add(listButton);
						}
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Ввести описание (Организатор)
			else if (tmReq.sText.startsWith("/event_edit")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_EDIT_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_EDIT_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// Для организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						tmMessage.text = "🏆 ✏ Введите описание:\nЗаголовок. Описание";
						InputWait.add(tmReq.chat_id, tmReq.from_id, "event_edit " + event);
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}
			}

			// Показать описание
			else if (tmReq.sText.startsWith("/event_show")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_SHOW_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_SHOW_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Определить event
					Split sp = new Split(tmReq.sText, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// Показать описание мероприятия
					tmMessage.text = "----------------------------------------------------";
					tmMessage.text += "\n🏆 🚫 Еще нет описания";
					tmMessage.text += "\n----------------------------------------------------";
					DB.event_row rowEvent = DB.event_selectDescription(tmReq, tmReq.chat_id, event); // Мероприятие
					DB.event_request_row rowReqEvent = DB.event_request_selectDescription(tmReq, tmReq.chat_id, event); // Заявка
					if (rowEvent != null) {
						// Описание мероприятия
						String description = "🏆 " + rowEvent.icon + rowEvent.title;
						if (!rowEvent.description.isEmpty())
							description += "\n" + rowEvent.description;
						tmMessage.text = "\n----------------------------------------------------";
						tmMessage.text += "\n" + description;
						tmMessage.text += "\n----------------------------------------------------";
						// Заявка участника
						if (rowReqEvent != null) {
							String reqDescription = "🏆 " + rowReqEvent.icon + rowReqEvent.title;
							if (!rowReqEvent.description.isEmpty())
								reqDescription += ". " + rowReqEvent.description;

							if (rowReqEvent.status.equals("accepted")) {
								reqDescription += "\nЗаявка принята";
								if (!rowReqEvent.feedback.isEmpty())
									reqDescription += ": " + rowReqEvent.feedback;
							}
							else if (rowReqEvent.status.equals("rejected")) {
								reqDescription += "\nЗаявка отклонена";
								if (!rowReqEvent.feedback.isEmpty())
									reqDescription += ": " + rowReqEvent.feedback;
							}
							else if (rowReqEvent.status.equals("canceled")) {
								reqDescription += "\nЗаявка отменена участником";
								if (!rowReqEvent.feedback.isEmpty())
									reqDescription += ": " + rowReqEvent.feedback;
							}
							else if (rowReqEvent.status.equals("result")) {
								reqDescription += "\nРезультаты";
								if (!rowReqEvent.feedback.isEmpty())
									reqDescription += ": " + rowReqEvent.feedback;
							}

							tmMessage.text += "\n" + reqDescription;
							tmMessage.text += "\n----------------------------------------------------";
						}
					}
					// Кнопки
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					// Для организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
						listButton.add(new TMSend.TMButton("🏆 ✏️ Ввести описание", "/event_edit " + event));
						tmMessage.tmButtons.add(listButton);
						if (rowEvent != null) {
							if (!rowEvent.status.equals("open") && !rowEvent.status.equals("open_reg")) {
								// Стартовать мероприятие
								listButton = new ArrayList<TMSend.TMButton>();
								listButton.add(new TMSend.TMButton("🏆 Стартовать мероприятие", "/event_open " + event));
								tmMessage.tmButtons.add(listButton);
							}
							if (rowEvent.status.equals("open")) {
								// Остановить мероприятие
								listButton = new ArrayList<TMSend.TMButton>();
								listButton.add(new TMSend.TMButton("🏆 Остановить мероприятие", "/event_close " + event));
								tmMessage.tmButtons.add(listButton);
								// Стартовать регистрацию
								listButton = new ArrayList<TMSend.TMButton>();
								listButton.add(new TMSend.TMButton("🏆 Стартовать регистрацию", "/event_open_reg " + event));
								tmMessage.tmButtons.add(listButton);
							}
							if (rowEvent.status.equals("open_reg")) {
								// Остановить регистрацию
								listButton = new ArrayList<TMSend.TMButton>();
								listButton.add(new TMSend.TMButton("🏆 Остановить регистрацию", "/event_close_reg " + event));
								tmMessage.tmButtons.add(listButton);
							}
							// Выгрузить заявки, Загрузить результаты
							listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🏆 📤 Выгрузить заявки", "/event_getreq " + event));
							listButton.add(new TMSend.TMButton("🏆 📥 Загрузить результаты", "/event_postreq " + event));
							tmMessage.tmButtons.add(listButton);
							// Очистить заявки
							listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🏆 ❌ Очистить заявки", "/event_confirm_all " + event));
							tmMessage.tmButtons.add(listButton);
						}
					}
					// Для участников
					if (rowEvent != null) {
						if (rowEvent.status.equals("open_reg")) {
							if (rowReqEvent != null) {
								if (!rowReqEvent.status.equals("canceled")) {
									// Внести изменения, Отменить регистрацию
									ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
									listButton.add(new TMSend.TMButton("🏆 ➕ Внести изменения", "/event_req " + event));
									listButton.add(new TMSend.TMButton("🏆 ❌ Отменить регистрацию", "/event_req_confirm_cancel " + event));
									tmMessage.tmButtons.add(listButton);
								}
								else
								{
									// Внести изменения
									ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
									listButton.add(new TMSend.TMButton("🏆 ➕ Внести изменения", "/event_req " + event));
									tmMessage.tmButtons.add(listButton);
								}
							}
							else
							{
								// Регистрация на мероприятие
								ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
								listButton.add(new TMSend.TMButton("🏆 ➕ Регистрация на мероприятие", "/event_req " + event));
								tmMessage.tmButtons.add(listButton);
							}
						}
					}
				}
			}

			// Справка
			else if (tmReq.sText.startsWith("/event_help")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_HELP_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_HELP_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Только для организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, "*");
					if (isOrg) {
						tmMessage.text = "----------------------------------------------------";
						tmMessage.text += "\n🏆 ❔ Инструкция для Организатора\n " + Main.BOT_EVENT_DOC;
						tmMessage.text += "\n----------------------------------------------------";
					}
				}
			}

			// Список
			else if (tmReq.sText.startsWith("/event")) {
				if (Main.isBOT_EVENT) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "EVENT_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Показать список мероприятий
					tmMessage.text = "🏆 Нет доступных мероприятий";
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<DB.event_row> rows = DB.event_selectEvents(tmReq, tmReq.chat_id);
					if (rows != null)
						if (rows.size() > 0) {
							tmMessage.text = "🏆 Доступные мероприятия:";
							for (DB.event_row row : rows) {
								ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
								listButton.add(new TMSend.TMButton("🏆 " + row.icon + row.title, "/event_show " + row.event));
								tmMessage.tmButtons.add(listButton);
							}
						}
					// Справка для организатора
					boolean isOrg = Main.config.matchValueInList("bot_event_org", ".+:" + tmReq.chat_id);
					if (isOrg) {
						ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
						listButton.add(new TMSend.TMButton("🏆 ❔ Справка", "/event_help"));
						tmMessage.tmButtons.add(listButton);
					}
				}
			}

			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_EVENT_SUCCESS", "");
			return tmMessage;
		}
		catch (Exception ex) {
			Logger.add("eventHandler: Error: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_EVENT_FAIL", ex.getMessage());
			return null;
		}
	}


	// Обратная связь
	// Обработать текст сообщения и вернуть ответный текст
	private TMSend.TMMessage feedbackHandler(TMRequest tmReq) {
		TMSend.TMMessage tmMessage = new TMSend.TMMessage();
		tmMessage.to_chat_id = tmReq.chat_id;
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_FEEDBACK_TOTAL", "");

		if (tmReq.chat_id.isEmpty()) {
			Logger.add("feedbackHandler: Error: chat_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_FEEDBACK_FAIL", "chat_id.isEmpty()");
			return null;
		}

		if (tmReq.from_id.isEmpty()) {
			Logger.add("feedbackHandler: Error: from_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_FEEDBACK_FAIL", "from_id.isEmpty()");
			return null;
		}

		if (!tmReq.isChatPrivate) {
			Logger.add("feedbackHandler: Error: not isChatPrivate");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_FEEDBACK_FAIL", "not isChatPrivate");
			return null;
		}

		try {
			// Ответить на указанное сообщение
			if (tmReq.sText.startsWith("/feedback_replyto")) {
				if (Main.isBOT_FEEDBACK) {
					if (tmReq.chat_id.equals(Main.BOT_OWNER_CHAT_ID)) {
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "FEEDBACK_REPLYTO_TOTAL", "");
						Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "FEEDBACK_REPLYTO_SUCCESS", "");
						InputWait.add(tmReq.chat_id, tmReq.from_id, "feedback_replyto");
						// Показать подсказку
						tmMessage.text = "✉️ Отправить ответ:";
						tmMessage.text += "\nid текст сообщения";
					}
				}
			}

			// Меню
			else if (tmReq.sText.startsWith("/feedback")) {
				if (Main.isBOT_FEEDBACK) {
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "FEEDBACK_TOTAL", "");
					Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "FEEDBACK_SUCCESS", "");
					InputWait.add(tmReq.chat_id, tmReq.from_id, "feedback");
					// Показать подсказку
					tmMessage.text = "✉️ Напиши сообщение моему хозяину, я передам. Но он человек занятой и не факт, что ответит:";
				}
			}

			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_FEEDBACK_SUCCESS", "");
			return tmMessage;
		}
		catch (Exception ex) {
			Logger.add("feedbackHandler: Error: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_FEEDBACK_FAIL", ex.getMessage());
			return null;
		}
	}


	// Справка
	// Обработать текст сообщения и вернуть ответный текст
	private TMSend.TMMessage helpHandler(TMRequest tmReq) {
		TMSend.TMMessage tmMessage = new TMSend.TMMessage();
		tmMessage.to_chat_id = tmReq.chat_id;
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_HELP_TOTAL", "");

		if (tmReq.chat_id.isEmpty()) {
			Logger.add("helpHandler: Error: chat_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_HELP_FAIL", "chat_id.isEmpty()");
			return null;
		}

		if (tmReq.from_id.isEmpty()) {
			Logger.add("helpHandler: Error: from_id.isEmpty()");
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_HELP_FAIL", "from_id.isEmpty()");
			return null;
		}

		try {
			// Справка
			if (tmReq.sText.startsWith("/help")) {
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "HELP_TOTAL", "");
				Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "HELP_SUCCESS", "");
				InputWait.add(tmReq.chat_id, tmReq.from_id, "");

				// Сформировать справку
				tmMessage.text = "----------------------------------------------------";
				tmMessage.text += "\n❔ Функции бота:";

				if (Main.isBOT_VCONTACT) {
					tmMessage.text += "\n— ВКС — создание личного или командного списка конференций со ссылками";
				}

				if (Main.isBOT_BUY) {
					tmMessage.text += "\n— Покупки — создание личного или командного списка покупок:";
					tmMessage.text += "\n        — отправить гонца за покупками для команды";
					tmMessage.text += "\n        — ежедневные семейные покупки: кто возвращается с работы через магазины, тот и затаривается";
					tmMessage.text += "\n        — личный список покупок";
				}

				if (Main.isBOT_SPORT) {
					tmMessage.text += "\n— Спорт — учёт тренировок, см. /sport_help@zda_gadget_bot";
				}

				if (Main.isBOT_EVENT) {
					tmMessage.text += "\n— Мероприятия — регистрация на мероприятия";
				}

				tmMessage.text += "\n----------------------------------------------------";
			}

			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_HELP_SUCCESS", "");
			return tmMessage;
		}
		catch (Exception ex) {
			Logger.add("helpHandler: Error: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_HELP_FAIL", ex.getMessage());
			return null;
		}
	}


	// Все другие сообщения пользователя
	// Вчастности  вводимый текст
	// Обработать текст сообщения и вернуть ответный текст
	private TMSend.TMMessage otherHandler(TMRequest tmReq) {
		TMSend.TMMessage tmMessage = new TMSend.TMMessage();
		tmMessage.to_chat_id = tmReq.chat_id;
		Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_OTHER_TOTAL", "");

		if (tmReq.chat_id.isEmpty()) {
			Logger.add("otherHandler: Error: chat_id.isEmpty()");
			//Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_OTHER_FAIL", "chat_id.isEmpty()");
			return null;
		}

		if (tmReq.from_id.isEmpty()) {
			Logger.add("otherHandler: Error: from_id.isEmpty()");
			//Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_OTHER_FAIL", "from_id.isEmpty()");
			return null;
		}

		try {
			String sAnswer = InputWait.check(tmReq.chat_id, tmReq.from_id);

			// ВКС
			// Введено значение
			if (Main.isBOT_VCONTACT) {
				if (sAnswer.equals("vcontact_add")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Добавить новую ВКС
					DB.vcontact_insert(tmReq, tmReq.sText);
					// Показать все ВКС в форме кнопок
					tmMessage.text = "🔵 Список пуст";
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🔵 ➕ Добавить ВКС", "/vcontact_add"));
					tmMessage.tmButtons.add(listButton);
					ArrayList<DB.vcontact_row> rows = DB.vcontact_selectByChatId(tmReq, tmReq.chat_id);
					if (rows != null)
						if (rows.size() > 0) {
							tmMessage.text = "🔵 Текущие ВКС:";
							listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🔵 ❌ Очистить список", "/vcontact_confirm_all"));
							tmMessage.tmButtons.add(listButton);
							for (DB.vcontact_row row : rows) {
								listButton = new ArrayList<TMSend.TMButton>();
								listButton.add(new TMSend.TMButton("🔵 " + row.name, "/vcontact_confirm " + row.id));
								tmMessage.tmButtons.add(listButton);
							}
						}
				}
			}

			// Покупки
			// Введено значение
			if (Main.isBOT_BUY) {
				if (sAnswer.equals("buy_add")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Добавить новую покупку
					DB.buy_insert(tmReq, tmReq.sText);
					// Показать все покупки в форме кнопок
					tmMessage.text = "🧀 Список пуст";
					tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
					ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
					listButton.add(new TMSend.TMButton("🧀 ➕ Добавить покупку", "/buy_add"));
					tmMessage.tmButtons.add(listButton);
					ArrayList<DB.buy_row> rows = DB.buy_selectByChatId(tmReq, tmReq.chat_id);
					if (rows != null)
						if (rows.size() > 0) {
							tmMessage.text = "🧀 Текущие покупки:";
							listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🧀 ❌ Очистить список", "/buy_confirm_all"));
							tmMessage.tmButtons.add(listButton);
							for (DB.buy_row row : rows) {
								listButton = new ArrayList<TMSend.TMButton>();
								if (tmReq.isChatPrivate)
									listButton.add(new TMSend.TMButton("🧀 " + row.name, "/buy_confirm " + row.id));
								else
									listButton.add(new TMSend.TMButton("🧀 " + row.from_name + ": " + row.name, "/buy_confirm " + row.id));
								tmMessage.tmButtons.add(listButton);
							}
						}
				}
			}

			// Спорт
			// Введено значение
			if (Main.isBOT_SPORT) {
				if (sAnswer.equals("sport_add_run")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Добавить значение
					double fCount = 0.0;
					String s = Utils.isNull(tmReq.sText, "").trim();
					s = s.replace(',', '.');
					try {
						fCount = Double.parseDouble(s);
					}
					catch (Exception ex) {
						fCount = 0.0;
					}
					if (fCount <= 0.0 || fCount > 1000000.0) {
						tmMessage.text = "⛷ 🚫 Я так не умею";
					}
					else {
						DB.sport_insert(tmReq, "RUN", "", fCount);
						tmMessage.text = "⛷ Принято: Бег " + fCount + " км";
						tmMessage.text += "\nСтатистика:";
						tmMessage.text += DB.sport_selectStatMonth(tmReq, tmReq.chat_id);
					}
				}

				else if (sAnswer.equals("sport_add_sky")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Добавить значение
					double fCount = 0.0;
					String s = Utils.isNull(tmReq.sText, "").trim();
					s = s.replace(',', '.');
					try {
						fCount = Double.parseDouble(s);
					}
					catch (Exception ex) {
						fCount = 0.0;
					}
					if (fCount <= 0.0 || fCount > 1000000.0) {
						tmMessage.text = "⛷ 🚫 Я так не умею";
					}
					else {
						DB.sport_insert(tmReq, "SKY", "", fCount);
						tmMessage.text = "⛷ Принято: Лыжи " + fCount + " км";
						tmMessage.text += "\nСтатистика:";
						tmMessage.text += DB.sport_selectStatMonth(tmReq, tmReq.chat_id);
					}
				}

				else if (sAnswer.equals("sport_add_walk")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Добавить значение
					double fCount = 0.0;
					String s = Utils.isNull(tmReq.sText, "").trim();
					s = s.replace(',', '.');
					try {
						fCount = Double.parseDouble(s);
					}
					catch (Exception ex) {
						fCount = 0.0;
					}
					if (fCount <= 0.0 || fCount > 1000000.0) {
						tmMessage.text = "⛷ 🚫 Я так не умею";
					}
					else {
						DB.sport_insert(tmReq, "WALK", "", fCount);
						tmMessage.text = "⛷ Принято: Ходьба " + fCount + " км";
						tmMessage.text += "\nСтатистика:";
						tmMessage.text += DB.sport_selectStatMonth(tmReq, tmReq.chat_id);
					}
				}

				else if (sAnswer.equals("sport_add_bike")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Добавить значение
					double fCount = 0.0;
					String s = Utils.isNull(tmReq.sText, "").trim();
					s = s.replace(',', '.');
					try {
						fCount = Double.parseDouble(s);
					}
					catch (Exception ex) {
						fCount = 0.0;
					}
					if (fCount <= 0.0 || fCount > 1000000.0) {
						tmMessage.text = "⛷ 🚫 Я так не умею";
					}
					else {
						DB.sport_insert(tmReq, "BIKE", "", fCount);
						tmMessage.text = "⛷ Принято: Велик " + fCount + " км";
						tmMessage.text += "\nСтатистика:";
						tmMessage.text += DB.sport_selectStatMonth(tmReq, tmReq.chat_id);
					}
				}

				else if (sAnswer.equals("sport_add_swim")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Добавить значение
					double fCount = 0.0;
					String s = Utils.isNull(tmReq.sText, "").trim();
					s = s.replace(',', '.');
					try {
						fCount = Double.parseDouble(s);
					}
					catch (Exception ex) {
						fCount = 0.0;
					}
					if (fCount <= 0.0 || fCount > 1000000.0) {
						tmMessage.text = "⛷ 🚫 Я так не умею";
					}
					else {
						DB.sport_insert(tmReq, "SWIM", "", fCount);
						tmMessage.text = "⛷ Принято: Плавание " + fCount + " км";
						tmMessage.text += "\nСтатистика:";
						tmMessage.text += DB.sport_selectStatMonth(tmReq, tmReq.chat_id);
					}
				}

				else if (sAnswer.equals("sport_add_other")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					// Добавить значение
					double fCount = 0.0;
					String s = Utils.isNull(tmReq.sText, "").trim();
					s = s.replace(',', '.');
					try {
						fCount = Double.parseDouble(s);
					}
					catch (Exception ex) {
						fCount = 0.0;
					}
					if (fCount <= 0.0 || fCount > 1000000.0) {
						tmMessage.text = "⛷ 🚫 Я так не умею";
					}
					else {
						DB.sport_insert(tmReq, "OTHER", "", fCount);
						tmMessage.text = "⛷ Принято: пр. активности " + fCount + " мин";
						tmMessage.text += "\nСтатистика:";
						tmMessage.text += DB.sport_selectStatMonth(tmReq, tmReq.chat_id);
					}
				}
			}

			// Мероприятие
			// Введено значение
			if (Main.isBOT_EVENT) {
				// Отправлен файл
				if (sAnswer.startsWith("event_postreq")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					tmMessage.text = "✉️ 🚫 Что-то пошло не так";
					// Определить event
					Split sp = new Split(sAnswer, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// Проверить права организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						String ext = Utils.getFileExt(tmReq.sDocFileName);
						if (!ext.equalsIgnoreCase("txt")) {
							tmMessage.text = "🏆 🚫 Нужен файл *.txt";
						}
						else {
							String sDoc = Utils.isNull(TMSend.TM_getFile(tmReq, tmReq.sDocFileId), "");
							if (sDoc.isEmpty()) {
								tmMessage.text = "🏆 🚫 Не удалось получить файл";
							}
							else {
								if (DB.event_request_updateRequestList(tmReq, event, sDoc))
									tmMessage.text = "🏆 Данные загружены";
								else
									tmMessage.text = "🏆 🚫 Ошибка загрузки данных";
							}
						}
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}

				// Введено описание мероприятия
				else if (sAnswer.startsWith("event_edit")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					tmMessage.text = "🏆 🚫 Что-то пошло не так";
					// Определить event
					Split sp = new Split(sAnswer, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// Проверить права организатора
					boolean isOrg = DB.event_isOrg(tmReq.chat_id, event);
					if (isOrg) {
						if (DB.event_updateDescription(tmReq, event, tmReq.sText)) {
							// Показать список мероприятий
							tmMessage.text = "🏆 Нет доступных мероприятий";
							tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
							ArrayList<DB.event_row> rows = DB.event_selectEvents(tmReq, tmReq.chat_id);
							if (rows != null)
								if (rows.size() > 0) {
									tmMessage.text = "🏆 Доступные мероприятия:";
									for (DB.event_row row : rows) {
										ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
										listButton.add(new TMSend.TMButton("🏆 " + row.icon + row.title, "/event_show " + row.event));
										tmMessage.tmButtons.add(listButton);
									}
								}
							// Справка
							ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🏆 ❔ Справка", "/event_help"));
							tmMessage.tmButtons.add(listButton);
						}
					}
					else
					{
						tmMessage.text = "🏆 🚫 Вы не являетесь организатором";
					}
				}

				// Введена регистрация на мероприятие
				else if (sAnswer.startsWith("event_req")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					tmMessage.text = "🏆 🚫 Что-то пошло не так";
					// Определить event
					Split sp = new Split(sAnswer, ' ', "");
					String event = Utils.isNull(sp.s2, "").trim();
					// if DB_EVENT_REQUEST_updateRequest(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent, sText, iGMT):
					if (DB.event_request_updateRequest(tmReq, tmReq.chat_id, event, tmReq.sText)) {
						// Показать список мероприятий
						tmMessage.text = "🏆 Нет доступных мероприятий";
						tmMessage.tmButtons = new ArrayList<ArrayList<TMSend.TMButton>>();
						ArrayList<DB.event_row> rows = DB.event_selectEvents(tmReq, tmReq.chat_id);
						if (rows != null)
							if (rows.size() > 0) {
								tmMessage.text = "🏆 ➕ Заявка отправлена";
								for (DB.event_row row : rows) {
									ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
									listButton.add(new TMSend.TMButton("🏆 " + row.icon + row.title, "/event_show " + row.event));
									tmMessage.tmButtons.add(listButton);
								}
							}
						// Справка
						if (DB.event_isOrg(tmReq.chat_id, event)) {
							ArrayList<TMSend.TMButton> listButton = new ArrayList<TMSend.TMButton>();
							listButton.add(new TMSend.TMButton("🏆 ❔ Справка", "/event_help"));
							tmMessage.tmButtons.add(listButton);
						}
					}
				}
			}


			// Обратная связь
			// Введено значение
			if (Main.isBOT_FEEDBACK) {
				if (sAnswer.equals("feedback")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					tmMessage.text = "✉️ 🚫 Что-то пошло не так";
					// Отправить
					if (tmReq.sText.isEmpty()) {
						tmMessage.text = "✉️ 🚫 Пустые сообщения не отправляю";
					}
					else
					{
						if (DB.feedback_insert(tmReq, tmReq.sText, "IN", "SENT", 0))
							tmMessage.text = "✉️ Сообщение принято";
					}
				}

				else if (sAnswer.equals("feedback_replyto")) {
					InputWait.add(tmReq.chat_id, tmReq.from_id, "");
					tmMessage.text = "✉️ 🚫 Что-то пошло не так";
					// Определить id
					Split sp = new Split(tmReq.sText, ' ', "");
					String sFeedbackId = Utils.isNull(sp.s1, "").trim();
					String sText2 = Utils.isNull(sp.s2, "").trim();
					int iFeedbackId = 0;
					try {
						iFeedbackId = Integer.parseInt(sFeedbackId);
					}
					catch (Exception ex) {
						iFeedbackId = 0;
					}
					Logger.add("otherHandler: feedback_replyto: iFeedbackId=" + iFeedbackId + ", sText2=" + sText2);
					if (iFeedbackId <= 0) {
						tmMessage.text = "✉️ 🚫 Не указан id";
					}
					else if (sText2.isEmpty()) {
						tmMessage.text = "✉️ 🚫 Пустые сообщения не отправляю";
					}
					else {
						// Проверить существование сообщения id = iFeedbackId
						if (DB.feedback_checkById(tmReq, iFeedbackId)) {
							// Отправить ответ
							if (DB.feedback_insert(tmReq, sText2, "OUT", "SENT", iFeedbackId))
								tmMessage.text = "✉️ Сообщение отправлено";
						}
						else
						{
							tmMessage.text = "✉️ 🚫 Не найдено сообщение id=" + iFeedbackId;
						}
					}
				}
			}


			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_OTHER_SUCCESS", "");
			return tmMessage;
		}
		catch (Exception ex) {
			Logger.add("otherHandler: Error: " + ex.getMessage());
			Monitor.add(tmReq.chat_id, tmReq.chat_name, tmReq.from_id, tmReq.from_username, tmReq.from_name, "BOT_HANDLER_OTHER_FAIL", ex.getMessage());
			return null;
		}
	}


}
