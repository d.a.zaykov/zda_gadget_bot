package ru.zdame.zda_gadget_bot;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.io.FileWriter;

public class Logger extends Thread {

	private static volatile boolean killed = false; // Признак завершения программы
	private static final int iPeriod = 7; // Период сброса сообщений на диск (сек)
	private static final Object lock = new Object(); // Объект для синхронизации потоков
	private static final int capacityLog = 100000; // Начальная длина listLog в символах. При маленьком capacity мусор собирается лучше. При большом capacity - меньше копирования внутри билдера.
	private static volatile StringBuilder listLog = new StringBuilder(capacityLog);
	public static volatile String fileName = ""; // Полное имя файла лога

	@Override
	public void run()
	{
		add("Logger: started");
		try {Thread.sleep(iPeriod*1000);} catch (Exception ex) {}
		while (!killed) {
			String sError = "";
			try {
				// Было бы не плохо собрать мусор
				System.gc(); // Без этого хрен дождешься пока сборщик начнет работу

				// Копировать список сообщений
				int lenLog = 0; // Для мониторинга нагрузки на логгер
				String s = "";
				synchronized (lock) {
					lenLog = listLog.length();
					s = listLog.toString();
					if (lenLog > capacityLog)
						listLog = new StringBuilder(capacityLog);
					else
						listLog.setLength(0);
				}

				// Запись сообщений на диск
				if (s != null)
					if (!s.isEmpty())
						Utils.writeStrToFile(fileName, s, true);

				// Мониторинг нагрузки на логгер
				if (lenLog > capacityLog) {
					Logger.add("Logger.run: Warning: length=" + lenLog + ", capacityLog=" + capacityLog);
					Monitor.add("", "", "", "", "", "LOGGER_WARN", "length=" + lenLog + ", capacityLog=" + capacityLog);
				}
			}
			catch (Exception ex)
			{
				add("Logger.run: Error: " + ex.getMessage());
			}

			if (!killed)
				try {Thread.sleep(iPeriod*1000);} catch (Exception ex) {}
		}
		add("Logger: stopped");
	}

	public static void stopLogger() {
		Logger.killed = true;
		flush();
	}

	// Сброс логов на диск
	public static void flush() {
		add("Logger.flush");

		// Копировать список сообщений
		String s = "";
		synchronized (lock) {
			s = listLog.toString();
			listLog = new StringBuilder();
		}

		// Запись сообщений на диск
		if (s != null)
			if (!s.isEmpty())
				Utils.writeStrToFile(fileName, s, true);
	}

	// Добавить сообщение в лог
	public static void add(String s) {
		System.out.println(Utils.date2str(LocalDateTime.now()) + ": " + s);

		// Добавить в список сообщений
		synchronized (lock) {
			//listLog.add(Utils.date2str(LocalDateTime.now()) + ": " + s);
			listLog.append(Utils.date2str(LocalDateTime.now()) + ": " + s + "\n");
		}
	}

}