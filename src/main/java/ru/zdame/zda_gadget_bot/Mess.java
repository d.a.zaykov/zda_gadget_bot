package ru.zdame.zda_gadget_bot;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Pattern;


// Фоновая обработка сообщений с обратной связью
public class Mess extends Thread {

	private static volatile boolean killed = false; // Признак завершения программы
	private static final int iPeriod = 17; // Период проверки новых сообщений (сек)
	private static ArrayList<String> listPath = null; // Наблюдаемый список папок

	@Override
	public void run()
	{
		Logger.add("Mess: started");
		try {Thread.sleep(iPeriod*1000);} catch (Exception ex) {}

		// Получить список наблюдаемых папок
		listPath = Main.config.getList("bot_mess_path");

		while (!killed) {
			try {
				// Обработать новые сообщения
				handler();
			}
			catch (Exception ex)
			{
				Logger.add("Mess.run: Error: " + ex.getMessage());
				Monitor.add("", "", "", "", "", "MESS_THREAD_FAIL", ex.getMessage());
			}

			if (!killed)
				try {Thread.sleep(iPeriod*1000);} catch (Exception ex) {}
		}
		Logger.add("Mess: stopped");
	}

	public static void stopMess() {
		killed = true;
	}

	// Обработать новые сообщения
	private static void handler() {
		try {
			//Logger.add("Mess.handler");
			Monitor.add("", "", "", "", "", "MESS_HANDLER_TOTAL", "");

			if (listPath == null)
				return;
			if (listPath.size() == 0)
				return;

			for (String s: listPath) {
				s = Utils.isNull(s, "").trim();

				// Определить получателя и путь
				Split sp = new Split(s, ':', "");
				String sPath = sp.s2.trim(); // Это путь
				//Logger.add("Mess.handler: sPath = [" + sPath + "]");

				// Получить список файлов в папке sPath
				File dir = new File(sPath);
				File[] arrFiles = dir.listFiles();
				for (int i = 0; i < arrFiles.length; i++) {
					if (killed)
						break;

					if (!arrFiles[i].isFile())
						continue;

					String sFullFileName = arrFiles[i].getPath();
					String sFileName = arrFiles[i].getName();
					//Logger.add("Mess.handler: i=" + i + ", filename=[" + sFileName + "]");

					// Определить тип сообщения
					if (Pattern.matches(".*.[t|T][x|X][t|T]", sFileName)) {
						// Текстовое сообщение из файла *mess.txt
						Logger.add("Mess.handler: Текстовый файл [" + sFileName + "]");

						// Прочитать сообщение
						String sText = Utils.readFileToStr(sFullFileName);
						sText = Utils.isNull(sText, "").trim();
						String sText2 = Utils.strtrunc(sText, 1000);

						// Отправить сообщение всем получателям папки sPath
						for (String ss: listPath) {
							ss = Utils.isNull(ss, "").trim();
							Split sp2 = new Split(ss, ':', "");
							String target_chat_id = sp2.s1.trim(); // Это получатель
							String sPath2 = sp2.s2.trim(); // Это путь
							if (sPath2.equals(sPath)) {
								//Logger.add("Mess.handler: target_chat_id = [" + target_chat_id + "]");

								TMSend.TMMessage tmMessage = new TMSend.TMMessage();
								tmMessage.to_chat_id = target_chat_id;
								tmMessage.text = "📩 Серверное сообщение: \n" + sText2;
								tmMessage.prior = 2;
								TMSend.send(tmMessage);

								// Длинное сообщение продублировать файлом
								if (sText.length() != sText2.length()) {
									tmMessage = new TMSend.TMMessage();
									tmMessage.to_chat_id = target_chat_id;
									tmMessage.file = arrFiles[i];
									TMSend.TM_sendDocument(tmMessage);
								}
							}
						}

						// Удалить файл
						arrFiles[i].delete();
					} else {
						// Бинарный файл
						Logger.add("Mess.handler: Бинарный файл [" + sFileName + "]");

						// Отправить файл всем получателям папки sPath
						for (String ss: listPath) {
							ss = Utils.isNull(ss, "").trim();
							Split sp2 = new Split(ss, ':', "");
							String target_chat_id = sp2.s1.trim(); // Это получатель
							String sPath2 = sp2.s2.trim(); // Это путь
							if (sPath2.equals(sPath)) {
								//Logger.add("Mess.handler: target_chat_id = [" + target_chat_id + "]");

								TMSend.TMMessage tmMessage = new TMSend.TMMessage();
								tmMessage.to_chat_id = target_chat_id;
								tmMessage.file = arrFiles[i];
								TMSend.TM_sendDocument(tmMessage);
							}
						}

						// Удалить файл
						arrFiles[i].delete();
					}
				} // for
			}

			Monitor.add("", "", "", "", "", "MESS_HANDLER_SUCCESS", "");
		}
		catch (Exception ex)
		{
			Logger.add("Mess.handler: Error: " + ex.getMessage());
			Monitor.add("", "", "", "", "", "MESS_HANDLER_FAIL", ex.getMessage());
		}
	}

}