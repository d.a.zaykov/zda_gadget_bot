import sys
import os
import signal
import re
#import random
from datetime import timedelta
from datetime import datetime
import calendar
import time
import threading
import json
#import base64
import requests
#import urllib.request
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import pymysql


# Глобальные параметры
bKILLED = False # True - получен сигнал SIGTERM/SIGINT. Необходимо завершить приложение

sAPP_NAME = "zda_gadget_bot"
sAPP_VERSION = "dev"
sAPP_PATH = os.path.dirname(os.path.abspath(__file__))  # Путь до приложения
sCONF = ""	# Имя фала конфига
sTEMP = ""	# Путь до папки с временными файлами
iPORT = 0	# Порт для прослушивания запросов
sLOG = ""	# Имя фала для журнала
sBOT_TOKEN = "" # Токен бота 0000000000:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
sTM_URL = "" # Базовый URL API "https://api.telegram.org/bot%s/" % sBOT_TOKEN
sTM_FILE_URL = "" # Базовый URL API "https://api.telegram.org/file/bot%s/" % sBOT_TOKEN
sBOT_URL = "" # URL приложения "https://www.zdame.ru/zda_gadget_bot"
iGMT = 0	# Часовой пояс X
sGMT_DESC = ""	# Часовой пояс GMT+X

sDB_HOST = "" # Хост базы
sDB_NAME = "" # Имя базы
sDB_USER = "" # Имя юзера
sDB_PASS = "" # Пароль
dbConnect = None # Коннект к БД
iDB_LONGTIME = 1 # Отслеживать запросы в БД длительностью более N сек. Отбрасывается метрика "%_LONGTIME"
lockDB = threading.Lock() # Блокировщик для синхронизации потоков. Подключение к БД
dtDBConnectTicker = None # Время последнего подключения к БД

sBOT_VCONTACT = "" # yes - активация функции списка ВКС. Нужна БД mysql
sBOT_BUY = "" # yes - активация функции списка покупок. Нужна БД mysql
sBOT_SPORT = "" # yes - активация функции спорта. Нужна БД mysql
sBOT_FEEDBACK = "" # yes - активация функции обратной связи Нужна БД mysql
sBOT_MESS = "" # yes - активация функции серверных сообщений
sBOT_MESS_PATH = ""	# Путь до папки с входящими сообщениями
sBOT_OWNER_CHAT_ID = "" # Владелец бота для обратной связи

sBOT_EVENT = "" # yes - активация функции "Мероприятия". Нужна БД mysql
lstBOT_EVENT_ORG = list()	# Список зарегистрированных мероприятий и их организаторов
sBOT_EVENT_DOC = "" # URL на инструкцию для организатора мероприятия

lockINPUT = threading.Lock() # Блокировщик для синхронизации потоков. Ввод данных пользоателем
dictINPUT = dict() # Признак ожидания ввода данных



# ===================================================================================================
# SIGTERM/SIGINT
# ===================================================================================================

def SignalHandler(sig, frame):
	global bKILLED
	bKILLED = True
	LOG("SignalHandler: sig=" + str(sig))
	MONITOR("", "", "", "", "", "SIGNAL", "sig=" + str(sig))
# def

signal.signal(signal.SIGINT, SignalHandler)
signal.signal(signal.SIGTERM, SignalHandler)


# ===================================================================================================
# Лимиты
# ===================================================================================================

dictLIMIT = dict()	# Зарегистрированные счетчики и их значения
lockLIMIT = threading.Lock() # Блокировщик для синхронизации потоков
iLIMIT_TIMEOUT = 1 # Периодичность вытекания из бассейна (сек)

class LIMIT_Thread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.name = "LIMIT_Thread"
	# def init

	def run(self):
		global bKILLED
		global lockLIMIT
		global dictLIMIT
		global iLIMIT_TIMEOUT
		LOG("LIMIT_Thread: Started")
		while True:
			if bKILLED:
				break
			sError = ""
			lockLIMIT.acquire()
			try:
				# Пройти по всем счетчикамм и вычесть из burst значение limit
				for sName in dictLIMIT:
					dBurst = float(dictLIMIT[sName]["burst"])
					dLimit = float(dictLIMIT[sName]["limit"])
					dValue = float(dictLIMIT[sName]["value"])
					if dValue >= dLimit:
						dValue = round(dValue - dLimit, 1)
					else:
						dValue = 0.0
					# if
					dictLIMIT[sName]["value"] = dValue
				# for
			except Exception as ex:
				sError = str(ex)
			finally:
				lockLIMIT.release()
			# end try

			if len(sError) > 0:
				LOG("LIMIT_Thread.run: Error: " + sError)
				MONITOR("", "", "", "", "", "LIMIT_THREAD_FAIL", sError)

			# Спим
			if not bKILLED:
				time.sleep(iLIMIT_TIMEOUT)
		# while
		print("LIMIT_Thread: Stopped")
	# def run
# class LIMIT_Thread


# Регистрация счетчика
# sName - имя счетчика
# dBurst - объем бассейна/ведра. Точность 1/10
# dLimit - количество вытекания из бассейна в сек. Точность 1/10
def LIMIT_reg(sName: str, dBurst: float, dLimit: float):
	global lockLIMIT
	global dictLIMIT
	lockLIMIT.acquire()
	try:
		if sName not in dictLIMIT:
			dictLIMIT[sName] = {'burst': dBurst, 'limit': dLimit, 'value': 0.0}
	except Exception as ex:
		print("LIMIT_reg: Error: " + str(ex))
	finally:
		lockLIMIT.release()
	# end try
# def LIMIT_reg

# Проверить, можно ли добавлять значение k в бассейн
# Если бассейн уже полон, то вернуть False
# Если такой счетчик не найден, то вернуть False
def LIMIT_check(sName: str, k: float) -> bool:
	global lockLIMIT
	global dictLIMIT
	lockLIMIT.acquire()
	try:
		if sName in dictLIMIT:
			dBurst = float(dictLIMIT[sName]["burst"])
			dValue = float(dictLIMIT[sName]["value"])
			if round(dValue + k, 1) > dBurst:
				return False
			else:
				return True
			# if
		else:
			return False
		# if
	except Exception as ex:
		print("LIMIT_check: Error: " + str(ex))
		return False
	finally:
		lockLIMIT.release()
	# end try
# def LIMIT_check

# Добавить значение k в бассейн
# Если такой счетчик не найден, то ничего не делать
def LIMIT_add(sName: str, k: float):
	global lockLIMIT
	global dictLIMIT
	lockLIMIT.acquire()
	try:
		if sName in dictLIMIT:
			dValue = float(dictLIMIT[sName]["value"])
			dictLIMIT[sName]["value"] = round(dValue + k, 1)
		# if
	except Exception as ex:
		print("LIMIT_add: Error: " + str(ex))
	finally:
		lockLIMIT.release()
	# end try
# def LIMIT_add






# ===================================================================================================
# Логгер
# ===================================================================================================

lstLOG = list() # Список входящий сообщений для логирования
lockLOG = threading.Lock() # Блокировщик для синхронизации потоков
iLOG_TIMEOUT = 7 # Периодичность сброса записей в лог (сек)

# Логгер запускается в отдельном потоке
class LOG_Thread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.name = "LOG_Thread"
	# def init

	def run(self):
		global bKILLED
		global sLOG
		global iLOG_TIMEOUT
		global lstLOG
		global lockLOG
		lstTemp = list()
		LOG("LOG_Thread: Started")
		# Спим, потом начинаем работать
		time.sleep(iLOG_TIMEOUT)
		while True:
			if bKILLED:
				break

			sError = ""
			# Копируем очередь сообщений
			lockLOG.acquire()
			try:
				lstTemp = lstLOG
				lstLOG = list()
			except Exception as ex:
				sError = str(ex)
			finally:
				lockLOG.release()
			# end try

			if len(sError) > 0:
				LOG("LOG_Thread.run: Error: " + sError)
				MONITOR("", "", "", "", "", "LOG_THREAD_FAIL", sError)

			# Запись сообщений на диск
			LOG_ListToFile(sLOG, lstTemp)

			# Спим
			if not bKILLED:
				time.sleep(iLOG_TIMEOUT)
		# while
		print("LOG_Thread: Stopped")
	# def run
# class LOG_Thread


# Интерфейсная функция для записи в лог
def LOG(sText: str) -> None:
	sdt = str(datetime.today().strftime("%Y-%m-%d %H:%M:%S"))
	print(sdt + ": " + sText)
	LOG_Add(sdt + ": " + sText)
# end def


# Добавить одну строку в список ожидания
def LOG_Add(sText: str) -> None:
	global lstLOG
	global lockLOG
	lockLOG.acquire()
	try:
		lstLOG.append(sText)
	except Exception as ex:
		print("LOG_Add: Error: " + str(ex))
	finally:
		lockLOG.release()
	# end try
# end def


# Добавить одну строку в файл
def LOG_StrToFileA(sFileName: str, sText: str) -> None:
	try:
		f = open(sFileName, "a")
		try:
			f.write(sText + "\n")
		except:
			f.close()
		# end try
	except Exception as ex:
		print("LOG_StrToFile(" + sFileName + "): Error: " + str(ex))
	# end try
# end def

def LOG_StrToFileW(sFileName: str, sText: str) -> None:
	try:
		f = open(sFileName, "w")
		try:
			f.write(sText + "\n")
		except:
			f.close()
		# end try
	except Exception as ex:
		print("LOG_StrToFileW(" + sFileName + "): Error: " + str(ex))
	# end try
# end def

# Добавить массив строк в файл
def LOG_ListToFile(sFileName: str, lst1: list) -> None:
	try:
		f = open(sFileName, "a")
		try:
			for s in lst1:
				f.write(s + "\n")
		except:
			f.close()
		# end try
	except Exception as ex:
		print("LOG_ListToFile(" + sFileName + "): Error: " + str(ex))
	# end try
# def LOG_ListToFile


# Сбросить лог на диск
def LOG_Flush() -> None:
	global sLOG
	global lstLOG
	global lockLOG
	lstTemp = list()

	# Копируем очередь сообщений
	lockLOG.acquire()
	try:
		lstTemp = lstLOG
		lstLOG = list()
	except Exception as ex:
		print("LOG_Flush: Error: " + str(ex))
	finally:
		lockLOG.release()
	# end try

	# Запись сообщений на диск
	LOG_ListToFile(sLOG, lstTemp)
# def LOG_Flush



# ===================================================================================================
# Мониторинг
# ===================================================================================================

dictMONITOR = dict() # Накопление метрик для последующего сброса в базу
lockMONITOR = threading.Lock() # Блокировщик для синхронизации потоков
iMONITOR_TIMEOUT = 11 # Периодичность сброса записей в базу (сек)

# Мониторинг запускается в отдельном потоке
class MONITOR_Thread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.name = "MONITOR_Thread"
	# def init

	def run(self):
		global bKILLED
		global iMONITOR_TIMEOUT
		global dictMONITOR
		global lockMONITOR
		lstTemp = list()
		lstTemp2 = list()
		LOG("MONITOR_Thread: Started")
		# Спим 15 сек, потом начинаем работать
		time.sleep(iMONITOR_TIMEOUT)
		while True:
			if bKILLED:
				break

			# Копируем очередь сообщений
			sError = ""
			lockMONITOR.acquire()
			try:
				# Проверить можем ли мы еще держать незаписанные метрики в памяти. 5760 - сутки
				if len(lstTemp) < 5760:
					lstTemp.append(dictMONITOR)
				dictMONITOR = dict()
			except Exception as ex:
				sError = str(ex)
			finally:
				lockMONITOR.release()
			# end try

			if len(sError) > 0:
				LOG("MONITOR_Thread.run: Error: " + sError)
				MONITOR("", "", "", "", "", "MONITOR_THREAD_FAIL", sError)

			# Пытаемся записать в базу что сумеем
			lstTemp2 = list() # для неуспешнозаписанных метрик
			for dictTemp in lstTemp:
				if not DB_MONITOR_insert(dictTemp):
					lstTemp2.append(dictTemp)
			# for
			lstTemp = lstTemp2

			# Спим
			if not bKILLED:
				time.sleep(iMONITOR_TIMEOUT)
		# while
		print("MONITOR_Thread: Stopped")
	# def run
# class MONITOR_Thread


# Мониторинг
# Добавить метрику
def MONITOR(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sName: str, sDescription: str):
	global lockMONITOR
	global dictMONITOR

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)
	sName = strtrunc(strisnull(sName, "").strip(), 255)
	sDescription = strtrunc(strisnull(sDescription, "").strip(), 4000)

	lockMONITOR.acquire()
	try:
		sKey = str(sChatId + "+" + sFromId + "+" + sName + "+" + sDescription) # ключ
		# "+" + sDescription  -- вот из-за этого фейловые метрики могут быть по одной штуке
		#LOG("MONITOR: " + sKey)

		# Найти такую же метрики и увеличить iCount
		iCount = 0
		if sKey in dictMONITOR:
			iCount = dictMONITOR[sKey]['iCount']
			iCount += 1
			dictMONITOR[sKey]['iCount'] = iCount
			dictMONITOR[sKey]['end_date'] = datetime.today()
		else:
			dictMONITOR[sKey] = {'iCount': int(1),
								'sChatId': str(sChatId),
								'sChatName': str(sChatName),
								'sFromId': str(sFromId),
								'sFromUser': str(sFromUser),
								'sFromName': str(sFromName),
								'sName': str(sName),
								'sDescription': str(sDescription),
								'start_date': datetime.today(),
								'end_date': datetime.today()
								}
		#fi
	except Exception as ex:
		print("MONITOR: Error: " + str(ex))
	finally:
		lockMONITOR.release()
	# end try
# def MONITOR


# Сбросить метрики в базу
def MONITOR_Flush() -> None:
	global lockMONITOR
	global dictMONITOR
	dictTemp = dict()

	# Копируем очередь сообщений
	lockMONITOR.acquire()
	try:
		dictTemp = dictMONITOR
		dictMONITOR = dict()
	except Exception as ex:
		print("MONITOR_Flush: Error: " + str(ex))
	finally:
		lockMONITOR.release()
	# end try

	# Пытаемся записать в базу
	if not DB_MONITOR_insert(dictTemp):
		print("MONITOR_Flush: Error: Не смогли записать в базу")
# def MONITOR_Flush




# ===================================================================================================
# Серверные сообщения
# ===================================================================================================

iMESS_TIMEOUT = 17 # Периодичность проверки папки входящих сообщений (сек)

class MESS_Thread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.name = "MESS_Thread"
	# def init

	def run(self):
		global bKILLED
		global sBOT_MESS_PATH
		global iMESS_TIMEOUT
		lstTemp = list()
		LOG("MESS_Thread: Started")
		# Спим, потом начинаем работать
		time.sleep(iMESS_TIMEOUT)
		while True:
			if bKILLED:
				break

			try:
				if os.path.isdir(sBOT_MESS_PATH):
					lstFiles = os.listdir(sBOT_MESS_PATH)
					for sFileName in lstFiles:
						if bKILLED:
							break

						sFullFileName = sBOT_MESS_PATH + "/" + sFileName
						if not os.path.isfile(sFullFileName):
							continue

						match1 = re.search(r'.*mess.txt', sFileName)
						# Текстовое сообщение
						if match1:
							# Прочитать сообщение
							sText = ReadFileToStr(sFullFileName)
							sText = strisnull(sText, "").strip()
							sText2 = strtrunc(strisnull(sText, "").strip(), 1000)

							# Отправить сообщение
							sResText = "📩 Серверное сообщение: \n" + sText2
							SENDMESSAGE(sBOT_OWNER_CHAT_ID, sBOT_OWNER_CHAT_ID, sResText, None, False)

							# Длинное сообщение продублировать файлом
							if len(sText) != len(sText2):
								API_sendDocumentTXT(sBOT_OWNER_CHAT_ID, sFileName, sFullFileName)

							# Удалить файл
							os.remove(sFullFileName)
						# Бинарник
						else:
							# Отправить файл
							API_sendDocumentTXT(sBOT_OWNER_CHAT_ID, sFileName, sFullFileName)

							# Удалить файл
							os.remove(sFullFileName)
						# if
					# for
				# if
			except Exception as ex:
				LOG("MESS_Thread.run: Error: " + str(ex))
				MONITOR("", "", "", "", "", "MESS_THREAD_FAIL", str(ex))
			# end try

			# Спим
			if not bKILLED:
				time.sleep(iMESS_TIMEOUT)
		# while
		print("MESS_Thread: Stopped")
	# def run
# class MESS_Thread



# ===================================================================================================
# Обратная связь
# Обработчик сообщений
# ===================================================================================================

iFEEDBACK_TIMEOUT = 13 # Периодичность проверки необработанных сообщений (сек)

# Обработчик сообщений запускается в отдельном потоке
class FEEDBACK_Thread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.name = "FEEDBACK_Thread"
	# def init

	def run(self):
		global bKILLED
		global iFEEDBACK_TIMEOUT
		LOG("FEEDBACK_Thread: Started")
		# Спим, потом начинаем работать
		time.sleep(iFEEDBACK_TIMEOUT)
		while True:
			if bKILLED:
				break

			# Обработать новые сообщения
			sError = ""
			try:
				FEEDBACK_Handler()
			except Exception as ex:
				sError = str(ex)
			# end try

			if len(sError) > 0:
				LOG("FEEDBACK_Thread.run: Error: " + sError)
				MONITOR("", "", "", "", "", "FEEDBACK_THREAD_FAIL", sError)

			# Спим
			if not bKILLED:
				time.sleep(iFEEDBACK_TIMEOUT)
		# while
		print("FEEDBACK_Thread: Stopped")
	# def run
# class FEEDBACK_Thread


# Обратная связь
# Обработать новые сообщения
def FEEDBACK_Handler():
	try:
		LOG("FEEDBACK_Handler")
		MONITOR("", "", "", "", "", "FEEDBACK_HANDLER_TOTAL", "")

		# Выбрать необработанные сообщения для хозяина
		rows = DB_FEEDBACK_selectByStatus("IN", "SENT") # Входящие сообщения со статусом Отправлено
		# id, chat_id, chat_name, from_id, from_name, from_user, message
		LOG("FEEDBACK_Handler: len(rows)=" + str(len(rows)))
		if len(rows) > 0:
			for row in rows:
				id = 0
				try:
					id = int(row[0])
					sChatId = strisnull(str(row[1]), "").strip()
					sChatName = strisnull(str(row[2]), "").strip()
					sFromId = strisnull(str(row[3]), "").strip()
					sFromName = strisnull(str(row[4]), "").strip()
					sFromUser = strisnull(str(row[5]), "").strip()
					sMessage = strisnull(str(row[6]), "").strip()

					# Отправить сообщение владельцу бота
					resText = "📩 (" + str(id) + ") @" + sFromUser + " " + sFromName + ":"
					resText += "\n" + sMessage
					SENDMESSAGE(sBOT_OWNER_CHAT_ID, "", resText, None, False)
					# Сменить статус сообщения на обработанное
					DB_FEEDBACK_updateStatus(sChatId, sChatName, sFromId, sFromUser, sFromName, id, "RECEIVED")
				except Exception as ex2:
					LOG("FEEDBACK_Handler: Error: id=" + str(id) + ": " + str(ex2))
					MONITOR("", "", "", "", "", "FEEDBACK_HANDLER_FAIL", "id=" + str(id) + ": " + str(ex2))
				# try
			# for
		# if


		# Выбрать необработанные ответы
		rows = DB_FEEDBACK_selectByStatus("OUT", "SENT") # Ответ со статусом Отправлено
		# id, chat_id, chat_name, from_id, from_name, from_user, message
		LOG("FEEDBACK_Handler: len(rows)=" + str(len(rows)))
		if len(rows) > 0:
			for row in rows:
				id = 0
				try:
					id = int(row[0])
					sChatId = strisnull(str(row[1]), "").strip()
					sChatName = strisnull(str(row[2]), "").strip()
					sFromId = strisnull(str(row[3]), "").strip()
					sFromName = strisnull(str(row[4]), "").strip()
					sFromUser = strisnull(str(row[5]), "").strip()
					sMessage = strisnull(str(row[6]), "").strip()
					reply_to_id = 0
					try:
						reply_to_id = int(row[7])
					except:
						reply_to_id = 0
					if reply_to_id <= 0:
						LOG("FEEDBACK_Handler: Error: reply_to_id <= 0, id=" + str(id))
						MONITOR("", "", "", "", "", "FEEDBACK_HANDLER_FAIL", "reply_to_id <= 0, id=" + str(id))
						continue
					# fi

					# Определить отправителя
					sChatId2, sFromUser2 = DB_FEEDBACK_chatById(reply_to_id)
					if len(sChatId2) > 0 and len(sFromUser2) > 0:
						try:
							# Отправить ответ
							resText = "📩 @" + sFromUser2 + ", пришел ответ от хозяина:"
							resText += "\n" + sMessage
							SENDMESSAGE(sChatId2, sFromUser2, resText, None, False)
							# Сменить статус сообщения на обработанное
							DB_FEEDBACK_updateStatus(sChatId, sChatName, sFromId, sFromUser, sFromName, id, "RECEIVED")
							# Спасть 1 сек чтоб не нарваться на лимиты по рассылке
							time.sleep(1)
						except Exception as ex3:
							LOG("FEEDBACK_Handler: Error: id=" + str(id) + ", sChatId2=" + sChatId2 + ": " + str(ex3))
							MONITOR("", "", "", "", "", "FEEDBACK_HANDLER_FAIL", "id=" + str(id) + ", sChatId2=" + sChatId2 + ": " + str(ex3))
						# try
					# if

				except Exception as ex2:
					LOG("FEEDBACK_Handler: Error: id=" + str(id) + ": " + str(ex2))
					MONITOR("", "", "", "", "", "FEEDBACK_HANDLER_FAIL", "id=" + str(id) + ": " + str(ex2))
				# try
			# for
		# if


		MONITOR("", "", "", "", "", "FEEDBACK_HANDLER_SUCCESS", "")
	except Exception as ex:
		LOG("FEEDBACK_Handler: Error: " + str(ex))
		MONITOR("", "", "", "", "", "FEEDBACK_HANDLER_FAIL", str(ex))
	# end try
# def FEEDBACK_Handler










# ===================================================================================================
# API. Отправка сообщений в отдельном потоке с целью укладываться в лимиты Телеграма на отправку сообщений
# https://core.telegram.org/bots/faq#my-bot-is-hitting-limits-how-do-i-avoid-this
# https://tlgrm.ru/docs/bots/faq
# 30 сообщений/сек - всего
# 1 сообщений/сек  - на один чат
# 20 сообщений/мин - в ту же группу
# ===================================================================================================

lstSENDMESSAGE = list() # Накопление сообщений. По пользователям
						# Массив словарей [ {'ToChatId': str, 'ToUser': str, 'Text': str, 'reply_markup': reply_markup }, {} ]
lockSENDMESSAGE = threading.Lock() # Блокировщик для синхронизации потоков
dSENDMESSAGE_TIMEOUT = 0.5 # Периодичность работы (сек)


class API_SENDMESSAGE_Thread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.name = "API_SENDMESSAGE_Thread"
	# def init

	def run(self):
		global bKILLED
		global dSENDMESSAGE_TIMEOUT
		global lstSENDMESSAGE
		global lockSENDMESSAGE
		lstTemp = list()
		lstTemp2 = list()
		print("API_SENDMESSAGE_Thread: Started")

		LIMIT_reg("common", 60, 30) # лимита 30 сообщений/сек = 0.5 сообщений/сек

		# Спим, потом начинаем работать
		time.sleep(dSENDMESSAGE_TIMEOUT)
		while True:
			if bKILLED:
				break
			# Копируем очередь сообщений
			lockSENDMESSAGE.acquire()
			try:
				if len(lstSENDMESSAGE) > 0:
					lstTemp = lstTemp + lstSENDMESSAGE
					lstSENDMESSAGE = list()
				# fi
			except Exception as ex:
				print("API_SENDMESSAGE_Thread.run: Error: " + str(ex))
			finally:
				lockSENDMESSAGE.release()
			# end try

			# Отправка сообщений с обычным приоритетом
			isExists = False # В очереди присутствуют сообщения с обычным приоритетом
			try:
				dictLimitByChat_chat = dict()  # Сбрасываем чаты достигшие лимитов в текущем цикле
				if len(lstTemp) > 0:
					lstTemp2 = list() # Сюда будем перекладывать пропущенные сообщения
					for d in lstTemp:
						# {'ToChatId': str, 'ToUser': str, 'Text': str, 'reply_markup': reply_markup, 'isLow': False }
						try:
							# Очередная запись в очереди
							sToChatId = str(d["ToChatId"])
							isLow = d["isLow"]
							LIMIT_reg("chat_id_" + sToChatId, 2, 1) # лимит 1 сообщений/сек  - на один чат

							# Выбираем сообщения с обычным приоритетом
							if not isLow:
								isExists = True  # В очереди присутствуют сообщения с обычным приориттом
								# Проверить достигал ли чат лимита в текущем цикле
								if sToChatId not in dictLimitByChat_chat:
									# Если не достигал, проверям счетчик чата
									if LIMIT_check("chat_id_" + sToChatId, 1):
										# Теперь проверям общий счетчик
										if LIMIT_check("common", 1):
											# Теперь можно отправлять сообщение
											sToUser = str(d["ToUser"])
											sText = str(d["Text"])
											reply_markup = d['reply_markup']
											API_sendMessage(sToChatId, sToUser, sText, reply_markup)
											LIMIT_add("chat_id_" + sToChatId, 1)
											LIMIT_add("common", 1)
										else:
											# иначе пускай еще подождет
											lstTemp2.append(d)
											MONITOR("", "", "", "", "", "API_SENDMESSAGE_LIMITWARNING", "common")  # Предупреждение по перелимиту
										# if
									else:
										# иначе пускай еще подождет
										dictLimitByChat_chat[sToChatId] = 1
										lstTemp2.append(d)
										MONITOR("", "", "", "", "", "API_SENDMESSAGE_LIMITWARNING", "chat_id_" + sToChatId)  # Предупреждение по перелимиту
									# if
								else:
									# если достигал, пусть еще подождет
									lstTemp2.append(d)
								# if
							else:
								# Сообщения с низким приоритетом подождут
								lstTemp2.append(d)
							# if
						except Exception as ex2:
							LOG("API_SENDMESSAGE_Thread.run: Error: 3: " + str(ex2) + ": d=" + str(d))
							MONITOR("", "", "", "", "", "API_SENDMESSAGE_HANDLER_FAIL", "API_SENDMESSAGE_Thread.run: 3: " + str(ex2) + ": d=" + str(d))
						# end try
					# for
					lstTemp = lstTemp2
				# if

				# Отправка ОДНОГО сообщения с низким приоритетом
				if (not isExists) and len(lstTemp) > 0:
					isFirst = True
					lstTemp2 = list() # Сюда будем перекладывать пропущенные сообщения
					for d in lstTemp:
						# {'ToChatId': str, 'ToUser': str, 'Text': str, 'reply_markup': reply_markup, 'isLow': False }
						try:
							# Очередная запись в очереди
							sToChatId = str(d["ToChatId"])
							isLow = d["isLow"]
							LIMIT_reg("chat_id_" + sToChatId, 2, 1) # лимит 1 сообщений/сек  - на один чат

							# Выбираем сообщения с низким приоритетом и только первое
							if isLow and isFirst:
								# Проверить достигал ли чат лимита в текущем цикле
								if sToChatId not in dictLimitByChat_chat:
									# Если не достигал, проверям счетчик чата
									if LIMIT_check("chat_id_" + sToChatId, 1):
										# Теперь проверям общий счетчик
										if LIMIT_check("common", 1):
											# Теперь можно отправлять сообщение
											isFirst = False # Первое сообщение отработали
											sToUser = str(d["ToUser"])
											sText = str(d["Text"])
											reply_markup = d['reply_markup']
											API_sendMessage(sToChatId, sToUser, sText, reply_markup)
											LIMIT_add("chat_id_" + sToChatId, 1)
											LIMIT_add("common", 1)
										else:
											# иначе пускай еще подождет
											lstTemp2.append(d)
											MONITOR("", "", "", "", "", "API_SENDMESSAGE_LIMITWARNING", "common")  # Предупреждение по перелимиту
										# if
									else:
										# иначе пускай еще подождет
										dictLimitByChat_chat[sToChatId] = 1
										lstTemp2.append(d)
										MONITOR("", "", "", "", "", "API_SENDMESSAGE_LIMITWARNING", "chat_id_" + sToChatId)  # Предупреждение по перелимиту
									# if
								else:
									# если достигал, пусть еще подождет
									lstTemp2.append(d)
								# if
							else:
								lstTemp2.append(d)
							# if
						except Exception as ex2:
							LOG("API_SENDMESSAGE_Thread.run: Error: 4: " + str(ex2) + ": d=" + str(d))
							MONITOR("", "", "", "", "", "API_SENDMESSAGE_HANDLER_FAIL", "API_SENDMESSAGE_Thread.run: 4: " + str(ex2) + ": d=" + str(d))
						# end try
					# for
					lstTemp = lstTemp2
				# if
			except Exception as ex:
				LOG("API_SENDMESSAGE_Thread.run: Error: 2: " + str(ex))
				MONITOR("", "", "", "", "", "API_SENDMESSAGE_HANDLER_FAIL", "API_SENDMESSAGE_Thread.run: 2: " + str(ex))
			# end try

			# Спим
			time.sleep(dSENDMESSAGE_TIMEOUT)
		# while
		print("API_SENDMESSAGE_Thread: Stopped")
	# def run
# class API_SENDMESSAGE_Thread




# SENDMESSAGE
# Добавить сообщение в очередь для отправки
def SENDMESSAGE(sToChatId: str, sToUser: str, sText: str, reply_markup, isLow: bool):
	global lockSENDMESSAGE
	global lstSENDMESSAGE

	sToChatId = strtrunc(strisnull(sToChatId, "").strip(), 255)
	sToUser = strtrunc(strisnull(sToUser, "").strip(), 255)
	sText = strtrunc(strisnull(sText, "").strip(), 4000)

	lockSENDMESSAGE.acquire()
	try:
		# Массив словарей [ {'ToChatId': str, 'ToUser': str, 'Text': str, 'reply_markup': reply_markup, 'isLow': False }, {} ]
		lstSENDMESSAGE.append({	'ToChatId': sToChatId,
								'ToUser': sToUser,
								'Text': sText,
								'reply_markup': reply_markup,
								'isLow': isLow
								})
	except Exception as ex:
		print("SENDMESSAGE: Error: " + str(ex))
	finally:
		lockSENDMESSAGE.release()
	# end try
# def SENDMESSAGE



# API: sendMessage - Отправка сообщения в чат
def API_sendMessage(sToChatId: str, sToUser: str, sText: str, reply_markup) -> bool:
	MONITOR("", "", "", "", "", "API_SENDMESSAGE_TOTAL", "")
	try:
		#LOG("sendMessage?url=%s" % sBOT_URL)
		LOG("sendMessage")
		sToChatId = strtrunc(strisnull(sToChatId, "").strip(), 255)
		sToUser = strtrunc(strisnull(sToUser, "").strip(), 255)
		sText = strtrunc(strisnull(sText, "").strip(), 4000)

		headers = {
			'Content-Type': 'application/json'
		}
		message = {
			'chat_id': sToChatId,
			'text': sText,
			'disable_web_page_preview': True
		}

		if len(sToUser) == 0:
			message['disable_notification'] = True

		if not reply_markup is None:
			message['reply_markup'] = reply_markup

		LOG(json.dumps(message, ensure_ascii=False))
		req = requests.Session()
		#set_hook = req.post(sTM_URL + "sendMessage?url=%s" % sBOT_URL, data=json.dumps(message), headers=headers)
		set_hook = req.post(sTM_URL + "sendMessage", data=json.dumps(message), headers=headers)
		iStatusCode = set_hook.status_code
		if iStatusCode != 200:
			LOG("API_sendMessage: Error: iStatusCode=" + str(iStatusCode) + "" + set_hook.text)
			MONITOR("", "", "", "", "", "API_SENDMESSAGE_FAIL", "iStatusCode=" + str(iStatusCode) + ": " + set_hook.text)
			if iStatusCode == 429:
				MONITOR("", "", "", "", "", "API_SENDMESSAGE_LIMITFAIL", "") # Если начнутся перелимиты по сообщениям, то надо растягивать отправку сообщений
			# fi
			return False
		# end if

		MONITOR("", "", "", "", "", "API_SENDMESSAGE_SUCCESS", "")
		return True
	except Exception as ex:
		print("API_sendMessage: Error: " + str(ex))
		MONITOR("", "", "", "", "", "API_SENDMESSAGE_FAIL", str(ex))
		return False
	# end try
# def API_sendMessage

# API: deleteMessage - Удаление сообщения
def API_deleteMessage(sChatId: str, sMessageId: str) -> bool:
	MONITOR("", "", "", "", "", "API_DELETEMESSAGE_TOTAL", "")
	try:
		#LOG("deleteMessage?url=%s" % sBOT_URL)
		LOG("deleteMessage")

		headers = {
			'Content-Type': 'application/json'
		}
		message = {
			'chat_id': sChatId,
			'message_id': sMessageId
		}

		LOG(json.dumps(message))
		req = requests.Session()
		#set_hook = req.post(sTM_URL + "deleteMessage?url=%s" % sBOT_URL, data=json.dumps(message), headers=headers)
		set_hook = req.post(sTM_URL + "deleteMessage", data=json.dumps(message), headers=headers)
		if set_hook.status_code != 200:
			LOG("API_deleteMessage: Error: " + set_hook.text)
			MONITOR("", "", "", "", "", "API_DELETEMESSAGE_FAIL", "status_code != 200: " + set_hook.text)
			return False
		# end if

		MONITOR("", "", "", "", "", "API_DELETEMESSAGE_SUCCESS", "")
		return True
	except Exception as ex:
		print("API_deleteMessage: Error: " + str(ex))
		MONITOR("", "", "", "", "", "API_DELETEMESSAGE_FAIL", str(ex))
		return False
	# end try
# def API_deleteMessage

# API: sendDocument - Отправка текстового файла
# Максимальный размер 50 МБайт
def API_sendDocumentTXT(sToChatId: str, sFileName: str, sFullFileName: str) -> bool:
	MONITOR("", "", "", "", "", "API_SENDDOCUMENTTXT_TOTAL", "")
	try:
		LOG("API_sendDocumentTXT: sToChatId=" + sToChatId + ", sFullFileName=" + sFullFileName)
		sToChatId = strtrunc(strisnull(sToChatId, "").strip(), 255)
		sFileName = strtrunc(strisnull(sFileName, "").strip(), 255)
		sFullFileName = strtrunc(strisnull(sFullFileName, "").strip(), 255)

		message = {
			'chat_id': sToChatId,
			'caption': sFileName
		}

		with open(sFullFileName, "rb") as f:
			files = {"document": f}
			set_hook = requests.post(sTM_URL + "sendDocument", data=message, files=files)
			iStatusCode = set_hook.status_code
			if iStatusCode != 200:
				LOG("API_sendDocumentTXT: Error: iStatusCode=" + str(iStatusCode) + "" + set_hook.text)
				MONITOR("", "", "", "", "", "API_SENDDOCUMENTTXT_FAIL", "iStatusCode=" + str(iStatusCode) + ": " + set_hook.text)
				if iStatusCode == 429:
					MONITOR("", "", "", "", "", "API_SENDDOCUMENTTXT_LIMITFAIL", "")
				# fi
				return False
			# end if
		# with

		MONITOR("", "", "", "", "", "API_SENDDOCUMENTTXT_SUCCESS", "")
		return True
	except Exception as ex:
		print("API_sendDocumentTXT: Error: " + str(ex))
		MONITOR("", "", "", "", "", "API_SENDDOCUMENTTXT_FAIL", str(ex))
		return False
	# end try
# def API_sendDocumentTXT

# API: getFile - Получить файл
def API_getFile(sChatId: str, sFromUser: str, sDocFileId: str) -> str:
	MONITOR(sChatId, "", "", "", "", "API_GETFILE_TOTAL", "")
	try:
		LOG("getFile")
		sFromChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
		sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
		sDocFileId = strisnull(sDocFileId, "").strip()

		if len(sDocFileId) == 0:
			LOG("API_getFile: Error: len(sDocFileId) == 0")
			MONITOR(sChatId, "", "", "", "", "API_GETFILE_FAIL", "len(sDocFileId) == 0")
			return ""
		# if

		# Получить ссылку на файл
		headers = {
			'Content-Type': 'application/json'
		}
		message = {
			'file_id': sDocFileId
		}

		req = requests.Session()
		set_hook = req.post(sTM_URL + "getFile", data=json.dumps(message), headers=headers)
		iStatusCode = set_hook.status_code
		if iStatusCode != 200:
			LOG("API_getFile: Error: iStatusCode=" + str(iStatusCode) + "" + set_hook.text)
			MONITOR(sChatId, "", "", "", "", "API_GETFILE_FAIL", "iStatusCode=" + str(iStatusCode) + ": " + set_hook.text)
			if iStatusCode == 429:
				MONITOR(sChatId, "", "", "", "", "API_GETFILE_LIMITFAIL", "")
			# fi
			return ""
		# end if

		# Получить URL-файла
		sFileURL = ""
		LOG("API_getFile: Result=" + set_hook.text)
		try:
			jBody = json.loads(set_hook.text)
			sFileURL = str(jBody["result"]["file_path"]).strip()
		except:
			sFileURL = ""
		# try
		if len(sFileURL) == 0:
			LOG("API_getFile: Error: len(sFileURL) == 0")
			MONITOR(sChatId, "", "", "", "", "API_GETFILE_FAIL", "len(sFileURL) == 0")
			return ""
		# if

		# Скачать файл
		LOG("API_getFile: sFileURL=" + sFileURL)
		r = requests.get(sTM_FILE_URL + sFileURL)
		iStatusCode = r.status_code
		if iStatusCode != 200:
			LOG("API_getFile: Error: sFileURL=" + sFileURL + ": " + r.text)
			MONITOR(sChatId, "", "", "", "", "API_GETFILE_FAIL", "sFileURL=" + sFileURL + ": " + r.text)
			if iStatusCode == 429:
				MONITOR(sChatId, "", "", "", "", "API_GETFILE_LIMITFAIL", "")
			# fi
			return ""
		# end if
		sRes = strisnull(r.text, "").strip()
		if len(sRes) == 0:
			LOG("API_getFile: Error: len(sRes) == 0")
			MONITOR(sChatId, "", "", "", "", "API_GETFILE_FAIL", "len(sRes) == 0")
			return ""
		# if
		#LOG("API_getFile: sRes=" + sRes)

		MONITOR(sChatId, "", "", "", "", "API_GETFILE_SUCCESS", "")
		return sRes
	except Exception as ex:
		print("API_getFile: Error: " + str(ex))
		MONITOR(sChatId, "", "", "", "", "API_GETFILE_FAIL", str(ex))
		return ""
	# end try
# def API_getFile


# API: deleteWebhook - Удаление веб-хука
def API_deleteWebhook() -> bool:
	MONITOR("", "", "", "", "", "API_DELETEWEBHOOK_TOTAL", "")
	try:
		LOG("deleteWebhook")
		req = requests.Session()
		set_hook = req.get(sTM_URL + "deleteWebhook")
		if set_hook.status_code != 200:
			LOG("API_deleteWebhook: Error: Не удалось удалить веб-хук: " + set_hook.text)
			MONITOR("", "", "", "", "", "API_DELETEWEBHOOK_FAIL", "status_code != 200: " + set_hook.text)
			return False
		# end if
		MONITOR("", "", "", "", "", "API_DELETEWEBHOOK_SUCCESS", "")
		return True
	except Exception as ex:
		print("API_deleteWebhook: Error: " + str(ex))
		MONITOR("", "", "", "", "", "API_DELETEWEBHOOK_FAIL", str(ex))
		return False
	# end try
# def API_deleteWebhook


# API: setWebhook - Установка веб-хука
def API_setWebhook() -> bool:
	MONITOR("", "", "", "", "", "API_SETWEBHOOK_TOTAL", "")
	try:
		LOG("setWebhook?url=%s" % sBOT_URL)
		req = requests.Session()
		set_hook = req.get(sTM_URL + "setWebhook?url=%s" % sBOT_URL)
		if set_hook.status_code != 200:
			LOG("API_setWebhook: Error: Не удалось установить веб-хук: " + set_hook.text)
			MONITOR("", "", "", "", "", "API_SETWEBHOOK_FAIL", "status_code != 200: " + set_hook.text)
			return False
		# end if
		MONITOR("", "", "", "", "", "API_SETWEBHOOK_SUCCESS", "")
		return True
	except Exception as ex:
		print("API_setWebhook: Error: " + str(ex))
		MONITOR("", "", "", "", "", "API_SETWEBHOOK_FAIL", str(ex))
		return False
	# end try
# def API_setWebhook







# ===================================================================================================
# Веб-сервер tornado и API телеграма
# ===================================================================================================


# web-сервер tornado
class WEB_Application(tornado.web.Application):
	def __init__(self):
		handlers = [
			(r"/", WEB_Handler_root),
			(r"/hc", WEB_Handler_hc),
		]
		settings = dict(
			template_path=os.path.join(os.path.dirname(__file__), "templates"),
			static_path=os.path.join(os.path.dirname(__file__), "static"),
			xsrf_cookies=True,
			cookie_secret="11oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
		)
		#tornado.web.Application.__init__(self, handlers, **settings)
		tornado.web.Application.__init__(self, handlers)

		# MongoDB
		#self.connection = pymongo.Connection(MONGODB_HOST, MONGODB_PORT)
		#self.db = self.connection["tornado"]
	# def
# class


def WEB_try_exit():
	global bKILLED
	if bKILLED:
		tornado.ioloop.IOLoop.instance().stop()
# def

# Обработчик запросов
# /hc
class WEB_Handler_hc(tornado.web.RequestHandler):
	def get(self):
		global bKILLED
		if bKILLED:
			return

		sIP = ""
		try:
			sIP = self.request.headers.get("X-Real-IP")
			if len(sIP) == 0:
				sIP = str(self.request.remote_ip)
		except:
			sIP = ""
		MONITOR("", "", "", "", "", "WEB_GET_HC_TOTAL", sIP)
		LOG("")
		LOG("------------------------------------------------------------------")
		LOG("GET /hc")
		LOG("remote_ip: " + sIP)

		if not DB_HC():
			LOG("db error 1")
			MONITOR("", "", "", "", "", "WEB_GET_HC_FAIL", sIP + ", db error 1")
			# Попроовать второй раз
			time.sleep(2)
			if not DB_HC():
				LOG("db error 2")
				MONITOR("", "", "", "", "", "WEB_GET_HC_FAIL", sIP + ", db error 2")
				self.write("db error")
			else:
				LOG("db ok")
				MONITOR("", "", "", "", "", "WEB_GET_HC_SUCCESS", sIP)
				self.write("ok")
		else:
			LOG("db ok")
			MONITOR("", "", "", "", "", "WEB_GET_HC_SUCCESS", sIP)
			self.write("ok")
		# if
	# def
# class


# Обработчик запросов
# /
class WEB_Handler_root(tornado.web.RequestHandler):
	def options(self):
		global bKILLED
		if bKILLED:
			return
		sIP = ""
		try:
			sIP = self.request.headers.get("X-Real-IP")
			if len(sIP) == 0:
				sIP = str(self.request.remote_ip)
		except:
			sIP = ""
		MONITOR("", "", "", "", "", "WEB_OPTIONS_ROOT_TOTAL", sIP)
		LOG("")
		LOG("------------------------------------------------------------------")
		LOG("OPTIONS /")
		LOG("remote_ip: " + sIP)
		MONITOR("", "", "", "", "", "WEB_OPTIONS_ROOT_SUCCESS", sIP)
		self.write("")
	# def options

	def get(self):
		global bKILLED
		if bKILLED:
			return
		sIP = ""
		try:
			sIP = self.request.headers.get("X-Real-IP")
			if len(sIP) == 0:
				sIP = str(self.request.remote_ip)
		except:
			sIP = ""
		MONITOR("", "", "", "", "", "WEB_GET_ROOT_TOTAL", sIP)
		try:
			LOG("")
			LOG("------------------------------------------------------------------")
			LOG("GET /")
			LOG("remote_ip: " + sIP)
			LOG("query: " + str(self.request.query))
			LOG("headers:\n" + str(self.request.headers))
			MONITOR("", "", "", "", "", "WEB_GET_ROOT_SUCCESS", sIP)
		except Exception as ex:
			LOG("WEB_Handler_root.get: Error: " + str(ex))
			MONITOR("", "", "", "", "", "WEB_GET_ROOT_FAIL", sIP + ": " + str(ex))
		# end try
		self.write("success")
	# def get

	def post(self):
		global bKILLED
		if bKILLED:
			return
		sIP = ""
		try:
			sIP = self.request.headers.get("X-Real-IP")
			if len(sIP) == 0:
				sIP = str(self.request.remote_ip)
			# self.request.headers.get("X-Real-IP") or \
			# self.request.headers.get("X-Forwarded-For") or \
			# self.request.remote_ip
		except:
			sIP = ""
		MONITOR("", "", "", "", "", "WEB_POST_ROOT_TOTAL", sIP)
		try:
			LOG("")
			LOG("------------------------------------------------------------------")
			LOG("POST /")
			LOG("remote_ip: " + sIP)
			LOG("query: " + str(self.request.query))
			LOG("headers:\n" + str(self.request.headers))
			#LOG("body:\n" + str(self.request.body))

			#LOG("q1=" + str(self.get_argument('q1', '', True)))
			#LOG("q2=" + str(self.get_argument('q2', '', True)))

			#if "q1" in self.request.query_arguments:
			#	LOG("q1=" + str(self.request.query_arguments["q1"]))
			#if "q2" in self.request.query_arguments:
			#	LOG("q2=" + str(self.request.query_arguments["q2"]))

			#for key in self.request.query_arguments:
				#LOG(str(key) + "=" + str(self.request.query_arguments[key]))

			if self.request.headers['Content-Type'] == 'application/json':
				#self.args = json_decode(self.request.body)
				jBody = json.loads(self.request.body)
				sRequest = str(jBody)
				LOG("json body:\n" + str(jBody))

				sChatId = ""
				sChatName = ""
				sChatType = ""
				sFromId = ""
				sFromUser = ""
				sFromName = ""
				sText = ""
				sSRCMessageId = ""
				sDocFileName = ""
				sDocFileId = ""
				isChatPrivate = False # Личный чат с ботом

				callback_query_id = ""
				try: callback_query_id = str(jBody["callback_query"]["id"]).strip()
				except: ex = 0

				message_id = ""
				try: message_id = str(jBody["message"]["message_id"]).strip()
				except: ex = 0

				# Ответ на кнопку
				if len(callback_query_id) > 0:
					try:
						sChatId = str(jBody["callback_query"]["message"]["chat"]["id"]).strip()
					except:
						ex = 0

					try:
						sChatType = str(jBody["callback_query"]["message"]["chat"]["type"]).strip()
					except:
						ex = 0

					sChatTitle = ""
					try:
						sChatTitle = str(jBody["callback_query"]["message"]["chat"]["title"]).strip()
					except:
						ex = 0

					sChatFirstName = ""
					try:
						sChatFirstName = str(jBody["callback_query"]["message"]["chat"]["first_name"]).strip()
					except:
						ex = 0

					sChatLastName = ""
					try:
						sChatLastName = str(jBody["callback_query"]["message"]["chat"]["last_name"]).strip()
					except:
						ex = 0

					sChatName = ( sChatTitle + " " + sChatFirstName + " " + sChatLastName ).strip()

					try:
						sSRCMessageId = str(jBody["callback_query"]["message"]["message_id"]).strip()
					except:
						ex = 0

					try:
						sFromId = str(jBody["callback_query"]["from"]["id"]).strip()
					except:
						ex = 0

					try:
						sFromUser = str(jBody["callback_query"]["from"]["username"]).strip()
					except:
						ex = 0

					sFromFirstName = ""
					try:
						sFromFirstName = str(jBody["callback_query"]["from"]["first_name"]).strip()
					except:
						ex = 0

					sFromLastName = ""
					try:
						sFromLastName = str(jBody["callback_query"]["from"]["last_name"]).strip()
					except:
						ex = 0

					sFromName = ( sFromFirstName + " " + sFromLastName ).strip()

					try:
						sText = str(jBody["callback_query"]["data"]).strip()
					except:
						ex = 0

				# Обычное сообщение
				elif len(message_id) > 0:
					try:
						sChatId = str(jBody["message"]["chat"]["id"]).strip()
					except:
						ex = 0

					try:
						sChatType = str(jBody["message"]["chat"]["type"]).strip()
					except:
						ex = 0

					sChatTitle = ""
					try:
						sChatTitle = str(jBody["message"]["chat"]["title"]).strip()
					except:
						ex = 0

					sChatFirstName = ""
					try:
						sChatFirstName = str(jBody["message"]["chat"]["first_name"]).strip()
					except:
						ex = 0

					sChatLastName = ""
					try:
						sChatLastName = str(jBody["message"]["chat"]["last_name"]).strip()
					except:
						ex = 0

					sChatName = ( sChatTitle + " " + sChatFirstName + " " + sChatLastName ).strip()

					try:
						sSRCMessageId = str(jBody["message"]["message_id"]).strip()
					except:
						ex = 0

					try:
						sFromId = str(jBody["message"]["from"]["id"]).strip()
					except:
						ex = 0

					try:
						sFromUser = str(jBody["message"]["from"]["username"]).strip()
					except:
						ex = 0

					sFromFirstName = ""
					try:
						sFromFirstName = str(jBody["message"]["from"]["first_name"]).strip()
					except:
						ex = 0

					sFromLastName = ""
					try:
						sFromLastName = str(jBody["message"]["from"]["last_name"]).strip()
					except:
						ex = 0

					sFromName = ( sFromFirstName + " " + sFromLastName ).strip()

					try:
						sText = str(jBody["message"]["text"]).strip()
					except:
						ex = 0

					try:
						sDocFileName = str(jBody["message"]["document"]["file_name"]).strip()
					except:
						ex = 0

					try:
						sDocFileId = str(jBody["message"]["document"]["file_id"]).strip()
					except:
						ex = 0
				# fi

				if (sChatType == "private") and (sChatId == sFromId):
					isChatPrivate = True

				LOG("\ncallback_query.id: " + str(callback_query_id) +
					"\nmessage.id: " + str(message_id) +
					"\nfrom.id: " + str(sFromId) +
					"\nfrom.user: " + str(sFromUser) +
					"\nfrom.name: " + str(sFromName) +
					"\nchat.id: " + str(sChatId) +
					"\nchat.type: " + str(sChatType) +
					"\nisChatPrivate: " + str(isChatPrivate) +
					"\nchat.name: " + str(sChatName) +
					"\ntext: " + str(sText) +
					"\nsDocFileName: " + str(sDocFileName) +
					"\nsDocFileId: " + str(sDocFileId)	)

				resText = ""
				reply_markup = None

				# Главное меню
				if (len(sChatId) > 0) and (len(sFromId) > 0) and (sText.startswith("/start") or sText.startswith("/menu")):
					resText, reply_markup = BOT_Handler_menu(sChatId, sChatName, sFromId, sFromUser, sFromName, sSRCMessageId, sText, sRequest, isChatPrivate)
				# ВКС
				elif (len(sChatId) > 0) and (len(sFromId) > 0) and (sText.startswith("/vcontact")):
					resText, reply_markup = BOT_Handler_vcontact(sChatId, sChatName, sFromId, sFromUser, sFromName, sSRCMessageId, sText, sRequest, isChatPrivate)
				# Покупки
				elif (len(sChatId) > 0) and (len(sFromId) > 0) and (sText.startswith("/buy")):
					resText, reply_markup = BOT_Handler_buy(sChatId, sChatName, sFromId, sFromUser, sFromName, sSRCMessageId, sText, sRequest, isChatPrivate)
				# Спорт
				elif (len(sChatId) > 0) and (len(sFromId) > 0) and (sText.startswith("/sport")):
					resText, reply_markup = BOT_Handler_sport(sChatId, sChatName, sFromId, sFromUser, sFromName, sSRCMessageId, sText, sRequest, isChatPrivate)
				# Мероприятия
				elif (len(sChatId) > 0) and (len(sFromId) > 0) and (sText.startswith("/event")):
					resText, reply_markup = BOT_Handler_event(sChatId, sChatName, sFromId, sFromUser, sFromName, sSRCMessageId, sText, sRequest, isChatPrivate)
				# Обратная связь
				elif (len(sChatId) > 0) and (len(sFromId) > 0) and (sText.startswith("/feedback")):
					resText, reply_markup = BOT_Handler_feedback(sChatId, sChatName, sFromId, sFromUser, sFromName, sSRCMessageId, sText, sRequest, isChatPrivate)
				# Справка
				elif (len(sChatId) > 0) and (len(sFromId) > 0) and (sText.startswith("/help")):
					resText, reply_markup = BOT_Handler_help(sChatId, sChatName, sFromId, sFromUser, sFromName, sSRCMessageId, sText, sRequest, isChatPrivate)
				# Прочие сообщения, включая ввод данных пользователем
				else:
					resText, reply_markup = BOT_Handler_other(sChatId, sChatName, sFromId, sFromUser, sFromName, sSRCMessageId, sText, sDocFileName, sDocFileId, sRequest, isChatPrivate)
				# if
				if ((len(resText) > 0) or not (reply_markup is None)):
					# Отправить сообщение обратно
					LOG("response text: " + resText)
					SENDMESSAGE(sChatId,  "", resText, reply_markup, False)
				# if
			# if
			MONITOR("", "", "", "", "", "WEB_POST_ROOT_SUCCESS", sIP)
		except Exception as ex:
			LOG("WEB_Handler_root.post: Error: " + str(ex))
			MONITOR("", "", "", "", "", "WEB_POST_ROOT_FAIL", sIP + ": " + str(ex))
		# end try

		self.write("")
	# def post

# class WEB_Application


# Главное меню
# Обработать текст сообщения и вернуть ответный текст
def BOT_Handler_menu(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sSRCMessageId: str, sText: str, sRequest: str, isChatPrivate: bool):
	resText = ""
	reply_markup = None

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_MENU_TOTAL", "")

	if len(sChatId) == 0:
		LOG("BOT_Handler_menu: len(sChatId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_MENU_FAIL", "len(sChatId) == 0")
		return resText, reply_markup
	# fi

	if len(sFromId) == 0:
		LOG("BOT_Handler_menu: len(sFromId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_MENU_FAIL", "len(sFromId) == 0")
		return resText, reply_markup
	# fi

	# Главное меню
	if sText.startswith("/start") or sText.startswith("/menu"):
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "MENU_TOTAL", "")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "MENU_SUCCESS", "")
		BOT_INPUT_add(sChatId, sFromId, "")

		# Сформировать главное меню
		resText = "Главное меню:"
		reply_markup = {
			"inline_keyboard": []
		}

		if sBOT_VCONTACT == "yes":
			reply_markup["inline_keyboard"].append( [{"text": "🔵 ВКС", "callback_data": "/vcontact"}] )

		if sBOT_BUY == "yes":
			reply_markup["inline_keyboard"].append( [{"text": "🧀 Покупки", "callback_data": "/buy"}] )

		if sBOT_SPORT == "yes" and isChatPrivate:
			reply_markup["inline_keyboard"].append( [{"text": "⛷ Спорт", "callback_data": "/sport"}] )

		if sBOT_EVENT == "yes" and isChatPrivate:
			dictEvents = DB_EVENT_selectEvents(sChatId)
			if len(dictEvents) > 0:
				reply_markup["inline_keyboard"].append([{"text": "🏆 Мероприятия", "callback_data": "/event"}])

		if sBOT_FEEDBACK == "yes" and isChatPrivate:
			if sChatId == sBOT_OWNER_CHAT_ID:
				reply_markup["inline_keyboard"].append([{"text": "✉️ Обратная связь", "callback_data": "/feedback"}, {"text": "✉️ Ответить", "callback_data": "/feedback_replyto"}])
			else:
				reply_markup["inline_keyboard"].append( [{"text": "✉️ Обратная связь", "callback_data": "/feedback"}] )

		reply_markup["inline_keyboard"].append([{"text": "❔ Справка", "callback_data": "/help"}])
	# if

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_MENU_SUCCESS", "")
	return resText, reply_markup
# def BOT_Handler_menu


# ВКС
# Обработать текст сообщения и вернуть ответный текст
def BOT_Handler_vcontact(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sSRCMessageId: str, sText: str, sRequest: str, isChatPrivate: bool):
	resText = ""
	reply_markup = None
	global sBOT_VCONTACT
	global iGMT
	global sGMT_DESC

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_VCONTACT_TOTAL", "")

	if len(sChatId) == 0:
		LOG("BOT_Handler_vcontact: len(sChatId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_VCONTACT_FAIL", "len(sChatId) == 0")
		return resText, reply_markup
	# fi

	if len(sFromId) == 0:
		LOG("BOT_Handler_vcontact: len(sFromId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_VCONTACT_VCONTACT_FAIL", "len(sFromId) == 0")
		return resText, reply_markup
	# fi

	# Очистка списка
	if sText.startswith("/vcontact_del_all"):
		if sBOT_VCONTACT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_DEL_ALL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_DEL_ALL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Удалить исходное сообщение
			if len(sSRCMessageId) > 0:
				API_deleteMessage(sChatId, sSRCMessageId)
			# Удалить по sChatId
			DB_VCONTACT_deleteAll(sChatId)
			resText = "🔵 Список пуст"
			reply_markup = {
				"inline_keyboard": []
			}
			reply_markup["inline_keyboard"].append([{"text": "🔵 ➕ Добавить ВКС", "callback_data": "/vcontact_add"}])
		# fi

	# Подтвердить очистку списка 3
	elif sText.startswith("/vcontact_confirm_all3"):
		if sBOT_VCONTACT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_CONFIRM_ALL3_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_CONFIRM_ALL3_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать вопрос
			resText = "🔵 ❌ Я ведь очищу!"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🔵 ДА!", "callback_data": "/vcontact_del_all "}, {"text": "🔵 Нет", "callback_data": "/vcontact"}]
				]
			}
		# fi

	# Подтвердить очистку списка 2
	elif sText.startswith("/vcontact_confirm_all2"):
		if sBOT_VCONTACT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_CONFIRM_ALL2_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_CONFIRM_ALL2_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать вопрос
			resText = "🔵 ❌ Точно очистить?"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🔵 Да", "callback_data": "/vcontact_confirm_all3 "}, {"text": "🔵 Нет", "callback_data": "/vcontact"}]
				]
			}
		# fi

	# Подтвердить очистку списка
	elif sText.startswith("/vcontact_confirm_all"):
		if sBOT_VCONTACT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_CONFIRM_ALL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_CONFIRM_ALL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать вопрос
			resText = "🔵 ❌ Очистить список?"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🔵 Да", "callback_data": "/vcontact_confirm_all2 "}, {"text": "🔵 Нет", "callback_data": "/vcontact"}]
				]
			}
		# fi

	# Удаление
	elif sText.startswith("/vcontact_del"):
		if sBOT_VCONTACT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_DEL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_DEL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Удалить исходное сообщение
			if len(sSRCMessageId) > 0:
				API_deleteMessage(sChatId, sSRCMessageId)
			# Определить id
			s1, sid = strsplit(sText, " ")
			sBuyId = strisnull(sid, "").strip()
			# Удалить по sBuyId и sChatId
			DB_VCONTACT_delete(sChatId, sBuyId)
			# Показать все ВКС в форме кнопок
			resText = "🔵 Список пуст"
			reply_markup = {
				"inline_keyboard": []
			}
			reply_markup["inline_keyboard"].append([{"text": "🔵 ➕ Добавить ВКС", "callback_data": "/vcontact_add"}])
			rows = DB_VCONTACT_selectByChatId(sChatId)
			try:
				if len(rows) > 0:
					resText = "🔵 Текущие ВКС:"
					reply_markup["inline_keyboard"].append([{"text": "🔵 ❌ Очистить список", "callback_data": "/vcontact_confirm_all"}])
					for row in rows:
						# LOG("id=" + str(row[0]) + ", from_name=" + str(row[1]) + ", name=" + str(row[2]) + ", description=" + str(row[3]) )
						reply_markup["inline_keyboard"].append([{"text": "🔵 " + str(row[2]), "callback_data": "/vcontact_confirm " + str(row[0])}])
					# for
				# fi
			except:
				s = ""
		# fi

	# Подтвердить удаление по id
	elif sText.startswith("/vcontact_confirm"):
		if sBOT_VCONTACT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_CONFIRM_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_CONFIRM_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить id
			s1, sBuyId = strsplit(sText, " ")
			sBuyId = strisnull(sBuyId, "").strip()
			# Показать ВКС и вопрос
			resText = "------------------------------------------------"
			rows = DB_VCONTACT_selectOne(sChatId, sBuyId)
			try:
				if len(rows) > 0:
					reply_markup = {
						"inline_keyboard": []
					}
					for row in rows:
						# LOG("id=" + str(row[0]) + ", from_name=" + str(row[1]) + ", name=" + str(row[2]) + ", description=" + str(row[3]) )
						resText = resText + "\n🔵 " + str(row[2])
						if len(str(row[3])) > 0:
							resText = resText + "\n" + str(row[3])
						break
					# for
				# fi
			except:
				s = ""
			resText = resText + "\n------------------------------------------------"
			resText = resText + "\n🔵 ❌ Удалить ВКС?"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🔵 Да", "callback_data": "/vcontact_del " + sBuyId}, {"text": "🔵 Нет", "callback_data": "/vcontact"}]
				]
			}
		# fi

	# Ввод новой записи
	elif sText.startswith("/vcontact_add"):
		if sBOT_VCONTACT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_ADD_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_ADD_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "vcontact_add")
			resText = "🔵 ➕ Введи новую ВКС:\nНазвание. Ссылка"
		# fi

	# Список
	elif sText.startswith("/vcontact"):
		if sBOT_VCONTACT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "VCONTACT_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать все ВКС в форме кнопок
			resText = "🔵 Список пуст"
			reply_markup = {
				"inline_keyboard": []
			}
			reply_markup["inline_keyboard"].append([{"text": "🔵 ➕ Добавить ВКС", "callback_data": "/vcontact_add"}])
			rows = DB_VCONTACT_selectByChatId(sChatId)
			try:
				if len(rows) > 0:
					resText = "🔵 Текущие ВКС:"
					reply_markup["inline_keyboard"].append([{"text": "🔵 ❌ Очистить список", "callback_data": "/vcontact_confirm_all"}])
					for row in rows:
						#LOG("id=" + str(row[0]) + ", from_name=" + str(row[1]) + ", name=" + str(row[2]) + ", description=" + str(row[3]))
						reply_markup["inline_keyboard"].append([{"text": "🔵 " + str(row[2]), "callback_data": "/vcontact_confirm " + str(row[0])}])
					# for
				# fi
			except:
				s = ""
		# fi

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_VCONTACT_SUCCESS", "")
	return resText, reply_markup
# def BOT_Handler_vcontact


# Покупки
# Обработать текст сообщения и вернуть ответный текст
def BOT_Handler_buy(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sSRCMessageId: str, sText: str, sRequest: str, isChatPrivate: bool):
	resText = ""
	reply_markup = None
	global sBOT_BUY
	global iGMT
	global sGMT_DESC

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_BUY_TOTAL", "")

	if len(sChatId) == 0:
		LOG("BOT_Handler_buy: len(sChatId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_BUY_FAIL", "len(sChatId) == 0")
		return resText, reply_markup
	# fi

	if len(sFromId) == 0:
		LOG("BOT_Handler_buy: len(sFromId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_BUY_FAIL", "len(sFromId) == 0")
		return resText, reply_markup
	# fi

	# Очистка списка
	if sText.startswith("/buy_del_all"):
		if sBOT_BUY == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_DEL_ALL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_DEL_ALL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Удалить исходное сообщение
			if len(sSRCMessageId) > 0:
				API_deleteMessage(sChatId, sSRCMessageId)
			# Удалить по sChatId
			DB_BUY_deleteAll(sChatId)
			resText = "🧀 Список пуст"
			reply_markup = {
				"inline_keyboard": []
			}
			reply_markup["inline_keyboard"].append([{"text": "🧀 ➕ Добавить покупку", "callback_data": "/buy_add"}])
		# fi

	# Подтвердить очистку списка 3
	elif sText.startswith("/buy_confirm_all3"):
		if sBOT_BUY == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_CONFIRM_ALL3_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_CONFIRM_ALL3_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать вопрос
			resText = "🧀 ❌ Я ведь очищу!"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🧀 ДА!", "callback_data": "/buy_del_all "}, {"text": "🧀 Нет", "callback_data": "/buy"}]
				]
			}
		# fi

	# Подтвердить очистку списка 2
	elif sText.startswith("/buy_confirm_all2"):
		if sBOT_BUY == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_CONFIRM_ALL2_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_CONFIRM_ALL2_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать вопрос
			resText = "🧀 ❌ Точно очистить?"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🧀 Да", "callback_data": "/buy_confirm_all3 "}, {"text": "🧀 Нет", "callback_data": "/buy"}]
				]
			}
		# fi

	# Подтвердить очистку списка
	elif sText.startswith("/buy_confirm_all"):
		if sBOT_BUY == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_CONFIRM_ALL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_CONFIRM_ALL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать вопрос
			resText = "🧀 ❌ Очистить список?"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🧀 Да", "callback_data": "/buy_confirm_all2 "}, {"text": "🧀 Нет", "callback_data": "/buy"}]
				]
			}
		# fi

	# Удаление покупки
	elif sText.startswith("/buy_del"):
		if sBOT_BUY == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_DEL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_DEL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Удалить исходное сообщение
			if len(sSRCMessageId) > 0:
				API_deleteMessage(sChatId, sSRCMessageId)
			# Определить id
			s1, sid = strsplit(sText, " ")
			sBuyId = strisnull(sid, "").strip()
			# Удалить по sBuyId и sChatId
			DB_BUY_delete(sChatId, sBuyId)
			# Показать все покупки в форме кнопок
			resText = "🧀 Список пуст"
			reply_markup = {
				"inline_keyboard": []
			}
			reply_markup["inline_keyboard"].append([{"text": "🧀 ➕ Добавить покупку", "callback_data": "/buy_add"}])
			rows = DB_BUY_selectByChatId(sChatId)
			try:
				if len(rows) > 0:
					resText = "🧀 Текущие покупки:"
					reply_markup["inline_keyboard"].append([{"text": "🧀 ❌ Очистить список", "callback_data": "/buy_confirm_all"}])
					for row in rows:
						# LOG("id=" + str(row[0]) + ", from_name=" + str(row[1]) + ", name=" + str(row[2]) + ", description=" + str(row[3]) )
						reply_markup["inline_keyboard"].append([{"text": "🧀 " + str(row[1]) + ": " + str(row[2]), "callback_data": "/buy_confirm " + str(row[0])}])
					# for
				# fi
			except:
				s = ""
		# fi

	# Подтвердить удаление покупки по id
	elif sText.startswith("/buy_confirm"):
		if sBOT_BUY == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_CONFIRM_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_CONFIRM_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить id
			s1, sBuyId = strsplit(sText, " ")
			sBuyId = strisnull(sBuyId, "").strip()
			# Показать покупку и вопрос
			resText = "------------------------------------------------"
			rows = DB_BUY_selectOne(sChatId, sBuyId)
			try:
				if len(rows) > 0:
					reply_markup = {
						"inline_keyboard": []
					}
					for row in rows:
						# LOG("id=" + str(row[0]) + ", from_name=" + str(row[1]) + ", name=" + str(row[2]) + ", description=" + str(row[3]) )
						resText = resText + "\n🧀 " + str(row[1]) + ": " + str(row[2])
						if len(str(row[3])) > 0:
							resText = resText + "\n" + str(row[3])
						break
					# for
				# fi
			except:
				s = ""
			# try
			resText = resText + "\n------------------------------------------------"
			resText = resText + "\n🧀 ❌ Удалить покупку?"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🧀 Да", "callback_data": "/buy_del " + sBuyId}, {"text": "🧀 Нет", "callback_data": "/buy"}]
				]
			}
		# fi

	# Ввод новой записи
	elif sText.startswith("/buy_add"):
		if sBOT_BUY == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_ADD_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_ADD_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "buy_add")
			resText = "🧀 ➕ Введи новую покупку:\nНазвание. Описание"
		# fi

	# Список
	elif sText.startswith("/buy"):
		if sBOT_BUY == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BUY_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать все покупки в форме кнопок
			resText = "🧀 Список пуст"
			reply_markup = {
				"inline_keyboard": []
			}
			reply_markup["inline_keyboard"].append([{"text": "🧀 ➕ Добавить покупку", "callback_data": "/buy_add"}])
			rows = DB_BUY_selectByChatId(sChatId)
			try:
				if len(rows) > 0:
					resText = "🧀 Текущие покупки"
					reply_markup["inline_keyboard"].append([{"text": "🧀 ❌ Очистить список", "callback_data": "/buy_confirm_all"}])
					for row in rows:
						#LOG("id=" + str(row[0]) + ", from_name=" + str(row[1]) + ", name=" + str(row[2]) + ", description=" + str(row[3]))
						reply_markup["inline_keyboard"].append([{"text": "🧀 " + str(row[1]) + ": " + str(row[2]), "callback_data": "/buy_confirm " + str(row[0])}])
					# for
				# fi
			except:
				s = ""
		# fi

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_BUY_SUCCESS", "")
	return resText, reply_markup
# def BOT_Handler_buy


# Спорт
# Обработать текст сообщения и вернуть ответный текст
def BOT_Handler_sport(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sSRCMessageId: str, sText: str, sRequest: str, isChatPrivate: bool):
	resText = ""
	reply_markup = None
	global sBOT_SPORT
	global iGMT
	global sGMT_DESC

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_SPORT_TOTAL", "")

	if len(sChatId) == 0:
		LOG("BOT_Handler_sport: len(sChatId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_SPORT_FAIL", "len(sChatId) == 0")
		return resText, reply_markup
	# fi

	if len(sFromId) == 0:
		LOG("BOT_Handler_sport: len(sFromId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_SPORT_FAIL", "len(sFromId) == 0")
		return resText, reply_markup
	# fi

	if not isChatPrivate:
		LOG("BOT_Handler_sport: not isChatPrivate")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_SPORT_FAIL", "not isChatPrivate")
		return resText, reply_markup
	# fi

	# Очистка данных
	if sText.startswith("/sport_del_all"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_DEL_ALL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_DEL_ALL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Удалить исходное сообщение
			if len(sSRCMessageId) > 0:
				API_deleteMessage(sChatId, sSRCMessageId)
			# Удалить по sChatId
			DB_SPORT_deleteAll(sChatId)
			resText = "⛷ Статистика:"
			resText += DB_SPORT_selectStatMonth(sChatId, iGMT)
		# fi

	# Подтвердить очистку данных 3
	elif sText.startswith("/sport_confirm_all3"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_CONFIRM_ALL3_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_CONFIRM_ALL3_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать вопрос
			resText = "⛷ ❌ Я ведь очищу!"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "⛷ ДА!", "callback_data": "/sport_del_all "}, {"text": "⛷ Нет", "callback_data": "/sport"}]
				]
			}
		# fi

	# Подтвердить очистку данных 2
	elif sText.startswith("/sport_confirm_all2"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_CONFIRM_ALL2_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_CONFIRM_ALL2_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать вопрос
			resText = "🧀 ❌ Точно очистить?"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "⛷ Да", "callback_data": "/sport_confirm_all3 "}, {"text": "⛷ Нет", "callback_data": "/sport"}]
				]
			}
		# fi

	# Подтвердить очистку данных
	elif sText.startswith("/sport_confirm_all"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_CONFIRM_ALL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_CONFIRM_ALL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать вопрос
			resText = "⛷ ❌ Очистить данные?"
			# Показать кнопки Да Нет
			reply_markup = {
				"inline_keyboard": [
					[{"text": "⛷ Да", "callback_data": "/sport_confirm_all2 "}, {"text": "⛷ Нет", "callback_data": "/sport"}]
				]
			}
		# fi

	# Ввод новой записи: Бег
	elif sText.startswith("/sport_add_run"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_RUN_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_RUN_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "sport_add_run")
			resText = "⛷ ➕ Добавить Бег 1 км = 1 км:"
		# fi

	# Ввод новой записи: Лыжи
	elif sText.startswith("/sport_add_sky"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_SKY_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_SKY_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "sport_add_sky")
			resText = "⛷ ➕ Добавить Лыжи 1 км = 1 км:"
		# fi

	# Ввод новой записи: Ходьба
	elif sText.startswith("/sport_add_walk"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_WALK_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_WALK_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "sport_add_walk")
			resText = "⛷ ➕ Добавить Ходьба 1 км = 0,7 км:"
		# fi

	# Ввод новой записи: Велик
	elif sText.startswith("/sport_add_bike"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_BIKE_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_BIKE_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "sport_add_bike")
			resText = "⛷ ➕ Добавить Велик 1 км = 0,5 км:"
		# fi

	# Ввод новой записи: Плавание
	elif sText.startswith("/sport_add_swim"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_SWIM_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_SWIM_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "sport_add_swim")
			resText = "⛷ ➕ Добавить Плавание 1 км = 5 км:"
		# fi

	# Ввод новой записи: Пр. активности
	elif sText.startswith("/sport_add_other"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_OTHER_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_ADD_OTHER_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "sport_add_other")
			resText = "⛷ ➕ Добавить Пр. активности 1 мин = 0,1 км:"
		# fi

	# Справка
	elif sText.startswith("/sport_help"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_HELP_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_HELP_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Справка
			resText = "----------------------------------------------------"
			resText += "\n⛷ ❔ Учёт тренировок:"
			resText += "\nУчетный часовой пояс – " + sGMT_DESC
			resText += "\nУчетная единица измеренния – км"
			resText += "\nКоэффициенты:"
			resText += "\n  – Бег 1 км = 1 км"
			resText += "\n  – Лыжи 1 км = 1 км"
			resText += "\n  – Ходьба 1 км = 0,7 км"
			resText += "\n  – Велик 1 км = 0,5 км"
			resText += "\n  – Плавание 1 км = 5 км"
			resText += "\n  – Пр. активности 1 мин = 0,1 км"
			resText += "\n----------------------------------------------------"
		# fi

	# Статистика
	elif sText.startswith("/sport_stat"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_STAT_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_STAT_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать статистику
			resText = "⛷ 📘 Статистика:"
			resText += DB_SPORT_selectStatFull(sChatId, iGMT)
		# fi

	# Меню
	elif sText.startswith("/sport"):
		if sBOT_SPORT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "SPORT_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать статистику и кнопки
			resText = "⛷ Статистика:"
			resText += DB_SPORT_selectStatMonth(sChatId, iGMT)
			reply_markup = {
				"inline_keyboard": []
			}
			reply_markup["inline_keyboard"].append([{"text": "⛷ ❔ Справка", "callback_data": "/sport_help"}, {"text": "⛷ 📘 Статистика", "callback_data": "/sport_stat"}])
			reply_markup["inline_keyboard"].append([{"text": "⛷ ❌ Очистить данные", "callback_data": "/sport_confirm_all"}])
			reply_markup["inline_keyboard"].append([{"text": "⛷ ➕ Бег", "callback_data": "/sport_add_run"}])
			reply_markup["inline_keyboard"].append([{"text": "⛷ ➕ Лыжи", "callback_data": "/sport_add_sky"}])
			reply_markup["inline_keyboard"].append([{"text": "⛷ ➕ Ходьба", "callback_data": "/sport_add_walk"}])
			reply_markup["inline_keyboard"].append([{"text": "⛷ ➕ Велик", "callback_data": "/sport_add_bike"}])
			reply_markup["inline_keyboard"].append([{"text": "⛷ ➕ Плавание", "callback_data": "/sport_add_swim"}])
			reply_markup["inline_keyboard"].append([{"text": "⛷ ➕ Пр. активности", "callback_data": "/sport_add_other"}])
		# fi

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_SPORT_SUCCESS", "")
	return resText, reply_markup
# def BOT_Handler_sport



# Мероприятия
# Обработать текст сообщения и вернуть ответный текст
def BOT_Handler_event(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sSRCMessageId: str, sText: str, sRequest: str, isChatPrivate: bool):
	resText = ""
	reply_markup = None
	global sBOT_EVENT
	global sBOT_EVENT_DOC
	global sTEMP
	global iGMT
	global sGMT_DESC

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_EVENT_TOTAL", "")

	if len(sChatId) == 0:
		LOG("BOT_Handler_event: len(sChatId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_EVENT_FAIL", "len(sChatId) == 0")
		return resText, reply_markup
	# fi

	if len(sFromId) == 0:
		LOG("BOT_Handler_event: len(sFromId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_EVENT_FAIL", "len(sFromId) == 0")
		return resText, reply_markup
	# fi

	if not isChatPrivate:
		LOG("BOT_Handler_event: not isChatPrivate")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_EVENT_FAIL", "not isChatPrivate")
		return resText, reply_markup
	# fi

	# Подтвердить отмену регистрации 3
	if sText.startswith("/event_req_cancel_confirm3"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_REQ_CANCEL_CONFIRM3_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_REQ_CANCEL_CONFIRM3_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 ❌ Я ведь отменю!"
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🏆 ДА!", "callback_data": "/event_req_cancel " + sEvent}, {"text": "🏆 Нет", "callback_data": "/event"}]
				]
			}
		# fi

	# Подтвердить отмену регистрации 2
	elif sText.startswith("/event_req_cancel_confirm2"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_REQ_CANCEL_CONFIRM2_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_REQ_CANCEL_CONFIRM2_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 ❌ Точно отменить?"
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🏆 Да", "callback_data": "/event_req_cancel_confirm3 " + sEvent}, {"text": "🏆 Нет", "callback_data": "/event"}]
				]
			}
		# fi

	# Подтвердить отмену регистрации
	elif sText.startswith("/event_req_cancel_confirm"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_REQ_CANCEL_CONFIRM_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_REQ_CANCEL_CONFIRM_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 ❌ Отменить регистрацию?"
			reply_markup = {
				"inline_keyboard": [
					[{"text": "🏆 Да", "callback_data": "/event_req_cancel_confirm2 " + sEvent}, {"text": "🏆 Нет", "callback_data": "/event"}]
				]
			}
		# fi

	# Отмена регистрации
	elif sText.startswith("/event_req_cancel"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_REQ_CANCEL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_REQ_CANCEL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			# Удалить исходное сообщение
			if len(sSRCMessageId) > 0:
				API_deleteMessage(sChatId, sSRCMessageId)
			# Отменить
			resText = "🏆 🚫 Что-то пошло не так"
			if DB_EVENT_REQUEST_updateRequestByChatId(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent, "canceled", "", "", "", iGMT):
				resText = "🏆 Регистрация отменена"
			# if
		# fi

	# Загрузить результаты
	elif sText.startswith("/event_postreq"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_POSTREQ_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_POSTREQ_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				resText = "🏆 📥 Отправьте текстовый файл:"
				BOT_INPUT_add(sChatId, sFromId, "event_postreq " + sEvent)
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi


	# Выгрузить заявки
	elif sText.startswith("/event_getreq"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_GETREQ_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_GETREQ_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			# Выгрузить
			resText = "🏆 🚫 Что-то пошло не так"
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				sDoc = DB_EVENT_REQUEST_getreq(sChatId, sEvent)
				if len(sDoc) > 0:
					sFileName = sEvent + "_out.txt"
					sFullFileName = sTEMP + "/" + sFileName
					LOG_StrToFileW(sFullFileName, sDoc)
					API_sendDocumentTXT(sChatId, sFileName, sFullFileName)
					resText = "🏆 📤 Файл отправлен"
				else:
					resText = "🏆 📤 Заявок нет"
				# if
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

	# Очистка очистку заявок мерпориятия
	elif sText.startswith("/event_del_all"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_DEL_ALL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_DEL_ALL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			# Удалить исходное сообщение
			if len(sSRCMessageId) > 0:
				API_deleteMessage(sChatId, sSRCMessageId)
			# Удалить
			resText = "🏆 🚫 Что-то пошло не так"
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				if DB_EVENT_REQUEST_delete(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent):
					resText = "🏆 Заявки очищены"
					# Показать список мероприятий
					dictEvents = DB_EVENT_selectEvents(sChatId)
					reply_markup = {
						"inline_keyboard": []
					}
					if len(dictEvents) > 0:
						for key in dictEvents:
							reply_markup["inline_keyboard"].append(dictEvents[key])
						# for
					# if
					reply_markup["inline_keyboard"].append([{"text": "🏆 ❔ Справка", "callback_data": "/event_help"}])
				# if
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

	# Подтвердить очистку заявок мерпориятия 3
	elif sText.startswith("/event_confirm_all3"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_CONFIRM_ALL3_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_CONFIRM_ALL3_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 🚫 Что-то пошло не так"
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				resText = "🏆 ❌ Я ведь очищу!"
				reply_markup = {
					"inline_keyboard": [
						[{"text": "🏆 ДА!", "callback_data": "/event_del_all " + sEvent}, {"text": "🏆 Нет", "callback_data": "/event"}]
					]
				}
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

	# Подтвердить очистку заявок мерпориятия 2
	elif sText.startswith("/event_confirm_all2"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_CONFIRM_ALL2_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_CONFIRM_ALL2_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 🚫 Что-то пошло не так"
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				resText = "🏆 ❌ Точно очистить?"
				reply_markup = {
					"inline_keyboard": [
						[{"text": "🏆 Да", "callback_data": "/event_confirm_all3 " + sEvent}, {"text": "🏆 Нет", "callback_data": "/event"}]
					]
				}
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

	# Подтвердить очистку заявок мерпориятия
	elif sText.startswith("/event_confirm_all"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_CONFIRM_ALL_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_CONFIRM_ALL_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 🚫 Что-то пошло не так"
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				resText = "🏆 ❌ Очистить заявки мероприятия?"
				reply_markup = {
					"inline_keyboard": [
						[{"text": "🏆 Да", "callback_data": "/event_confirm_all2 " + sEvent}, {"text": "🏆 Нет", "callback_data": "/event"}]
					]
				}
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

	# Регистрация на мероприятие (Участник)
	elif sText.startswith("/event_req"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_REG_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_REG_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 ➕ Введите свои данные:"
			BOT_INPUT_add(sChatId, sFromId, "event_req " + sEvent)
		# fi

	# Остановить регистрацию (Организатор)
	elif sText.startswith("/event_close_reg"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_CLOSEREG_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_CLOSEREG_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 🚫 Что-то пошло не так"
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				if DB_EVENT_updateStatus(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent, "open"):
					resText = "🏆 Регистрация остановлена"
					# Показать список мероприятий
					dictEvents = DB_EVENT_selectEvents(sChatId)
					reply_markup = {
						"inline_keyboard": []
					}
					if len(dictEvents) > 0:
						for key in dictEvents:
							reply_markup["inline_keyboard"].append(dictEvents[key])
						# for
					# if
					reply_markup["inline_keyboard"].append([{"text": "🏆 ❔ Справка", "callback_data": "/event_help"}])
				# if
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

	# Стартовать регистрацию (Организатор)
	elif sText.startswith("/event_open_reg"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_OPENREG_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_OPENREG_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 🚫 Что-то пошло не так"
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				if DB_EVENT_updateStatus(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent, "open_reg"):
					resText = "🏆 Регистрация стартовала"
					# Показать список мероприятий
					dictEvents = DB_EVENT_selectEvents(sChatId)
					reply_markup = {
						"inline_keyboard": []
					}
					if len(dictEvents) > 0:
						for key in dictEvents:
							reply_markup["inline_keyboard"].append(dictEvents[key])
						# for
					# if
					reply_markup["inline_keyboard"].append([{"text": "🏆 ❔ Справка", "callback_data": "/event_help"}])
				# if
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

	# Остановить мероприятие (Организатор)
	elif sText.startswith("/event_close"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_CLOSE_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_CLOSE_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 🚫 Что-то пошло не так"
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				if DB_EVENT_updateStatus(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent, ""):
					resText = "🏆 Мероприятие остановлено"
					# Показать список мероприятий
					dictEvents = DB_EVENT_selectEvents(sChatId)
					reply_markup = {
						"inline_keyboard": []
					}
					if len(dictEvents) > 0:
						for key in dictEvents:
							reply_markup["inline_keyboard"].append(dictEvents[key])
						# for
					# if
					reply_markup["inline_keyboard"].append([{"text": "🏆 ❔ Справка", "callback_data": "/event_help"}])
				# if
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

	# Стартовать мероприятие (Организатор)
	elif sText.startswith("/event_open"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_OPEN_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_OPEN_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			resText = "🏆 🚫 Что-то пошло не так"
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				if DB_EVENT_updateStatus(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent, "open"):
					resText = "🏆 Мероприятие стартовало"
					# Показать список мероприятий
					dictEvents = DB_EVENT_selectEvents(sChatId)
					reply_markup = {
						"inline_keyboard": []
					}
					if len(dictEvents) > 0:
						for key in dictEvents:
							reply_markup["inline_keyboard"].append(dictEvents[key])
						# for
					# if
					reply_markup["inline_keyboard"].append([{"text": "🏆 ❔ Справка", "callback_data": "/event_help"}])
				# if
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

	# Ввести описание (Организатор)
	elif sText.startswith("/event_edit"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_EDIT_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_EDIT_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			#
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				resText = "🏆 ✏ Введите описание:\nЗаголовок. Описание"
				BOT_INPUT_add(sChatId, sFromId, "event_edit " + sEvent)
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

	# Показать описание
	elif sText.startswith("/event_show"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_SHOW_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_SHOW_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sText, " ")
			sEvent = strisnull(s2, "").strip()
			# Показать описание мероприятия
			resText = ""
			resText += "\n----------------------------------------------------"
			resText += "\n🏆 🚫 Еще нет описания"
			resText += "\n----------------------------------------------------"
			sDescription, sStatus, iEvent = DB_EVENT_selectDescription(sChatId, sEvent)
			if len(sDescription) > 0:
				resText = ""
				resText += "\n----------------------------------------------------"
				resText += "\n" + sDescription
				resText += "\n----------------------------------------------------"
				# Отобразить заявку участника
				sReqDescription, sReqStatus, idReq = DB_EVENT_REQUEST_selectDescription(sChatId, sEvent)
				if len(sReqDescription) > 0:
					resText += "\n" + sReqDescription
					resText += "\n----------------------------------------------------"
				# if
				# Кнопки
				reply_markup = {
					"inline_keyboard": []
				}
				# Для организатора
				isOrg = DB_EVENT_isOrg(sChatId, sEvent)
				if isOrg:
					reply_markup["inline_keyboard"].append([{"text": "🏆 ✏️ Ввести описание", "callback_data": "/event_edit " + sEvent}])
					if iEvent > 0:
						if (sStatus != "open") and (sStatus != "open_reg"):
							reply_markup["inline_keyboard"].append([{"text": "🏆 Стартовать мероприятие", "callback_data": "/event_open " + sEvent}])
						# if
						if (sStatus == "open"):
							reply_markup["inline_keyboard"].append([{"text": "🏆 Остановить мероприятие", "callback_data": "/event_close " + sEvent}])
							reply_markup["inline_keyboard"].append([{"text": "🏆 Стартовать регистрацию", "callback_data": "/event_open_reg " + sEvent}])
						# if
						if (sStatus == "open_reg"):
							reply_markup["inline_keyboard"].append([{"text": "🏆 Остановить регистрацию", "callback_data": "/event_close_reg " + sEvent}])
						# if
					# if
					reply_markup["inline_keyboard"].append([{"text": "🏆 📤 Выгрузить заявки", "callback_data": "/event_getreq " + sEvent},
															{"text": "🏆 📥 Загрузить результаты", "callback_data": "/event_postreq " + sEvent}])
					reply_markup["inline_keyboard"].append([{"text": "🏆 ❌ Очистить заявки", "callback_data": "/event_confirm_all " + sEvent}])
				# if
				# Для участников
				if (sStatus == "open_reg"):
					if (idReq > 0) and (sReqStatus != "canceled"):
						reply_markup["inline_keyboard"].append([{"text": "🏆 ➕ Внести изменения", "callback_data": "/event_req " + sEvent},
																{"text": "🏆 ❌ Отменить регистрацию", "callback_data": "/event_req_cancel_confirm " + sEvent}])
					else:
						reply_markup["inline_keyboard"].append([{"text": "🏆 ➕ Регистрация на мероприятие", "callback_data": "/event_req " + sEvent}])
					# if
				# if
			# if
		# fi

	# Справка
	elif sText.startswith("/event_help"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_HELP_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_HELP_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Справка
			isOrg = DB_EVENT_isOrg(sChatId, "*")
			if isOrg:
				resText = "----------------------------------------------------"
				resText += "\n🏆 ❔ Инструкция для Организатора\n " + sBOT_EVENT_DOC
				resText += "\n----------------------------------------------------"
			# if
		# fi

	# Список
	elif sText.startswith("/event"):
		if sBOT_EVENT == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "EVENT_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "")
			# Показать список мероприятий
			resText = "🏆 Нет доступных мероприятий"
			dictEvents = DB_EVENT_selectEvents(sChatId)
			reply_markup = {
				"inline_keyboard": []
			}
			if len(dictEvents) > 0:
				resText = "🏆 Доступные мероприятия:"
				for key in dictEvents:
					reply_markup["inline_keyboard"].append(dictEvents[key])
				# for
			# if
			isOrg = DB_EVENT_isOrg(sChatId, "*")
			if isOrg:
				reply_markup["inline_keyboard"].append([{"text": "🏆 ❔ Справка", "callback_data": "/event_help"}])
			# if
		# fi

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_EVENT_SUCCESS", "")
	return resText, reply_markup
# def BOT_Handler_event


# Обратная связь
# Обработать текст сообщения и вернуть ответный текст
def BOT_Handler_feedback(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sSRCMessageId: str, sText: str, sRequest: str, isChatPrivate: bool):
	resText = ""
	reply_markup = None
	global sBOT_FEEDBACK
	global iGMT
	global sGMT_DESC

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_FEEDBACK_TOTAL", "")

	if len(sChatId) == 0:
		LOG("BOT_Handler_feedback: len(sChatId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_FEEDBACK_FAIL", "len(sChatId) == 0")
		return resText, reply_markup
	# fi

	if len(sFromId) == 0:
		LOG("BOT_Handler_feedback: len(sFromId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_FEEDBACK_FAIL", "len(sFromId) == 0")
		return resText, reply_markup
	# fi

	if not isChatPrivate:
		LOG("BOT_Handler_feedback: not isChatPrivate")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_FEEDBACK_FAIL", "not isChatPrivate")
		return resText, reply_markup
	# fi

	# Ответить на указанное сообщение
	if sText.startswith("/feedback_replyto"):
		if sBOT_FEEDBACK == "yes":
			if sChatId == sBOT_OWNER_CHAT_ID:
				MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "FEEDBACK_REPLYTO_TOTAL", "")
				MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "FEEDBACK_REPLYTO_SUCCESS", "")
				#
				BOT_INPUT_add(sChatId, sFromId, "feedback_replyto")
				resText = "✉️ Отправить ответ:"
				resText += "\nid текст сообщения"
			# fi
		# fi

	# Меню
	elif sText.startswith("/feedback"):
		if sBOT_FEEDBACK == "yes":
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "FEEDBACK_TOTAL", "")
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "FEEDBACK_SUCCESS", "")
			BOT_INPUT_add(sChatId, sFromId, "feedback")
			# Показать подсказку
			resText = "✉️ Напиши сообщение моему хозяину, я передам. Но он человек занятой и не факт, что ответит:"
		# fi

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_FEEDBACK_SUCCESS", "")
	return resText, reply_markup
# def BOT_Handler_feedback


# Справка
# Обработать текст сообщения и вернуть ответный текст
def BOT_Handler_help(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sSRCMessageId: str, sText: str, sRequest: str, isChatPrivate: bool):
	resText = ""
	reply_markup = None

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_HELP_TOTAL", "")

	if len(sChatId) == 0:
		LOG("BOT_Handler_help: len(sChatId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_HELP_FAIL", "len(sChatId) == 0")
		return resText, reply_markup
	# fi

	if len(sFromId) == 0:
		LOG("BOT_Handler_help: len(sFromId) == 0")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_HELP_FAIL", "len(sFromId) == 0")
		return resText, reply_markup
	# fi

	# Help
	if sText.startswith("/help"):
		# Мониторинг
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "HELP_TOTAL", "")
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "HELP_SUCCESS", "")
		# Справка
		resText = "----------------------------------------------------"
		resText += "\n❔ Функции бота:"
		if sBOT_VCONTACT == "yes":
			resText += "\n– ВКС – создание личного или командного списка конференций со ссылками"
		if sBOT_BUY == "yes":
			resText += "\n– Покупки – создание личного или командного списка покупок:"
			resText += "\n        – отправить гонца за покупками для команды"
			resText += "\n        – ежедневные семейные покупки: кто возвращается с работы через магазины, тот и затаривается"
			resText += "\n        – личный список покупок"
		if sBOT_SPORT == "yes":
			resText += "\n– Спорт – учёт тренировок, см. /sport_help@zda_gadget_bot"
		if sBOT_EVENT == "yes":
			resText += "\n– Мероприятия – регистрация на мероприятия"
		resText += "\n----------------------------------------------------"
	# if

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_HELP_SUCCESS", "")
	return resText, reply_markup
# def BOT_Handler_help



# Обработать текст сообщения и вернуть ответный текст
def BOT_Handler_other(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sSRCMessageId: str, sText: str, sDocFileName: str, sDocFileId: str, sRequest: str, isChatPrivate: bool):
	resText = ""
	reply_markup = None
	global sBOT_VCONTACT
	global sBOT_BUY
	global sBOT_SPORT
	global sBOT_EVENT
	global sBOT_FEEDBACK
	global iGMT
	global sGMT_DESC

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_OTHER_TOTAL", "")

	if len(sChatId) == 0:
		LOG("BOT_Handler_other: len(sChatId) == 0")
		#MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_OTHER_FAIL", "len(sChatId) == 0")
		return resText, reply_markup
	# fi

	if len(sFromId) == 0:
		LOG("BOT_Handler_other: len(sFromId) == 0")
		#MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_OTHER_FAIL", "len(sFromId) == 0")
		return resText, reply_markup
	# fi

	sDocFileName = strisnull(sDocFileName, "").strip()
	sDocFileId = strisnull(sDocFileId, "").strip()


	# Свободный поиск по тексту сообщения
	# Покупка
	# Введено новое название покупки
	if sBOT_BUY == "yes":
		sAnswer = BOT_INPUT_check(sChatId, sFromId)
		if sAnswer == "buy_add":
			BOT_INPUT_add(sChatId, sFromId, "")
			# Добавить новую покупку
			DB_BUY_insert(sChatId, sChatName, sFromId, sFromUser, sFromName, sText)
			# Показать все покупки в форме кнопок
			resText = "🧀 Список пуст"
			reply_markup = {
				"inline_keyboard": []
			}
			reply_markup["inline_keyboard"].append([{"text": "🧀 ➕ Добавить покупку", "callback_data": "/buy_add"}])
			rows = DB_BUY_selectByChatId(sChatId)
			try:
				if len(rows) > 0:
					resText = "🧀 Текущие покупки:"
					reply_markup["inline_keyboard"].append([{"text": "🧀 ❌ Очистить список", "callback_data": "/buy_confirm_all"}])
					for row in rows:
						#LOG("id=" + str(row[0]) + ", from_name=" + str(row[1]) + ", name=" + str(row[2]) + ", description=" + str(row[3]) )
						reply_markup["inline_keyboard"].append([{"text": "🧀 " + str(row[1]) + ": " + str(row[2]), "callback_data": "/buy_confirm " + str(row[0])}])
					# for
				# fi
			except:
				s = ""
		# fi

	# ВКС
	# Введено новое название
	if sBOT_VCONTACT == "yes":
		sAnswer = BOT_INPUT_check(sChatId, sFromId)
		if sAnswer == "vcontact_add":
			BOT_INPUT_add(sChatId, sFromId, "")
			# Добавить новую ВКС
			DB_VCONTACT_insert(sChatId, sChatName, sFromId, sFromUser, sFromName, sText)
			# Показать все ВКС в форме кнопок
			resText = "🔵 Список пуст"
			reply_markup = {
				"inline_keyboard": []
			}
			reply_markup["inline_keyboard"].append([{"text": "🔵 ➕ Добавить ВКС", "callback_data": "/vcontact_add"}])
			rows = DB_VCONTACT_selectByChatId(sChatId)
			try:
				if len(rows) > 0:
					resText = "🔵 Текущие ВКС:"
					reply_markup["inline_keyboard"].append([{"text": "🔵 ❌ Очистить список", "callback_data": "/vcontact_confirm_all"}])
					for row in rows:
						#LOG("id=" + str(row[0]) + ", from_name=" + str(row[1]) + ", name=" + str(row[2]) + ", description=" + str(row[3]) )
						reply_markup["inline_keyboard"].append([{"text": "🔵 " + str(row[2]), "callback_data": "/vcontact_confirm " + str(row[0])}])
					# for
				# fi
			except:
				s = ""
		# fi

	# Спорт
	# Введено значение
	if sBOT_SPORT == "yes":
		sAnswer = BOT_INPUT_check(sChatId, sFromId)
		if sAnswer == "sport_add_run":
			BOT_INPUT_add(sChatId, sFromId, "")
			# Добавить значение
			fCount = 0.0
			s = strisnull(sText, "").strip().replace(",", ".")
			try:
				fCount = float(s)
			except Exception as ex:
				fCount = 0.0
			# end try
			if (fCount <= 0.0) or (fCount > 1000000.0):
				resText = "⛷ 🚫 Я так не умею"
			else:
				DB_SPORT_insert(sChatId, sChatName, sFromId, sFromUser, sFromName, "RUN", "", fCount, iGMT)
				resText = "⛷ Принято: Бег " + str(fCount) + " км"
				resText += "\nСтатистика:"
				resText += DB_SPORT_selectStatMonth(sChatId, iGMT)
			# fi

		elif sAnswer == "sport_add_sky":
			BOT_INPUT_add(sChatId, sFromId, "")
			# Добавить значение
			fCount = 0.0
			s = strisnull(sText, "").strip().replace(",", ".")
			try:
				fCount = float(s)
			except Exception as ex:
				fCount = 0.0
			# end try
			if (fCount <= 0.0) or (fCount > 1000000.0):
				resText = "⛷ 🚫 Я так не умею"
			else:
				DB_SPORT_insert(sChatId, sChatName, sFromId, sFromUser, sFromName, "SKY", "", fCount, iGMT)
				resText = "⛷ Принято: Лыжи " + str(fCount) + " км"
				resText += "\nСтатистика:"
				resText += DB_SPORT_selectStatMonth(sChatId, iGMT)
			# fi

		elif sAnswer == "sport_add_walk":
			BOT_INPUT_add(sChatId, sFromId, "")
			# Добавить значение
			fCount = 0.0
			s = strisnull(sText, "").strip().replace(",", ".")
			try:
				fCount = float(s)
			except Exception as ex:
				fCount = 0.0
			# end try
			if (fCount <= 0.0) or (fCount > 1000000.0):
				resText = "⛷ 🚫 Я так не умею"
			else:
				DB_SPORT_insert(sChatId, sChatName, sFromId, sFromUser, sFromName, "WALK", "", fCount, iGMT)
				resText = "⛷ Принято: Ходьба " + str(fCount) + " км"
				resText += "\nСтатистика:"
				resText += DB_SPORT_selectStatMonth(sChatId, iGMT)
			# fi

		elif sAnswer == "sport_add_bike":
			BOT_INPUT_add(sChatId, sFromId, "")
			# Добавить значение
			fCount = 0.0
			s = strisnull(sText, "").strip().replace(",", ".")
			try:
				fCount = float(s)
			except Exception as ex:
				fCount = 0.0
			# end try
			if (fCount <= 0.0) or (fCount > 1000000.0):
				resText = "⛷ 🚫 Я так не умею"
			else:
				DB_SPORT_insert(sChatId, sChatName, sFromId, sFromUser, sFromName, "BIKE", "", fCount, iGMT)
				resText = "⛷ Принято: Велик " + str(fCount) + " км"
				resText += "\nСтатистика:"
				resText += DB_SPORT_selectStatMonth(sChatId, iGMT)
			# fi

		elif sAnswer == "sport_add_swim":
			BOT_INPUT_add(sChatId, sFromId, "")
			# Добавить значение
			fCount = 0.0
			s = strisnull(sText, "").strip().replace(",", ".")
			try:
				fCount = float(s)
			except Exception as ex:
				fCount = 0.0
			# end try
			if (fCount <= 0.0) or (fCount > 1000000.0):
				resText = "⛷ 🚫 Я так не умею"
			else:
				DB_SPORT_insert(sChatId, sChatName, sFromId, sFromUser, sFromName, "SWIM", "", fCount, iGMT)
				resText = "⛷ Принято: Плавание " + str(fCount) + " км"
				resText += "\nСтатистика:"
				resText += DB_SPORT_selectStatMonth(sChatId, iGMT)
			# fi

		elif sAnswer == "sport_add_other":
			BOT_INPUT_add(sChatId, sFromId, "")
			# Добавить значение
			fCount = 0.0
			s = strisnull(sText, "").strip().replace(",", ".")
			try:
				fCount = float(s)
			except Exception as ex:
				fCount = 0.0
			# end try
			if (fCount <= 0.0) or (fCount > 1000000.0):
				resText = "⛷ 🚫 Я так не умею"
			else:
				DB_SPORT_insert(sChatId, sChatName, sFromId, sFromUser, sFromName, "OTHER", "", fCount, iGMT)
				resText = "⛷ Принято: пр. активности " + str(fCount) + " мин"
				resText += "\nСтатистика:"
				resText += DB_SPORT_selectStatMonth(sChatId, iGMT)
			# fi
		# fi


	# Мероприятие
	if sBOT_EVENT == "yes":
		sInput = BOT_INPUT_check(sChatId, sFromId)

		# Отправлен файл
		if sInput.startswith("event_postreq"):
			LOG(sInput)
			resText = "🏆 🚫 Что-то пошло не так"
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sInput, " ")
			sEvent = strisnull(s2, "").strip()
			# Проверить права организатора
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				# Обработать загруженный файл
				sDocFileName = strisnull(sDocFileName, "").strip()
				sDocFileId = strisnull(sDocFileId, "").strip()
				s1, s2 = os.path.splitext(sDocFileName)
				if s2.lower() != ".txt":
					resText = "🏆 🚫 Нужен файл *.txt"
					return
				# if
				sFile = API_getFile(sChatId, sFromUser, sDocFileId)
				if len(sFile) == 0:
					resText = "🏆 🚫 Не удалось получить файл"
					return
				# if
				if DB_EVENT_REQUEST_updateRequestList(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent, sFile, iGMT):
					resText = "🏆 Данные загружены"
				else:
					resText = "🏆 🚫 Ошибка загрузки данных"
				# if
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

		# Введено описание мероприятия
		elif sInput.startswith("event_edit"):
			LOG(sInput)
			resText = "🏆 🚫 Что-то пошло не так"
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить event
			s1, s2 = strsplit(sInput, " ")
			sEvent = strisnull(s2, "").strip()
			# Проверить права организатора
			isOrg = DB_EVENT_isOrg(sChatId, sEvent)
			if isOrg:
				if DB_EVENT_updateDescription(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent, sText, iGMT):
					# Показать список мероприятий
					resText = "🏆 Нет доступных мероприятий"
					dictEvents = DB_EVENT_selectEvents(sChatId)
					reply_markup = {
						"inline_keyboard": []
					}
					if len(dictEvents) > 0:
						resText = "🏆 Доступные мероприятия:"
						for key in dictEvents:
							reply_markup["inline_keyboard"].append(dictEvents[key])
						# for
					# if
					reply_markup["inline_keyboard"].append([{"text": "🏆 ❔ Справка", "callback_data": "/event_help"}])
				# if
			else:
				resText = "🏆 🚫 Вы не являетесь организатором"
			# if
		# fi

		# Введена регистрация на мероприятие
		elif sInput.startswith("event_req"):
			LOG(sInput)
			resText = "🏆 🚫 Что-то пошло не так"
			BOT_INPUT_add(sChatId, sFromId, "")
			# Определить id
			s1, s2 = strsplit(sInput, " ")
			sEvent = strisnull(s2, "").strip()
			if DB_EVENT_REQUEST_updateRequest(sChatId, sChatName, sFromId, sFromUser, sFromName, sEvent, sText, iGMT):
				# Показать список мероприятий
				resText = "🏆 ➕ Заявка отправлена"
				dictEvents = DB_EVENT_selectEvents(sChatId)
				if len(dictEvents) > 0:
					reply_markup = {
						"inline_keyboard": []
					}
					for key in dictEvents:
						reply_markup["inline_keyboard"].append(dictEvents[key])
					# for
				# if
			# if
		# fi

	# Обратная связь
	# Введено значение
	if sBOT_FEEDBACK == "yes":
		sAnswer = BOT_INPUT_check(sChatId, sFromId)

		# Сообщение хозяину
		if sAnswer == "feedback":
			LOG("feedback")
			resText = "✉️ 🚫 Что-то пошло не так"
			BOT_INPUT_add(sChatId, sFromId, "")
			# Отправить
			sText = strisnull(sText, "").strip()
			if len(sText) == 0:
				resText = "✉️ 🚫 Пустые сообщения не отправляю"
			else:
				if DB_FEEDBACK_insert(sChatId, sChatName, sFromId, sFromUser, sFromName, sText, "IN", "SENT", 0, iGMT):
					resText = "✉️ Сообщение принято"
			# fi

		# Ответ на сообщение
		elif sAnswer == "feedback_replyto":
			LOG("feedback_replyto")
			resText = "✉️ 🚫 Что-то пошло не так"
			# Определить id
			s = strisnull(sText, "").strip()
			sFeedbackId, sText2 = strsplit(s, " ")
			sFeedbackId = strisnull(sFeedbackId, "").strip()
			sText2 = strisnull(sText2, "").strip()
			iFeedbackId = 0
			try:
				iFeedbackId = int(sFeedbackId)
			except:
				iFeedbackId = 0
			LOG("feedback_replyto: iFeedbackId=" + str(iFeedbackId) + ", sText2=" + sText2)
			if iFeedbackId <= 0:
				resText = "✉️ 🚫 Не указан id"
			elif len(sText2) == 0:
				resText = "✉️ 🚫 Пустые сообщения не отправляю"
			else:
				# Проверить существование сообщения id=iFeedbackId
				if DB_FEEDBACK_checkById(iFeedbackId):
					# Отправить ответ
					if DB_FEEDBACK_insert(sChatId, sChatName, sFromId, sFromUser, sFromName, sText2, "OUT", "SENT", iFeedbackId, iGMT):
						resText = "✉️ Сообщение отправлено"
				# if
			# fi
		# fi

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "BOT_HANDLER_OTHER_SUCCESS", "")
	return resText, reply_markup
# def BOT_Handler_other


# Ввод данных пользователем. Добавить признак ожидания ввода данных
def BOT_INPUT_add(sChatId: str, sFromId: str, sText: str):
	global lockINPUT
	global dictINPUT

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sText = strtrunc(strisnull(sText, "").strip(), 255)
	s = sChatId + "+" + sFromId

	lockINPUT.acquire()
	try:
		dictINPUT[s] = sText
	except Exception as ex:
		print("BOT_INPUT_add: Error: " + str(ex))
	finally:
		lockINPUT.release()
	# end try
# def BOT_INPUT_add

# Ввод данных пользователем. Проверить наличие признака ожидания ввода данных
def BOT_INPUT_check(sChatId: str, sFromId: str) -> str:
	global lockINPUT
	global dictINPUT

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	s = sChatId + "+" + sFromId
	sText = ""

	lockINPUT.acquire()
	try:
		sText = dictINPUT[s]
	except Exception as ex:
		print("BOT_INPUT_check: Error: " + str(ex))
	finally:
		lockINPUT.release()
	# end try
	return sText
# def BOT_INPUT_check






# ===================================================================================================
# БД mysql
# ===================================================================================================

# Подключение к БД
def DB_connect() -> bool:
	global dbConnect
	global dtDBConnectTicker
	if len(sDB_HOST) == 0:
		return False

	LOG("DB_connect: db_host=" + sDB_HOST + ", db_name=" + sDB_NAME + ", db_user=" + sDB_USER)
	MONITOR("", "", "", "", "", "DB_CONNECT_TOTAL", "")
	sVersion = ""
	sError = ""

	lockDB.acquire()
	try:
		dbConnect = pymysql.connect(sDB_HOST, sDB_USER, sDB_PASS, sDB_NAME)
		dbCursor = dbConnect.cursor()
		try:
			dbCursor.execute("select version()")
			row = dbCursor.fetchone()
			if len(row) > 0:
				sVersion = row[0]
				if len(sVersion) > 0:
					dtDBConnectTicker = time.time()  # Время последнего подключения к БД
				# if
			# if
		except Exception as exsql:
			sVersion = ""
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sVersion = ""
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sVersion) > 0:
		LOG("DB_connect: version=" + sVersion)
		MONITOR("", "", "", "", "", "DB_CONNECT_SUCCESS", "")
		return True
	else:
		LOG("DB_connect: Error: " + sError)
		MONITOR("", "", "", "", "", "DB_CONNECT_FAIL", sError)
		return False
	# if
# def DB_connect


# Проверка соединения и переподключение
def DB_check() -> bool:
	global dbConnect
	global dtDBConnectTicker

	if len(sDB_HOST) == 0:
		return False

	try:
		if dbConnect is None:
			# LOG("DB_check: Нет соединения с базой 1. Переподключаюсь...")
			# MONITOR("", "", "", "", "", "DB_CONNECT_CHECK_FAIL", "1")
			if not DB_connect():
				# LOG("DB_check: Нет соединения с базой 2. Переподключаюсь...")
				# MONITOR("", "", "", "", "", "DB_CONNECT_CHECK_FAIL", "2")
				time.sleep(2)
				if not DB_connect():
					LOG("DB_check: Не удалось подключиться к базе 1")
					MONITOR("", "", "", "", "", "DB_CONNECT_CHECK_FAIL", "1")
					return False

		isConnected = False

		# ping не работает
		#try:
		#	dbConnect.ping(True)
		#	isConnected = True
		#except Exception as ex:
		#	isConnected = False
		# end try

		# Будем просто смотреть на вермя последнего подключения и переподключаться
		try:
			if not (dtDBConnectTicker is None):
				iDeltaTicker = time.time() - dtDBConnectTicker
				# Каждые 50 сек должны переподключаться
				if iDeltaTicker <= 50:
					isConnected = True
			# if
		except:
			isConnected = False
			print("DB_check: Error: try iDeltaTicker")
		# if


		if not isConnected:
			#LOG("DB_check: Нет соединения с базой 4. Переподключаюсь...")
			#MONITOR("", "", "", "", "", "DB_CONNECT_CHECK_FAIL", "4")
			#time.sleep(2)
			if not DB_connect():
				#LOG("DB_check: Нет соединения с базой 5. Переподключаюсь...")
				#MONITOR("", "", "", "", "", "DB_CONNECT_CHECK_FAIL", "5")
				time.sleep(2)
				if not DB_connect():
					LOG("DB_check: Не удалось подключиться к базе 2")
					MONITOR("", "", "", "", "", "DB_CONNECT_CHECK_FAIL", "2")
					return False

		return True
	except Exception as ex:
		LOG("DB_check: Error: " + str(ex))
		MONITOR("", "", "", "", "", "DB_CONNECT_CHECK_FAIL", str(ex))
		return False
	# end try
# def DB_connect


# Проверка работы БД
def DB_HC() -> bool:
	global dbConnect
	LOG("DB_HC")
	MONITOR("", "", "", "", "", "DB_HC_TOTAL", "")

	if not DB_check():
		LOG("DB_HC: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_HC_FAIL", "not DB_check()")
		return False

	sVersion = ""
	iDeltaTicker = 0.0
	sError = ""

	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("select version()")
			iDeltaTicker = time.time() - dtStartTicker
			row = dbCursor.fetchone()
			if not (row is None):
				if len(row) > 0:
					sVersion = row[0]
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_HC: Error: " + sError)
		MONITOR("", "", "", "", "", "DB_HC_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR("", "", "", "", "", "DB_HC_LONGTIME", str(iDeltaTicker))

	if len(sVersion) == 0:
		LOG("DB_HC: Error: len(sVersion) == 0")
		MONITOR("", "", "", "", "", "DB_HC_FAIL", "len(sVersion) == 0")
		return False

	LOG("DB_HC: version=" + sVersion)
	MONITOR("", "", "", "", "", "DB_HC_SUCCESS", "")
	return True
# def DB_HC




# Обновление структуры БД
def DB_create() -> bool:
	global dbConnect

	if not DB_check():
		LOG("DB_create: Error: not DB_check()")
		return False

	lockDB.acquire()
	try:
		# Таблица vcontact - Список ВКС
		print("DB_create: create table vcontact")
		dbCursor = dbConnect.cursor()
		try:
			dbCursor.execute("""
				CREATE TABLE IF NOT EXISTS vcontact (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					chat_id varchar(255) NOT NULL,
					chat_name varchar(255) NOT NULL,
					from_id varchar(255) NOT NULL,
					from_name varchar(255) NOT NULL,
					from_user varchar(255) NOT NULL,
					sort varchar(255) NOT NULL,
					name varchar(255) NOT NULL,
					description varchar(4000) NOT NULL,
					create_date DATETIME NOT NULL,  
					PRIMARY KEY (id)
				)
				""")

			# Добавить столбец chat_name
			#dbCursor.execute("""
			#	SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=%s AND TABLE_NAME='vcontact' AND COLUMN_NAME='chat_name'
			#	""", (sDB_NAME))
			#rows = dbCursor.fetchall()
			#if len(rows) == 0:
			#	dbCursor.execute("""
			#		ALTER TABLE vcontact
			#			ADD chat_name varchar(255)
			#			AFTER chat_id
			#		""")
			# if

			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_id ON vcontact (chat_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_name ON vcontact (chat_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_id ON vcontact (from_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_name ON vcontact (from_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_user ON vcontact (from_user)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS sort ON vcontact (sort)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date ON vcontact (create_date)")
			dbConnect.commit()
		except Exception as exsql:
			print("DB_create: Error: create table vcontact: " + str(exsql))
			return False
		finally:
			dbCursor.close()
		# end try


		# Таблица buy - Список покупок
		print("DB_create: create table buy")
		dbCursor = dbConnect.cursor()
		try:
			# Таблица
			dbCursor.execute("""
				CREATE TABLE IF NOT EXISTS buy (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					chat_id varchar(255) NOT NULL,
					chat_name varchar(255) NOT NULL,
					from_id varchar(255) NOT NULL,
					from_name varchar(255) NOT NULL,
					from_user varchar(255) NOT NULL,
					sort varchar(255) NOT NULL,
					name varchar(255) NOT NULL,
					description varchar(4000) NOT NULL,
					create_date DATETIME NOT NULL,  
					PRIMARY KEY (id)
				)
				""")

			# Добавить столбец chat_name
			#dbCursor.execute("""
			#	SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=%s AND TABLE_NAME='buy' AND COLUMN_NAME='chat_name'
			#	""", (sDB_NAME))
			#rows = dbCursor.fetchall()
			#if len(rows) == 0:
			#	dbCursor.execute("""
			#		ALTER TABLE buy
			#			ADD chat_name varchar(255)
			#			AFTER chat_id
			#		""")
			# if

			# Индексы
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_id ON buy (chat_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_name ON buy (chat_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_id ON buy (from_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_name ON buy (from_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_user ON buy (from_user)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS sort ON buy (sort)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date ON buy (create_date)")
			dbConnect.commit()
		except Exception as exsql:
			print("DB_create: Error: create table buy: " + str(exsql))
			return False
		finally:
			dbCursor.close()
		# end try


		# Таблица sport - Тренировки
		print("DB_create: create table sport")
		dbCursor = dbConnect.cursor()
		try:
			# Таблица
			dbCursor.execute("""
				CREATE TABLE IF NOT EXISTS sport (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					chat_id varchar(255) NOT NULL,
					chat_name varchar(255) NOT NULL,
					from_id varchar(255) NOT NULL,
					from_name varchar(255) NOT NULL,
					from_user varchar(255) NOT NULL,
					name varchar(255) NOT NULL,
					description varchar(4000) NOT NULL,
					count DECIMAL(10, 1) NOT NULL,
					koeff DECIMAL(10, 1) NOT NULL,
					km DECIMAL(10, 1) NOT NULL,
					create_date DATETIME NOT NULL,  
					gmt TINYINT NOT NULL,  
					create_date_local DATETIME NOT NULL,  
					PRIMARY KEY (id)
				)
				""")

			# Индексы
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_id ON sport (chat_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_name ON sport (chat_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_id ON sport (from_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_name ON sport (from_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_user ON sport (from_user)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date ON sport (create_date)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date_local ON sport (create_date_local)")
			dbConnect.commit()
		except Exception as exsql:
			print("DB_create: Error: create table sport: " + str(exsql))
			return False
		finally:
			dbCursor.close()
		# end try


		# Таблица feedback - Обратная связь
		print("DB_create: create table feedback")
		dbCursor = dbConnect.cursor()
		try:
			# Таблица
			dbCursor.execute("""
				CREATE TABLE IF NOT EXISTS feedback (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					chat_id varchar(255) NOT NULL,
					chat_name varchar(255) NOT NULL,
					from_id varchar(255) NOT NULL,
					from_name varchar(255) NOT NULL,
					from_user varchar(255) NOT NULL,
					message varchar(4000) NOT NULL,
					folder varchar(255) NOT NULL,
					status varchar(255) NOT NULL,
					reply_to_id bigint(20) unsigned,
					create_date DATETIME NOT NULL,  
					gmt TINYINT NOT NULL,  
					create_date_local DATETIME NOT NULL,  
					PRIMARY KEY (id)
				)
				""")

			# Индексы
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_id ON feedback (chat_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_name ON feedback (chat_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_id ON feedback (from_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_name ON feedback (from_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_user ON feedback (from_user)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS folder ON feedback (folder)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS status ON feedback (status)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS reply_to_id ON feedback (reply_to_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date ON feedback (create_date)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date_local ON feedback (create_date_local)")
			dbConnect.commit()
		except Exception as exsql:
			print("DB_create: Error: create table feedback: " + str(exsql))
			return False
		finally:
			dbCursor.close()
		# end try


		# Таблица event - Мероприятие
		print("DB_create: create table event")
		dbCursor = dbConnect.cursor()
		try:
			# Таблица
			dbCursor.execute("""
				CREATE TABLE IF NOT EXISTS event (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					chat_id varchar(255) NOT NULL,
					chat_name varchar(255) NOT NULL,
					from_id varchar(255) NOT NULL,
					from_name varchar(255) NOT NULL,
					from_user varchar(255) NOT NULL,
					event varchar(255) NOT NULL,
					title varchar(255) NOT NULL,
					description varchar(4000) NOT NULL,
					status varchar(255) NOT NULL,
					create_date DATETIME NOT NULL,  
					gmt TINYINT NOT NULL,  
					create_date_local DATETIME NOT NULL,  
					PRIMARY KEY (id)
				)
				""")

			# Индексы
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_id ON event (chat_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_name ON event (chat_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_id ON event (from_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_name ON event (from_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_user ON event (from_user)")
			dbCursor.execute("CREATE UNIQUE INDEX IF NOT EXISTS event ON event (event)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS status ON event (status)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date ON event (create_date)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date_local ON event (create_date_local)")
			dbConnect.commit()
		except Exception as exsql:
			print("DB_create: Error: create table event: " + str(exsql))
			return False
		finally:
			dbCursor.close()
		# end try


		# Таблица event_request - Заявка на мероприятие
		print("DB_create: create table event_request")
		dbCursor = dbConnect.cursor()
		try:
			# Таблица
			dbCursor.execute("""
				CREATE TABLE IF NOT EXISTS event_request (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					chat_id varchar(255) NOT NULL,
					chat_name varchar(255) NOT NULL,
					from_id varchar(255) NOT NULL,
					from_name varchar(255) NOT NULL,
					from_user varchar(255) NOT NULL,
					event varchar(255) NOT NULL,
					title varchar(255) NOT NULL,
					description varchar(4000) NOT NULL,
					status varchar(255) NOT NULL,
					feedback varchar(4000) NOT NULL,
					create_date DATETIME NOT NULL,  
					gmt TINYINT NOT NULL,  
					create_date_local DATETIME NOT NULL,  
					PRIMARY KEY (id)
				)
				""")

			# Индексы
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_id ON event_request (chat_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_name ON event_request (chat_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_id ON event_request (from_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_name ON event_request (from_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_user ON event_request (from_user)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS event ON event_request (event)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS status ON event_request (status)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date ON event_request (create_date)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date_local ON event_request (create_date_local)")
			dbConnect.commit()
		except Exception as exsql:
			print("DB_create: Error: create table event_request: " + str(exsql))
			return False
		finally:
			dbCursor.close()
		# end try


		# Таблица monitor - Мониторинг
		print("DB_create: create table monitor")
		dbCursor = dbConnect.cursor()
		try:
			# Таблица
			dbCursor.execute("""
				CREATE TABLE IF NOT EXISTS monitor (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					chat_id varchar(255) NOT NULL,
					chat_name varchar(255) NOT NULL,
					from_id varchar(255) NOT NULL,
					from_name varchar(255) NOT NULL,
					from_user varchar(255) NOT NULL,
					name varchar(255) NOT NULL,
					description varchar(4000) NOT NULL,
					count SMALLINT UNSIGNED NOT NULL,
					start_date DATETIME NOT NULL,  
					end_date DATETIME NOT NULL,
					create_date DATETIME NOT NULL,
					PRIMARY KEY (id)
				)
				""")

			# Добавить столбец end_date
			#dbCursor.execute("""
			#	SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=%s AND TABLE_NAME='monitor' AND COLUMN_NAME='end_date'
			#	""", (sDB_NAME))
			#rows = dbCursor.fetchall()
			#if len(rows) == 0:
			#	dbCursor.execute("""
			#		ALTER TABLE monitor
			#			ADD end_date DATETIME
			#			AFTER count
			#		""")
			# if

			# Добавить столбец start_date
			#dbCursor.execute("""
			#	SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=%s AND TABLE_NAME='monitor' AND COLUMN_NAME='start_date'
			#	""", (sDB_NAME))
			#rows = dbCursor.fetchall()
			#if len(rows) == 0:
			#	dbCursor.execute("""
			#		ALTER TABLE monitor
			#			ADD start_date DATETIME
			#			AFTER count
			#		""")
			# if

			# Индексы
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_id ON monitor (chat_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS chat_name ON monitor (chat_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_id ON monitor (from_id)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_name ON monitor (from_name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS from_user ON monitor (from_user)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS name ON monitor (name)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS start_date ON monitor (start_date)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS end_date ON monitor (end_date)")
			dbCursor.execute("CREATE INDEX IF NOT EXISTS create_date ON monitor (create_date)")
			dbConnect.commit()
		except Exception as exsql:
			print("DB_create: Error: create table monitor: " + str(exsql))
			return False
		finally:
			dbCursor.close()
		# end try

		return True
	except Exception as ex:
		print("DB_create: Error: " + str(ex))
		return False
	finally:
		lockDB.release()
	# end try
# def DB_create


# Покупки
# Добавить
def DB_BUY_insert(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sText: str) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)

	sText = strisnull(sText, "").strip()
	sName, sDescription = strsplit(sText, ".")
	sName = strtrunc(strisnull(sName, "").strip(), 255)
	sDescription = strtrunc(strisnull(sDescription, "").strip(), 4000)

	sSort = strtrunc(sFromName.lower() + "+" + sName.lower(), 255)

	LOG("DB_BUY_insert: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", sName=" + sName +
		", sDescription=" + sDescription)
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_BUY_INSERT_TOTAL", "")

	if not DB_check():
		LOG("DB_BUY_insert: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_BUY_INSERT_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				INSERT INTO buy (chat_id, chat_name, from_id, from_name, from_user, sort, name, description, create_date) 
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
				""", (sChatId, sChatName, sFromId, sFromName, sFromUser, sSort, sName, sDescription, datetime.today()) )
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_BUY_insert: Error: " + sError)
		MONITOR("", "", "", "", "", "DB_BUY_INSERT_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR("", "", "", "", "", "DB_BUY_INSERT_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_BUY_INSERT_SUCCESS", "")
	return True
# def DB_BUY_insert


# Покупки
# Выбрать покупки по sChatId
# Вернуть список: id, from_name, name, description
def DB_BUY_selectByChatId(sChatId: str):
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)

	LOG("DB_BUY_selectByChatId: sChatId=" + sChatId)
	MONITOR(sChatId, "", "", "", "", "DB_BUY_SELECTBYCHATID_TOTAL", "")

	rows = list()

	if not DB_check():
		LOG("DB_BUY_selectByChatId: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_BUY_SELECTBYCHATID_FAIL", "not DB_check()")
		return list()

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id, from_name, name, description 
				FROM buy 
				WHERE chat_id = %s
				ORDER BY sort
				""", (sChatId))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_BUY_selectByChatId: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_BUY_SELECTBYCHATID_FAIL", sError)
		return list()

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_BUY_SELECTBYCHATID_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, "", "", "", "", "DB_BUY_SELECTBYCHATID_SUCCESS", "")
	return rows
# def DB_BUY_selectByChatId


# Покупки
# Выбрать покупки по sChatId и sBuyId
# Вернуть список: id, name
def DB_BUY_selectOne(sChatId: str, sBuyId: str):
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sBuyId = strtrunc(strisnull(sBuyId, "").strip(), 255)

	LOG("DB_BUY_selectOne: sChatId=" + sChatId + ", sBuyId=" + sBuyId)
	MONITOR(sChatId, "", "", "", "", "DB_BUY_SELECTONE_TOTAL", "")

	rows = list()

	if not DB_check():
		LOG("DB_BUY_selectOne: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_BUY_SELECTONE_FAIL", "not DB_check()")
		return list()

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id, from_name, name, description 
				FROM buy 
				WHERE chat_id = %s and id = %s
				ORDER BY sort
				""", (sChatId, sBuyId))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_BUY_selectOne: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_BUY_SELECTONE_FAIL", sError)
		return list()

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_BUY_SELECTONE_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, "", "", "", "", "DB_BUY_SELECTONE_SUCCESS", "")
	return rows
# def DB_BUY_selectOne


# Покупки
# Удалить
def DB_BUY_delete(sChatId: str, sBuyId: str) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sBuyId = strtrunc(strisnull(sBuyId, "").strip(), 255)

	LOG("DB_BUY_delete: sChatId=" + sChatId + ", sBuyId=" + sBuyId)
	MONITOR(sChatId, "", "", "", "", "DB_BUY_DELETE_TOTAL", "")

	if not DB_check():
		LOG("DB_BUY_delete: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_BUY_DELETE_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				DELETE FROM buy 
				WHERE chat_id = %s and id = %s
				""", (sChatId, sBuyId) )
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_BUY_delete: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_BUY_DELETE_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_BUY_DELETE_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, "", "", "", "", "DB_BUY_DELETE_SUCCESS", "")
	return True
# def DB_BUY_delete


# Покупки
# Удалить все
def DB_BUY_deleteAll(sChatId: str) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)

	LOG("DB_BUY_deleteAll: sChatId=" + sChatId)
	MONITOR(sChatId, "", "", "", "", "DB_BUY_DELETEALL_TOTAL", "")

	if not DB_check():
		LOG("DB_BUY_deleteAll: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_BUY_DELETEALL_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				DELETE FROM buy 
				WHERE chat_id = %s
				""", (sChatId) )
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_BUY_deleteAll: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_BUY_DELETEALL_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_BUY_DELETEALL_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, "", "", "", "", "DB_BUY_DELETEALL_SUCCESS", "")
	return True
# def DB_BUY_deleteAll


# ВКС
# Добавить
def DB_VCONTACT_insert(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sText: str) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)

	sText = strisnull(sText, "").strip()
	sName, sDescription = strsplit(sText, ".")
	sName = strtrunc(strisnull(sName, "").strip(), 255)
	sDescription = strtrunc(strisnull(sDescription, "").strip(), 4000)

	sSort = strtrunc(sName.lower(), 255)

	LOG("DB_VCONTACT_insert: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", sName=" + sName +
		", sDescription=" + sDescription)
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_VCONTACT_INSERT_TOTAL", "")

	if not DB_check():
		LOG("DB_VCONTACT_insert: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_VCONTACT_INSERT_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				INSERT INTO vcontact (chat_id, chat_name, from_id, from_name, from_user, sort, name, description, create_date) 
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
				""", (sChatId, sChatName, sFromId, sFromName, sFromUser, sSort, sName, sDescription, datetime.today()) )
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_VCONTACT_insert: Error: " + sError)
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_VCONTACT_INSERT_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_VCONTACT_INSERT_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_VCONTACT_INSERT_SUCCESS", "")
	return True
# def DB_VCONTACT_insert


# ВКС
# Выбрать по sChatId
# Вернуть список: id, name
def DB_VCONTACT_selectByChatId(sChatId: str):
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)

	LOG("DB_VCONTACT_selectByChatId: sChatId=" + sChatId)
	MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_SELECTBYCHATID_TOTAL", "")

	rows = list()

	if not DB_check():
		LOG("DB_VCONTACT_selectByChatId: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_VCONTACT_SELECTBYCHATID_FAIL", "not DB_check()")
		return list()

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id, from_name, name, description 
				FROM vcontact 
				WHERE chat_id = %s
				ORDER BY sort
				""", (sChatId))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_VCONTACT_selectByChatId: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_SELECTBYCHATID_FAIL", sError)
		return list()

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_SELECTBYCHATID_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_SELECTBYCHATID_SUCCESS", "")
	return rows
# def DB_VCONTACT_selectByChatId


# ВКС
# Выбрать по sChatId и sBuyId
# Вернуть список: id, name
def DB_VCONTACT_selectOne(sChatId: str, sBuyId: str):
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sBuyId = strtrunc(strisnull(sBuyId, "").strip(), 255)

	LOG("DB_VCONTACT_selectOne: sChatId=" + sChatId + ", sBuyId=" + sBuyId)
	MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_SELECTONE_TOTAL", "")

	rows = list()

	if not DB_check():
		LOG("DB_VCONTACT_selectOne: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_VCONTACT_SELECTONE_FAIL", "not DB_check()")
		return list()

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id, from_name, name, description 
				FROM vcontact 
				WHERE chat_id = %s and id = %s
				ORDER BY sort
				""", (sChatId, sBuyId))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_VCONTACT_selectOne: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_SELECTONE_FAIL", sError)
		return list()

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_SELECTONE_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_SELECTONE_SUCCESS", "")
	return rows
# def DB_VCONTACT_selectOne


# ВКС
# Удалить
def DB_VCONTACT_delete(sChatId: str, sBuyId: str) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sBuyId = strtrunc(strisnull(sBuyId, "").strip(), 255)

	LOG("DB_VCONTACT_delete: sChatId=" + sChatId + ", sBuyId=" + sBuyId)
	MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_DELETE_TOTAL", "")

	if not DB_check():
		LOG("DB_VCONTACT_delete: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_VCONTACT_DELETE_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				DELETE FROM vcontact 
				WHERE chat_id = %s and id = %s
				""", (sChatId, sBuyId) )
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_VCONTACT_delete: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_DELETE_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_DELETE_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_DELETE_SUCCESS", "")
	return True
# def DB_VCONTACT_delete


# ВКС
# Удалить все
def DB_VCONTACT_deleteAll(sChatId: str) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)

	LOG("DB_VCONTACT_deleteAll: sChatId=" + sChatId)
	MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_DELETEALL_TOTAL", "")

	if not DB_check():
		LOG("DB_VCONTACT_deleteAll: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_VCONTACT_DELETEALL_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				DELETE FROM vcontact 
				WHERE chat_id = %s
				""", (sChatId) )
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_VCONTACT_deleteAll: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_DELETEALL_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_DELETEALL_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, "", "", "", "", "DB_VCONTACT_DELETEALL_SUCCESS", "")
	return True
# def DB_VCONTACT_deleteAll


# Спорт
# Добавить
def DB_SPORT_insert(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sName: str, sDescription: str, fCount: float, gmt: int) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)
	sName = strtrunc(strisnull(sName, "").strip(), 255)
	sDescription = strtrunc(strisnull(sDescription, "").strip(), 4000)

	LOG("DB_SPORT_insert: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", sName=" + sName +
		", sDescription=" + sDescription +
		", fCount=" + str(fCount))
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_SPORT_INSERT_TOTAL", "")

	if fCount <= 0:
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_SPORT_INSERT_FAIL", "fCount <= 0")
		return False
	# fi

	# Перевод в км
	fkoeff = 1.0
	if sName == "RUN": # Бег
		fkoeff = 1.0
	elif sName == "SKY": # Лыжи
		fkoeff = 1.0
	elif sName == "WALK": # Ходьба
		fkoeff = 0.7
	elif sName == "BIKE": # Велосипед
		fkoeff = 0.5
	elif sName == "SWIM": # Плавание
		fkoeff = 5.0
	elif sName == "OTHER": # Прочие активности в минутах
		fkoeff = 0.1
	fkm = fCount * fkoeff

	# Даты
	if gmt < -23:
		gmt = -23
	if gmt > 23:
		gmt = 23
	create_date = datetime.today()
	create_date_local = create_date + timedelta(hours=gmt)

	if not DB_check():
		LOG("DB_SPORT_insert: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_SPORT_INSERT_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				INSERT INTO sport (chat_id, chat_name, from_id, from_name, from_user, name, description, count, koeff, km, create_date, gmt, create_date_local) 
				VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
				""", (sChatId, sChatName, sFromId, sFromName, sFromUser, sName, sDescription, fCount, fkoeff, fkm, create_date, gmt, create_date_local) )
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_SPORT_insert: Error: " + sError)
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_SPORT_INSERT_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_SPORT_INSERT_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_SPORT_INSERT_SUCCESS", "")
	return True
# def DB_SPORT_insert


# Спорт
# Определить дату первой записи
def DB_SPORT_selectMinDate(sChatId: str, gmt: int) -> datetime:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)

	LOG("DB_SPORT_selectMinDate: sChatId=" + sChatId + ", gmt=" + str(gmt))
	MONITOR(sChatId, "", "", "", "", "DB_SPORT_SELECTMINDATE_TOTAL", "")

	if gmt < -23:
		gmt = -23
	if gmt > 23:
		gmt = 23

	dtRes = datetime.today() + timedelta(hours=gmt)
	rows = list()

	if not DB_check():
		LOG("DB_SPORT_selectMinDate: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_SPORT_SELECTMINDATE_FAIL", "not DB_check()")
		return dtRes

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT min(create_date_local) FROM sport WHERE chat_id = %s
				""", (sChatId))
			rows = dbCursor.fetchall()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_SPORT_selectMinDate: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_SPORT_SELECTMINDATE_FAIL", sError)
		return dtRes

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_SPORT_SELECTMINDATE_LONGTIME", str(iDeltaTicker))

	try:
		if len(rows) > 0:
			dtRes = rows[0][0]
	except:
		dtRes = dtRes
	# try

	MONITOR(sChatId, "", "", "", "", "DB_SPORT_SELECTMINDATE_SUCCESS", "")
	return dtRes
# def DB_SPORT_selectMinDate


# Спорт
# Выбрать статистику за период
def DB_SPORT_selectStatPeriod(sChatId: str, dtStart: datetime, dtEnd: datetime, sTitle: str) -> str:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)

	LOG("DB_SPORT_selectStatPeriod: sChatId=" + sChatId + ", Период: с " + str(dtStart) + " по " + str(dtEnd))
	MONITOR(sChatId, "", "", "", "", "DB_SPORT_SELECTPERIOD_TOTAL", "")

	sRes = ""
	rows = list()

	if not DB_check():
		LOG("DB_SPORT_selectStatPeriod: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_SPORT_SELECTPERIOD_FAIL", "not DB_check()")
		return ""

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dKmAll = 0.0

			# Бег
			dCount = 0.0
			dKm = 0.0
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT sum(count), sum(km) FROM sport WHERE chat_id = %s and create_date_local >= %s and create_date_local <= %s AND name = %s
				""", (sChatId, dtStart, dtEnd, "RUN"))
			iDeltaTicker2 = time.time() - dtStartTicker
			if iDeltaTicker2 > iDeltaTicker:
				iDeltaTicker = iDeltaTicker2
			rows = dbCursor.fetchall()
			if len(rows) > 0:
				try:
					dCount = float(rows[0][0])
				except:
					dCount = 0.0
				try:
					dKm = float(rows[0][1])
				except:
					dKm = 0.0
			# fi
			if dKm > 0:
				dKmAll += dKm
				sRes += "\n  – Бег " + str(dCount) + " км * 1 = " + str(dKm) + " км"
			# fi

			# Лыжи
			dCount = 0.0
			dKm = 0.0
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT sum(count), sum(km) FROM sport WHERE chat_id = %s and create_date_local >= %s and create_date_local <= %s AND name = %s
				""", (sChatId, dtStart, dtEnd, "SKY"))
			iDeltaTicker2 = time.time() - dtStartTicker
			if iDeltaTicker2 > iDeltaTicker:
				iDeltaTicker = iDeltaTicker2
			rows = dbCursor.fetchall()
			if len(rows) > 0:
				try:
					dCount = float(rows[0][0])
				except:
					dCount = 0.0
				try:
					dKm = float(rows[0][1])
				except:
					dKm = 0.0
			# fi
			if dKm > 0:
				dKmAll += dKm
				sRes += "\n  – Лыжи " + str(dCount) + " км * 1 = " + str(dKm) + " км"
			# fi

			# Ходьба
			dCount = 0.0
			dKm = 0.0
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT sum(count), sum(km) FROM sport WHERE chat_id = %s and create_date_local >= %s and create_date_local <= %s AND name = %s
				""", (sChatId, dtStart, dtEnd, "WALK"))
			iDeltaTicker2 = time.time() - dtStartTicker
			if iDeltaTicker2 > iDeltaTicker:
				iDeltaTicker = iDeltaTicker2
			rows = dbCursor.fetchall()
			if len(rows) > 0:
				try:
					dCount = float(rows[0][0])
				except:
					dCount = 0.0
				try:
					dKm = float(rows[0][1])
				except:
					dKm = 0.0
			# fi
			if dKm > 0:
				dKmAll += dKm
				sRes += "\n  – Ходьба " + str(dCount) + " км * 0.7 = " + str(dKm) + " км"
			# fi

			# Велик
			dCount = 0.0
			dKm = 0.0
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT sum(count), sum(km) FROM sport WHERE chat_id = %s and create_date_local >= %s and create_date_local <= %s AND name = %s
				""", (sChatId, dtStart, dtEnd, "BIKE"))
			iDeltaTicker2 = time.time() - dtStartTicker
			if iDeltaTicker2 > iDeltaTicker:
				iDeltaTicker = iDeltaTicker2
			rows = dbCursor.fetchall()
			if len(rows) > 0:
				try:
					dCount = float(rows[0][0])
				except:
					dCount = 0.0
				try:
					dKm = float(rows[0][1])
				except:
					dKm = 0.0
			# fi
			if dKm > 0:
				dKmAll += dKm
				sRes += "\n  – Велик " + str(dCount) + " км * 0.5 = " + str(dKm) + " км"
			# fi

			# Плавание
			dCount = 0.0
			dKm = 0.0
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT sum(count), sum(km) FROM sport WHERE chat_id = %s and create_date_local >= %s and create_date_local <= %s AND name = %s
				""", (sChatId, dtStart, dtEnd, "SWIM"))
			iDeltaTicker2 = time.time() - dtStartTicker
			if iDeltaTicker2 > iDeltaTicker:
				iDeltaTicker = iDeltaTicker2
			rows = dbCursor.fetchall()
			if len(rows) > 0:
				try:
					dCount = float(rows[0][0])
				except:
					dCount = 0.0
				try:
					dKm = float(rows[0][1])
				except:
					dKm = 0.0
			# fi
			if dKm > 0:
				dKmAll += dKm
				sRes += "\n  – Плавание " + str(dCount) + " км * 5 = " + str(dKm) + " км"
			# fi

			# Пр. активности
			dCount = 0.0
			dKm = 0.0
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT sum(count), sum(km) FROM sport WHERE chat_id = %s and create_date_local >= %s and create_date_local <= %s AND name = %s
				""", (sChatId, dtStart, dtEnd, "OTHER"))
			iDeltaTicker2 = time.time() - dtStartTicker
			if iDeltaTicker2 > iDeltaTicker:
				iDeltaTicker = iDeltaTicker2
			rows = dbCursor.fetchall()
			if len(rows) > 0:
				try:
					dCount = float(rows[0][0])
				except:
					dCount = 0.0
				try:
					dKm = float(rows[0][1])
				except:
					dKm = 0.0
			# fi
			if dKm > 0:
				dKmAll += dKm
				sRes += "\n  – Пр. активности " + str(dCount) + " мин * 0,1 = " + str(dKm) + " км"
			# fi

			dKmAll = round(dKmAll, 1)
			if dKmAll > 0.0:
				sRes = "\n" + sTitle + " = " + str(dKmAll) + " км:" + sRes
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_SPORT_selectStatPeriod: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_SPORT_SELECTPERIOD_FAIL", sError)
		return ""

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_SPORT_SELECTPERIOD_LONGTIME", "RUN: " + str(iDeltaTicker))

	MONITOR(sChatId, "", "", "", "", "DB_SPORT_SELECTPERIOD_SUCCESS", "")
	return sRes
# def DB_SPORT_selectStatPeriod


# Спорт
# Выбрать статистику за текущий месяц
def DB_SPORT_selectStatMonth(sChatId: str, gmt: int) -> str:
	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)

	LOG("DB_SPORT_selectStatMonth: sChatId=" + sChatId)

	if gmt < -23:
		gmt = -23
	if gmt > 23:
		gmt = 23

	try:
		# Текущий месяц
		dtToday = datetime.today() + timedelta(hours=gmt)
		dtStart = dtToday.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
		dtEnd = dtToday.replace(microsecond=0)
		sRes = str(DB_SPORT_selectStatPeriod(sChatId, dtStart, dtEnd, "Месяц " + dtStart.strftime("%Y.%m")))
		if len(sRes) == 0:
			sRes = "\nПусто"
		return sRes
	except Exception as ex:
		LOG("DB_SPORT_selectStatMonth: Error: " + str(ex))
		return ""
	# end try
# def DB_SPORT_selectStatMonth


# Спорт
# Выбрать статистику полную
def DB_SPORT_selectStatFull(sChatId: str, gmt: int) -> str:
	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)

	LOG("DB_SPORT_selectStatFull: sChatId=" + sChatId)

	if gmt < -23:
		gmt = -23
	if gmt > 23:
		gmt = 23

	sRes = ""
	rows = list()
	try:
		# Текущая дата
		dtToday = datetime.today() + timedelta(hours=gmt)
		iYearCur = dtToday.year # Текущий год
		dtYearCur = dtToday.replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)
		iMonthCur = dtToday.month # Текущий месяц
		LOG("DB_SPORT_selectStatFull: dtToday=" + str(dtToday))

		# Стартовая дата статистики
		dtMin = DB_SPORT_selectMinDate(sChatId, gmt)
		if dtMin is None:
			dtMin = dtToday
		LOG("DB_SPORT_selectStatFull: dtMin=" + str(dtMin))

		# Стартовый месяц в текущем году
		dtMonthStart = dtMin
		if dtMin < dtYearCur:
			dtMonthStart = dtYearCur # Взять текущий месяц из dtYearCur
		else:
			dtMonthStart = dtMin.replace(day=1, hour=0, minute=0, second=0, microsecond=0) # Взять текущий месяц из dtMin
		iMonthStart = dtMonthStart.month

		# Пройти по всем месяцам текущего года
		lst = list(range(iMonthStart, iMonthCur+1))
		LOG("DB_SPORT_selectStatFull: dtMonthStart=" + str(dtMonthStart) + ", list=" + str(lst))
		for i in lst:
			t = calendar.monthrange(iYearCur, i)
			lst2 = list(t)
			d2 = lst2[len(lst2)-1]
			dtStart = dtYearCur.replace(month=i, day=1, hour=0, minute=0, second=0, microsecond=0)
			dtEnd = dtYearCur.replace(month=i, day=d2, hour=23, minute=59, second=59, microsecond=0)
			LOG("DB_SPORT_selectStatFull: Месяц " + str(i) + ", dtStart=" + str(dtStart) + ", dtEnd=" + str(dtEnd))

			s = str(DB_SPORT_selectStatPeriod(sChatId, dtStart, dtEnd, "Месяц " + dtStart.strftime("%Y.%m")))
			sRes += s
		# for

		# Стартовый год
		iYearStart = dtMin.year

		# Пройти по всем годам от начального до текущего
		lst = list(range(iYearStart, iYearCur+1))
		LOG("DB_SPORT_selectStatFull: iYearStart=" + str(iYearStart) + ", list=" + str(lst))
		for i in lst:
			dtStart = datetime(i, 1, 1, 0, 0, 0, 0)
			dtEnd = datetime(i, 12, 31, 23, 59, 59, 0)
			LOG("DB_SPORT_selectStatFull: Год " + str(i) + ", dtStart=" + str(dtStart) + ", dtEnd=" + str(dtEnd))

			s = str(DB_SPORT_selectStatPeriod(sChatId, dtStart, dtEnd, "Год " + dtStart.strftime("%Y")))
			sRes += s
		# for

		if len(sRes) == 0:
			sRes = "\nПусто"

		return sRes
	except Exception as ex:
		LOG("DB_SPORT_selectStatFull: Error: " + str(ex))
		MONITOR(sChatId, "", "", "", "", "DB_SPORT_SELECTSTATFULL_FAIL", str(ex))
		return ""
	# end try
# def DB_SPORT_selectStatFull


# Спорт
# Удалить все
def DB_SPORT_deleteAll(sChatId: str) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)

	LOG("DB_SPORT_deleteAll: sChatId=" + sChatId)
	MONITOR(sChatId, "", "", "", "", "DB_SPORT_DELETEALL_TOTAL", "")

	if not DB_check():
		LOG("DB_SPORT_deleteAll: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_SPORT_DELETEALL_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				DELETE FROM sport 
				WHERE chat_id = %s
				""", (sChatId) )
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_SPORT_deleteAll: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_SPORT_DELETEALL_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_SPORT_DELETEALL_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, "", "", "", "", "DB_SPORT_DELETEALL_SUCCESS", "")
	return True
# def DB_SPORT_deleteAll


# Мероприятия
# Ввести описание
def DB_EVENT_updateDescription(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sEvent: str, sText: str, gmt: int) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	sText = strisnull(sText, "").strip()
	sTitle, sDescription = strsplit(sText, ".")
	sTitle = strtrunc(strisnull(sTitle, "").strip(), 255)
	sDescription = strtrunc(strisnull(sDescription, "").strip(), 4000)

	LOG("DB_EVENT_updateDescription: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", sEvent=" + sEvent +
		", sTitle=" + sTitle +
		", sDescription=" + sDescription)
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATEDESCRIPTION_TOTAL", "")

	# Даты
	if gmt < -23:
		gmt = -23
	if gmt > 23:
		gmt = 23
	create_date = datetime.today()
	create_date_local = create_date + timedelta(hours=gmt)

	if not DB_check():
		LOG("DB_EVENT_updateDescription: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_UPDATEDESCRIPTION_FAIL", "not DB_check()")
		return False

	# Найти уже существующую запись мероприятия
	iEventId = DB_EVENT_selectOne(sChatId, sEvent)

	# Добавление мероприятия
	if iEventId <= 0:
		iDeltaTicker = 0.0
		sError = ""
		lockDB.acquire()
		try:
			dbCursor = dbConnect.cursor()
			try:
				dtStartTicker = time.time()
				dbCursor.execute("""
					INSERT INTO event (chat_id, chat_name, from_id, from_name, from_user, event, title, description, status, create_date, gmt, create_date_local) 
					VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
					""", (sChatId, sChatName, sFromId, sFromName, sFromUser, sEvent, sTitle, sDescription, "", create_date, gmt, create_date_local) )
				dbConnect.commit()
				iDeltaTicker = time.time() - dtStartTicker
			except Exception as exsql:
				sError = str(exsql)
			finally:
				dbCursor.close()
			# end try
		except Exception as ex:
			sError = str(ex)
		finally:
			lockDB.release()
		# end try

		if len(sError) > 0:
			LOG("DB_EVENT_updateDescription: Error: INSERT: " + sError)
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATEDESCRIPTION_FAIL", "INSERT: " + sError)
			return False

		if iDeltaTicker > iDB_LONGTIME:
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATEDESCRIPTION_LONGTIME", "INSERT: " + str(iDeltaTicker))

		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATEDESCRIPTION_SUCCESS", "")
		return True

	# Обновление описания мероприятия
	else:
		iDeltaTicker = 0.0
		sError = ""
		lockDB.acquire()
		try:
			dbCursor = dbConnect.cursor()
			try:
				dtStartTicker = time.time()
				dbCursor.execute("""
					UPDATE event
					SET title = %s, description = %s
					WHERE id = %s
					""", (sTitle, sDescription, iEventId))
				dbConnect.commit()
				iDeltaTicker = time.time() - dtStartTicker
			except Exception as exsql:
				sError = str(exsql)
			finally:
				dbCursor.close()
			# end try
		except Exception as ex:
			sError = str(ex)
		finally:
			lockDB.release()
		# end try

		if len(sError) > 0:
			LOG("DB_EVENT_updateDescription: Error: UPDATE: " + sError)
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATEDESCRIPTION_FAIL", "UPDATE: " + sError)
			return False

		if iDeltaTicker > iDB_LONGTIME:
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATEDESCRIPTION_LONGTIME", "UPDATE: " + str(iDeltaTicker))

		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATEDESCRIPTION_SUCCESS", "")
		return True
	# if
# def DB_EVENT_updateDescription


# Мероприятия
# Обновить статус
def DB_EVENT_updateStatus(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sEvent: str, sStatus: str) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)
	sStatus = strtrunc(strisnull(sStatus, "").strip(), 255)

	LOG("DB_EVENT_updateStatus: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", sEvent=" + sEvent +
		", sStatus=" + sStatus)
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATESTATUS_TOTAL", "")

	if not DB_check():
		LOG("DB_EVENT_updateStatus: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_UPDATESTATUS_FAIL", "not DB_check()")
		return False

	# Найти уже существующую запись мероприятия
	iEventId = DB_EVENT_selectOne(sChatId, sEvent)

	if iEventId <= 0:
		LOG("DB_EVENT_updateStatus: Error: Не найдено мероприятие " + sEvent)
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATESTATUS_FAIL", "Не найдено мероприятие " + sEvent)
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				UPDATE event
				SET status = %s
				WHERE id = %s
				""", (sStatus, iEventId))
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_updateStatus: Error: " + sError)
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATESTATUS_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATESTATUS_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_UPDATESTATUS_SUCCESS", "")
	return True
# def DB_EVENT_updateStatus


# Мероприятия
# Удалить заявки мероприятия
def DB_EVENT_REQUEST_delete(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sEvent: str) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	LOG("DB_EVENT_REQUEST_delete: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", sEvent=" + sEvent)
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_DELETE_TOTAL", "")

	if not DB_check():
		LOG("DB_EVENT_REQUEST_delete: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_REQUEST_DELETE_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				DELETE FROM event_request
				WHERE event = %s
				""", (sEvent))
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_REQUEST_delete: Error: " + sError)
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_DELETE_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_DELETE_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_DELETE_SUCCESS", "")
	return True
# def DB_EVENT_REQUEST_delete


# Мероприятия
# Выбрать список доступных пользователю мероприятий
# Вернуть справочник "event: кнопочки"
def DB_EVENT_selectEvents(sChatId: str):
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)

	LOG("DB_EVENT_selectEvents: sChatId=" + sChatId)
	MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTEVENTS_TOTAL", "")

	rows = list()

	if not DB_check():
		LOG("DB_EVENT_selectEvents: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_SELECTEVENTS_FAIL", "not DB_check()")
		return dict()

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			# Выбрать все мероприятия из БД
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT event, title, status FROM event ORDER BY title
				""")
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_selectEvents: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTEVENTS_FAIL", sError)
		return dict()

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTEVENTS_LONGTIME", str(iDeltaTicker))

	try:
		dictRes = dict()
		if len(rows) > 0:
			for row in rows:
				sEvent = str(row[0])
				sTitle = str(row[1])
				sStatus = str(row[2])

				# Иконка статуса
				sStatusIcon = ""
				# open - нет иконки
				# open_reg - ➕
				# 🚫
				if sStatus == "open":
					sStatusIcon = ""  # нет иконки
				elif sStatus == "open_reg":
					sStatusIcon = "➕ "  # Регистрация открыта
				else:
					sStatusIcon = "🚫 "  # Мероприятие закрыто

				# Определить является ли sChatId организатором
				isOrg = DB_EVENT_isOrg(sChatId, sEvent)

				# Если организатор, то показать ему мероприятие в любом случае
				if isOrg:
					dictRes[sEvent] = [{"text": "🏆 " + sStatusIcon + sTitle, "callback_data": "/event_show " + sEvent}]
				# Если не организатор, то показать ему мероприятие только если оно запущено
				else:
					if (sStatus == "open") or (sStatus == "open_reg"):
						dictRes[sEvent] = [{"text": "🏆 " + sStatusIcon + sTitle, "callback_data": "/event_show " + sEvent}]
				# if
			# for
		# if

		# Выбрать все мероприятия из конфига по организатору sChatId
		for d in lstBOT_EVENT_ORG:
			if d['chat_id'] == sChatId:
				sEvent2 = str(d['event'])
				if not (sEvent2 in dictRes):
					dictRes[sEvent2] = [{"text": "🏆 🚫 " + sEvent2, "callback_data": "/event_show " + sEvent2}]
				# if
			# if
		# for

		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTEVENTS_SUCCESS", "")
		return dictRes
	except Exception as ex:
		LOG("DB_EVENT_selectEvents: Error: 2: " + str(ex))
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTEVENTS_FAIL", "2: " + str(ex))
		return dict()
	# try
# def DB_EVENT_selectEvents


# Мероприятия
# Выбрать описание мероприятия
# Вернуть sDecription, sStatus, iEvent
def DB_EVENT_selectDescription(sChatId: str, sEvent: str):
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	LOG("DB_EVENT_selectDescription: sChatId=" + sChatId + ", sEvent=" + sEvent)
	MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTDESCRIPTION_TOTAL", "")

	sDescription = ""
	sStatus = ""
	iEvent = 0
	rows = list()

	if not DB_check():
		LOG("DB_EVENT_selectDescription: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_SELECTDESCRIPTION_FAIL", "not DB_check()")
		return "", "", 0

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			# Выбрать все мероприятия из БД
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id, event, title, status, description
				FROM event
				WHERE event = %s
				""", (sEvent))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_selectDescription: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTDESCRIPTION_FAIL", sError)
		return "", "", 0

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTDESCRIPTION_LONGTIME", str(iDeltaTicker))

	try:
		if len(rows) > 0:
			try:
				iEvent = int(rows[0][0])
			except:
				iEvent = 0
			sTitle = str(rows[0][2])
			sStatus = str(rows[0][3])
			sDescription2 = str(rows[0][4])

			# Иконка статуса
			sStatusIcon = ""
			# open - нет иконки
			# open_reg - ➕
			# 🚫
			if sStatus == "open":
				sStatusIcon = ""  # нет иконки
			elif sStatus == "open_reg":
				sStatusIcon = "➕ "  # Регистрация открыта
			else:
				sStatusIcon = "🚫 "  # Мероприятие закрыто

			sDescription = "🏆 " + sStatusIcon + sTitle
			if len(sDescription2) > 0:
				sDescription += "\n" + sDescription2
		# if

		# Если в базе еще нет описания мероприятия
		if len(sDescription) == 0:
			sDescription = "🏆 🚫 Еще нет описания"
		# if

		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTDESCRIPTION_SUCCESS", "")
		return sDescription, sStatus, iEvent
	except Exception as ex:
		LOG("DB_EVENT_selectDescription: Error: 2: " + str(ex))
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTDESCRIPTION_FAIL", "2: " + str(ex))
		return "", "", 0
	# try
# def DB_EVENT_selectDescription


# Мероприятия
# Выбрать заголовок мероприятия
# Вернуть sTitle, sStatus, iEvent
def DB_EVENT_selectTitle(sChatId: str, sEvent: str):
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	LOG("DB_EVENT_selectTitle: sChatId=" + sChatId + ", sEvent=" + sEvent)
	MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTDESCRIPTION_TOTAL", "")

	sTitle = ""
	sStatus = ""
	iEvent = 0
	rows = list()

	if not DB_check():
		LOG("DB_EVENT_selectTitle: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_SELECTTITLE_FAIL", "not DB_check()")
		return "", "", 0

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id, event, title, status, description FROM event WHERE event = %s
				""", (sEvent))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_selectTitle: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTTITLE_FAIL", sError)
		return "", "", 0

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTTITLE_LONGTIME", str(iDeltaTicker))

	try:
		if len(rows) > 0:
			try:
				iEvent = int(rows[0][0])
			except:
				iEvent = 0
			sTitle = str(rows[0][2])
			sStatus = str(rows[0][3])
		# if

		# Если в базе еще нет описания мероприятия
		if len(sTitle) == 0:
			sTitle = sEvent
		# if

		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTTITLE_SUCCESS", "")
		return sTitle, sStatus, iEvent
	except Exception as ex:
		LOG("DB_EVENT_selectTitle: Error: 2: " + str(ex))
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTTITLE_FAIL", "2: " + str(ex))
		return "", "", 0
	# end try
# def DB_EVENT_selectTitle


# Мероприятия
# Найти мероприятие
# Вернуть iEventId
def DB_EVENT_selectOne(sChatId: str, sEvent: str) -> int:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	LOG("DB_EVENT_selectOne: sChatId=" + sChatId + ", sEvent=" + sEvent)
	MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTONE_TOTAL", "")

	iEventId = 0
	rows = list()

	if not DB_check():
		LOG("DB_EVENT_selectOne: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_SELECTONE_FAIL", "not DB_check()")
		return 0

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id
				FROM event
				WHERE event = %s
				""", (sEvent))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_selectOne: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTONE_FAIL", sError)
		return 0

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTONE_LONGTIME", str(iDeltaTicker))

	try:
		if len(rows) > 0:
			try:
				iEventId = int(rows[0][0])
			except:
				iEventId = 0
			# try
		# if

		LOG("DB_EVENT_selectOne: " + str(iEventId))
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTONE_SUCCESS", "")
		return iEventId
	except Exception as ex:
		LOG("DB_EVENT_selectOne: Error: 2: " + str(ex))
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_SELECTONE_FAIL", "2: " + str(ex))
		return 0
	# end try
# def DB_EVENT_selectOne


# Мероприятия
# Найти заявку на мероприятие
# Вернуть id
def DB_EVENT_REQUEST_selectOne(sChatId: str, sEvent: str) -> int:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	LOG("DB_EVENT_REQUEST_selectOne: sChatId=" + sChatId + ", sEvent=" + sEvent)
	MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECTONE_TOTAL", "")

	id = 0
	rows = list()

	if not DB_check():
		LOG("DB_EVENT_REQUEST_selectOne: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_REQUEST_SELECTONE_FAIL", "not DB_check()")
		return 0

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id
				FROM event_request
				WHERE event = %s and chat_id = %s
				""", (sEvent, sChatId))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_REQUEST_selectOne: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECTONE_FAIL", sError)
		return 0

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECTONE_LONGTIME", str(iDeltaTicker))

	try:
		if len(rows) > 0:
			try:
				id = int(rows[0][0])
			except:
				id = 0
			# try
		# if

		LOG("DB_EVENT_REQUEST_selectOne: " + str(id))
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECTONE_SUCCESS", "")
		return id
	except Exception as ex:
		LOG("DB_EVENT_REQUEST_selectOne: Error: 2: " + str(ex))
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECTONE_FAIL", "2: " + str(ex))
		return 0
	# end try
# def DB_EVENT_REQUEST_selectOne


# Мероприятия
# Вернуть данные по заявке
# id, title, description, status, feedback
def DB_EVENT_REQUEST_select(sChatId: str, sEvent: str):
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	LOG("DB_EVENT_REQUEST_select: sChatId=" + sChatId + ", sEvent=" + sEvent)
	MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECT_TOTAL", "")

	id = 0
	sTitle = ""
	sDescription = ""
	sStatus = ""
	sFeedback = ""
	rows = list()

	if not DB_check():
		LOG("DB_EVENT_REQUEST_selectOne: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_REQUEST_SELECT_FAIL", "not DB_check()")
		return 0, "", "", "", ""

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id, title, description, status, feedback FROM event_request WHERE event = %s and chat_id = %s
				""", (sEvent, sChatId))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_REQUEST_select: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECT_FAIL", sError)
		return 0, "", "", "", ""

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECT_LONGTIME", str(iDeltaTicker))

	try:
		if len(rows) > 0:
			try:
				id = int(rows[0][0])
			except:
				id = 0
			# try
			sTitle = str(rows[0][1])
			sDescription = str(rows[0][2])
			sStatus = str(rows[0][3])
			sFeedback = str(rows[0][4])
		# if
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECT_SUCCESS", "")
		return id, sTitle, sDescription, sStatus, sFeedback
	except Exception as ex:
		LOG("DB_EVENT_REQUEST_select: Error: 2: " + str(ex))
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECT_FAIL", "2: " + str(ex))
		return 0, "", "", "", ""
	# end try
# def DB_EVENT_REQUEST_select


# Мероприятия
# Ввести данные по заявке
def DB_EVENT_REQUEST_updateRequest(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sEvent: str, sText: str, gmt: int) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	sText = strisnull(sText, "").strip()
	sTitle, sDescription = strsplit(sText, ".")
	sTitle = strtrunc(strisnull(sTitle, "").strip(), 255)
	sDescription = strtrunc(strisnull(sDescription, "").strip(), 4000)

	LOG("DB_EVENT_REQUEST_updateRequest: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", sEvent=" + sEvent +
		", sTitle=" + sTitle +
		", sDescription=" + sDescription)
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUEST_TOTAL", "")

	# Даты
	if gmt < -23:
		gmt = -23
	if gmt > 23:
		gmt = 23
	create_date = datetime.today()
	create_date_local = create_date + timedelta(hours=gmt)

	if not DB_check():
		LOG("DB_EVENT_REQUEST_updateRequest: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_REQUEST_UPDATEREQUEST_FAIL", "not DB_check()")
		return False

	# Найти уже существующую заявку на мероприятие
	id = DB_EVENT_REQUEST_selectOne(sChatId, sEvent)

	# Добавление мероприятия
	if id <= 0:
		iDeltaTicker = 0.0
		sError = ""
		lockDB.acquire()
		try:
			dbCursor = dbConnect.cursor()
			try:
				dtStartTicker = time.time()
				dbCursor.execute("""
					INSERT INTO event_request (chat_id, chat_name, from_id, from_name, from_user, event, title, description, status, feedback, create_date, gmt, create_date_local) 
					VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
					""", (sChatId, sChatName, sFromId, sFromName, sFromUser, sEvent, sTitle, sDescription, "", "", create_date, gmt, create_date_local) )
				dbConnect.commit()
				iDeltaTicker = time.time() - dtStartTicker
			except Exception as exsql:
				sError = str(exsql)
			finally:
				dbCursor.close()
			# end try
		except Exception as ex:
			sError = str(ex)
		finally:
			lockDB.release()
		# end try

		if len(sError) > 0:
			LOG("DB_EVENT_REQUEST_updateRequest: Error: INSERT: " + sError)
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUEST_FAIL", "INSERT: " + sError)
			return False

		if iDeltaTicker > iDB_LONGTIME:
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUEST_LONGTIME", "INSERT: " + str(iDeltaTicker))

		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUEST_SUCCESS", "")
		return True

	# Обновление описания мероприятия
	else:
		iDeltaTicker = 0.0
		sError = ""
		lockDB.acquire()
		try:
			dbCursor = dbConnect.cursor()
			try:
				dtStartTicker = time.time()
				dbCursor.execute("""
					UPDATE event_request
					SET title = %s, description = %s, status = %s
					WHERE id = %s
					""", (sTitle, sDescription, "", id))
				dbConnect.commit()
				iDeltaTicker = time.time() - dtStartTicker
			except Exception as exsql:
				sError = str(exsql)
			finally:
				dbCursor.close()
			# end try
		except Exception as ex:
			sError = str(ex)
		finally:
			lockDB.release()
		# end try

		if len(sError) > 0:
			LOG("DB_EVENT_REQUEST_updateRequest: Error: UPDATE: " + sError)
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUEST_FAIL", "UPDATE: " + sError)
			return False

		if iDeltaTicker > iDB_LONGTIME:
			MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUEST_LONGTIME", "UPDATE: " + str(iDeltaTicker))

		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUEST_SUCCESS", "")
		return True
	# if
# def DB_EVENT_REQUEST_updateRequest


# Мероприятия
# Обновить заявку по id записи
def DB_EVENT_REQUEST_updateRequestById(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, id: int, sStatus: str, sFeedback: str, sTitle: str, sDescription: str, gmt: int) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)

	sStatus = strtrunc(strisnull(sStatus, "").strip(), 255)
	sFeedback = strtrunc(strisnull(sFeedback, "").strip(), 4000)
	sTitle = strtrunc(strisnull(sTitle, "").strip(), 255)
	sDescription = strtrunc(strisnull(sDescription, "").strip(), 4000)

	LOG("DB_EVENT_REQUEST_updateRequestById: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", id=" + str(id) +
		", sStatus=" + sStatus +
		", sFeedback=" + sFeedback +
		", sTitle=" + sTitle +
		", sDescription=" + sDescription)
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTBYID_TOTAL", "")

	# Даты
	if gmt < -23:
		gmt = -23
	if gmt > 23:
		gmt = 23
	create_date = datetime.today()
	create_date_local = create_date + timedelta(hours=gmt)

	if not DB_check():
		LOG("DB_EVENT_REQUEST_updateRequestById: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_REQUEST_UPDATEREQUESTBYID_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				UPDATE event_request
				SET status = %s, feedback= %s, title = %s, description = %s
				WHERE id = %s
				""", (sStatus, sFeedback, sTitle, sDescription, id))
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_REQUEST_updateRequestById: Error: " + sError)
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTBYID_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTBYID_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTBYID_SUCCESS", "")
	return True
# def DB_EVENT_REQUEST_updateRequestById


# Мероприятия
# Обновить заявку по chat_id участника
def DB_EVENT_REQUEST_updateRequestByChatId(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sEvent: str, sStatus: str, sFeedback: str, sTitle: str, sDescription: str, gmt: int) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)

	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)
	sStatus = strtrunc(strisnull(sStatus, "").strip(), 255)
	sFeedback = strtrunc(strisnull(sFeedback, "").strip(), 4000)
	sTitle = strtrunc(strisnull(sTitle, "").strip(), 255)
	sDescription = strtrunc(strisnull(sDescription, "").strip(), 4000)

	LOG("DB_EVENT_REQUEST_updateRequestByUserId: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", sEvent=" + sEvent +
		", sStatus=" + sStatus +
		", sFeedback=" + sFeedback +
		", sTitle=" + sTitle +
		", sDescription=" + sDescription)
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_TOTAL", "")

	# Даты
	if gmt < -23:
		gmt = -23
	if gmt > 23:
		gmt = 23
	create_date = datetime.today()
	create_date_local = create_date + timedelta(hours=gmt)

	if not DB_check():
		LOG("DB_EVENT_REQUEST_updateRequestById: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				UPDATE event_request
				SET status = %s, feedback = %s, title = %s, description = %s
				WHERE event = %s and chat_id = %s
				""", (sStatus, sFeedback, sTitle, sDescription, sEvent, sChatId))
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_REQUEST_updateRequestByUserId: Error: " + sError)
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTBYUSERID_SUCCESS", "")
	return True
# def DB_EVENT_REQUEST_updateRequestByUserId


# Мероприятия
# Обновить все заявки по загруженным данным от Организатора
def DB_EVENT_REQUEST_updateRequestList(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str, sEvent: str, sText: str, gmt: int) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	sText = strisnull(sText, "").strip()
	lstReq = sText.split("\n")

	LOG("DB_EVENT_REQUEST_updateRequestList: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", sEvent=" + sEvent)
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTLIST_TOTAL", "")

	# Даты
	if gmt < -23:
		gmt = -23
	if gmt > 23:
		gmt = 23
	create_date = datetime.today()
	create_date_local = create_date + timedelta(hours=gmt)

	iCountOk = 0  # Количество успешно обработанных заявок
	iCountUpdated = 0  # Количество успешно обновленных заявок
	iCountError = 0  # Количество ошибок
	sError = ""  # Все ошибки

	if not DB_check():
		LOG("DB_EVENT_REQUEST_updateRequestList: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_REQUEST_UPDATEREQUESTLIST_FAIL", "not DB_check()")
		return False

	sEventTitle, sEventStatus, iEventId = DB_EVENT_selectTitle(sChatId, sEvent)

	try:
		for sReq in lstReq:
			# probike_bzp	481648490			12	Зайков Дмитрий	@dazaykov	Дмитрий Зайков
			sReq2 = strisnull(sReq, "").strip()
			if len(sReq2) == 0:
				continue

			lstRecord = sReq2.split("\t")

			sREvent = ""
			if len(lstRecord) >= 1:
				sREvent = strisnull(str(lstRecord[0]), "").strip()
			if sREvent != sEvent:
				continue

			sRUserId = ""
			if len(lstRecord) >= 2:
				sRUserId = strisnull(str(lstRecord[1]), "").strip()
			if len(sRUserId) == 0:
				continue
			iDBId, sDBTitle, sDBDescription, sDBStatus, sDBFeedback = DB_EVENT_REQUEST_select(sRUserId, sREvent)
			if iDBId <=0:
				continue
			LOG("DB_EVENT_REQUEST_updateRequestList: iDBId=" + str(iDBId))
			LOG("sDBStatus=" + sDBStatus + ", sDBFeedback=" + sDBFeedback + ", sDBTitle=" + sDBTitle + ", sDBDescription=" + sDBDescription)

			sRStatus = ""
			if len(lstRecord) >= 3:
				sRStatus = strisnull(str(lstRecord[2]), "").strip()

			sRFeedback = ""
			if len(lstRecord) >= 4:
				sRFeedback = strisnull(str(lstRecord[3]), "").strip()

			sRTitle = ""
			if len(lstRecord) >= 5:
				sRTitle = strisnull(str(lstRecord[4]), "").strip()
			if len(sRTitle) == 0:
				sRTitle = sDBTitle

			sRDescription = ""
			if len(lstRecord) >= 6:
				sRDescription = strisnull(str(lstRecord[5]), "").strip()
			if len(sRDescription) == 0:
				sRDescription = sDBDescription

			LOG("sRStatus=" + sRStatus + ", sRFeedback=" + sRFeedback + ", sRTitle=" + sRTitle + ", sRDescription=" + sRDescription)

			if (sRStatus != sDBStatus) or (sRFeedback != sDBFeedback) or (sRTitle != sDBTitle) or (sRDescription != sDBDescription):
				LOG("DB_EVENT_REQUEST_updateRequestList: NEED UPDATE")
				if DB_EVENT_REQUEST_updateRequestById(sChatId, sChatName, sFromId, sFromUser, sFromName, iDBId, sRStatus, sRFeedback, sRTitle, sRDescription, gmt):
					iCountUpdated += 1
					if (sRStatus != sDBStatus):
						LOG("DB_EVENT_REQUEST_updateRequestList: NEED MESSAGE status")
						sMessage = "🏆 " + sEventTitle + ": Статус заявки обновлен"
						SENDMESSAGE(sRUserId, sRUserId, sMessage, None, True)
					elif (sRFeedback != sDBFeedback):
						LOG("DB_EVENT_REQUEST_updateRequestList: NEED MESSAGE feedback")
						sMessage = "🏆 " + sEventTitle + ": Пришло сообщение по заявке"
						SENDMESSAGE(sRUserId, sRUserId, sMessage, None, True)
					# if
				else:
					iCountError += 1
					sError += "Ошибка обновления: Event=" + sEvent + ", UserId=" + sRUserId + ", Status=" + sRStatus + ", Feedback=" + sRFeedback + ", Key=" + sRTitle + ", Value=" + sRDescription + "\n"
				# if
			# if

			iCountOk += 1
		# for

		sMessage = "🏆 " + sEventTitle + ":"
		LOG("Обработано записей: " + str(iCountOk))
		sMessage += "\nОбработано записей: " + str(iCountOk)
		LOG("Обновлено записей: " + str(iCountUpdated))
		sMessage += "\nОбновлено записей: " + str(iCountUpdated)
		LOG("Ошибок: " + str(iCountError))
		sMessage += "\nОшибок: " + str(iCountError)
		if len(sError) > 0:
			LOG("sError=" + sError)
			sMessage += "\n" + sError
		# if
		SENDMESSAGE(sChatId, "", sMessage, None, False)

		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTLIST_SUCCESS", "")
		return True
	except Exception as ex:
		LOG("DB_EVENT_REQUEST_updateRequestList: Error: " + str(ex))
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_EVENT_REQUEST_UPDATEREQUESTLIST_FAIL", str(ex))
		return False
	# end try
# def DB_EVENT_REQUEST_updateRequestList


# Мероприятия
# Выбрать описание заявки на мероприятие
# Вернуть sDecription, sStatus, id
def DB_EVENT_REQUEST_selectDescription(sChatId: str, sEvent: str):
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	LOG("DB_EVENT_REQUEST_selectDescription: sChatId=" + sChatId + ", sEvent=" + sEvent)
	MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECTDESCRIPTION_TOTAL", "")

	sDescription = ""
	sStatus = ""
	id = 0
	rows = list()

	if not DB_check():
		LOG("DB_EVENT_REQUEST_selectDescription: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_REQUEST_SELECTDESCRIPTION_FAIL", "not DB_check()")
		return "", "", 0

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id, event, title, status, description, feedback
				FROM event_request
				WHERE event = %s and chat_id = %s
				""", (sEvent, sChatId))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_REQUEST_selectDescription: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECTDESCRIPTION_FAIL", sError)
		return "", "", 0

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECTDESCRIPTION_LONGTIME", str(iDeltaTicker))

	try:
		if len(rows) > 0:
			try:
				id = int(rows[0][0])
			except:
				id = 0
			sTitle = str(rows[0][2])
			sStatus = str(rows[0][3])
			sDescription2 = str(rows[0][4])
			sFeedback = str(rows[0][5])

			# Иконка статуса
			sStatusIcon = ""
			if sStatus == "accepted":
				sStatusIcon = "✅ "  # Заявка принята организатором
			elif sStatus == "rejected":
				sStatusIcon = "🚫 "  # Заявка отклонена организатором
			elif sStatus == "canceled":
				sStatusIcon = "🚫 "  # Заявка отменена участником
			elif sStatus == "result":
				sStatusIcon = "✅ "  # Результаты
			else:
				sStatusIcon = "➕ "  # Заявка создана

			sDescription = "🏆 " + sStatusIcon + sTitle
			if len(sDescription2) > 0:
				sDescription += ". " + sDescription2

			if (sStatus == "accepted"):
				sDescription += "\nЗаявка принята"
				if len(sFeedback) > 0:
					sDescription += ": " + sFeedback
			elif (sStatus == "rejected"):
				sDescription += "\nЗаявка отклонена"
				if len(sFeedback) > 0:
					sDescription += ": " + sFeedback
			elif (sStatus == "canceled"):
				sDescription += "\nЗаявка отменена участником"
				if len(sFeedback) > 0:
					sDescription += ": " + sFeedback
			elif (sStatus == "result"):
				sDescription += "\nРезультаты"
				if len(sFeedback) > 0:
					sDescription += ": " + sFeedback
		# if

		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECTDESCRIPTION_SUCCESS", "")
		return sDescription, sStatus, id
	except Exception as ex:
		LOG("DB_EVENT_REQUEST_selectDescription: Error: 2: " + str(ex))
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_SELECTDESCRIPTION_FAIL", "2: " + str(ex))
		return "", "", 0
	# end try
# def DB_EVENT_REQUEST_selectDescription


# Мероприятия
# Выгрузить заявки мероприятия
# Вернуть строку
def DB_EVENT_REQUEST_getreq(sChatId: str, sEvent: str):
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	LOG("DB_EVENT_REQUEST_getreq: sChatId=" + sChatId + ", sEvent=" + sEvent)
	MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_GETREQ_TOTAL", "")

	sDoc = ""
	rows = list()

	if not DB_check():
		LOG("DB_EVENT_REQUEST_getreq: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_EVENT_REQUEST_GETREQ_FAIL", "not DB_check()")
		return ""

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT from_id, from_user, from_name, title, description, status, feedback
				FROM event_request
				WHERE event = %s
				ORDER BY id
				""", (sEvent))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_EVENT_REQUEST_getreq: Error: " + sError)
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_GETREQ_FAIL", sError)
		return ""

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_GETREQ_LONGTIME", str(iDeltaTicker))

	try:
		if len(rows) > 0:
			for row in rows:
				sFromId = strisnull(str(row[0]), "").strip()
				sFromUser = strisnull(str(row[1]), "").strip()
				sFromName = strisnull(str(row[2]), "").strip()
				sTitle = strisnull(str(row[3]), "").strip()
				sDescription = strisnull(str(row[4]), "").strip()
				sStatus = strisnull(str(row[5]), "").strip()
				sFeedback = strisnull(str(row[6]), "").strip()

				sDoc += sEvent + "\t" + sFromId + "\t" + sStatus + "\t" + sFeedback + "\t" + sTitle + "\t" + sDescription + "\t@" + sFromUser + "\t" + sFromName + "\n"
			# for
		# if

		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_GETREQ_SUCCESS", "")
		return sDoc
	except Exception as ex:
		LOG("DB_EVENT_REQUEST_getreq: Error: 2: " + str(ex))
		MONITOR(sChatId, "", "", "", "", "DB_EVENT_REQUEST_GETREQ_FAIL", "2: " + str(ex))
		return ""
	# end try
# def DB_EVENT_REQUEST_getreq


# Мероприятия
# Является ли sChatId организатором мероприятия sEvent
def DB_EVENT_isOrg(sChatId: str, sEvent: str) -> bool:
	global lstBOT_EVENT_ORG
	isOrg = False

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sEvent = strtrunc(strisnull(sEvent, "").strip(), 255)

	try:
		for d in lstBOT_EVENT_ORG:
			if (d['event'] == sEvent) and (d['chat_id'] == sChatId):
				isOrg = True
				break
			# if
			if (sEvent == "*") and (d['chat_id'] == sChatId):
				isOrg = True
				break
			# if
		# for
		return isOrg
	except Exception as ex:
		return False
	# end try
# def DB_EVENT_isOrg



# Обратная связь
# Добавить
def DB_FEEDBACK_insert(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str,
					   sMessage: str, sFolder: str, sStatus: str, reply_to_id: int, gmt: int) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)
	sMessage = strtrunc(strisnull(sMessage, "").strip(), 4000)
	sFolder = strtrunc(strisnull(sFolder, "").strip(), 255)
	sStatus = strtrunc(strisnull(sStatus, "").strip(), 255)

	LOG("DB_FEEDBACK_insert: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", sMessage=" + sMessage +
		", sFolder=" + sFolder +
		", sStatus=" + sStatus +
		", reply_to_id=" + str(reply_to_id) +
		", gmt=" + str(gmt))
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_FEEDBACK_INSERT_TOTAL", "")

	# Даты
	if gmt < -23:
		gmt = -23
	if gmt > 23:
		gmt = 23
	create_date = datetime.today()
	create_date_local = create_date + timedelta(hours=gmt)

	if not DB_check():
		LOG("DB_FEEDBACK_insert: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_FEEDBACK_INSERT_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			if reply_to_id > 0:
				dbCursor.execute("""
					INSERT INTO feedback (chat_id, chat_name, from_id, from_name, from_user, message, folder, status, reply_to_id, create_date, gmt, create_date_local) 
					VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
					""", (sChatId, sChatName, sFromId, sFromName, sFromUser, sMessage, sFolder, sStatus, reply_to_id, create_date, gmt, create_date_local) )
			else:
				dbCursor.execute("""
					INSERT INTO feedback (chat_id, chat_name, from_id, from_name, from_user, message, folder, status, create_date, gmt, create_date_local) 
					VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
					""", (sChatId, sChatName, sFromId, sFromName, sFromUser, sMessage, sFolder, sStatus, create_date, gmt, create_date_local) )
			# fi
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_FEEDBACK_insert: Error: " + sError)
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_FEEDBACK_INSERT_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_FEEDBACK_INSERT_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_FEEDBACK_INSERT_SUCCESS", "")
	return True
# def DB_FEEDBACK_insert


# Обратная связь
# Обновить статус
def DB_FEEDBACK_updateStatus(sChatId: str, sChatName: str, sFromId: str, sFromUser: str, sFromName: str,
							id: int, sStatus: str) -> bool:
	global dbConnect

	sChatId = strtrunc(strisnull(sChatId, "").strip(), 255)
	sChatName = strtrunc(strisnull(sChatName, "").strip(), 255)
	sFromId = strtrunc(strisnull(sFromId, "").strip(), 255)
	sFromUser = strtrunc(strisnull(sFromUser, "").strip(), 255)
	sFromName = strtrunc(strisnull(sFromName, "").strip(), 255)
	sStatus = strtrunc(strisnull(sStatus, "").strip(), 255)

	LOG("DB_FEEDBACK_updateStatus: sChatId=" + sChatId +
		", sChatName=" + sChatName +
		", sFromId=" + sFromId +
		", sFromUser=" + sFromUser +
		", sFromName=" + sFromName +
		", id=" + str(id) +
		", sStatus=" + sStatus)
	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_FEEDBACK_UPDATESTATUS_TOTAL", "")

	if not DB_check():
		LOG("DB_FEEDBACK_updateStatus: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_FEEDBACK_UPDATESTATUS_FAIL", "not DB_check()")
		return False

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				UPDATE feedback SET status = %s 
				WHERE id = %s
				""", (sStatus, id) )
			dbConnect.commit()
			iDeltaTicker = time.time() - dtStartTicker
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_FEEDBACK_updateStatus: Error: " + sError)
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_FEEDBACK_UPDATESTATUS_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_FEEDBACK_UPDATESTATUS_LONGTIME", str(iDeltaTicker))

	MONITOR(sChatId, sChatName, sFromId, sFromUser, sFromName, "DB_FEEDBACK_UPDATESTATUS_SUCCESS", "")
	return True
# def DB_FEEDBACK_updateStatus


# Обратная связь
# Выбрать сообщения по статусу
def DB_FEEDBACK_selectByStatus(sFolder: str, sStatus: str):
	global dbConnect

	sFolder = strtrunc(strisnull(sFolder, "").strip(), 255)
	sStatus = strtrunc(strisnull(sStatus, "").strip(), 255)

	LOG("DB_FEEDBACK_selectByStatus: sFolder=" + sFolder + ", sStatus=" + sStatus)
	MONITOR("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_TOTAL", "")

	rows = list()

	if not DB_check():
		LOG("DB_FEEDBACK_selectByStatus: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_FAIL", "not DB_check()")
		return list()

	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id, chat_id, chat_name, from_id, from_name, from_user, message, reply_to_id
				FROM feedback
				WHERE folder = %s AND status = %s
				ORDER BY id
				""", (sFolder, sStatus))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_FEEDBACK_selectByStatus: Error: " + sError)
		MONITOR("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_FAIL", sError)
		return list()

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_LONGTIME", str(iDeltaTicker))

	MONITOR("", "", "", "", "", "DB_FEEDBACK_SELECTBYSTATUS_SUCCESS", "")
	return rows
# def DB_FEEDBACK_selectByStatus


# Обратная связь
# Вернуть chat_id и from_user по id
def DB_FEEDBACK_chatById(id: int):
	global dbConnect

	LOG("DB_FEEDBACK_chatById: id=" + str(id))
	MONITOR("", "", "", "", "", "DB_FEEDBACK_CHATBYID_TOTAL", "")

	if not DB_check():
		LOG("DB_FEEDBACK_chatById: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_FEEDBACK_CHATBYID_FAIL", "not DB_check()")
		return "", ""

	rows = list()
	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT chat_id, from_user
				FROM feedback
				WHERE id = %s
				""", (id))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_FEEDBACK_chatById: Error: " + sError)
		MONITOR("", "", "", "", "", "DB_FEEDBACK_CHATBYID_FAIL", sError)
		return "", ""

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR("", "", "", "", "", "DB_FEEDBACK_CHATBYID_LONGTIME", str(iDeltaTicker))

	try:
		if len(rows) > 0:
			sChatId = str(rows[0][0])
			sFromUser = str(rows[0][1])
			LOG("DB_FEEDBACK_chatById: id=" + str(id) + ", return sChatId=" + sChatId + ", return sFromUser=" + sFromUser)
			MONITOR("", "", "", "", "", "DB_FEEDBACK_CHATBYID_SUCCESS", "")
			return sChatId, sFromUser
		else:
			LOG("DB_FEEDBACK_chatById: Error: Запись не найдена")
			MONITOR("", "", "", "", "", "DB_FEEDBACK_CHATBYID_FAIL", "Запись не найдена: id=" + str(id))
			return "", ""
	except Exception as ex:
		LOG("DB_FEEDBACK_chatById: Error: 2: " + str(ex))
		MONITOR("", "", "", "", "", "DB_FEEDBACK_CHATBYID_FAIL", "2: " + str(ex))
		return "", ""
	# end try
# def DB_FEEDBACK_chatById


# Обратная связь
# Проверить существование сообщения по id
def DB_FEEDBACK_checkById(id: int) -> bool:
	global dbConnect

	LOG("DB_FEEDBACK_checkById: id=" + str(id))
	MONITOR("", "", "", "", "", "DB_FEEDBACK_CHECKBYID_TOTAL", "")

	if not DB_check():
		LOG("DB_FEEDBACK_checkById: Error: not DB_check()")
		MONITOR("", "", "", "", "", "DB_FEEDBACK_CHECKBYID_FAIL", "not DB_check()")
		return False

	rows = list()
	iDeltaTicker = 0.0
	sError = ""
	lockDB.acquire()
	try:
		dbCursor = dbConnect.cursor()
		try:
			dtStartTicker = time.time()
			dbCursor.execute("""
				SELECT id FROM feedback WHERE id = %s
				""", (id))
			iDeltaTicker = time.time() - dtStartTicker
			rows = dbCursor.fetchall()
		except Exception as exsql:
			sError = str(exsql)
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		sError = str(ex)
	finally:
		lockDB.release()
	# end try

	if len(sError) > 0:
		LOG("DB_FEEDBACK_checkById: Error: " + sError)
		MONITOR("", "", "", "", "", "DB_FEEDBACK_CHECKBYID_FAIL", sError)
		return False

	if iDeltaTicker > iDB_LONGTIME:
		MONITOR("", "", "", "", "", "DB_FEEDBACK_CHECKBYID_LONGTIME", str(iDeltaTicker))

	try:
		if len(rows) > 0:
			LOG("DB_FEEDBACK_chatById: id=" + str(id) + ", return True")
			MONITOR("", "", "", "", "", "DB_FEEDBACK_CHECKBYID_SUCCESS", "")
			return True
		else:
			LOG("DB_FEEDBACK_checkById: Error: Запись не найдена")
			MONITOR("", "", "", "", "", "DB_FEEDBACK_CHECKBYID_FAIL", "Запись не найдена: id=" + str(id))
			return False
	except Exception as ex:
		LOG("DB_FEEDBACK_checkById: Error: 2: " + str(ex))
		MONITOR("", "", "", "", "", "DB_FEEDBACK_CHECKBYID_FAIL", "2: " + str(ex))
		return False
	# end try
# def DB_FEEDBACK_checkById


# Мониторинг
# Добавить
def DB_MONITOR_insert(dictTemp) -> bool:
	global dbConnect

	#LOG("DB_MONITOR_insert")

	if not DB_check():
		LOG("DB_MONITOR_insert: Error: not DB_check()")
		return False

	try:
		dictTemp2 = dict()
		for sKey in dictTemp:
			dictTemp2 = dictTemp[sKey]
			sChatId = strtrunc(strisnull(dictTemp2['sChatId'], "").strip(), 255)
			sChatName = strtrunc(strisnull(dictTemp2['sChatName'], "").strip(), 255)
			sFromId = strtrunc(strisnull(dictTemp2['sFromId'], "").strip(), 255)
			sFromUser = strtrunc(strisnull(dictTemp2['sFromUser'], "").strip(), 255)
			sFromName = strtrunc(strisnull(dictTemp2['sFromName'], "").strip(), 255)
			sName = strtrunc(strisnull(dictTemp2['sName'], "").strip(), 255)
			sDescription = strtrunc(strisnull(dictTemp2['sDescription'], "").strip(), 4000)
			iCount = int(dictTemp2['iCount'])
			start_date = dictTemp2['start_date']
			end_date = dictTemp2['end_date']

			iDeltaTicker = 0.0
			sError = ""
			lockDB.acquire()
			try:
				dbCursor = dbConnect.cursor()
				try:
					dtStartTicker = time.time()
					dbCursor.execute("""
						INSERT INTO monitor (chat_id, chat_name, from_id, from_name, from_user, name, description, count, start_date, end_date, create_date) 
						VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
						""", (sChatId, sChatName, sFromId, sFromName, sFromUser, sName, sDescription, iCount, start_date, end_date, datetime.today()) )
					dbConnect.commit() # каждый insert коммитится отдельно во измежание слишком больших пакетов
										# 2021-11-25  9:13:03 2054 [Warning] Aborted connection 2054 to db: 'zda_gadget_bot' user: 'zda_gadget_bot_user' host: 'localhost' (Got an error reading communication packets)
										# https://qastack.ru/dba/19135/mysql-error-reading-communication-packets
					iDeltaTicker = time.time() - dtStartTicker
				except Exception as exsql2:
					sError = str(exsql2)
				finally:
					dbCursor.close()
				# try
			except Exception as exsql:
				sError = str(exsql)
			finally:
				lockDB.release()
			# end try

			if len(sError) > 0:
				LOG("DB_MONITOR_insert: Error: INSERT: " + sError)
				MONITOR("", "", "", "", "", "DB_MONITOR_INSERT_FAIL", "INSERT: " + sError)

			if iDeltaTicker > iDB_LONGTIME:
				MONITOR("", "", "", "", "", "DB_MONITOR_INSERT_LONGTIME", str(iDeltaTicker))
		# for
		return True
	except Exception as ex:
		LOG("DB_MONITOR_insert: Error: 2: " + str(ex))
		return False
	# end try
# def DB_MONITOR_insert




# ===================================================================================================
# Прочие статичные функции
# ===================================================================================================


# Подготовка строки. Замена пустой строки на sDef
def strisnull(s: str, sDef: str) -> str:
	if len(s) == 0:
		return sDef
	else:
		return s
# end def

# Подготовка строки. Обрезать сроку до max
def strtrunc(s: str, max: int) -> str:
	if len(s) > max:
		return s[:max]
	else:
		return s
# end def

# Разбить строку на две по символу
def strsplit(s: str, byChar: str):
	s = strisnull(s, "")
	s1 = ""
	s2 = ""
	try:
		k = s.find(byChar)
		if k >= 0:
			s1 = s[:k]
			s2 = s[k + 1:]
		else:
			s1 = s
			s2 = ""
		s1 = strisnull(s1, "")
		s2 = strisnull(s2, "")
	except:
		s1 = ""
		s2 = ""
	# end try
	return s1, s2
# end def



# Прочитать текстовый файл
def ReadFile(sFileName: str) -> list:
	lst1 = list()
	try:
		f = open(sFileName, "r")
		try:
			lst1 = f.readlines()
			f.close()
		except:
			f.close()
		# end try
	except Exception as ex:
		print("ReadFile(" + sFileName + "): " + str(ex))
	# end try
	return lst1
# end def

def ReadFileToStr(sFileName: str) -> str:
	res = ""
	try:
		f = open(sFileName, "r")
		try:
			lst1 = f.readlines()
			for s in lst1:
				res = res + s
			f.close()
		except:
			f.close()
		# end try
	except Exception as ex:
		print("ReadFile(" + sFileName + "): " + str(ex))
	# end try
	return res
# end def

# Получить значение параметра из конфига
def GetConfParam(lst: list, sParam: str) -> str:
	sValue = ""
	#print("GetConfParam(" + sParam + ")")

	for s in lst:
		# Определить ключ и значение параметра
		s1, s2 = strsplit(s, "=")
		s1 = strisnull(s1, "").strip() # Это искомый ключ
		s2 = strisnull(s2, "").strip()

		# Отсечь из значения параметра комментарии #
		s3, s4 = strsplit(s2, "#")
		s3 = strisnull(s3, "").strip() # Это искомое значение
		s4 = strisnull(s4, "").strip()

		#print("GetConfParam(" + sParam + "): sKey=" + s1 + ", sValue=" + s2)
		if s1 == sParam:
			sValue = s3
	# end for

	return sValue
# end def

# Получить список значений из конфига
def GetConfParamList(lst: list, sParam: str):
	lstValue = list()
	#print("GetConfParam(" + sParam + ")")

	for s in lst:
		# Определить ключ и значение параметра
		s1, s2 = strsplit(s, "=")
		s1 = strisnull(s1, "").strip() # Это искомый ключ
		s2 = strisnull(s2, "").strip()

		# Отсечь из значения параметра комментарии #
		s3, s4 = strsplit(s2, "#")
		s3 = strisnull(s3, "").strip() # Это искомое значение
		s4 = strisnull(s4, "").strip()

		#print("GetConfParam(" + sParam + "): sKey=" + s1 + ", sValue=" + s2)
		if s1 == sParam:
			lstValue.append(s3)
	# end for

	return lstValue
# end def






###############################################
# Старт программы
###############################################
def main(argv) -> int:
	LOG("")
	LOG("")
	LOG("==================================================================")
	LOG("Старт " + sAPP_NAME + "-" + sAPP_VERSION)
	MONITOR("", "", "", "", "", "START", "")

	# Командная строка
	global sCONF
	if len(argv) >= 2:
		sCONF = str(argv[1])
	# end if
	# print('len = ', str(len(sys.argv)))
	LOG("conf: " + sCONF)

	# Прочитать настройки
	global iPORT
	global iGMT
	global sGMT_DESC
	global sLOG
	global sTEMP
	global sBOT_TOKEN
	global sTM_URL
	global sTM_FILE_URL
	global sBOT_URL
	global sDB_HOST
	global sDB_NAME
	global sDB_USER
	global sDB_PASS
	global sBOT_VCONTACT
	global sBOT_BUY
	global sBOT_SPORT
	global sBOT_FEEDBACK
	global sBOT_MESS
	global sBOT_MESS_PATH
	global sBOT_OWNER_CHAT_ID
	global sBOT_EVENT
	global sBOT_EVENT_DOC
	global lstBOT_EVENT_ORG

	lstOpt = ReadFile(sCONF)

	# Порт
	s = GetConfParam(lstOpt, "port")
	try:
		iPORT = int(s)
	except Exception as ex:
		LOG("main: Error: Не верный формат порта " + s + ": " + str(ex))
		return 1
	# end try
	if iPORT <= 0:
		LOG("main: Error: Не задан порт")
		return 1
	# end if
	LOG("port: " + str(iPORT))

	# Часовой пояс
	s = GetConfParam(lstOpt, "gmt")
	try:
		iGMT = int(s)
	except Exception as ex:
		LOG("main: Error: Не верный формат часового пояса " + s + ": " + str(ex))
		return 1
	# end try
	if iGMT <= 0:
		LOG("main: Error: Не задан часовой пояс")
		return 1
	# end if
	LOG("gmt: " + str(iGMT))
	if iGMT < -23:
		iGMT = -23
	if iGMT > 23:
		iGMT = 23
	if iGMT < 0:
		sGMT_DESC = "GMT" + str(iGMT)
	else:
		sGMT_DESC = "GMT+" + str(iGMT)
	LOG("sGMT_DESC: " + str(sGMT_DESC))

	# Лог
	sLOG = GetConfParam(lstOpt, "log")
	LOG("log: " + sLOG)

	# Папка с временными файлами
	sTEMP = GetConfParam(lstOpt, "temp")
	LOG("temp: " + sTEMP)
	if not os.path.isdir(sTEMP):
		LOG("main: Error: Not exists temp=" + sTEMP)
		return 1
	# if

	# Токен
	sBOT_TOKEN = GetConfParam(lstOpt, "bot_token")
	LOG("bot_token: " + sBOT_TOKEN)

	# Веб хуки
	sTM_URL = GetConfParam(lstOpt, "tm_url")
	sTM_URL = sTM_URL + sBOT_TOKEN + "/"
	LOG("tm_url: " + sTM_URL)

	sTM_FILE_URL = GetConfParam(lstOpt, "tm_file_url")
	sTM_FILE_URL = sTM_FILE_URL + sBOT_TOKEN + "/"
	LOG("tm_file_url: " + sTM_FILE_URL)

	sBOT_URL = GetConfParam(lstOpt, "bot_url")
	LOG("bot_url: " + sBOT_URL)

	# БД
	sDB_HOST = GetConfParam(lstOpt, "db_host")
	LOG("db_host: " + sDB_HOST)
	if len(sDB_HOST) == 0:
		LOG("main: Error: Не задан db_host")
		return 1

	sDB_NAME = GetConfParam(lstOpt, "db_name")
	LOG("db_name: " + sDB_NAME)
	if len(sDB_NAME) == 0:
		LOG("main: Error: Не задан db_name")
		return 1

	sDB_USER = GetConfParam(lstOpt, "db_user")
	LOG("db_user: " + sDB_USER)
	if len(sDB_USER) == 0:
		LOG("main: Error: Не задан db_user")
		return 1

	sDB_PASS = GetConfParam(lstOpt, "db_pass")
	LOG("db_pass: " + sDB_PASS)
	if len(sDB_PASS) == 0:
		LOG("main: Error: Не задан db_pass")
		return 1

	# Функция список ВКС
	sBOT_VCONTACT = GetConfParam(lstOpt, "bot_vcontact")
	LOG("bot_vcontact: " + sBOT_VCONTACT)

	# Функция список покупок
	sBOT_BUY = GetConfParam(lstOpt, "bot_buy")
	LOG("bot_buy: " + sBOT_BUY)

	# Функция Спорт
	sBOT_SPORT = GetConfParam(lstOpt, "bot_sport")
	LOG("bot_sport: " + sBOT_SPORT)

	# Функция Обратная связь
	sBOT_FEEDBACK = GetConfParam(lstOpt, "bot_feedback")
	LOG("bot_feedback: " + sBOT_FEEDBACK)

	# Функция Серверных сообщений
	sBOT_MESS = GetConfParam(lstOpt, "bot_mess")
	LOG("bot_mess: " + sBOT_MESS)

	# Папка для входящих сообщений
	if sBOT_MESS == "yes":
		sBOT_MESS_PATH = GetConfParam(lstOpt, "bot_mess_path")
		LOG("bot_mess_path: " + sBOT_MESS_PATH)
		if len(sBOT_MESS_PATH) == 0:
			LOG("main: Error: Не задан bot_mess_path")
			return 1
		# if
		if not os.path.isdir(sBOT_MESS_PATH):
			LOG("main: Error: Not exists bot_mess_path=" + sBOT_MESS_PATH)
			return 1
		# if
	# if

	# Владелец бота
	sBOT_OWNER_CHAT_ID = GetConfParam(lstOpt, "owner_chat_id")
	LOG("owner_chat_id: " + sBOT_OWNER_CHAT_ID)
	if sBOT_MESS == "yes" or sBOT_FEEDBACK == "yes":
		if len(sBOT_OWNER_CHAT_ID) == 0:
			LOG("main: Error: Не задан owner_chat_id")
			return 1
		# if
	# if


	# Функция Мерпориятия
	sBOT_EVENT = GetConfParam(lstOpt, "bot_event")
	lstBOT_EVENT_ORG = list()
	LOG("bot_event: " + sBOT_EVENT)
	if sBOT_EVENT == "yes":
		# Инструкция
		sBOT_EVENT_DOC = GetConfParam(lstOpt, "bot_event_doc")
		LOG("bot_event_doc=" + sBOT_EVENT_DOC)
		# Привилегии
		lstTemp = GetConfParamList(lstOpt, "bot_event_org")
		for s in lstTemp:
			# event:chat_id
			s1, s2 = strsplit(s, ":")
			s1 = strisnull(s1, "").strip()  # Это event
			s2 = strisnull(s2, "").strip()	# Это chat_id

			if (len(s1) > 0) and (len(s2) > 0):
				lstBOT_EVENT_ORG.append({'event': str(s1), 'chat_id': str(s2)})
				LOG("bot_event_org=" + s1 + ":" + s2)
			# if
		# for
	# if


	LOG("Активированы функции:")
	if sBOT_VCONTACT == "yes":
		LOG("- ВКС")
	if sBOT_BUY == "yes":
		LOG("- Покупки")
	if sBOT_SPORT == "yes":
		LOG("- Спорт")
	if sBOT_EVENT == "yes":
		LOG("- Мероприятия")
	if sBOT_FEEDBACK == "yes":
		LOG("- Обратная связь")
	if sBOT_MESS == "yes":
		LOG("- Серверные сообщения")

	# Старт лимитов
	tLIMIT = LIMIT_Thread()
	tLIMIT.setDaemon(True)
	tLIMIT.start()

	# Старт логгера
	tLOG = LOG_Thread()
	tLOG.setDaemon(True)
	tLOG.start()

	# Подключение к БД
	if not DB_connect():
		LOG("main: Error: БД не подключена")
		return 1

	# Обновление структуры БД
	if not DB_create():
		LOG("main: Error: БД не обновлена")
		return 1

	# Старт мониторинга
	tMONITOR = MONITOR_Thread()
	tMONITOR.setDaemon(True)
	tMONITOR.start()
	LOG("Старт мониторинга")

	# Старт фоновой отправки сообщений
	tSENDMESSAGE = API_SENDMESSAGE_Thread()
	tSENDMESSAGE.setDaemon(True)
	tSENDMESSAGE.start()

	# Удаление и Установка веб-хуков
	API_deleteWebhook()
	if not API_setWebhook():
		return 1
	# end if

	# Старт обработчика обратной связи
	tFEEDBACK = FEEDBACK_Thread()
	if sBOT_FEEDBACK == "yes":
		tFEEDBACK.setDaemon(True)
		tFEEDBACK.start()
	# if

	# Старт обработчика серверных сообщений
	tMESS = MESS_Thread()
	if sBOT_MESS == "yes":
		tMESS.setDaemon(True)
		tMESS.start()
	# if

	# Старт web-сервера
	LOG("Старт web-сервера  127.0.0.1:" + str(iPORT))
	tornado.options.parse_command_line()
	http_server = tornado.httpserver.HTTPServer(WEB_Application())
	http_server.listen(iPORT, "127.0.0.1")
	tornado.ioloop.PeriodicCallback(WEB_try_exit, 100).start()
	tornado.ioloop.IOLoop.instance().start()
	LOG("tornado stopped")

	# Сбросить метркии в базу
	LOG("MONITOR_Flush")
	MONITOR_Flush()

	# Сбросить лог на диск
	LOG("LOG_Flush")
	LOG_Flush()

	#time.sleep(20)
	print("END")
	return 0
# end def




if __name__ == "__main__":
	res = main(sys.argv)
	exit(res)
