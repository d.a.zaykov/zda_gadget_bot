"""
Мониторинг

Функции:
- Поиск пиковых нагрузок в минуту


Запуск
$ python3 monitor.py /path/zda_gadget_bot.conf day
	day - за сколько дней анализировать данные. 0 - за текущие сутки. 1 - за прошлые сутки. 7 - за прошлые 7 суток.
	/path/zda_gadget_bot.conf - основной конфиг бота

Результаты выводятся на стандартный вывод

"""


import sys
import os
from datetime import timedelta
from datetime import datetime
#import calendar
#import time
#import threading
import pymysql


# Глобальные параметры
sAPP_NAME = "monitor"
sAPP_VERSION = "dev"
sAPP_PATH = os.path.dirname(os.path.abspath(__file__))  # Путь до приложения
sCONF = ""	# Имя фала конфига

sDB_HOST = "" # Хост базы
sDB_NAME = "" # Имя базы
sDB_USER = "" # Имя юзера
sDB_PASS = "" # Пароль
dbConnect = None # Коннект к БД

iDAY = 0 # за сколько дней анализировать данные. 0 - за текущие сутки. 1 - за прошлые сутки. 7 - за прошлые 7 суток.




# ===================================================================================================
# БД mysql
# ===================================================================================================

# Подключение к БД
def DB_connect() -> bool:
	global dbConnect
	if len(sDB_HOST) == 0:
		return False

	try:
		#print("DB_connect: db_host=" + sDB_HOST + ", db_name=" + sDB_NAME + ", db_user=" + sDB_USER)
		dbConnect = pymysql.connect(sDB_HOST, sDB_USER, sDB_PASS, sDB_NAME)
		dbCursor = dbConnect.cursor()
		try:
			dbCursor.execute("select version()")
			lstVersion = dbCursor.fetchone()
			if lstVersion is None:
				return False
			if len(lstVersion) < 1:
				return False
			s = lstVersion[0]
			#print("DB_connect: version=" + s)
			return True
		except Exception as exsql:
			print("DB_connect: Error: cselect version(): " + str(exsql))
			return False
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		print("DB_connect: Error: " + str(ex))
		return False
	# end try
# def DB_connect


# Проверка соединения и переподключение
def DB_check() -> bool:
	global dbConnect

	if len(sDB_HOST) == 0:
		return False

	try:
		if dbConnect is None:
			print( "DB_check: Нет соединения с базой 1. Переподключаюсь...")
			if not DB_connect():
				return False

		#dbConnect = pymysql.connect(sDB_HOST, sDB_USER, sDB_PASS, sDB_NAME)
		#if not dbConnect.open:
		#	print("DB_check: Нет соединения с базой 2. Переподключаюсь...")
		#	if not DB_connect():
		#		return False

		isConnected = False
		try:
			dbConnect.ping(True)
			isConnected = True
		except Exception as ex:
			isConnected = False
		# end try
		if not isConnected:
			print("DB_check: Нет соединения с базой 2. Переподключаюсь...")
			if not DB_connect():
				return False

		return True
	except Exception as ex:
		print("DB_check: Error: " + str(ex))
		return False
	# end try
# def DB_connect




# Поиск паксимального пика нагрузки
def DB_MONITOR_searchMax(dtStart: datetime, dtEnd: datetime) -> bool:
	global dbConnect
	print("")
	print("-- DB_MONITOR_searchMax ------------------------------------------")

	sRes = ""
	rows = list()

	try:
		#dbConnect = pymysql.connect(sDB_HOST, sDB_USER, sDB_PASS, sDB_NAME)
		if not DB_check():
			print("DB_MONITOR_searchMax: Error: not DB_check()")
			return False

		dbCursor = dbConnect.cursor()
		try:
			dMaxCount = 0 # Максимальное число запросов в минуту
			sMaxMinute = dtStart.strftime("%d.%m.%Y %H:%M") # Минута с максимальным количеством запросов
			sCurrMinute = dtStart.strftime("%d.%m.%Y %H:%M") # Текущая анализируемая минута
			dCurrCount = 0 # Количество запросов в текущей анализируемой минуте

			# Всего запросов
			dSum = 0
			dbCursor.execute("""
				select sum(`count`) 
				from monitor 
				where start_date >= %s and end_date <= %s and name = 'WEB_POST_ROOT_TOTAL' 
				""", (dtStart, dtEnd))
			rows = dbCursor.fetchall()
			if len(rows) > 0:
				try:
					dSum = int(rows[0][0])
				except:
					dSum = 0
				# try
			# fi
			print("Всего запросов: " + str(dSum))

			# Пик
			dbCursor.execute("""
				select `count`, end_date 
				from monitor 
				where start_date >= %s and end_date <= %s and name = 'WEB_POST_ROOT_TOTAL' 
				""", (dtStart, dtEnd))
			rows = dbCursor.fetchall()
			if len(rows) > 0:
				for row in rows:
					d = int(row[0])
					dt = row[1]
					sdt = dt.strftime("%d.%m.%Y %H:%M")
					if sdt == sCurrMinute:
						dCurrCount += d
					else:
						dCurrCount = d
						sCurrMinute = sdt
					# if
					if dCurrCount > dMaxCount:
						dMaxCount = dCurrCount
						sMaxMinute = sdt
					# if
				# for
			# fi

			print("Пик запросов: " + str(dMaxCount) + " / " + sMaxMinute)
			print("------------------------------------------------------------------")

			return True
		except Exception as exsql:
			print("DB_MONITOR_searchMax: Error: " + str(exsql))
			return False
		finally:
			dbCursor.close()
		# end try
	except Exception as ex:
		print("DB_MONITOR_searchMax: Error: " + str(ex))
		return False
	# end try
# def DB_MONITOR_searchMax




# ===================================================================================================
# Прочие статичные функции
# ===================================================================================================


# Подготовка строки. Замена пустой строки на sDef
def strisnull(s: str, sDef: str) -> str:
	if len(s) == 0:
		return sDef
	else:
		return s
# end def

# Подготовка строки. Обрезать сроку до max
def strtrunc(s: str, max: int) -> str:
	if len(s) > max:
		return s[:max]
	else:
		return s
# end def

# Разбить строку на две по символу
def strsplit(s: str, byChar: str):
	s = strisnull(s, "")
	s1 = ""
	s2 = ""
	try:
		k = s.find(byChar)
		if k >= 0:
			s1 = s[:k]
			s2 = s[k + 1:]
		else:
			s1 = s
			s2 = ""
		s1 = strisnull(s1, "")
		s2 = strisnull(s2, "")
	except:
		s1 = ""
		s2 = ""
	# end try
	return s1, s2
# end def


# Прочитать текстовый файл
def ReadFile(sFileName: str) -> list:
	lst1 = list()
	try:
		f = open(sFileName, "r")
		try:
			lst1 = f.readlines()
			f.close()
		except:
			f.close()
		# end try
	except Exception as ex:
		print("ReadFile(" + sFileName + "): " + str(ex))
	# end try
	return lst1
# end def


# Получить значение параметра настроек
def GetConfParam(lst: list, sParam: str) -> str:
	sValue = ""
	#print("GetConfParam(" + sParam + ")")

	for s in lst:
		# Определить ключ и значение параметра
		s1, s2 = strsplit(s, "=")
		s1 = strisnull(s1, "").strip() # Это искомый ключ
		s2 = strisnull(s2, "").strip()

		# Отсечь из значения параметра комментарии #
		s3, s4 = strsplit(s2, "#")
		s3 = strisnull(s3, "").strip() # Это искомое значение
		s4 = strisnull(s4, "").strip()

		#print("GetConfParam(" + sParam + "): sKey=" + s1 + ", sValue=" + s2)
		if s1 == sParam:
			sValue = s3
	# end for

	return sValue
# end def






###############################################
# Старт программы
###############################################
def main(argv) -> int:
	#print("Старт")

	# Командная строка
	global sCONF
	global iDAY
	iDAY = 0
	sDAY = ""
	if len(argv) >= 2:
		sCONF = str(argv[1])
	# end if
	if len(argv) >= 3:
		sDAY = str(argv[2])
	# end if
	#print('len = ', str(len(sys.argv)))
	# print('args = ', str(sys.argv))
	# print('port = ', sPORT)
	# print('log = ', sLOG)
	#print("sCONF: " + sCONF)
	#print("sDAY: " + sDAY)

	# Прочитать настройки
	global sDB_HOST
	global sDB_NAME
	global sDB_USER
	global sDB_PASS

	lstOpt = ReadFile(sCONF)

	# Дней
	try:
		iDAY = int(sDAY)
	except Exception as ex:
		iDAY = 0
	# end try
	if iDAY <= 0:
		iDAY = 0
	# end if

	# Период
	dtToday = datetime.today()
	dtStart = dtToday.replace(hour=0, minute=0, second=0, microsecond=0)
	dtEnd = dtToday.replace(hour=23, minute=59, second=59, microsecond=0)
	if iDAY > 0:
		dtStart = dtStart - timedelta(days=iDAY)
		dtEnd = dtEnd - timedelta(days=1)
	# if
	print("За " + str(iDAY) + " суток: " + str(dtStart) + " - " + str(dtEnd))

	# БД
	sDB_HOST = GetConfParam(lstOpt, "db_host")
	#print("db_host: " + sDB_HOST)
	if len(sDB_HOST) == 0:
		print("main: Error: Не задан db_host")
		return 1

	sDB_NAME = GetConfParam(lstOpt, "db_name")
	#print("db_name: " + sDB_NAME)
	if len(sDB_NAME) == 0:
		print("main: Error: Не задан db_name")
		return 1

	sDB_USER = GetConfParam(lstOpt, "db_user")
	#print("db_user: " + sDB_USER)
	if len(sDB_USER) == 0:
		print("main: Error: Не задан db_user")
		return 1

	sDB_PASS = GetConfParam(lstOpt, "db_pass")
	#print("db_pass: " + sDB_PASS)
	if len(sDB_PASS) == 0:
		print("main: Error: Не задан db_pass")
		return 1

	# Подключение к БД
	if not DB_connect():
		print("main: Error: БД не подключена")
		return 1


	# Поиск максимального пика нагрузки
	DB_MONITOR_searchMax(dtStart, dtEnd)


	return 0
# end def




if __name__ == "__main__":
	res = main(sys.argv)
	exit(res)
